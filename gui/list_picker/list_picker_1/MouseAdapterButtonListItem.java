/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of list_picker_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * list_picker_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * list_picker_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with list_picker_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/gui/list_picker/list_picker_1/MouseAdapterButtonListItem.java
 * @author Stephan Kreutzer
 * @since 2022-11-13
 */



import java.awt.event.*;



class MouseAdapterButtonListItem
  extends MouseAdapter
{
    public MouseAdapterButtonListItem(ListPickerDialog parent, String idNode)
    {
        super();

        this.parent = parent;
        this.idNode = idNode;
    }

    public void mouseClicked(MouseEvent event)
    {
        if (event.getClickCount() >= 2)
        {
            this.parent.optionSelected(this.idNode, true);
        }
        else
        {
            this.parent.optionSelected(this.idNode, false);
        }
    }

    protected ListPickerDialog parent = null;
    protected String idNode = null;
}
