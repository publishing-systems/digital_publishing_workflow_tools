/* Copyright (C) 2015-2022 Stephan Kreutzer
 *
 * This file is part of list_picker_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * list_picker_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * list_picker_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with list_picker_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/gui/list_picker/list_picker_1/ListPickerDialog.java
 * @brief GUI dialog to display options and for selecting one of them.
 * @author Stephan Kreutzer
 * @since 2015-11-14
 */



import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.UnsupportedEncodingException;



public class ListPickerDialog
  extends JDialog
  implements ActionListener
{
    public ListPickerDialog(String title, List<OptionEntry> options)
    {
        if (title != null)
        {
            setTitle(title);
        }
        else
        {
            throw constructTermination("messageDialogNoTitle", null, null);
        }

        if (options != null)
        {
            this.options = options;
        }
        else
        {
            throw constructTermination("messageDialogNoOptions", null, null);
        }

        int optionsCount = this.options.size();

        if (optionsCount <= 0)
        {
            throw constructTermination("messageDialogNoOptions", null, null);
        }


        JPanel panelMain = new JPanel();
        panelMain.setLayout(new GridLayout(1, 2));

        JPanel panelList = new JPanel();

        GridBagLayout gridbag = new GridBagLayout();
        panelList.setLayout(gridbag);

        GridBagConstraints gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.PAGE_START;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.weighty = 0.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        for (int i = 0; i < optionsCount; i++)
        {
            JButton button = new JButton(this.options.get(i).GetCaption());
            button.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            button.addMouseListener(new MouseAdapterButtonListItem(this, this.options.get(i).GetId()));

            if (this.options.get(i).GetDescription() != null)
            {
                button.setToolTipText(this.options.get(i).GetDescription());
            }

            panelList.add(button, gridbagConstraints);
        }

        //panelList.setPreferredSize(new Dimension(250, 150));

        JScrollPane scrollPane = new JScrollPane(panelList);

        gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.PAGE_START;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.weighty = 1.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        panelMain.add(scrollPane, gridbagConstraints);
        getContentPane().add(panelMain);


        JPanel panelButtons = new JPanel();

        this.buttonAbort = new JButton(getI10nString("guiDialogButtonAbort"));
        buttonAbort.addActionListener(this);
        panelButtons.add(buttonAbort);

        this.buttonSelect = new JButton(getI10nString("guiDialogButtonSelect"));
        buttonSelect.addActionListener(this);
        panelButtons.add(buttonSelect);

        panelButtons.setBorder(BorderFactory.createEtchedBorder()); 
        getContentPane().add(panelButtons, BorderLayout.SOUTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event)
            {
                event.getWindow().setVisible(false);
                event.getWindow().dispose();
            }
        });

        setModal(true);
        setLocation(100, 100);
        pack();
        setVisible(true);
    }

    public int optionSelected(String optionId, boolean doubleClick)
    {
        this.selectedOptionId = optionId;

        if (doubleClick == true)
        {
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            return 1;
        }

        return 0;
    }

    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == this.buttonAbort)
        {
            this.selectedOptionId = null;
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            return;
        }
        else if (event.getSource() == this.buttonSelect)
        {
            if (this.selectedOptionId == null)
            {
                return;
            }

            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            return;
        }
        else
        {

        }
    }

    /**
     * @retval null Selection aborted by user.
     */
    public String GetSelectedOptionId()
    {
        return this.selectedOptionId;
    }

    private ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "list_picker_1: " + getI10nString(id);
            }
            else
            {
                message = "list_picker_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10n == null)
        {
            this.l10n = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10n.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10n.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected List<OptionEntry> options;
    protected String selectedOptionId = null;

    private JButton buttonAbort;
    private JButton buttonSelect;

    private static final String L10N_BUNDLE = "l10n.l10nListPickerDialog";
    private ResourceBundle l10n;
}
