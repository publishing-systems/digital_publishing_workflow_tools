/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of xml_structure_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_structure_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_structure_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_structure_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/gui/xml_structure_editor/xml_structure_editor_1/TreeIconRenderer.java
 * @author Stephan Kreutzer
 * @since 2022-01-10
 */



import javax.swing.*;
import javax.swing.tree.*;
import java.util.Map;
import java.awt.*;



/**
 * @class TreeIconRenderer
 * @brief Sets the icon for each tree node/cell individually.
 */
class TreeIconRenderer extends JLabel implements TreeCellRenderer
{
    public TreeIconRenderer(xml_structure_editor_1 parent, Map<DefaultMutableTreeNode, Integer> treeNodeTypes)
    {
        this.parent = parent;
        this.treeNodeTypes = treeNodeTypes;
    }

    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value,
                                                  boolean selected,
                                                  boolean expanded,
                                                  boolean leaf,
                                                  int row,
                                                  boolean hasFocus)
    {
        setText(value.toString());
        setOpaque(selected);

        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;

        if (node == null)
        {
            throw this.parent.constructTermination("messageTreeNodeWrongClassType", null, null);
        }

        switch (this.treeNodeTypes.get(node))
        {
        case 1:
            setIcon(this.defaults.getOpenIcon());
            break;
        case 2:
            setIcon(this.defaults.getLeafIcon());
            break;
        default:
            break;
        }

        return this;
    }

    protected xml_structure_editor_1 parent = null;
    protected Map<DefaultMutableTreeNode, Integer> treeNodeTypes = null;

    protected static DefaultTreeCellRenderer defaults = new DefaultTreeCellRenderer();
}
