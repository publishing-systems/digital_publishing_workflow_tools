/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of xml_structure_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_structure_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_structure_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_structure_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/gui/xml_structure_editor/xml_structure_editor_1/KeyEventListener.java
 * @author Stephan Kreutzer
 * @since 2021-11-18
 */



import java.awt.event.*;



class KeyEventListener implements KeyListener
{
    public KeyEventListener(xml_structure_editor_1 parent)
    {
        if (parent == null)
        {
            throw new NullPointerException();
        }

        this.parent = parent;
    }

    public void keyPressed(KeyEvent event)
    {
        // Otherwise default Ctrl + C could win over the keyReleased() event.
        //event.consume();
    }

    public void keyReleased(KeyEvent event)
    {
        if (event.getKeyCode() == KeyEvent.VK_DELETE)
        {
            this.parent.removeNodeEvent();
        }
        else if (event.getKeyCode() == KeyEvent.VK_Z && ((event.getModifiers() & KeyEvent.CTRL_MASK) != 0))
        {
            this.parent.undo();
        }
    }

    public void keyTyped(KeyEvent event)
    {

    }

    protected xml_structure_editor_1 parent = null;
}
