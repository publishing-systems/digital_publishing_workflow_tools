/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of xml_structure_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_structure_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_structure_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_structure_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/gui/xml_structure_editor/xml_structure_editor_1/TreeSelectionEventListener.java
 * @author Stephan Kreutzer
 * @since 2022-01-08
 */



import javax.swing.event.*;
import javax.swing.tree.TreePath;
import javax.swing.tree.DefaultMutableTreeNode;



class TreeSelectionEventListener implements TreeSelectionListener
{
    public TreeSelectionEventListener(xml_structure_editor_1 parent)
    {
        if (parent == null)
        {
            throw new NullPointerException();
        }

        this.parent = parent;
    }

    public void valueChanged(TreeSelectionEvent event)
    {
        TreePath path = event.getPath();
        Object[] pathElements = path.getPath();

        if (pathElements.length <= 0)
        {
            return;
        }

        DefaultMutableTreeNode node = (DefaultMutableTreeNode)(pathElements[pathElements.length - 1]);

        if (node == null)
        {
            return;
        }

        this.parent.selectNodeEvent(node);
    }

    protected xml_structure_editor_1 parent = null;
}
