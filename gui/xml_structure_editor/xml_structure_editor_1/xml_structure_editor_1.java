/* Copyright (C) 2014-2022 Stephan Kreutzer
 *
 * This file is part of xml_structure_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_structure_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_structure_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_structure_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/gui/xml_structure_editor/xml_structure_editor_1/xml_structure_editor_1.java
 * @brief Currently only deletes nodes.
 * @todo Undo feature.
 * @author Stephan Kreutzer
 * @since 2022-01-08
 */



import javax.swing.*;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.DTD;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.EntityReference;
import javax.xml.stream.events.Comment;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;



public class xml_structure_editor_1
  extends JFrame
{
    public static void main(String[] args)
    {
        System.out.print("xml_structure_editor_1 Copyright (C) 2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        xml_structure_editor_1 instance = new xml_structure_editor_1();

        try
        {
            instance.run(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xml_structure_editor_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xml-structure-editor-1-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</xml-structure-editor-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public xml_structure_editor_1()
    {
        super("xml_structure_editor_1");

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event)
            {
                event.getWindow().setVisible(false);
                event.getWindow().dispose();

                save();

                System.exit(2);
            }
        });
    }

    public int run(String[] args)
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\txml_structure_editor_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        xml_structure_editor_1.resultInfoFile = resultInfoFile;

        this.programPath = xml_structure_editor_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            this.programPath = new File(this.programPath).getCanonicalPath() + File.separator;
            this.programPath = URLDecoder.decode(this.programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }

        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("xml_structure_editor_1: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));


        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input-file") == true)
                    {
                        StartElement inputFileElement = event.asStartElement();
                        Attribute attributePath = inputFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (inputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        inputFile = new File(attributePath.getValue());

                        if (inputFile.isAbsolute() != true)
                        {
                            inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            inputFile = inputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.exists() != true)
                        {
                            throw constructTermination("messageInputFileDoesntExist", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.isFile() != true)
                        {
                            throw constructTermination("messageInputPathIsntAFile", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.canRead() != true)
                        {
                            throw constructTermination("messageInputFileIsntReadable", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("output-file") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (outputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        outputFile = new File(attributePath.getValue());

                        if (outputFile.isAbsolute() != true)
                        {
                            outputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            outputFile = outputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (outputFile.exists() == true)
                        {
                            if (outputFile.isFile() == true)
                            {
                                boolean deleteSuccessful = false;

                                try
                                {
                                    deleteSuccessful = outputFile.delete();
                                }
                                catch (SecurityException ex)
                                {

                                }

                                if (deleteSuccessful != true)
                                {
                                    if (outputFile.canWrite() != true)
                                    {
                                        throw constructTermination("messageOutputFileExistsButIsntWritable", null, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                                    }
                                }
                            }
                            else
                            {
                                throw constructTermination("messageOutputPathExistsButIsntAFile", null, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                            }
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (inputFile == null)
        {
            throw constructTermination("messageJobFileInputFileNotConfigured", null, null, jobFile.getAbsolutePath());
        }

        if (outputFile == null)
        {
            throw constructTermination("messageJobFileOutputFileNotConfigured", null, null, jobFile.getAbsolutePath());
        }


        JPanel panelMain = new JPanel();

        GridBagLayout gridbag = new GridBagLayout();
        panelMain.setLayout(gridbag);

        GridBagConstraints gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.NORTH;
        gridbagConstraints.gridx = 0;
        gridbagConstraints.gridy = 0;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.weighty = 1.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.BOTH;

        DefaultMutableTreeNode treeRoot = loadXmlTree(inputFile);

        this.treeModel = new DefaultTreeModel(treeRoot);
        this.tree = new JTree(this.treeModel);

        TreeIconRenderer renderer = new TreeIconRenderer(this, this.treeNodeTypes);
        this.tree.setCellRenderer(renderer);

        this.tree.addKeyListener(new KeyEventListener(this));
        this.tree.addTreeSelectionListener(new TreeSelectionEventListener(this));

        expandAll(treeRoot);

        this.mainScrollPane = new JScrollPane();
        panelMain.add(this.mainScrollPane, gridbagConstraints);

        this.mainScrollPane.getViewport().removeAll();
        this.mainScrollPane.getViewport().add(this.tree);

        this.mainScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.mainScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.SOUTH;
        gridbagConstraints.gridy = 1;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        JTextField positionField = new JTextField();
        positionField.setText(getI10nString("windowStatusInfoStart"));
        positionField.setEditable(false);

        panelMain.add(positionField, gridbagConstraints);

        getContentPane().add(panelMain, BorderLayout.CENTER);

        setLocation(100, 100);
        setSize(500, 400);
        setVisible(true);

        return 0;
    }

    public DefaultMutableTreeNode loadXmlTree(File inputFile)
    {
        DefaultMutableTreeNode treeRoot = null;

        this.treeNodeTypes = new HashMap<DefaultMutableTreeNode, Integer>();
        this.sourceReferences = new HashMap<DefaultMutableTreeNode, Integer>();
        this.deletions = new ArrayList<Integer>();
        this.deletionsParents = new HashMap<Integer, Integer>();
        this.deletionsChildIndexes = new HashMap<Integer, Integer>();

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();

            // This is an editor for generic XML, don't want to deal with legacy DTD
            // remnants. Consider using $/xml_dtd_entity_resolver/xml_dtd_entity_resolver_1.
            inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
            inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);

            InputStream in = new FileInputStream(inputFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    StartElement startElement = event.asStartElement();
                    QName elementName = startElement.getName();

                    treeRoot = new DefaultMutableTreeNode(elementName.getLocalPart());
                    this.treeNodeTypes.put(treeRoot, 1);
                    this.sourceReferences.put(treeRoot, this.sourceReferences.size() + 1);

                    loadXmlElement(eventReader, treeRoot);
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
        }

        return treeRoot;
    }

    protected int loadXmlElement(XMLEventReader eventReader, DefaultMutableTreeNode nodeParent) throws XMLStreamException
    {
        StringBuilder textNodeBuffer = null;

        while (eventReader.hasNext() == true)
        {
            XMLEvent event = eventReader.nextEvent();

            if (event.isStartElement() == true)
            {
                if (textNodeBuffer != null)
                {
                    DefaultMutableTreeNode nodeText = new DefaultMutableTreeNode(textNodeBuffer.toString());
                    this.treeNodeTypes.put(nodeText, 2);
                    this.sourceReferences.put(nodeText, this.sourceReferences.size() + 1);

                    nodeParent.add(nodeText);

                    textNodeBuffer = null;
                }

                StartElement startElement = event.asStartElement();
                QName elementName = startElement.getName();

                DefaultMutableTreeNode nodeChild = new DefaultMutableTreeNode(elementName.getLocalPart());
                this.treeNodeTypes.put(nodeChild, 1);
                this.sourceReferences.put(nodeChild, this.sourceReferences.size() + 1);

                loadXmlElement(eventReader, nodeChild);

                nodeParent.add(nodeChild);
            }
            else if (event.isEndElement() == true)
            {
                if (textNodeBuffer != null)
                {
                    DefaultMutableTreeNode nodeText = new DefaultMutableTreeNode(textNodeBuffer.toString());
                    this.treeNodeTypes.put(nodeText, 2);
                    this.sourceReferences.put(nodeText, this.sourceReferences.size() + 1);

                    nodeParent.add(nodeText);

                    textNodeBuffer = null;
                }

                return 0;
            }
            else if (event.isCharacters() == true)
            {
                if (textNodeBuffer == null)
                {
                    textNodeBuffer = new StringBuilder();
                }

                textNodeBuffer.append(event.asCharacters().getData());
            }
        }

        if (textNodeBuffer != null)
        {
            DefaultMutableTreeNode nodeText = new DefaultMutableTreeNode(textNodeBuffer.toString());
            this.treeNodeTypes.put(nodeText, 2);
            this.sourceReferences.put(nodeText, this.sourceReferences.size() + 1);

            nodeParent.add(nodeText);

            textNodeBuffer = null;
        }

        return -1;
    }

    public int expandAll(DefaultMutableTreeNode node)
    {
        if (this.tree == null)
        {
            return 1;
        }

        this.tree.expandPath(new TreePath(node.getPath()));

        for (int i = 0, max = node.getChildCount(); i < max; i++)
        {
            DefaultMutableTreeNode childNode = (DefaultMutableTreeNode)node.getChildAt(i);

            if (childNode == null)
            {
                return -1;
            }

            if (expandAll(childNode) != 0)
            {
                break;
            }
        }

        return 0;
    }

    public int removeNodeEvent()
    {
        if (this.selectedNode == null)
        {
            return 1;
        }

        if (this.selectedNode == this.treeModel.getRoot())
        {
            return 2;
        }

        DefaultMutableTreeNode nodeParent = (DefaultMutableTreeNode)this.selectedNode.getParent();

        if (nodeParent == null)
        {
            throw constructTermination("messageTreeNodeWrongClassType", null, null);
        }


        int[] index = { nodeParent.getIndex(this.selectedNode) };
        Object[] child = { this.selectedNode };

        nodeParent.remove(this.selectedNode);
        //this.treeModel.reload(nodeParent); // probably less efficient than this.treeModel.nodesWereRemoved().
        this.treeModel.nodesWereRemoved(nodeParent, index, child);

        Integer deletedNodeIndex = this.sourceReferences.get(this.selectedNode);

        this.deletions.add(deletedNodeIndex);
        this.deletionsParents.put(deletedNodeIndex, this.sourceReferences.get(nodeParent));
        this.deletionsChildIndexes.put(deletedNodeIndex, index[0]);

        this.selectedNode = null;

        if (index[0] < nodeParent.getChildCount())
        {
            DefaultMutableTreeNode nextNode = (DefaultMutableTreeNode)nodeParent.getChildAt(index[0]);

            if (nextNode == null)
            {
                throw constructTermination("messageTreeNodeWrongClassType", null, null);
            }

            this.tree.setSelectionPath(new TreePath(nextNode.getPath()));
        }
        else
        {
            if (nodeParent.getChildCount() > 0)
            {
                DefaultMutableTreeNode nextNode = (DefaultMutableTreeNode)nodeParent.getChildAt(index[0] - 1);

                if (nextNode == null)
                {
                    throw constructTermination("messageTreeNodeWrongClassType", null, null);
                }

                this.tree.setSelectionPath(new TreePath(nextNode.getPath()));
            }
            else
            {
                this.tree.setSelectionPath(new TreePath(nodeParent.getPath()));
            }
        }

        return 0;
    }

    public int undo()
    {
        int indexLast = this.deletions.size() - 1;

        if (indexLast < 0)
        {
            return 1;
        }

        Integer sourceIndex = this.deletions.get(indexLast);
        Integer parentIndex = this.deletionsParents.get(sourceIndex);
        DefaultMutableTreeNode nodeRestore = null;
        DefaultMutableTreeNode nodeParent = null;

        for (Map.Entry<DefaultMutableTreeNode, Integer> entry : this.sourceReferences.entrySet())
        {
            if (entry.getValue() == sourceIndex)
            {
                if (nodeRestore != null)
                {
                    /** @todo constructTermination()? */
                }

                nodeRestore = entry.getKey();
            }

            if (entry.getValue() == parentIndex)
            {
                if (nodeParent != null)
                {
                    /** @todo constructTermination()? */
                }

                nodeParent = entry.getKey();
            }

            if (nodeRestore != null &&
                nodeParent != null)
            {
                break;
            }
        }

        if (nodeRestore == null)
        {
            return -1;
        }

        if (nodeParent == null)
        {
            return -2;
        }

        this.treeModel.insertNodeInto(nodeRestore, nodeParent, this.deletionsChildIndexes.get(sourceIndex));

        this.deletions.remove(indexLast);
        this.deletionsParents.remove(sourceIndex);
        this.deletionsChildIndexes.remove(sourceIndex);

        expandAll(nodeRestore);
        this.tree.setSelectionPath(new TreePath(nodeRestore.getPath()));

        return 0;
    }

    public int selectNodeEvent(DefaultMutableTreeNode node)
    {
        this.selectedNode = node;

        return 0;
    }

    protected int save()
    {
        /** @todo Aargh! Sorted set with random access, no? */
        this.deletionsSorted = new TreeMap<Integer, Integer>();

        /** @todo Maybe Map<T, U> has a (sorted?) valueSet() method or similar. */
        for (int i = 0, max = this.deletions.size(); i < max; i++)
        {
            deletionsSorted.put(i, this.deletions.get(i));
        }

        this.currentDeletionIndex = 0;
        this.currentDeletionCandidate = 0;
        this.isDeleting = false;

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(this.outputFile),
                                    "UTF-8"));

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();

                // This is an editor for generic XML, don't want to deal with legacy DTD
                // remnants. Consider using $/xml_dtd_entity_resolver/xml_dtd_entity_resolver_1.
                inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
                inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);

                InputStream in = new FileInputStream(this.inputFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                saveXmlNode(eventReader, writer);
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
            }

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
        }

        return 0;
    }

    protected int saveXmlNode(XMLEventReader eventReader, BufferedWriter writer) throws
      XMLStreamException, IOException
    {
        while (eventReader.hasNext() == true)
        {
            XMLEvent event = eventReader.nextEvent();

            if (event.isStartDocument() == true)
            {
                StartDocument startDocument = (StartDocument)event;

                writer.write("<?xml version=\"" + startDocument.getVersion() + "\"");

                if (startDocument.encodingSet() == true)
                {
                    writer.write(" encoding=\"" + startDocument.getCharacterEncodingScheme() + "\"");
                }

                if (startDocument.standaloneSet() == true)
                {
                    writer.write(" standalone=\"");

                    if (startDocument.isStandalone() == true)
                    {
                        writer.write("yes");
                    }
                    else
                    {
                        writer.write("no");
                    }

                    writer.write("\"");
                }

                writer.write("?>\n");
            }
            else if (event.isStartElement() == true)
            {
                this.currentDeletionCandidate += 1;

                boolean delete = false;

                if (this.currentDeletionIndex < this.deletionsSorted.size())
                {
                    if (this.deletionsSorted.get(this.currentDeletionIndex) == this.currentDeletionCandidate)
                    {
                        delete = true;
                        this.currentDeletionIndex += 1;
                    }
                }

                if (delete == true)
                {
                    this.isDeleting = true;
                }

                if (this.isDeleting != true)
                {
                    QName elementName = event.asStartElement().getName();
                    String fullElementName = elementName.getLocalPart();

                    if (elementName.getPrefix().isEmpty() != true)
                    {
                        fullElementName = elementName.getPrefix() + ":" + fullElementName;
                    }

                    writer.write("<" + fullElementName);

                    // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                    @SuppressWarnings("unchecked")
                    Iterator<Namespace> namespaces = (Iterator<Namespace>)event.asStartElement().getNamespaces();

                    while (namespaces.hasNext() == true)
                    {
                        Namespace namespace = namespaces.next();

                        if (namespace.isDefaultNamespaceDeclaration() == true &&
                            namespace.getPrefix().length() <= 0)
                        {
                            writer.write(" xmlns=\"" + namespace.getNamespaceURI() + "\"");
                        }
                        else
                        {
                            writer.write(" xmlns:" + namespace.getPrefix() + "=\"" + namespace.getNamespaceURI() + "\"");
                        }
                    }

                    // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                    @SuppressWarnings("unchecked")
                    Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

                    while (attributes.hasNext() == true)
                    {
                        Attribute attribute = attributes.next();
                        QName attributeName = attribute.getName();
                        String fullAttributeName = attributeName.getLocalPart();

                        if (attributeName.getPrefix().length() > 0)
                        {
                            fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
                        }

                        String attributeValue = attribute.getValue();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        attributeValue = attributeValue.replaceAll("&", "&amp;");
                        attributeValue = attributeValue.replaceAll("\"", "&quot;");
                        attributeValue = attributeValue.replaceAll("'", "&apos;");
                        attributeValue = attributeValue.replaceAll("<", "&lt;");
                        attributeValue = attributeValue.replaceAll(">", "&gt;");

                        writer.write(" " + fullAttributeName + "=\"" + attributeValue + "\"");
                    }

                    writer.write(">");
                }

                saveXmlNode(eventReader, writer);

                if (delete == true)
                {
                    this.isDeleting = false;
                }
            }
            else if (event.isEndElement() == true)
            {
                if (this.isDeleting != true)
                {
                    QName elementName = event.asEndElement().getName();
                    String fullElementName = elementName.getLocalPart();

                    if (elementName.getPrefix().isEmpty() != true)
                    {
                        fullElementName = elementName.getPrefix() + ":" + fullElementName;
                    }

                    writer.write("</" + fullElementName + ">");
                }

                return 0;
            }
            else if (event.isCharacters() == true)
            {
                this.currentDeletionCandidate += 1;

                boolean delete = false;

                if (this.currentDeletionIndex < this.deletionsSorted.size())
                {
                    if (this.deletionsSorted.get(this.currentDeletionIndex) == this.currentDeletionCandidate)
                    {
                        delete = true;
                        this.currentDeletionIndex += 1;
                    }
                }

                if (delete != true &&
                    this.isDeleting != true)
                {
                    event.writeAsEncodedUnicode(writer);
                }
            }
            else if (event.getEventType() == XMLStreamConstants.COMMENT)
            {
                if (this.isDeleting != true)
                {
                    writer.write("<!--" + ((Comment)event).getText() + "-->");
                }
            }
            else if (event.getEventType() == XMLStreamConstants.DTD)
            {
                DTD dtd = (DTD) event;

                if (dtd != null)
                {
                    writer.write(dtd.getDocumentTypeDeclaration());
                }
            }
            else if (event.isEndDocument() == true)
            {

            }
            else
            {
                throw constructTermination("messageUnsupportedXmlEventType", null, null, event.getEventType(), "javax.xml.stream.XMLStreamConstants", inputFile.getAbsolutePath());
            }
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xml_structure_editor_1: " + getI10nString(id);
            }
            else
            {
                message = "xml_structure_editor_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xml_structure_editor_1: " + getI10nString(id);
            }
            else
            {
                message = "xml_structure_editor_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (xml_structure_editor_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xml_structure_editor_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xml_structure_editor_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xml-structure-editor-1-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</xml-structure-editor-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        xml_structure_editor_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10n == null)
        {
            this.l10n = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10n.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10n.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected JScrollPane mainScrollPane = null;
    protected JTree tree = null;
    protected DefaultTreeModel treeModel = null;
    protected DefaultMutableTreeNode selectedNode = null;

    protected Map<DefaultMutableTreeNode, Integer> treeNodeTypes = null;
    protected Map<DefaultMutableTreeNode, Integer> sourceReferences = null;
    protected List<Integer> deletions = null;
    protected Map<Integer, Integer> deletionsParents = null;
    // OK, maybe these can be calculated, but too lazy to over-engineer for now...
    protected Map<Integer, Integer> deletionsChildIndexes = null;

    protected File inputFile = null;
    protected File outputFile = null;

    protected int currentDeletionIndex = 0;
    protected int currentDeletionCandidate = 0;
    protected SortedMap<Integer, Integer> deletionsSorted = null;
    protected boolean isDeleting = false;

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private String programPath = null;
    private static final String L10N_BUNDLE = "l10n.l10nXmlStructureEditor1";
    private ResourceBundle l10n;
}
