/* Copyright (C) 2019-2022 Stephan Kreutzer
 *
 * This file is part of option_picker_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * option_picker_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * option_picker_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with option_picker_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/gui/option_picker/option_picker_1/OptionEntry.java
 * @author Stephan Kreutzer
 * @since 2019-04-21
 */



class OptionEntry
{
    public OptionEntry(String id,
                       String caption,
                       String description)
    {
        this.id = id;
        this.caption = caption;
        this.description = description;
    }

    public String GetId()
    {
        return this.id;
    }

    public String GetCaption()
    {
        return this.caption;
    }

    public String GetDescription()
    {
        return this.description;
    }

    protected String id;
    protected String caption;
    protected String description;
}
