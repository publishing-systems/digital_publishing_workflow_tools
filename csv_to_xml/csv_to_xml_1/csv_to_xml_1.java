/* Copyright (C) 2016-2022  Stephan Kreutzer
 *
 * This file is part of csv_to_xml_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * csv_to_xml_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * csv_to_xml_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with csv_to_xml_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/csv_to_xml/csv_to_xml_1/csv_to_xml_1.java
 * @brief Converts CSV to XML.
 * @author Stephan Kreutzer
 * @since 2019-05-30
 */



import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.ArrayList;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.HashMap;



public class csv_to_xml_1
{
    public static void main(String args[])
    {
        System.out.print("csv_to_xml_1 Copyright (C) 2016-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");


        csv_to_xml_1 instance = new csv_to_xml_1();

        try
        {
            instance.call(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by csv_to_xml_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<csv-to-xml-1-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</csv-to-xml-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int call(String args[]) throws ProgramTerminationException
    {
        this.getInfoMessages().clear();

        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\tcsv_to_xml_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        csv_to_xml_1.resultInfoFile = resultInfoFile;


        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("csv_to_xml_1: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));


        File inputFile = null;
        File outputFile = null;
        String delimiter = null;
        boolean ignoreFirstLine = false;
        String rootElementName = null;
        String encapsulationElementName = null;
        String recordElementName = null;
        Map<Integer, String> fieldToElementNameMapping = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("csv-input-file") == true)
                    {
                        if (inputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StartElement inputFileElement = event.asStartElement();
                        Attribute pathAttribute = inputFileElement.getAttributeByName(new QName("path"));

                        if (pathAttribute == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        String inputFilePath = pathAttribute.getValue();

                        if (inputFilePath.isEmpty() == true)
                        {
                            throw constructTermination("messageJobFileAttributeValueIsEmpty", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        inputFile = new File(inputFilePath);

                        if (inputFile.isAbsolute() != true)
                        {
                            inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + inputFilePath);
                        }

                        try
                        {
                            inputFile = inputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, new File(inputFilePath).getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, new File(inputFilePath).getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.exists() != true)
                        {
                            throw constructTermination("messageInputFileDoesntExist", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.isFile() != true)
                        {
                            throw constructTermination("messageInputPathIsntAFile", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());  
                        }

                        if (inputFile.canRead() != true)
                        {
                            throw constructTermination("messageInputFileIsntReadable", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("xml-output-file") == true)
                    {
                        if (outputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StartElement outputFileElement = event.asStartElement();
                        Attribute pathAttribute = outputFileElement.getAttributeByName(new QName("path"));

                        if (pathAttribute == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        String outputFilePath = pathAttribute.getValue();

                        if (outputFilePath.isEmpty() == true)
                        {
                            throw constructTermination("messageJobFileAttributeValueIsEmpty", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        outputFile = new File(outputFilePath);

                        if (outputFile.isAbsolute() != true)
                        {
                            outputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + outputFilePath);
                        }

                        try
                        {
                            outputFile = outputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, new File(outputFilePath).getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, new File(outputFilePath).getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (outputFile.exists() == true)
                        {
                            if (outputFile.isFile() == true)
                            {
                                if (outputFile.canWrite() != true)
                                {
                                    throw constructTermination("messageOutputFileIsntWritable", null, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                                }
                            }
                            else
                            {
                                throw constructTermination("messageOutputPathIsntAFile", null, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                            }
                        }
                    }
                    else if (tagName.equals("delimiter") == true)
                    {
                        if (delimiter != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StringBuilder sb = null;

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isCharacters() == true)
                            {
                                if (sb == null)
                                {
                                    sb = new StringBuilder();
                                }

                                sb.append(event.asCharacters().getData());
                            }
                            else if (event.isEndElement() == true)
                            {
                                String tagNameEnd = event.asEndElement().getName().getLocalPart();

                                if (tagNameEnd.equals(tagName) == true)
                                {
                                    break;
                                }
                            }
                        }

                        if (sb == null)
                        {
                            throw constructTermination("messageJobFileElementChildrenIncomplete", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        delimiter = sb.toString();

                        if (delimiter.length() <= 0)
                        {
                            throw constructTermination("messageJobFileDelimiterElementIsEmpty", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        if (delimiter.indexOf('"') >= 0 ||
                            delimiter.indexOf('\r') >= 0 ||
                            delimiter.indexOf('\n') >= 0)
                        {
                            throw constructTermination("messageJobFileDelimiterContainsCsvControlCharacter", null, null, jobFile.getAbsolutePath(), tagName, delimiter);
                        }
                    }
                    else if (tagName.equals("ignore-first-line") == true)
                    {
                        if (eventReader.hasNext() != true)
                        {
                            throw constructTermination("messageJobFileEndReachedWhileMoreWasExpected", null, null, jobFile.getAbsolutePath());
                        }

                        event = eventReader.nextEvent();

                        if (event.isCharacters() != true)
                        {
                            throw constructTermination("messageJobFileNextEventIsntCharacters", null, null, jobFile.getAbsolutePath());
                        }

                        if (!event.asCharacters().getData().isEmpty())
                        {
                            ignoreFirstLine = !event.asCharacters().getData().equals("false");
                        }
                    }
                    else if (tagName.equals("root-element-name") == true)
                    {
                        if (rootElementName != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StringBuilder sb = null;

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isCharacters() == true)
                            {
                                if (sb == null)
                                {
                                    sb = new StringBuilder();
                                }

                                sb.append(event.asCharacters().getData());
                            }
                            else if (event.isEndElement() == true)
                            {
                                String tagNameEnd = event.asEndElement().getName().getLocalPart();

                                if (tagNameEnd.equals(tagName) == true)
                                {
                                    break;
                                }
                            }
                        }

                        if (sb == null)
                        {
                            throw constructTermination("messageJobFileElementChildrenIncomplete", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        rootElementName = sb.toString();

                        if (IsValidXmlElementName(rootElementName) != true)
                        {
                            throw constructTermination("messageJobFileInvalidXmlElementName", null, null, jobFile.getAbsolutePath(), tagName, rootElementName);
                        }
                    }
                    else if (tagName.equals("encapsulation-element-name") == true)
                    {
                        if (encapsulationElementName != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StringBuilder sb = null;

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isCharacters() == true)
                            {
                                if (sb == null)
                                {
                                    sb = new StringBuilder();
                                }

                                sb.append(event.asCharacters().getData());
                            }
                            else if (event.isEndElement() == true)
                            {
                                String tagNameEnd = event.asEndElement().getName().getLocalPart();

                                if (tagNameEnd.equals(tagName) == true)
                                {
                                    break;
                                }
                            }
                        }

                        if (sb == null)
                        {
                            throw constructTermination("messageJobFileElementChildrenIncomplete", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        encapsulationElementName = sb.toString();

                        if (IsValidXmlElementName(encapsulationElementName) != true)
                        {
                            throw constructTermination("messageJobFileInvalidXmlElementName", null, null, jobFile.getAbsolutePath(), tagName, encapsulationElementName);
                        }
                    }
                    else if (tagName.equals("record-element-name") == true)
                    {
                        if (recordElementName != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StringBuilder sb = null;

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isCharacters() == true)
                            {
                                if (sb == null)
                                {
                                    sb = new StringBuilder();
                                }

                                sb.append(event.asCharacters().getData());
                            }
                            else if (event.isEndElement() == true)
                            {
                                String tagNameEnd = event.asEndElement().getName().getLocalPart();

                                if (tagNameEnd.equals(tagName) == true)
                                {
                                    break;
                                }
                            }
                        }

                        if (sb == null)
                        {
                            throw constructTermination("messageJobFileElementChildrenIncomplete", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        recordElementName = sb.toString();

                        if (IsValidXmlElementName(recordElementName) != true)
                        {
                            throw constructTermination("messageJobFileInvalidXmlElementName", null, null, jobFile.getAbsolutePath(), tagName, recordElementName);
                        }
                    }
                    else if (tagName.equals("mapping") == true)
                    {
                        if (fieldToElementNameMapping != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        fieldToElementNameMapping = new HashMap<Integer, String>();

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isStartElement() == true)
                            {
                                StartElement startElement = event.asStartElement();
                                tagName = startElement.getName().getLocalPart();

                                if (tagName.equals("csv-field") == true)
                                {
                                    Attribute attributeNumber = startElement.getAttributeByName(new QName("number"));

                                    if (attributeNumber == null)
                                    {
                                        throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "number");
                                    }

                                    Attribute attributeXmlElementName = startElement.getAttributeByName(new QName("xml-element-name"));

                                    if (attributeXmlElementName == null)
                                    {
                                        throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "xml-element-name");
                                    }

                                    String fieldNumberString = attributeNumber.getValue();

                                    int fieldNumber = -1;

                                    try
                                    {
                                        fieldNumber = Integer.parseInt(fieldNumberString);
                                    }
                                    catch (NumberFormatException ex)
                                    {
                                        throw constructTermination("messageJobFileFailedToParseInteger", ex, null, jobFile.getAbsolutePath(), tagName, "number");
                                    }

                                    if (fieldNumber < 0)
                                    {
                                        throw constructTermination("messageJobFileIntegerIsNegative", null, null, jobFile.getAbsolutePath(), tagName, "number");
                                    }

                                    String xmlElementName = attributeXmlElementName.getValue();

                                    if (IsValidXmlElementName(xmlElementName) != true)
                                    {
                                        throw constructTermination("messageJobFileInvalidXmlElementName", null, null, jobFile.getAbsolutePath(), tagName, xmlElementName);
                                    }

                                    if (fieldToElementNameMapping.containsKey(fieldNumber) != true)
                                    {
                                        fieldToElementNameMapping.put(fieldNumber, xmlElementName);
                                    }
                                    else
                                    {
                                        throw constructTermination("messageJobFileFieldNumberConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName, "number", fieldNumber);
                                    }
                                }
                            }
                            else if (event.isEndElement() == true)
                            {
                                tagName = event.asEndElement().getName().getLocalPart();

                                if (tagName.equals("mapping") == true)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (inputFile == null)
        {
            throw constructTermination("messageJobFileNoInputFile", null, null, jobFile.getAbsolutePath());
        }

        if (delimiter == null)
        {
            throw constructTermination("messageJobFileNoDelimiter", null, null, jobFile.getAbsolutePath());
        }

        if (rootElementName == null)
        {
            throw constructTermination("messageJobFileNoRootElementName", null, null, jobFile.getAbsolutePath());
        }

        if (encapsulationElementName == null)
        {
            throw constructTermination("messageJobFileNoEncapsulationElementName", null, null, jobFile.getAbsolutePath());
        }

        if (recordElementName == null)
        {
            throw constructTermination("messageJobFileNoRecordElementName", null, null, jobFile.getAbsolutePath());
        }

        if (fieldToElementNameMapping == null)
        {
            throw constructTermination("messageJobFileNoFieldMapping", null, null, jobFile.getAbsolutePath());
        }

        if (fieldToElementNameMapping.size() <= 0)
        {
            throw constructTermination("messageJobFileFieldMappingIsEmpty", null, null, jobFile.getAbsolutePath());
        }

        if (outputFile == null)
        {
            throw constructTermination("messageJobFileNoOutputFile", null, null, jobFile.getAbsolutePath());
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(outputFile),
                                    "UTF-8"));

            try
            {
                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by csv_to_xml_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/clients/ and https://publishing-systems.org). -->\n");
                writer.write("<");
                writer.write(rootElementName);
                writer.write("><");
                writer.write(encapsulationElementName);
                writer.write(">");

                /** @todo What about the XML namespace for introducing semantics?
                  * Maybe also allow encapsulationElementName == null without
                  * writing it to output? */

                try
                {
                    BufferedReader reader = new BufferedReader(
                                            new InputStreamReader(
                                            new FileInputStream(inputFile),
                                            "UTF-8"));

                    try
                    {
                        ParserCsv parser = new ParserCsv();

                        if (parser.parse(reader,
                                         getInfoMessages(),
                                         delimiter,
                                         ignoreFirstLine,
                                         encapsulationElementName,
                                         recordElementName,
                                         fieldToElementNameMapping,
                                         writer) != 0)
                        {
                            throw constructTermination("messageParserErrorWhileParsing", null, null);
                        }
                    }
                    finally
                    {
                        reader.close();
                    }
                }
                catch (FileNotFoundException ex)
                {
                    throw constructTermination("messageParserErrorWhileParsing", ex, null);
                }
                catch (UnsupportedEncodingException ex)
                {
                    throw constructTermination("messageParserErrorWhileParsing", ex, null);
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageParserErrorWhileParsing", ex, null);
                }

                writer.write("</");
                writer.write(encapsulationElementName);
                writer.write("></");
                writer.write(rootElementName);
                writer.write(">");
            }
            finally
            {
                writer.close();
            }
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageParserErrorWhileParsing", ex, null);
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageParserErrorWhileParsing", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageParserErrorWhileParsing", ex, null);
        }

        return 0;
    }

    public boolean IsValidXmlElementName(String elementName)
    {
        if (elementName.length() >= 1)
        {
            if (Character.isLetter(elementName.charAt(0)) != true &&
                elementName.charAt(0) != '_')
            {
                return false;
            }

            if (elementName.length() >= 3)
            {
                if (elementName.substring(0, 3).equalsIgnoreCase("XML") == true)
                {
                    return false;
                }
            }

            for (int i = 1, max = elementName.length(); i < max; i++)
            {
                if (Character.isLetterOrDigit(elementName.charAt(i)) != true &&
                    elementName.charAt(i) != '-' &&
                    elementName.charAt(i) != '_' &&
                    elementName.charAt(i) != '.')
                {
                    return false;
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "csv_to_xml_1: " + getI10nString(id);
            }
            else
            {
                message = "csv_to_xml_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "csv_to_xml_1: " + getI10nString(id);
            }
            else
            {
                message = "csv_to_xml_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (csv_to_xml_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(csv_to_xml_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by csv_to_xml_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<csv-to-xml-1-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</csv-to-xml-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        csv_to_xml_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nCsvToXml1Console";
    private ResourceBundle l10nConsole;
}
