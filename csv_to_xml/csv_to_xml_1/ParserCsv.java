/* Copyright (C) 2016-2022  Stephan Kreutzer
 *
 * This file is part of csv_to_xml_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * csv_to_xml_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * csv_to_xml_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with csv_to_xml_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/csv_to_xml/csv_to_xml_1/ParserCsv.java
 * @brief Parses the tokens of a CSV file and converts them into XML.
 * @details Some things are complicated because multi-character delimiter/separator
 *     doesn't have its own handling method, which could also try to match after
 *     an enclosed field, instead of tracking this in state.
 * @author Stephan Kreutzer
 * @since 2019-05-30
 */



import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.UnsupportedEncodingException;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;



public class ParserCsv
{
    public ParserCsv()
    {

    }

    public int parse(BufferedReader reader,
                     List<InfoMessage> infoMessages,
                     String delimiter,
                     boolean ignoreFirstLine,
                     String encapsulationElementName,
                     String recordElementName,
                     Map<Integer, String> fieldToElementNameMapping,
                     BufferedWriter writer) throws IOException
    {
        this.reader = reader;
        this.infoMessages = infoMessages;
        this.delimiter = delimiter;
        this.ignoreFirstLine = ignoreFirstLine;
        this.encapsulationElementName = encapsulationElementName;
        this.recordElementName = recordElementName;
        this.fieldToElementNameMapping = fieldToElementNameMapping;
        this.writer = writer;

        this.countDelimiterMatch = 0;
        this.countFieldMax = -1;
        this.countField = 0;
        this.countFieldRelevantMax = 0;
        this.countFieldRelevant = 0;
        this.countCharacter = 0L;
        this.omitOutput = false;

        for (int fieldNumber : this.fieldToElementNameMapping.keySet())
        {
            if (this.countFieldRelevantMax < fieldNumber)
            {
                this.countFieldRelevantMax = fieldNumber;
            }
        }

        // These numbers are 0-based, count is 1-based.
        this.countFieldRelevantMax += 1;

        if (this.ignoreFirstLine == true)
        {
            this.omitOutput = true;

            int result = HandleLine();

            if (result == this.RETURN_HANDLELINE_ENDOFLINE)
            {
                // Successfully omitted.
            }
            else if (result == RETURN_HANDLELINE_OUTOFCHARACTERS)
            {
                return 0;
            }
            else if (result == RETURN_HANDLELINE_NOCHARACTERS)
            {
                return 0;
            }
            else
            {
                throw new UnsupportedOperationException();
            }

            this.omitOutput = false;
        }

        while (true)
        {
            int result = HandleLine();

            if (result == this.RETURN_HANDLELINE_ENDOFLINE)
            {
                continue;
            }
            else if (result == RETURN_HANDLELINE_OUTOFCHARACTERS)
            {
                break;
            }
            else if (result == RETURN_HANDLELINE_NOCHARACTERS)
            {
                break;
            }
            else
            {
                throw new UnsupportedOperationException();
            }
        }

        return 0;
    }

    protected static final int RETURN_HANDLELINE_ENDOFLINE = 0;
    protected static final int RETURN_HANDLELINE_OUTOFCHARACTERS = 1;
    protected static final int RETURN_HANDLELINE_NOCHARACTERS = 2;

    protected int HandleLine() throws IOException
    {
        this.countDelimiterMatch = 0;
        this.countField = 0;
        this.countFieldRelevant = 0;
        this.countCharacter = 0L;

        while (true)
        {
            int result = HandleField();

            if (result == this.RETURN_HANDLEFIELD_ENDOFFIELD)
            {
                continue;
            }
            else if (result == this.RETURN_HANDLEFIELD_ENDOFLINE ||
                     result == this.RETURN_HANDLEFIELD_OUTOFCHARACTERS)
            {
                if (this.countFieldMax < 0)
                {
                    if (this.countField < this.countFieldRelevantMax)
                    {
                        throw constructTermination("messageParserFieldCountDoesntCoverRelevantFields", null, null, this.countField, this.countFieldRelevantMax);
                    }

                    this.countFieldMax = this.countField;
                }
                else
                {
                    if (this.countField != this.countFieldMax)
                    {
                        throw constructTermination("messageParserLineIsIrregular", null, null, this.countField, this.countFieldMax);
                    }
                }

                Write("</", false);
                Write(this.recordElementName, false);
                Write(">", false);

                if (result == this.RETURN_HANDLEFIELD_ENDOFLINE)
                {
                    return this.RETURN_HANDLELINE_ENDOFLINE;
                }
                else if (result == this.RETURN_HANDLEFIELD_OUTOFCHARACTERS)
                {
                    return this.RETURN_HANDLELINE_OUTOFCHARACTERS;
                }
                else
                {
                    throw new UnsupportedOperationException();
                }
            }
            else if (result == this.RETURN_HANDLEFIELD_NOCHARACTERS)
            {
                if (this.countFieldMax < 0)
                {
                    // First line and EOF as the first field,
                    // while relevant fields are always configured
                    // in the jobfile, that's a certain exception.

                    if (this.countField < this.countFieldRelevantMax)
                    {
                        throw constructTermination("messageParserEndOfFileWithRelevantFieldsMissing", null, null, this.countFieldRelevantMax);
                    }
                    else
                    {
                      // Should never happen, RETURN_HANDLEFIELD_NOCHARACTERS did check for
                      // this.countField <= 0 which also must be < 1-based this.countFieldRelevantMax.
                      throw new UnsupportedOperationException();
                    }
                }
                else
                {
                    if (this.countField <= 0)
                    {
                        // CRLF at the end of previous line, then EOF
                        // as the first field, valid according to the
                        // RFC and shouldn't lead to an exception because
                        // of irregularity, shouldn't lead to a record
                        // in the XML output with a first empty field in it.
                        return this.RETURN_HANDLELINE_NOCHARACTERS;
                    }
                    else
                    {
                        // Should never happen, RETURN_HANDLEFIELD_NOCHARACTERS did check for
                        // this.countField <= 0.
                        throw new UnsupportedOperationException();
                    }
                }
            }
            else
            {
                throw new UnsupportedOperationException();
            }
        }
    }

    protected static final int RETURN_HANDLEFIELD_ENDOFFIELD = 0;
    protected static final int RETURN_HANDLEFIELD_ENDOFLINE = 1;
    protected static final int RETURN_HANDLEFIELD_OUTOFCHARACTERS = 2;
    protected static final int RETURN_HANDLEFIELD_NOCHARACTERS = 3;

    protected int HandleField() throws IOException
    {
        this.countDelimiterMatch = 0;

        boolean wasInEnclosedField = false;

        int character = this.reader.read();

        if (character < 0 &&
            this.countField <= 0)
        {
            return this.RETURN_HANDLEFIELD_NOCHARACTERS;
        }

        this.countCharacter = 0L;

        if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
        {
            if (this.countFieldRelevant <= 0)
            {
                Write("<", false);
                Write(this.recordElementName, false);
                Write(">", false);
            }

            Write("<", false);
            Write(this.fieldToElementNameMapping.get(this.countField), false);
            Write(">", false);
        }

        while (character >= 0)
        {
            ++this.countCharacter;

            if (character == '\r')
            {
                if (this.countDelimiterMatch > 0)
                {
                    if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
                    {
                        Write(this.delimiter.substring(0, this.countDelimiterMatch), true);
                    }

                    this.countDelimiterMatch = 0;
                }

                character = this.reader.read();

                if (character == '\n')
                {
                    if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
                    {
                        Write("</", false);
                        Write(this.fieldToElementNameMapping.get(this.countField), false);
                        Write(">", false);

                        ++this.countFieldRelevant;
                    }

                    ++this.countField;
                    wasInEnclosedField = false;

                    return this.RETURN_HANDLEFIELD_ENDOFLINE;
                }
                else
                {
                    if (wasInEnclosedField == true)
                    {
                        throw constructTermination("messageParserAdditionalCharactersInFieldAfterEndOfEnclosedFieldValue", null, null);
                    }

                    if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
                    {
                        Write('\r', true);
                    }

                    continue;
                }
            }
            else if (character == this.delimiter.charAt(this.countDelimiterMatch))
            {
                ++this.countDelimiterMatch;

                if (this.countDelimiterMatch == this.delimiter.length())
                {
                    if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
                    {
                        Write("</", false);
                        Write(this.fieldToElementNameMapping.get(this.countField), false);
                        Write(">", false);

                        ++this.countFieldRelevant;
                    }

                    this.countDelimiterMatch = 0;
                    ++this.countField;

                    wasInEnclosedField = false;

                    return this.RETURN_HANDLEFIELD_ENDOFFIELD;
                }
            }
            else if (character == '"')
            {
                if (wasInEnclosedField == true)
                {
                    throw constructTermination("messageParserAdditionalCharactersInFieldAfterEndOfEnclosedFieldValue", null, null);
                }

                if (this.countCharacter == 1L)
                {
                    int nextCharacter = HandleFieldEnclosed(this.fieldToElementNameMapping.containsKey(this.countField));

                    --this.countCharacter;
                    character = nextCharacter;
                    wasInEnclosedField = true;

                    continue;
                }
                else
                {
                    if (this.countDelimiterMatch > 0)
                    {
                        if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
                        {
                            Write(this.delimiter.substring(0, this.countDelimiterMatch), true);
                        }

                        this.countDelimiterMatch = 0;
                    }

                    if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
                    {
                        Write(character, true);
                    }
                }
            }
            else
            {
                if (wasInEnclosedField == true)
                {
                    throw constructTermination("messageParserAdditionalCharactersInFieldAfterEndOfEnclosedFieldValue", null, null);
                }

                if (this.countDelimiterMatch > 0)
                {
                    if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
                    {
                        Write(this.delimiter.substring(0, this.countDelimiterMatch), true);
                    }

                    this.countDelimiterMatch = 0;
                }

                if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
                {
                    Write(character, true);
                }
            }

            character = this.reader.read();
        }

        if (this.fieldToElementNameMapping.containsKey(this.countField) == true)
        {
            Write("</", false);
            Write(this.fieldToElementNameMapping.get(this.countField), false);
            Write(">", false);

            ++this.countFieldRelevant;
        }

        ++this.countField;

        return this.RETURN_HANDLEFIELD_OUTOFCHARACTERS;
    }

    protected int HandleFieldEnclosed(boolean writeFieldToOutput) throws IOException
    {
        int character = this.reader.read();

        while (character >= 0)
        {
            ++this.countCharacter;

            if (character == '"')
            {
                character = this.reader.read();

                if (character < 0)
                {
                    throw constructTermination("messageParserEndOfFileWithinEnclosedFieldValue", null, null);
                }

                ++this.countCharacter;

                if (character == '"')
                {
                    if (writeFieldToOutput == true)
                    {
                        Write(character, true);
                    }
                }
                else
                {
                    return character;
                }
            }
            else
            {
                if (writeFieldToOutput == true)
                {
                    Write(character, true);
                }
            }

            character = this.reader.read();
        }

        throw constructTermination("messageParserEndOfFileWithinEnclosedFieldValue", null, null);
    }

    protected int Write(int character, boolean escapeXmlSpecialCharacters) throws IOException
    {
        if (this.omitOutput == true)
        {
            return 0;
        }

        if (escapeXmlSpecialCharacters == true)
        {
            if (character == '&')
            {
                this.writer.write("&amp;");
            }
            else if (character == '<')
            {
                this.writer.write("&lt;");
            }
            else if (character == '>')
            {
                this.writer.write("&gt;");
            }
            else
            {
                this.writer.write(character);
            }
        }
        else
        {
            this.writer.write(character);
        }

        return 0;
    }

    protected int Write(String string, boolean escapeXmlSpecialCharacters) throws IOException
    {
        if (this.omitOutput == true)
        {
            return 0;
        }

        if (escapeXmlSpecialCharacters == true)
        {
            // Ampersand needs to be the first, otherwise it would double-encode
            // other entities.
            string = string.replaceAll("&", "&amp;");
            string = string.replaceAll("<", "&lt;");
            string = string.replaceAll(">", "&gt;");
        }

        this.writer.write(string);

        return 0;
    }


    BufferedReader reader = null;
    BufferedWriter writer = null;
    String delimiter = null;
    boolean ignoreFirstLine = false;
    String encapsulationElementName = null;
    String recordElementName = null;
    Map<Integer, String> fieldToElementNameMapping = null;
    boolean omitOutput = false;

    protected int countDelimiterMatch = 0;
    protected int countFieldMax = -1;
    protected int countField = 0;
    protected int countFieldRelevantMax = -1;
    protected int countFieldRelevant = 0;
    protected long countCharacter = 0L;


    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "csv_to_xml_1 (ParserCsv): " + getI10nString(id);
            }
            else
            {
                message = "csv_to_xml_1 (ParserCsv): " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "csv_to_xml_1 (ParserCsv): " + getI10nString(id);
            }
            else
            {
                message = "csv_to_xml_1 (ParserCsv): " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nParserCsv == null)
        {
            this.l10nParserCsv = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nParserCsv.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nParserCsv.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected List<InfoMessage> infoMessages = null;

    private static final String L10N_BUNDLE = "l10n.l10nCsvToXml1ParserCsv";
    private ResourceBundle l10nParserCsv;
}
