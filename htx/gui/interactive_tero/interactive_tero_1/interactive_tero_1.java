/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of interactive_tero_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * interactive_tero_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * interactive_tero_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with interactive_tero_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/interactive_tero/interactive_tero_1/interactive_tero_1.java
 * @brief Stepping debugger for Tero grammars.
 * @author Stephan Kreutzer
 * @since 2021-10-13
 */



import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.io.File;
import org.publishing_systems._20140527t120137z.jterosta.JTeroLoader;
import org.publishing_systems._20140527t120137z.jterosta.JTeroPattern;
import org.publishing_systems._20140527t120137z.jterosta.JTeroFunction;
import org.publishing_systems._20140527t120137z.jterosta.JTeroThenCase;
import org.publishing_systems._20140527t120137z.jterosta.JTeroStAInterpreter;
import org.publishing_systems._20140527t120137z.jterosta.JTeroEvent;
import org.publishing_systems._20140527t120137z.jterosta.JTeroInputStreamInterface;
import org.publishing_systems._20140527t120137z.jterosta.JTeroInputStreamStd;
import org.publishing_systems._20140527t120137z.jterosta.JTeroException;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;



public class interactive_tero_1
  extends JFrame
  implements ActionListener
{
    public static void main(String[] args)
    {
        System.out.print("interactive_tero_1 Copyright (C) 2021-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://hypertext-systems.org.\n\n");

        interactive_tero_1 instance = new interactive_tero_1();

        try
        {
            instance.run(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by interactive_tero_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<interactive-tero-1-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</interactive-tero-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public interactive_tero_1()
    {
        super("Interactive Tero 1");

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event)
            {
                event.getWindow().setVisible(false);
                event.getWindow().dispose();
                System.exit(0);
            }
        });
    }

    public int run(String[] args)
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\tinteractive_tero_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        interactive_tero_1.resultInfoFile = resultInfoFile;
        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("interactive_tero_1: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));

        File codeDirectory = null;
        int fontSize = 16;
        boolean debugBarEnable = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input-file") == true)
                    {
                        if (this.inputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        this.inputFile = new File(attributePath.getValue());

                        if (this.inputFile.isAbsolute() != true)
                        {
                            this.inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        if (this.inputFile.exists() != true)
                        {
                            throw constructTermination("messageJobFileInputFileDoesntExist", null, null, jobFile.getAbsolutePath(), this.inputFile.getAbsolutePath());
                        }

                        if (this.inputFile.isFile() != true)
                        {
                            throw constructTermination("messageJobFileInputPathIsntAFile", null, null, jobFile.getAbsolutePath(), this.inputFile.getAbsolutePath());
                        }

                        if (this.inputFile.canRead() != true)
                        {
                            throw constructTermination("messageJobFileInputFileIsntReadable", null, null, jobFile.getAbsolutePath(), this.inputFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("code-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (codeDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        codeDirectory = new File(attributePath.getValue());

                        if (codeDirectory.isAbsolute() != true)
                        {
                            codeDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            codeDirectory = codeDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageCodeDirectoryCantGetCanonicalPath", ex, null, codeDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageCodeDirectoryCantGetCanonicalPath", ex, null, codeDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (codeDirectory.exists() != true)
                        {
                            throw constructTermination("messageCodeDirectoryDoesntExist", null, null, codeDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (codeDirectory.isDirectory() != true)
                        {
                            throw constructTermination("messageCodePathIsntADirectory", null, null, codeDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (codeDirectory.canRead() != true)
                        {
                            throw constructTermination("messageCodeDirectoryIsntReadable", null, null, codeDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("start-function") == true)
                    {
                        Attribute attributeStartFunctionName = event.asStartElement().getAttributeByName(new QName("name"));

                        if (attributeStartFunctionName == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "name");
                        }

                        if (this.startFunctionName != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        this.startFunctionName = attributeStartFunctionName.getValue();
 
                        if (this.startFunctionName.isEmpty() == true)
                        {
                            throw constructTermination("messageJobFileStartFunctionNameIsEmpty", null, null, jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("font-size") == true)
                    {
                        Attribute attributePoint = event.asStartElement().getAttributeByName(new QName("point"));

                        if (attributePoint == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "point");
                        }

                        fontSize = Integer.parseInt(attributePoint.getValue());
                    }
                    else if (tagName.equals("debug-bar") == true)
                    {
                        Attribute attributeEnable = event.asStartElement().getAttributeByName(new QName("enable"));

                        if (attributeEnable != null)
                        {
                            debugBarEnable = attributeEnable.getValue().equals("true") == true;
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (this.inputFile == null)
        {
            throw constructTermination("messageJobFileInputFileIsntConfigured", null, null, jobFile.getAbsolutePath());
        }

        if (codeDirectory == null)
        {
            throw constructTermination("messageJobFileCodeDirectoryIsntConfigured", null, null, jobFile.getAbsolutePath());
        }

        if (this.startFunctionName == null)
        {
            throw constructTermination("messageJobFileStartFunctionNameIsntConfigured", null, null, jobFile.getAbsolutePath());
        }

        StringBuilder inputText = new StringBuilder();

        try
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(this.inputFile), "UTF-8"));

            while (true)
            {
                int character = reader.read();

                // This is probably a flawed "solution".
                if (character >= 0)
                {
                    boolean isUTF16 = false;

                    if (Character.isHighSurrogate((char)character) == true)
                    {
                        int character2 = reader.read();

                        if (character2 == 0)
                        {
                            throw constructTermination("messageInputFileSurrogateAborted", null, null, (char)character, String.format("0x%X", (int)character));
                        }

                        if (Character.isLowSurrogate((char)character2) != true)
                        {
                            throw constructTermination("messageInputFileSurrogateIncomplete", null, null, (char)character2, String.format("0x%X", (int)character2));
                        }

                        character = character * 0x10000;
                        character += character2;

                        isUTF16 = true;
                    }

                    if (isUTF16 == false)
                    {
                        inputText.append((char)character);
                    }
                    else
                    {
                        byte[] codePoints = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).putInt(character).array();
                        inputText.append(new String(codePoints, "UTF-16"));
                    }
                }
                else
                {
                    break;
                }
            }
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageErrorWhileReadingInputFile", ex, null, this.inputFile);
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageErrorWhileReadingInputFile", ex, null, this.inputFile);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageErrorWhileReadingInputFile", ex, null, this.inputFile);
        }


        try
        {
            JTeroLoader teroLoader = new JTeroLoader(codeDirectory.getAbsolutePath());
            teroLoader.load(true);

            this.patterns = teroLoader.getPatterns();
            this.functions = teroLoader.getFlow();
        }
        catch (JTeroException ex)
        {
            throw constructTermination("messageErrorWhileLoadingCode", ex, null, codeDirectory.getAbsolutePath());
        }

        try
        {
            JTeroInputStreamInterface stream = new JTeroInputStreamStd(new FileInputStream(this.inputFile));

            this.interpreter = new JTeroStAInterpreter(this.functions, this.patterns, this.startFunctionName, stream, false);
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageErrorWhileSettingUpTheInterpreter", ex, null);
        }
        catch (JTeroException ex)
        {
            throw constructTermination("messageErrorWhileSettingUpTheInterpreter", ex, null);
        }


        JPanel mainPanel = new JPanel(new GridLayout(1, 2));

        GridBagLayout gridbag = new GridBagLayout();

        JPanel panelLeft = new JPanel(gridbag);
        JPanel panelRight = new JPanel(new GridLayout(1, 1));

        mainPanel.add(panelLeft);
        mainPanel.add(panelRight);


        GridBagConstraints gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.NORTH;
        gridbagConstraints.gridy = 0;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.weighty = 1.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.BOTH;

        this.textArea = new JTextArea(inputText.toString());

        this.textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, fontSize));
        this.textArea.setLineWrap(true);
        this.textArea.setWrapStyleWord(true);
        this.textArea.setEditable(false);
        this.textArea.getCaret().setVisible(true);
        this.textArea.getCaret().setSelectionVisible(true);

        JScrollPane textAreaScroll = new JScrollPane(this.textArea);
        textAreaScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        textAreaScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        panelLeft.add(textAreaScroll, gridbagConstraints);


        if (debugBarEnable == true)
        {
            this.statusTextField = new JTextField();
            this.statusTextField.setText("");
            this.statusTextField.setEditable(false);

            gridbagConstraints = new GridBagConstraints();
            gridbagConstraints.anchor = GridBagConstraints.SOUTH;
            gridbagConstraints.gridy = 1;
            gridbagConstraints.weightx = 1.0;
            gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
            gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

            panelLeft.add(this.statusTextField, gridbagConstraints);
        }


        JPanel panelRightSections = new JPanel(new GridBagLayout());

        gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.PAGE_START;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.weighty = 0.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.REMAINDER;

        JButton buttonReset = new JButton(getI10nString("guiCaptionButtonReset"));
        buttonReset.addActionListener(this);

        JButton buttonStep = new JButton(getI10nString("guiCaptionButtonStep"));
        buttonStep.addActionListener(this);

        JPanel panelInteractions = new JPanel(new GridLayout(1, 4));
        panelInteractions.add(buttonReset);
        panelInteractions.add(buttonStep);

        {
            JPanel borderLayoutPanel = new JPanel(new BorderLayout());
            borderLayoutPanel.add(panelInteractions, BorderLayout.NORTH);

            panelRightSections.add(borderLayoutPanel, gridbagConstraints);
        }

        panelRight.add(panelRightSections);


        JTabbedPane paneTabbed = new JTabbedPane();

        {
            JPanel panelFlow = new JPanel(new GridBagLayout());

            GridBagConstraints gridbagConstraints2 = new GridBagConstraints();
            gridbagConstraints2.anchor = GridBagConstraints.NORTH;
            gridbagConstraints2.weightx = 1.0;
            gridbagConstraints2.weighty = 0.0;
            gridbagConstraints2.gridwidth = GridBagConstraints.REMAINDER;
            gridbagConstraints2.gridwidth = GridBagConstraints.REMAINDER;
            gridbagConstraints2.fill = GridBagConstraints.BOTH;

            for (Map.Entry<String, JTeroFunction> entry : this.functions.entrySet())
            {
                JTeroFunction function = entry.getValue();

                JPanel panelGroup = new JPanel();

                GroupLayout layout = new GroupLayout(panelGroup);
                panelGroup.setLayout(layout);

                layout.setAutoCreateGaps(false);
                layout.setAutoCreateContainerGaps(true);


                JTextField textFieldName = new JTextField(function.getName());

                this.fieldsFunctions.put(function.getName(), textFieldName);

                GroupLayout.ParallelGroup targetPre = null;
                targetPre = layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                  .addComponent(textFieldName);

                GroupLayout.ParallelGroup target = null;
                target = targetPre.addGroup(layout.createSequentialGroup());

                layout.setHorizontalGroup(layout.createSequentialGroup()
                  .addGroup(targetPre)
                );


                List<JPanel> fields = new ArrayList<JPanel>();
                List<JTeroThenCase> thenCases = function.getThenCases();

                for (int j = 0, max2 = thenCases.size(); j < max2; j++)
                {
                    JTeroThenCase thenCase = thenCases.get(j);

                    List<JTeroPattern> patternList = thenCase.getPatterns();

                    if (patternList == null)
                    {
                        // Should not be possible, already checked/detected by the loader.
                        throw constructTermination("messageErrorThenCaseWithoutPatterns", null, null);
                    }

                    // In theory, would support multiple patterns for a then-case, but notation
                    // currently supports only one, probably for the better (is cleaner?).

                    if (patternList.get(0) == null)
                    {
                        // Should not be possible, already checked/detected by the loader.
                        throw constructTermination("messageErrorThenCaseFirstPatternIsNull", null, null);
                    }

                    String patternName = patternList.get(0).getName();

                    if (this.fieldsThenCaseFunctions.containsKey(function.getName()) != true)
                    {
                        this.fieldsThenCaseFunctions.put(function.getName(), new HashMap<String, JTextField>());
                    }

                    JTextField fieldPattern = new JTextField(patternName);
                    JTextField fieldThenCaseFunction = new JTextField(thenCase.getThenCaseFunction());

                    this.fieldsThenCaseFunctions.get(function.getName()).put(patternName, fieldThenCaseFunction);

                    JPanel panelGroupThenCaseRow = new JPanel(new GridLayout(1, 2));
                    panelGroupThenCaseRow.add(fieldPattern);
                    panelGroupThenCaseRow.add(fieldThenCaseFunction);

                    fields.add(panelGroupThenCaseRow);

                    target.addGroup(layout.createSequentialGroup()
                      .addPreferredGap((j == 0 ? textFieldName : fields.get(fields.size() - 1)), panelGroupThenCaseRow, LayoutStyle.ComponentPlacement.INDENT)
                      .addComponent(panelGroupThenCaseRow)
                    );
                }

                String elseCaseCode = function.getElseCaseCode();
                String elseCaseFunction = function.getElseCaseFunction();
                boolean hasRetain = function.getHasRetain();

                JTextField fieldElseCaseFunction = new JTextField(elseCaseFunction);

                this.fieldsElseCaseFunctions.put(function.getName(), fieldElseCaseFunction);

                JPanel panelGroupElseCaseRow = new JPanel(new GridLayout(1, 2));
                panelGroupElseCaseRow.add(new JLabel());
                panelGroupElseCaseRow.add(fieldElseCaseFunction);
                // GridLayout has also setHgap(), for indentation?

                fields.add(panelGroupElseCaseRow);

                target.addGroup(layout.createSequentialGroup()
                  .addPreferredGap((thenCases.size() == 0 ? textFieldName : fields.get(fields.size() - 1)), panelGroupElseCaseRow, LayoutStyle.ComponentPlacement.INDENT)
                  .addComponent(panelGroupElseCaseRow)
                );

                // Now adding all fields another time, as required to define
                // the vertical arrangement.

                GroupLayout.SequentialGroup target2 = null;
                target2 = layout.createSequentialGroup();

                target2.addGroup(
                  layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(textFieldName)
                );

                for (int x = 0, y = fields.size(); x < y; x++)
                {
                    target2.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      .addComponent(fields.get(x))
                    );
                }

                layout.setVerticalGroup(target2);

                panelFlow.add(panelGroup, gridbagConstraints2);
            }

            JPanel borderLayoutPanel = new JPanel(new BorderLayout());
            borderLayoutPanel.add(panelFlow, BorderLayout.NORTH);

            JScrollPane panelFlowScroll = new JScrollPane(borderLayoutPanel);
            panelFlowScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            panelFlowScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

            paneTabbed.addTab(getI10nString("guiCaptionTabFlow"), panelFlowScroll);
        }


        {
            JPanel panelPatterns = new JPanel(new GridBagLayout());

            JPanel borderLayoutPanel = new JPanel(new BorderLayout());
            borderLayoutPanel.add(panelPatterns, BorderLayout.NORTH);

            JScrollPane panelPatternsScroll = new JScrollPane(borderLayoutPanel);
            panelPatternsScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            panelPatternsScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

            paneTabbed.addTab(getI10nString("guiCaptionTabPatterns"), panelPatternsScroll);


            GridBagConstraints gridbagConstraints2 = new GridBagConstraints();
            gridbagConstraints2.anchor = GridBagConstraints.NORTH;
            gridbagConstraints2.weightx = 1.0;
            gridbagConstraints2.weighty = 0.0;
            gridbagConstraints2.gridwidth = GridBagConstraints.REMAINDER;
            gridbagConstraints2.fill = GridBagConstraints.HORIZONTAL;

            for (Map.Entry<String, JTeroPattern> entry : this.patterns.entrySet())
            {
                JTeroPattern pattern = entry.getValue();

                JPanel panelGroup = new JPanel();

                GroupLayout layout = new GroupLayout(panelGroup);
                panelGroup.setLayout(layout);

                layout.setAutoCreateGaps(false);
                layout.setAutoCreateContainerGaps(false);

                JTextField textFieldName = new JTextField(pattern.getName());
                JTextField textFieldSequence = new JTextField(pattern.getRules().get(0).getSequence());

                layout.setHorizontalGroup(layout.createSequentialGroup()
                  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldName) 
                  )
                  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldSequence)
                  )
                );

                layout.setVerticalGroup(layout.createSequentialGroup()
                  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldName)
                    .addComponent(textFieldSequence)
                  )
                );

                panelPatterns.add(panelGroup, gridbagConstraints2);
            }
        }

        gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.CENTER;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.weighty = 1.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.BOTH;

        panelRightSections.add(paneTabbed, gridbagConstraints);

        getContentPane().add(mainPanel);

        setLocation(100, 100);
        setSize(600, 400);
        setVisible(true);

        return 0;
    }

    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() instanceof JButton)
        {
            JButton sender = (JButton)event.getSource();

            if (sender.getText().equals(getI10nString("guiCaptionButtonReset")) == true)
            {
                for (Map.Entry<String, JTextField> fieldFunction : this.fieldsFunctions.entrySet())
                {
                    fieldFunction.getValue().setBackground(Color.WHITE);
                }

                for (Map.Entry<String, Map<String, JTextField>> function : this.fieldsThenCaseFunctions.entrySet())
                {
                    for (Map.Entry<String, JTextField> fieldPattern : function.getValue().entrySet())
                    {
                        fieldPattern.getValue().setBackground(Color.WHITE);
                    }
                }

                for (Map.Entry<String, JTextField> fieldFunction : this.fieldsElseCaseFunctions.entrySet())
                {
                    fieldFunction.getValue().setBackground(Color.WHITE);
                }

                if (this.statusTextField != null)
                {
                    this.statusTextField.setText("");
                }

                try
                {
                    JTeroInputStreamInterface stream = new JTeroInputStreamStd(new FileInputStream(this.inputFile));

                    this.interpreter = new JTeroStAInterpreter(this.functions, this.patterns, this.startFunctionName, stream, false);
                }
                catch (FileNotFoundException ex)
                {
                    throw constructTermination("messageErrorWhileSettingUpTheInterpreter", ex, null);
                }
                catch (JTeroException ex)
                {
                    throw constructTermination("messageErrorWhileSettingUpTheInterpreter", ex, null);
                }

                this.selectionStart = 0;
                this.selectionSecondary = -1;
                this.selectionSecondaryPattern = null;
                this.selectionSecondaryElseCase = false;
                this.currentFunction = null;
            }
            else if (sender.getText().equals(getI10nString("guiCaptionButtonStep")) == true)
            {
                for (Map.Entry<String, JTextField> fieldFunction : this.fieldsFunctions.entrySet())
                {
                    fieldFunction.getValue().setBackground(Color.WHITE);
                }

                for (Map.Entry<String, Map<String, JTextField>> function : this.fieldsThenCaseFunctions.entrySet())
                {
                    for (Map.Entry<String, JTextField> fieldPattern : function.getValue().entrySet())
                    {
                        fieldPattern.getValue().setBackground(Color.WHITE);
                    }
                }

                for (Map.Entry<String, JTextField> fieldFunction : this.fieldsElseCaseFunctions.entrySet())
                {
                    fieldFunction.getValue().setBackground(Color.WHITE);
                }

                if (this.selectionSecondary < 0 &&
                    this.selectionSecondaryPattern == null &&
                    this.selectionSecondaryElseCase == false)
                {
                    JTeroEvent teroEvent = null;

                    try
                    {
                        if (this.interpreter.hasNext() != true)
                        {
                            return;
                        }

                        teroEvent = this.interpreter.nextEvent();
                    }
                    catch (JTeroException ex)
                    {
                        throw constructTermination("messageErrorWhileParsing", ex, null);
                    }

                    if (this.statusTextField != null)
                    {
                        this.statusTextField.setText(teroEvent.getCurrentFunctionName() + " -" + teroEvent.getCurrentFunctionRepeatPatternName() + "|" + teroEvent.getCurrentFunctionRepeatElseCase() + "/" + teroEvent.getNextFunctionPatternName() + "|" + teroEvent.getNextFunctionElseCase() + "-> " + teroEvent.getNextFunctionName() + "," +  teroEvent.getCurrentCount() + "," + teroEvent.getNextCount() + ": \"" + teroEvent.getData() + "\"");
                    }

                    this.currentFunction = teroEvent.getCurrentFunctionName();

                    if (teroEvent.getCurrentCount() > 0 &&
                        teroEvent.getNextCount() > 0)
                    {
                        this.textArea.requestFocus();
                        this.textArea.select(this.selectionStart, this.selectionStart + teroEvent.getCurrentCount());
                        this.selectionStart += teroEvent.getCurrentCount();

                        this.selectionSecondary = teroEvent.getNextCount();
                    }
                    else
                    {
                        if (teroEvent.getCurrentCount() > 0)
                        {
                            this.textArea.requestFocus();
                            this.textArea.select(this.selectionStart, this.selectionStart + teroEvent.getCurrentCount());
                            this.selectionStart += teroEvent.getCurrentCount();
                        }
                        else if (teroEvent.getNextCount() > 0)
                        {
                            this.textArea.requestFocus();
                            this.textArea.select(this.selectionStart, this.selectionStart + teroEvent.getNextCount());
                            this.selectionStart += teroEvent.getNextCount();
                        }
                    }

                    this.fieldsFunctions.get(teroEvent.getCurrentFunctionName()).setBackground(Color.RED);

                    if ((teroEvent.getCurrentFunctionRepeatPatternName() != null ||
                         teroEvent.getCurrentFunctionRepeatElseCase() != false) &&
                        (teroEvent.getNextFunctionPatternName() != null ||
                         teroEvent.getNextFunctionElseCase() != false))
                    {
                        if (teroEvent.getCurrentFunctionRepeatPatternName() != null)
                        {
                            this.fieldsThenCaseFunctions.get(teroEvent.getCurrentFunctionName()).get(teroEvent.getCurrentFunctionRepeatPatternName()).setBackground(Color.ORANGE);
                        }
                        else if (teroEvent.getCurrentFunctionRepeatElseCase() != false)
                        {
                            this.fieldsElseCaseFunctions.get(teroEvent.getCurrentFunctionName()).setBackground(Color.ORANGE);
                        }

                        this.selectionSecondaryPattern = teroEvent.getNextFunctionPatternName();
                        this.selectionSecondaryElseCase = teroEvent.getNextFunctionElseCase();
                    }
                    else
                    {
                        if (teroEvent.getCurrentFunctionRepeatPatternName() != null ||
                            teroEvent.getCurrentFunctionRepeatElseCase() != false)
                        {
                            if (teroEvent.getCurrentFunctionRepeatPatternName() != null)
                            {
                                this.fieldsThenCaseFunctions.get(teroEvent.getCurrentFunctionName()).get(teroEvent.getCurrentFunctionRepeatPatternName()).setBackground(Color.ORANGE);
                            }
                            else if (teroEvent.getCurrentFunctionRepeatElseCase() != false)
                            {
                                this.fieldsElseCaseFunctions.get(teroEvent.getCurrentFunctionName()).setBackground(Color.ORANGE);
                            }
                        }
                        else if (teroEvent.getNextFunctionPatternName() != null ||
                                  teroEvent.getNextFunctionElseCase() != false)
                        {
                            if (teroEvent.getNextFunctionPatternName() != null)
                            {
                                this.fieldsThenCaseFunctions.get(teroEvent.getCurrentFunctionName()).get(teroEvent.getNextFunctionPatternName()).setBackground(Color.ORANGE);
                            }
                            else if (teroEvent.getNextFunctionElseCase() != false)
                            {
                                this.fieldsElseCaseFunctions.get(teroEvent.getCurrentFunctionName()).setBackground(Color.ORANGE);
                            }
                        }
                    }
                }
                else
                {
                    if (this.selectionSecondary > 0)
                    {
                        this.textArea.requestFocus();
                        this.textArea.select(this.selectionStart, this.selectionStart + this.selectionSecondary);
                        this.selectionStart += this.selectionSecondary;
                    }

                    this.fieldsFunctions.get(this.currentFunction).setBackground(Color.RED);

                    if (this.selectionSecondaryPattern != null)
                    {
                        this.fieldsThenCaseFunctions.get(this.currentFunction).get(this.selectionSecondaryPattern).setBackground(Color.ORANGE);
                    }
                    else if (this.selectionSecondaryElseCase == true)
                    {
                        this.fieldsElseCaseFunctions.get(this.currentFunction).setBackground(Color.ORANGE);
                    }

                    this.selectionSecondary = -1;
                    this.selectionSecondaryPattern = null;
                    this.selectionSecondaryElseCase = false;
                }
            }
            else
            {
                throw new UnsupportedOperationException();
            }
        }
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "interactive_tero_1: " + getI10nString(id);
            }
            else
            {
                message = "interactive_tero_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "interactive_tero_1: " + getI10nString(id);
            }
            else
            {
                message = "interactive_tero_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (interactive_tero_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(interactive_tero_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by interactive_tero_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<interactive-tero-1-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</interactive-tero-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        interactive_tero_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10n == null)
        {
            this.l10n = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10n.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10n.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected JTextArea textArea = null;
    protected JTextField statusTextField = null;

    protected Map<String, JTextField> fieldsFunctions = new HashMap<String, JTextField>();
    protected Map<String, Map<String, JTextField>> fieldsThenCaseFunctions = new HashMap<String, Map<String, JTextField>>();
    protected Map<String, JTextField> fieldsElseCaseFunctions = new HashMap<String, JTextField>();

    protected int selectionStart = 0;
    protected int selectionSecondary = -1;
    protected String selectionSecondaryPattern = null;
    protected boolean selectionSecondaryElseCase = false;
    protected String currentFunction = null;

    protected File inputFile = null;
    protected Map<String, JTeroPattern> patterns = null;
    protected Map<String, JTeroFunction> functions = null;
    protected String startFunctionName = null;
    protected JTeroStAInterpreter interpreter = null;

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nInteractiveTero1";
    private ResourceBundle l10n = null;
}
