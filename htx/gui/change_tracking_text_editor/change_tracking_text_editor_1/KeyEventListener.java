/* Copyright (C) 2017-2023 Stephan Kreutzer
 *
 * This file is part of change_tracking_text_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_tracking_text_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_tracking_text_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_tracking_text_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/KeyEventListener.java
 * @author Stephan Kreutzer
 * @since 2023-08-30
 */


import javax.swing.*;
import java.awt.event.*;
import java.awt.*;


class KeyEventListener implements KeyListener
{
    public KeyEventListener(ContextMenu contextMenu,
                            JTextArea textArea)
    {
        if (contextMenu == null ||
            textArea == null)
        {
            throw new NullPointerException();
        }

        this.contextMenu = contextMenu;
        this.textArea = textArea;
    }

    public void keyPressed(KeyEvent event)
    {

    }

    public void keyTyped(KeyEvent event)
    {

    }

    public void keyReleased(KeyEvent event)
    {
        int result = -1;

        if (event.getKeyCode() == KeyEvent.VK_CONTEXT_MENU)
        {
            Point caretPoint = textArea.getCaret().getMagicCaretPosition();

            result = this.contextMenu.handle(event.getSource(),
                                             true,
                                             event.getComponent(),
                                             caretPoint.x,
                                             caretPoint.y);
        }
        else if (event.getKeyCode() == KeyEvent.VK_ENTER)
        {
            result = this.contextMenu.handle(event.getSource(), false, null, 0, 0);
        }
        else if (event.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            result = this.contextMenu.abort();
        }

        if (result == ContextMenu.RETURN_HANDLE_EVENTCONSUME)
        {
            event.consume();
        }
    }

    protected ContextMenu contextMenu = null;
    protected JTextArea textArea = null;
}
