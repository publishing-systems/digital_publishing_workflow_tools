/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of change_tracking_text_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_tracking_text_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_tracking_text_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_tracking_text_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/ContextMenu.java
 * @author Stephan Kreutzer
 * @since 2023-08-30
 */


import javax.swing.*;
import java.util.Map;
import java.util.HashMap;
import java.awt.*;


class ContextMenu
{
    public ContextMenu(change_tracking_text_editor_1 parent,
                       JTextArea textArea,
                       JPopupMenu popupMenu,
                       Map<JMenuItem, String> insertCharacterOptionsCharacter)
    {
        if (parent == null ||
            textArea == null ||
            popupMenu == null ||
            insertCharacterOptionsCharacter == null)
        {
            throw new NullPointerException();
        }

        this.parent = parent;
        this.textArea = textArea;
        this.popupMenu = popupMenu;
        this.insertCharacterOptionsCharacter = insertCharacterOptionsCharacter;
    }

    public int handle(Object source,
                      boolean isRightClick,
                      Component component,
                      int x,
                      int y)
    {
        for (Map.Entry<JMenuItem, String> selection : this.insertCharacterOptionsCharacter.entrySet())
        {
            if (source == selection.getKey())
            {
                this.isContextMenu = false;

                // Doesn't happen automatically if the MenuKeyEventListener receives the VK_ENTER.
                this.popupMenu.setVisible(false);

                this.parent.resetCaretPosition(this.caretPositionCurrent);
                this.parent.insertCharacter(this.caretPositionCurrent, selection.getValue());
                this.caretPositionCurrent = -1;

                return RETURN_HANDLE_EVENTCONSUME;
            }
        }

        if (source == this.textArea)
        {
            if (isRightClick == true)
            {
                if (component != null)
                {
                    if (this.isContextMenu == false)
                    {
                        this.caretPositionCurrent = this.textArea.getCaretPosition();
                        System.out.println(this.caretPositionCurrent);
                        this.isContextMenu = true;
                    }

                    this.popupMenu.show(component, x, y);
                }
            }
            else
            {
                if (this.isContextMenu == true)
                {
                    this.isContextMenu = false;
                    this.parent.resetCaretPosition(this.caretPositionCurrent);
                    this.caretPositionCurrent = -1;

                    return RETURN_HANDLE_EVENTCONSUME;
                }
            }
        }
        else
        {
            if (this.isContextMenu == true)
            {
                this.isContextMenu = false;
                this.parent.resetCaretPosition(this.caretPositionCurrent);
                this.caretPositionCurrent = -1;

                return RETURN_HANDLE_EVENTCONSUME;
            }
        }

        return 0;
    }

    public int abort()
    {
        if (this.isContextMenu == true)
        {
            this.isContextMenu = false;

            // Just to be safe.
            this.popupMenu.setVisible(false);

            this.parent.resetCaretPosition(this.caretPositionCurrent);
            this.caretPositionCurrent = -1;
        }

        return 0;
    }

    public static final int RETURN_HANDLE_EVENTCONSUME = 1;

    protected change_tracking_text_editor_1 parent = null;
    protected JTextArea textArea = null;
    // <context-menu-item, character>
    Map<JMenuItem, String> insertCharacterOptionsCharacter = null;

    protected boolean isContextMenu = false;
    protected int caretPositionCurrent = -1;

    protected JPopupMenu popupMenu = null;
}
