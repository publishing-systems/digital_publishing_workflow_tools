<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2022 Stephan Kreutzer

This file is part of the digital_publishing_workflow_tools package.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with this program. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:htx-src="htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment> This file was created by change_instructions_navigator_2.xsl, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). </xsl:comment>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2022 Stephan Kreutzer

This program is part of the digital_publishing_workflow_tools package.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="text-history"/&gt; is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#x0A;</xsl:text>
        <title>change_instructions_navigator_2</title>

        <style type="text/css">
          #text
          {
              font-family: monospace;
          }

          .original-plaintext
          {
              display: none;
          }

          .whitespace
          {
              color: #808080;
          }

          .instruction-add
          {
              background-color: #00FF00;
          }

          .instruction-delete
          {
              background-color: #FF0000;
          }
        </style>

        <script type="text/javascript">

"use strict";

// As browsers/W3C/WHATWG are incredibly evil, they might ignore the self-declarative
// XML namespace of this document and the given content type in the header, and instead
// assume/render "text/html", which then fails with JavaScript characters that are
// XML/XHTML special characters which need to be escaped, if the file is saved under
// a name that happens to end with ".html" (!!!). Just have the file name end with
// ".xhtml" and it might magically start to work.

let currentInstruction = 0;
let history = null;
let isAuto = false;
let skipRendering = false;

function instructionInterval()
{
    if (isAuto == false)
    {
        return false;
    }

    if (executeNextInstruction() == true)
    {
        window.setTimeout(instructionInterval, 200);
    }
    else
    {
        window.setTimeout(function() { resetInstructions(); instructionInterval(); }, 2000);
    }
}

function resetInstructions()
{
    let nodeText = document.getElementById("text");

    if (nodeText != null)
    {
        while (nodeText.hasChildNodes() == true)
        {
            nodeText.removeChild(nodeText.lastChild);
        }
    }
    else
    {
        console.log("No \"text\" node found.");
    }

    currentInstruction = 0;
}

document.addEventListener("DOMContentLoaded", function() {
    //instructionInterval();
    //while (executeNextInstruction() == true);
});

function executeNextInstruction()
{
    if (history == null)
    {
        let element = document.getElementsByTagNameNS("htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z", "change-tracking-text-editor-1-text-history");

        if (element == null)
        {
            console.log("No history found.");
            currentInstruction = 0;
            return false;
        }

        if (element.length &lt;= 0)
        {
            console.log("No history found.");
            currentInstruction = 0;
            return false;
        }

        history = element[0].children;

        if (history.length &lt;= 0)
        {
            return false;
        }
    }

    if (currentInstruction &lt; 0)
    {
        currentInstruction = 0;
    }
    else if (currentInstruction &gt; history.length - 1)
    {
        return false;
    }

    let nodeText = document.getElementById("text");

    if (nodeText == null)
    {
        console.log("No \"text\" node found.");
        currentInstruction = 0;
        return false;
    }

    if (clearChange() != true)
    {
        currentInstruction = 0;
        return false;
    }


    let instruction = history[currentInstruction];

    if (instruction.nodeName === "htx:add")
    {
        if (instruction.hasAttribute("position") != true)
        {
            console.log("The \"position\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        let position = parseInt(instruction.getAttribute("position"));
        let length = getTextLength(nodeText);

        if (position &lt; 0)
        {
            console.log("Negative value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (position &gt; length)
        {
            console.log("Value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            currentInstruction = 0;
            return false;
        }

        let result = insertText(0, position, instruction.textContent, nodeText);

        if (result != -1)
        {
            if (result &gt;= 0)
            {
                console.log("Value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            }

            currentInstruction = 0;
            return false;
        }

        if (renderChange(instruction.nodeName, position, position + instruction.textContent.length) !== true)
        {
            return false;
        }
    }
    else if (instruction.nodeName == "htx:delete")
    {
        if (instruction.hasAttribute("position") != true)
        {
            console.log("The \"position\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (instruction.hasAttribute("count") != true)
        {
            console.log("The \"count\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        let position = parseInt(instruction.getAttribute("position"));
        let count = parseInt(instruction.getAttribute("count"));
        let length = getTextLength(nodeText);

        if (position &lt; 0)
        {
            console.log("Negative value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (count &gt;= 0)
        {
            console.log("Positive value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if ((position - (count * -1)) &lt; 0)
        {
            console.log("Value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element would delete more characters than there are present in the remaining text.");
            currentInstruction = 0;
            return false;
        }

        if (position &gt; length)
        {
            console.log("Value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            currentInstruction = 0;
            return false;
        }

        let result = deleteText(position - (count * -1), count * -1, nodeText);

        if (result.result != 0)
        {
            currentInstruction = 0;
            return false;
        }

        if (instruction.hasChildNodes() != true)
        {
            instruction.appendChild(document.createTextNode(result.textDeleted));
        }

        if (renderChange(instruction.nodeName, position - (count * -1), position - (count * -1), result.textDeleted) !== true)
        {
            return false;
        }
    }

    currentInstruction += 1;

    return true;
}

function executePreviousInstruction()
{
    if (history == null)
    {
        let element = document.getElementsByTagNameNS("htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z", "change-tracking-text-editor-1-text-history");

        if (element == null)
        {
            console.log("No history found.");
            currentInstruction = 0;
            return false;
        }

        if (element.length &lt;= 0)
        {
            console.log("No history found.");
            currentInstruction = 0;
            return false;
        }

        history = element[0].children;

        if (history.length &lt;= 0)
        {
            return false;
        }
    }

    if (currentInstruction &lt;= 0)
    {
        return false;
    }
    else if (currentInstruction &gt; history.length - 1)
    {
        currentInstruction = history.length - 1;
    }
    else
    {
        currentInstruction -= 1;
    }

    let nodeText = document.getElementById("text");

    if (nodeText == null)
    {
        console.log("No \"text\" node found.");
        currentInstruction = 0;
        return false;
    }

    if (clearChange() != true)
    {
        currentInstruction = 0;
        return false;
    }


    let instruction = history[currentInstruction];

    if (instruction.nodeName === "htx:add")
    {
        if (instruction.hasAttribute("position") != true)
        {
            console.log("The \"position\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        let position = parseInt(instruction.getAttribute("position"));
        let length = getTextLength(nodeText);
        let count = instruction.textContent.length;

        if (position &lt; 0)
        {
            console.log("Negative value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if ((position + count) &gt; length)
        {
            console.log("The position " + (position + count) + " resulting from value " + position + " for the \"position\" attribute and value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            currentInstruction = 0;
            return false;
        }

        let result = deleteText(position, instruction.textContent.length, nodeText);

        if (result.result != 0)
        {
            currentInstruction = 0;
            return false;
        }

        if (renderChange("htx:delete", position, position, instruction.textContent) !== true)
        {
            return false;
        }
    }
    else if (instruction.nodeName == "htx:delete")
    {
        if (instruction.hasAttribute("position") != true)
        {
            console.log("The \"position\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (instruction.hasAttribute("count") != true)
        {
            console.log("The \"count\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (instruction.hasChildNodes() != true)
        {
            console.log("To navigate backwards, a \"delete\" instruction must have been read in forward direction earlier to reconstruct the destructive operation.");
            currentInstruction = 0;
            return false;
        }

        let deletedText = instruction.firstChild.textContent;
        let position = parseInt(instruction.getAttribute("position"));
        let count = parseInt(instruction.getAttribute("count"));
        let length = getTextLength(nodeText);

        if (position &lt; 0)
        {
            console.log("Negative value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (count &gt;= 0)
        {
            console.log("Positive value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if ((position - (count * -1)) &lt; 0)
        {
            console.log("Value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element would delete more characters than there are present in the remaining text.");
            currentInstruction = 0;
            return false;
        }

        if ((position - (count * -1)) &gt; length)
        {
            console.log("The position " + (position - (count * -1)) + " resulting from value " + position + " for the \"position\" attribute and value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            currentInstruction = 0;
            return false;
        }

        if (count != (deletedText.length * -1))
        {
            console.log("Difference between \"count\" and reconstructed text of a \"" + instruction.nodeName + "\" instruction.");
            currentInstruction = 0;
            return false;
        }

        let result = insertText(0, position - deletedText.length, deletedText, nodeText);

        if (result != -1)
        {
            if (result &gt;= 0)
            {
                console.log("Value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            }

            currentInstruction = 0;
            return false;
        }

        if (renderChange("htx:add", position - deletedText.length, position) !== true)
        {
            return false;
        }
    }

    return true;
}

/**
 * @details Operations on pure text are much more faster than dealing
 *     with lots of nodes.
 */
function executeToLastInstruction()
{
    let target = document.getElementById("text");

    if (target == null)
    {
        console.log("No \"text\" node found.");
        return false;
    }

    // Fast way to get to pure text, by throwing away everything and
    // recreating the text from start (first instruction), and then on each
    // instruction operation skipping the rendering of the change operation +
    // whitespace display compounds.
    target.innerHTML = "";
    currentInstruction = 0;

    skipRendering = true;
    while (executeNextInstruction() == true);
    executePreviousInstruction();
    skipRendering = false;

    renderAllText();
    // Enacts rendering of the last instruction where previous rendering was skipped.
    executeNextInstruction(); 
}

/**
 * @retval -1 Done, complete.
 * @retval &gt; 0 How many characters done. Caller can calculate how many more left to do.
 * @retval 0 Why?
 * @retval -2 Error.
 */
function insertText(positionCurrent, position, text, target)
{
    let positionInitial = positionCurrent;

    for (let i = 0, max = target.childNodes.length; i &lt; max; i++)
    {
        let node = target.childNodes[i];

        if (node.nodeType === Node.TEXT_NODE)
        {
            if ((positionCurrent + node.length) &lt; position)
            {
                positionCurrent += node.length;
                continue;
            }
            else if ((positionCurrent + node.length) == position)
            {
                let parent = node.parentNode;
                let after = node.nextSibling;

                while (parent.classList.contains("original-plaintext") == true ||
                       parent.classList.contains("compound") == true)
                {
                    after = parent.nextSibling
                    parent = parent.parentNode;
                }

                parent.insertBefore(renderText(text), after);
                return -1;
            }
            else
            {
                let before = node.textContent.slice(0, position - positionCurrent);
                let after = node.textContent.slice(position - positionCurrent);

                before = renderText(before);
                let center = renderText(text);
                after = renderText(after);

                node.parentNode.insertBefore(before, node);
                node.parentNode.insertBefore(center, node);
                node.parentNode.insertBefore(after, node);

                node.parentNode.removeChild(node);

                return -1;
            }
        }
        else if (node.nodeType === Node.ELEMENT_NODE)
        {
            if (node.classList.contains("rendering") == true)
            {
                continue;
            }

            let result = insertText(positionCurrent, position, text, node);

            if (result &lt; 0)
            {
                return result;
            }
            else
            {
                positionCurrent += result;
            }
        }
    }

    if (position == positionCurrent)
    {
        target.appendChild(renderText(text));
        return -1;
    }
    else if (position &gt; positionCurrent)
    {
        return positionCurrent - positionInitial;
    }
    else
    {
        // Not handled before with -1 for success?
        return -2;
    }
}

/**
 * @retval 0 Done, complete.
 * @retval -1 Error.
 */
function deleteText(position, length, target)
{
    let positionCurrent = 0;
    let max = target.childNodes.length;
    let textDeleted = "";

    for (let i = 0; i &lt; max; i++)
    {
        let node = target.childNodes[i];

        if (node.nodeType === Node.TEXT_NODE)
        {
            if ((positionCurrent + node.length) &lt;= position)
            {
                positionCurrent += node.length;
                continue;
            }

            let result = deleteTextNode(positionCurrent, position, length, node, target);

            textDeleted += result;

            if (result.length == length)
            {
                return { "result": 0, "textDeleted": textDeleted };
            }
            else
            {
                if (result.length &lt; length)
                {
                    length -= result.length;

                    if (positionCurrent &lt; position)
                    {
                        positionCurrent = position;
                    }

                    if (result.length == node.length)
                    {
                        i -= 1;
                        max -= 1;
                    }
                }
                else
                {
                    return { "result": -1, "textDeleted": null };
                }
            }
        }
        else if (node.nodeType === Node.ELEMENT_NODE)
        {
            if (node.classList.contains("compound") == true)
            {
                let deleteNode = false;
                let found = false;

                for (let j = 0, max2 = node.childNodes.length; j &lt; max2; j++)
                {
                    let subNode = node.childNodes[j];

                    if (subNode.classList.contains("original-plaintext") != true)
                    {
                        continue;
                    }

                    if ((positionCurrent + subNode.textContent.length) &lt;= position)
                    {
                        positionCurrent += subNode.textContent.length;
                        found = true;
                        break;
                    }
                    else
                    {
                        let result = deleteTextNode(positionCurrent, position, length, subNode.childNodes[0], subNode);

                        textDeleted += result;

                        if (result.length == length)
                        {
                            target.removeChild(node);
                            return { "result": 0, "textDeleted": textDeleted };
                        }
                        else
                        {
                            length -= result.length;

                            if (positionCurrent &lt; position)
                            {
                                positionCurrent = position;
                            }

                            deleteNode = true;
                            found = true;
                            break;
                        }
                    }

                    // All paths above break inner loop.
                    // Should only be one "original-plaintext".
                    break;
                }

                if (found != true)
                {
                    return { "result": -1, "textDeleted": null };
                }

                if (deleteNode == true)
                {
                    target.removeChild(node);

                    i -= 1;
                    max -= 1;
                }
            }
            else
            {
                return { "result": -1, "textDeleted": null };
            }
        }
    }

    return { "result": 0, "textDeleted": textDeleted };
}

function deleteTextNode(positionCurrent, position, length, node, target)
{
    let textDeleted = node.textContent.slice(position - positionCurrent, (position - positionCurrent) + length);

    if (position &gt; positionCurrent)
    {
        let before = node.textContent.slice(0, position - positionCurrent);

        target.insertBefore(document.createTextNode(before), node);
    }

    if (node.length &gt; length)
    {
        let after = node.textContent.slice((position - positionCurrent) + length);

        target.insertBefore(document.createTextNode(after), node);
    }

    target.removeChild(node);

    return textDeleted;
}

function clearChange()
{
    if (skipRendering == true)
    {
        return true;
    }

    let target = document.getElementById("text");

    if (target == null)
    {
        console.log("No \"text\" node found.");
        return false;
    }

    for (let i = 0, max = target.childNodes.length; i &lt; max; i++)
    {
        let node = target.childNodes[i];

        if (node.nodeType === Node.ELEMENT_NODE)
        {
            if (node.classList.contains("instruction-delete") == true)
            {
                node.parentNode.removeChild(node);

                i -= 1;
                max -= 1;
            }
            else if (node.classList.contains("instruction-add") == true)
            {
                while (node.hasChildNodes() == true)
                {
                    let childNode = node.removeChild(node.firstChild);

                    node.parentNode.insertBefore(childNode, node);

                    i += 1;
                    max += 1;
                }

                node.parentNode.removeChild(node);

                i -= 1;
                max -= 1;
            }
            else if (node.classList.contains("compound") == true)
            {
                for (let j = 0, max2 = node.childNodes.length; j &lt; max2; j++)
                {
                    if (node.childNodes[j].classList.contains("instruction-add") == true)
                    {
                        node = node.childNodes[j];

                        while (node.hasChildNodes() == true)
                        {
                            let childNode = node.removeChild(node.firstChild);

                             node.parentNode.insertBefore(childNode, node);
                        }

                        node.parentNode.removeChild(node);

                        break;
                    }
                }
            }
        }
    }

    return true;
}

/**
 * @details This primarily works based on the assumption/condition
 *     that the prior text editing operations created fitting
 *     text nodes even if the characters involved (sliced/split)
 *     were consecutive, so renderChange() does not need to
 *     consider/repeat any text operations.
 */
function renderChange(type, start, end, textDeleted = "")
{
    if (skipRendering == true)
    {
        return true;
    }

    if (type !== "htx:add" &amp;&amp;
        type !== "htx:delete")
    {
        console.log("Type \"" + type + "\" not supported.");
        return false;
    }

    if (start &gt; end)
    {
        console.log("Start position &gt; end position.");
        return false;
    }

    let nodeTarget = document.getElementById("text");

    if (nodeTarget == null)
    {
        console.log("No \"text\" node found.");
        currentInstruction = 0;
        return false;
    }

    if (start &lt; 0)
    {
        console.log("Start position " + start + " is negative.");
        return false;
    }

    if (end &lt; 0)
    {
        console.log("End position " + end + " is negative.");
        return false;
    }


    let nodeSource = new Array();

    for (let i = 0, max = nodeTarget.childNodes.length; i &lt; max; i++)
    {
        nodeSource.push(nodeTarget.childNodes[i]);
    }

    let collector = null;
    let positionCurrent = 0;

    for (let i = 0, max = nodeSource.length; i &lt; max; i++)
    {
        let node = nodeSource[i];

        if (node.nodeType === Node.TEXT_NODE)
        {
            positionCurrent += node.length;

            if (positionCurrent &lt;= start)
            {
                continue;
            }

            if (collector == null)
            {
                collector = document.createDocumentFragment();

                let span = document.createElement("span");

                if (type === "htx:add")
                {
                    span.classList.add("instruction-add");
                }
                else if (type === "htx:delete")
                {
                    span.classList.add("instruction-delete");
                    span.appendChild(renderText(textDeleted));
                }
                else
                {
                    console.log("Type \"" + type + "\" not supported.");
                    return false;
                }

                collector.appendChild(span);

                node.parentNode.insertBefore(collector, node);

                collector = span;
            }

            if (positionCurrent == end)
            {
                collector.appendChild(node);
                break;
            }
            else if (positionCurrent &lt; end)
            {
                collector.appendChild(node);
            }
            else
            {
                break;
            }
        }
        else if (node.nodeType === Node.ELEMENT_NODE)
        {
            if (node.classList.contains("compound") != true)
            {
                // Which?
                continue;
            }

            let nodePlaintext = null;
            let nodeRendering = null;

            for (let j = 0, max2 = node.childNodes.length; j &lt; max2; j++)
            {
                if (node.childNodes[j].classList.contains("original-plaintext") == true)
                {
                    nodePlaintext = node.childNodes[j];
                }

                if (node.childNodes[j].classList.contains("rendering") == true)
                {
                    nodeRendering = node.childNodes[j];
                }

                if (nodePlaintext != null &amp;&amp;
                    nodeRendering != null)
                {
                    break;
                }
            }

            if (nodePlaintext == null)
            {
                // How?
                return false;
            }

            if (nodeRendering == null)
            {
                // How?
                return false;
            }

            positionCurrent += nodePlaintext.textContent.length;

            if (positionCurrent &lt;= start)
            {
                continue;
            }

            if (collector == null)
            {
                collector = document.createDocumentFragment();

                let span = document.createElement("span");

                if (type === "htx:add")
                {
                    span.classList.add("instruction-add");
                }
                else if (type === "htx:delete")
                {
                    span.classList.add("instruction-delete");
                    span.appendChild(renderText(textDeleted));
                }
                else
                {
                    console.log("Type \"" + type + "\" not supported.");
                    return false;
                }

                collector.appendChild(span);

                node.parentNode.insertBefore(collector, node);

                collector = span;
            }

            if (positionCurrent == end)
            {
                collector.appendChild(node);
                break;
            }
            else if (positionCurrent &lt; end)
            {
                collector.appendChild(node);
            }
            else
            {
                break;
            }
        }

        if (positionCurrent &gt;= end)
        {
            break;
        }
    }

    if (positionCurrent &lt; end)
    {
        // How?
    }

    if (type === "htx:delete" &amp;&amp;
        positionCurrent == start)
    {
        // Characters deleted at the very end of the loop above are not in
        // the source (nodeText.textContent) any more (already removed), so
        // the loop above didn't get a chance to arrive at the position and
        // to add the deletion instruction rendering yet (before loop condition
        // exceeded).

        let span = document.createElement("span");
        span.classList.add("instruction-delete");
        span.appendChild(renderText(textDeleted));

        nodeTarget.insertBefore(span, null);
        return true;
    }

    return true;
}

function renderText(text)
{
    let fragment = document.createDocumentFragment();

    if (skipRendering == true)
    {
        fragment.appendChild(document.createTextNode(text));
        return fragment;
    }

    let string = "";

    for (let i = 0, max = text.length; i &lt; max; i++)
    {
        let character = text.charAt(i);

        switch (character)
        {
        case ' ':
            {
                if (string.length &gt; 0)
                {
                    fragment.appendChild(document.createTextNode(string));
                    string = "";
                }

                let container = document.createElement("span");
                container.classList.add("compound");

                let spanPlaintext = document.createElement("span");
                spanPlaintext.classList.add("original-plaintext");
                spanPlaintext.appendChild(document.createTextNode(character));
                container.appendChild(spanPlaintext);

                let spanRendering = document.createElement("span");
                spanRendering.classList.add("rendering");
                spanRendering.classList.add("whitespace");
                spanRendering.appendChild(document.createTextNode("·"));
                container.appendChild(spanRendering);

                container.appendChild(document.createElement("wbr"));

                fragment.appendChild(container);
            }
            break;

        case '\n':
            {
                if (string.length &gt; 0)
                {
                    fragment.appendChild(document.createTextNode(string));
                    string = "";
                }

                let container = document.createElement("span");
                container.classList.add("compound");

                let spanPlaintext = document.createElement("span");
                spanPlaintext.classList.add("original-plaintext");
                spanPlaintext.appendChild(document.createTextNode(character));
                container.appendChild(spanPlaintext);

                let spanRendering = document.createElement("span");
                spanRendering.classList.add("rendering");
                spanRendering.classList.add("whitespace");
                spanRendering.appendChild(document.createTextNode("↳"));
                container.appendChild(spanRendering);

                container.appendChild(document.createElement("br"));

                fragment.appendChild(container);
            }
            break;

        case '\r':
            {
                if (string.length &gt; 0)
                {
                    fragment.appendChild(document.createTextNode(string));
                    string = "";
                }

                let container = document.createElement("span");
                container.classList.add("compound");

                let spanPlaintext = document.createElement("span");
                spanPlaintext.classList.add("original-plaintext");
                spanPlaintext.appendChild(document.createTextNode(character));
                container.appendChild(spanPlaintext);

                let spanRendering = document.createElement("span");
                spanRendering.classList.add("rendering");
                spanRendering.classList.add("whitespace");
                spanRendering.appendChild(document.createTextNode("?"));
                container.appendChild(spanRendering);

                container.appendChild(document.createElement("wbr"));

                fragment.appendChild(container);
            }
            break;

        case '\t':
            {
                if (string.length &gt; 0)
                {
                    fragment.appendChild(document.createTextNode(string));
                    string = "";
                }

                let container = document.createElement("span");
                container.classList.add("compound");

                let spanPlaintext = document.createElement("span");
                spanPlaintext.classList.add("original-plaintext");
                spanPlaintext.appendChild(document.createTextNode(character));
                container.appendChild(spanPlaintext);

                let spanRendering = document.createElement("span");
                spanRendering.classList.add("rendering");
                spanRendering.classList.add("whitespace");
                spanRendering.appendChild(document.createTextNode("⇆⇆⇆⇆"));
                container.appendChild(spanRendering);

                container.appendChild(document.createElement("wbr"));

                fragment.appendChild(container);
            }
            break;

        default:
            string += character;
            break;

        }
    }

    if (string.length &gt; 0)
    {
        fragment.appendChild(document.createTextNode(string));
    }

    return fragment;
}

/**
 * @details Exists solely for performance optimization to get faster to the
 *     last instruction without rendering the text/compounds/whitespace
 *     at each step in between, because pure text operations are faster,
 *     node operations are slower. This function allows to re-render the
 *     entire text, given/assuming ideally there's no compounds nor renderings
 *     in the "text" target element.
 */
function renderAllText()
{
    let target = document.getElementById("text");

    if (target == null)
    {
        console.log("No \"text\" node found.");
        return false;
    }

    let nodeSource = new Array();

    for (let i = 0, max = target.childNodes.length; i &lt; max; i++)
    {
        nodeSource.push(target.childNodes[i]);
    }

    for (let i = 0, max = nodeSource.length; i &lt; max; i++)
    {
        let node = nodeSource[i];

        if (node.nodeType === Node.TEXT_NODE)
        {
            node.parentNode.replaceChild(renderText(node.textContent), node);
        }
        else if (node.nodeType === Node.ELEMENT_NODE)
        {
            if (node.classList.contains("compound") == true)
            {
                continue;
            }
        }
    }

    return true;
}

function getTextLength(node)
{
    if (node.nodeType === Node.TEXT_NODE)
    {
        return node.length;
    }
    else if (node.nodeType === Node.ELEMENT_NODE)
    {
        if (node.classList.contains("rendering") == true)
        {
            return 0;
        }

        let length = 0;

        for (let i = 0, max = node.childNodes.length; i &lt; max; i++)
        {
            length += getTextLength(node.childNodes[i]);
        }

        return length;
    }
}
        </script>
      </head>
      <body>
        <div>
          <a href="#" onclick="executeNextInstruction();">next</a>
          <!-- Because whitespace isn't preserved, it needs to be outputted explicitly. -->
          <xsl:text> </xsl:text>
          <a href="#" onclick="executePreviousInstruction();">prev</a>
          <xsl:text> </xsl:text>
          <a href="#" onclick="resetInstructions();">first</a>
          <xsl:text> </xsl:text>
          <a href="#" onclick="executeToLastInstruction();">last</a>
          <xsl:text> </xsl:text>
          <a href="#" onclick="isAuto = !isAuto; instructionInterval();">auto</a>
          <xsl:text> </xsl:text>
          <a href="https://hypertext-systems.org/downloads.php#change_tracking_text_editor_1">tool</a>
        </div>
        <div id="text"/>
        <div id="text-history" style="display:none;">
          <xsl:comment> The data contained in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <htx:change-tracking-text-editor-1-text-history xmlns:htx="htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z">
            <xsl:apply-templates/>
          </htx:change-tracking-text-editor-1-text-history>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//htx-src:add | //htx-src:delete">
    <xsl:element name="htx:{local-name()}">
      <xsl:copy-of select="@*|text()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
