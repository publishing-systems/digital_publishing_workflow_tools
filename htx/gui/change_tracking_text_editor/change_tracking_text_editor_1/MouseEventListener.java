/* Copyright (C) 2017-2023 Stephan Kreutzer
 *
 * This file is part of change_tracking_text_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_tracking_text_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_tracking_text_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_tracking_text_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/MouseEventListener.java
 * @author Stephan Kreutzer
 * @since 2023-08-28
 */


import java.awt.event.*;
import javax.swing.*;


class MouseEventListener extends MouseAdapter
{
    public MouseEventListener(ContextMenu contextMenu)
    {
        if (contextMenu == null)
        {
            throw new NullPointerException();
        }

        this.contextMenu = contextMenu;
    }

    public void mousePressed(MouseEvent event)
    {

    }

    public void mouseReleased(MouseEvent event)
    {
        int result = this.contextMenu.handle(event.getSource(),
                                             SwingUtilities.isRightMouseButton(event),
                                             event.getComponent(),
                                             event.getX(),
                                             event.getY());

        if (result == ContextMenu.RETURN_HANDLE_EVENTCONSUME)
        {
            event.consume();
        }
    }

    protected ContextMenu contextMenu = null;
}
