<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2022 Stephan Kreutzer

This file is part of the digital_publishing_workflow_tools package.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with this program. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:htx-src="htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment> This file was created by change_instructions_navigator_1.xsl, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). </xsl:comment>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2022 Stephan Kreutzer

This program is part of the digital_publishing_workflow_tools package.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="text-history"/&gt; is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#x0A;</xsl:text>
        <title>change_instructions_navigator_1</title>

        <style type="text/css">
          .whitespace
          {
              color: #808080;
          }

          #rendering
          {
              font-family: monospace;
          }

          .instruction-add
          {
              background-color: #00FF00;
          }

          .instruction-delete
          {
              background-color: #FF0000;
          }
        </style>

        <script type="text/javascript">

"use strict";

// As browsers/W3C/WHATWG are incredibly evil, they might ignore the self-declarative
// XML namespace of this document and the given content type in the header, and instead
// assume/render "text/html", which then fails with JavaScript characters that are
// XML/XHTML special characters which need to be escaped, if the file is saved under
// a name that happens to end with ".html" (!!!). Just have the file name end with
// ".xhtml" and it might magically start to work.

let currentInstruction = 0;
let history = null;
let isAuto = false;
let skipRendering = false;

function InstructionInterval()
{
    if (isAuto == false)
    {
        return false;
    }

    if (ExecuteNextInstruction() == true)
    {
        window.setTimeout(InstructionInterval, 200);
    }
    else
    {
        window.setTimeout(function() { ResetInstructions(); InstructionInterval(); }, 2000);
    }
}

function ResetInstructions()
{
    let nodeText = document.getElementById("text");

    if (nodeText != null)
    {
        while (nodeText.hasChildNodes() == true)
        {
            nodeText.removeChild(nodeText.lastChild);
        }
    }
    else
    {
        console.log("No text node found.");
    }

    let nodeRendering = document.getElementById("rendering");

    if (nodeRendering != null)
    {
        while (nodeRendering.hasChildNodes() == true)
        {
            nodeRendering.removeChild(nodeRendering.lastChild);
        }
    }
    else
    {
        console.log("No rendering node found.");
    }

    currentInstruction = 0;
}

document.addEventListener('DOMContentLoaded', function() {
    //InstructionInterval();
    //while (ExecuteNextInstruction() == true);
});

function ExecuteNextInstruction()
{
    if (history == null)
    {
        let element = document.getElementsByTagNameNS("htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z", "change-tracking-text-editor-1-text-history");

        if (element == null)
        {
            console.log("No history found.");
            currentInstruction = 0;
            return false;
        }

        if (element.length &lt;= 0)
        {
            console.log("No history found.");
            currentInstruction = 0;
            return false;
        }

        history = element[0].children;

        if (history.length &lt;= 0)
        {
            return false;
        }
    }

    if (currentInstruction &lt; 0)
    {
        currentInstruction = 0;
    }
    else if (currentInstruction &gt; history.length - 1)
    {
        return false;
    }

    let nodeText = document.getElementById("text");

    if (nodeText == null)
    {
        console.log("No text node found.");
        currentInstruction = 0;
        return false;
    }


    let instruction = history[currentInstruction];

    if (instruction.nodeName === "htx:add")
    {
        if (instruction.hasAttribute("position") != true)
        {
            console.log("The \"position\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        let position = parseInt(instruction.getAttribute("position"));
        let length = nodeText.textContent.length;

        if (position &lt; 0)
        {
            console.log("Negative value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (position &gt; length)
        {
            console.log("Value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            currentInstruction = 0;
            return false;
        }

        if (position == length)
        {
            nodeText.textContent += instruction.textContent;
        }
        else
        {
            nodeText.textContent = nodeText.textContent.slice(0, position) + instruction.textContent + nodeText.textContent.slice(position);
        }

        if (RenderResult(instruction.nodeName, position, position + instruction.textContent.length) !== true)
        {
            return false;
        }
    }
    else if (instruction.nodeName == "htx:delete")
    {
        if (instruction.hasAttribute("position") != true)
        {
            console.log("The \"position\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (instruction.hasAttribute("count") != true)
        {
            console.log("The \"count\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        let position = parseInt(instruction.getAttribute("position"));
        let count = parseInt(instruction.getAttribute("count"));
        let length = nodeText.textContent.length;

        if (position &lt; 0)
        {
            console.log("Negative value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (position &gt; length)
        {
            console.log("Value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            currentInstruction = 0;
            return false;
        }

        if (count &gt;= 0)
        {
            console.log("Positive value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if ((position - (count * -1)) &lt; 0)
        {
            console.log("Value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element would delete more characters than there are present in the remaining text.");
            currentInstruction = 0;
            return false;
        }

        let deletedText = nodeText.textContent.slice(position - (count * -1), position);

        if (instruction.hasChildNodes() != true)
        {
            instruction.appendChild(document.createTextNode(deletedText));
        }

        nodeText.textContent = nodeText.textContent.slice(0, position - (count * -1)) + nodeText.textContent.slice(position);

        if (RenderResult(instruction.nodeName, position - (count * -1), position - (count * -1), deletedText) !== true)
        {
            return false;
        }
    }

    currentInstruction += 1;

    return true;
}

function ExecutePreviousInstruction()
{
    if (history == null)
    {
        let element = document.getElementsByTagNameNS("htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z", "change-tracking-text-editor-1-text-history");

        if (element == null)
        {
            console.log("No history found.");
            currentInstruction = 0;
            return false;
        }

        if (element.length &lt;= 0)
        {
            console.log("No history found.");
            currentInstruction = 0;
            return false;
        }

        history = element[0].children;

        if (history.length &lt;= 0)
        {
            return false;
        }
    }

    if (currentInstruction &lt;= 0)
    {
        return false;
    }
    else if (currentInstruction &gt; history.length - 1)
    {
        currentInstruction = history.length - 1;
    }
    else
    {
        currentInstruction -= 1;
    }

    let nodeText = document.getElementById("text");

    if (nodeText == null)
    {
        console.log("No text node found.");
        currentInstruction = 0;
        return false;
    }


    let instruction = history[currentInstruction];

    if (instruction.nodeName === "htx:add")
    {
        if (instruction.hasAttribute("position") != true)
        {
            console.log("The \"position\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        let position = parseInt(instruction.getAttribute("position"));
        let length = nodeText.textContent.length;
        let count = instruction.textContent.length;

        if (position &lt; 0)
        {
            console.log("Negative value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if ((position + count) &gt; length)
        {
            console.log("The position " + (position + count) + " resulting from value " + position + " for the \"position\" attribute and value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            currentInstruction = 0;
            return false;
        }

        let deletedText = nodeText.textContent.slice(position, position + instruction.textContent.length);

        nodeText.textContent = nodeText.textContent.slice(0, position) + nodeText.textContent.slice(position + deletedText.length);

        if (RenderResult("htx:delete", position, position, deletedText) !== true)
        {
            return false;
        }
    }
    else if (instruction.nodeName == "htx:delete")
    {
        if (instruction.hasAttribute("position") != true)
        {
            console.log("The \"position\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (instruction.hasAttribute("count") != true)
        {
            console.log("The \"count\" attribute is missing in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (instruction.hasChildNodes() != true)
        {
            console.log("To navigate backwards, a 'delete' instruction must have been read in forward direction earlier to reconstruct the destructive operation.");
            currentInstruction = 0;
            return false;
        }

        let insertedText = instruction.firstChild.textContent;
        let position = parseInt(instruction.getAttribute("position"));
        let count = parseInt(instruction.getAttribute("count"));
        let length = nodeText.textContent.length;

        if (position &lt; 0)
        {
            console.log("Negative value " + position + " for the \"position\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if (count &gt;= 0)
        {
            console.log("Positive value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element.");
            currentInstruction = 0;
            return false;
        }

        if ((position - (count * -1)) &lt; 0)
        {
            console.log("Value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element would delete more characters than there are present in the remaining text.");
            currentInstruction = 0;
            return false;
        }

        if ((position - (count * -1)) &gt; length)
        {
            console.log("The position " + (position - (count * -1)) + " resulting from value " + position + " for the \"position\" attribute and value " + count + " for the \"count\" attribute in the \"" + instruction.nodeName + "\" element exceeds the current maximum of " + length + ".");
            currentInstruction = 0;
            return false;
        }

        if (count != (insertedText.length * -1))
        {
            console.log("Difference between 'count' and reconstructed text of a \"" + instruction.nodeName + "\" instruction.");
            currentInstruction = 0;
            return false;
        }

        if ((position - (count * -1)) == length)
        {
            nodeText.textContent += insertedText;
        }
        else
        {
            nodeText.textContent = nodeText.textContent.slice(0, (position - insertedText.length)) + insertedText + nodeText.textContent.slice(position - insertedText.length);
        }

        if (RenderResult("htx:add", position - (count * -1), position) !== true)
        {
            return false;
        }
    }

    return true;
}

function RenderResult(type, start, end, deletedText = "")
{
    if (skipRendering == true)
    {
        return true;
    }

    if (type !== "htx:add" &amp;&amp;
        type !== "htx:delete")
    {
        console.log("Type \"" + type + "\" not supported.");
        return false;
    }

    let nodeText = document.getElementById("text");

    if (nodeText == null)
    {
        console.log("No text node found.");
        currentInstruction = 0;
        return false;
    }

    if (start &lt; 0 ||
        start &gt; nodeText.textContent.length)
    {
        console.log("Start position " + start + " is out of range.");
        return false;
    }

    if (end &lt; 0 ||
        end &gt; nodeText.textContent.length)
    {
        console.log("End position " + end + " is out of range.");
        return false;
    }


    let nodeRendering = document.getElementById("rendering");

    if (nodeRendering == null)
    {
        console.log("No rendering node found.");
        currentInstruction = 0;
        return false;
    }

    while (nodeRendering.hasChildNodes() == true)
    {
        nodeRendering.removeChild(nodeRendering.lastChild);
    }

    let preparedText = document.createDocumentFragment();
    let position = 0;
    let stream = new CharacterStream(nodeText.textContent);

    do
    {
        let character = stream.get();

        if (stream.eof() == true)
        {
            break;
        }

        if (stream.bad() == true)
        {
            console.log("Stream is bad.");
            return -1;
        }

        if (position == start)
        {
            if (type === "htx:add")
            {
                nodeRendering.appendChild(preparedText);
                preparedText = document.createDocumentFragment();
            }
            else if (type === "htx:delete")
            {
                let deletionPreparedText = document.createElement("span");
                deletionPreparedText.setAttribute("class", "instruction-delete");

                let deletionStream = new CharacterStream(deletedText);

                do
                {
                    let deletionCharacter = deletionStream.get();

                    if (deletionStream.eof() == true)
                    {
                        break;
                    }

                    if (deletionStream.bad() == true)
                    {
                        console.log("Stream is bad.");
                        return -1;
                    }

                    RenderCharacter(deletionCharacter, deletionPreparedText);

                } while (true);

                preparedText.appendChild(deletionPreparedText);
            }
        }

        RenderCharacter(character, preparedText);

        position += 1;

        if (type === "htx:add" &amp;&amp;
            position == end)
        {
            let span = document.createElement("span");

            if (type === "htx:add")
            {
                span.setAttribute("class", "instruction-add");
            }

            span.appendChild(preparedText);

            preparedText = document.createDocumentFragment();
            preparedText.appendChild(span);

            nodeRendering.appendChild(preparedText);
            preparedText = document.createDocumentFragment();
        }

    } while (true);

    if (type === "htx:delete" &amp;&amp;
        position == start)
    {
        // Characters deleted at the end are not in the source
        // (nodeText.textContent) any more (already removed), so the loop
        // above didn't get a chance to add the deletion instruction yet.

        let deletionPreparedText = document.createElement("span");
        deletionPreparedText.setAttribute("class", "instruction-delete");

        let deletionStream = new CharacterStream(deletedText);

        do
        {
            let deletionCharacter = deletionStream.get();

            if (deletionStream.eof() == true)
            {
                break;
            }

            if (deletionStream.bad() == true)
            {
                console.log("Stream is bad.");
                return -1;
            }

            RenderCharacter(deletionCharacter, deletionPreparedText);

        } while (true);

        preparedText.appendChild(deletionPreparedText);
    }

    nodeRendering.appendChild(preparedText);

    return true;
}

function RenderCharacter(character, parent)
{
    switch (character)
    {
    case ' ':
        {
            let span = document.createElement("span");
            span.setAttribute("class", "whitespace");
            span.appendChild(document.createTextNode("·"));
            parent.appendChild(span);
            parent.appendChild(document.createElement("wbr"));
        }
        break;
    case '\n':
        {
            let span = document.createElement("span");
            span.setAttribute("class", "whitespace");
            span.appendChild(document.createTextNode("↳"));
            parent.appendChild(span);
            parent.appendChild(document.createElement("br"));
        }
        break;
    case '\r':
        {
            let span = document.createElement("span");
            span.setAttribute("class", "whitespace");
            span.appendChild(document.createTextNode("?"));
            parent.appendChild(span);
            parent.appendChild(document.createElement("wbr"));
        }
        break;
    case '\t':
        {
            let span = document.createElement("span");
            span.setAttribute("class", "whitespace");
            span.appendChild(document.createTextNode("⇆⇆⇆⇆"));
            parent.appendChild(span);
            parent.appendChild(document.createElement("wbr"));
        }
        break;
    default:
        parent.appendChild(document.createTextNode(character));
        break;
    }
}

function CharacterStream(string)
{
    var self = this;

    self._string = string;
    self._cursor = 0;
    self._bad = false;
    self._eof = false;

    self.get = function()
    {
        if (self._cursor &gt;= self._string.length)
        {
            self._bad = true;
            self._eof = true;
            return '\0';
        }

        ++self._cursor;

        return self._string.charAt(self._cursor - 1);
    }

    self.unget = function()
    {
        if (self._cursor &lt;= 0)
        {
            self._bad = true;
        }

        --self._cursor;
    }

    self.eof = function()
    {
        return self._eof;
    }

    self.bad = function()
    {
        return self._bad;
    }
}
        </script>
      </head>
      <body>
        <p id="text" style="display:none;"/>
        <div>
          <a href="#" onclick="ExecuteNextInstruction();">next</a>
          <!-- Because whitespace isn't preserved, it needs to be outputted explicitly. -->
          <xsl:text> </xsl:text>
          <a href="#" onclick="ExecutePreviousInstruction();">prev</a>
          <xsl:text> </xsl:text>
          <a href="#" onclick="ResetInstructions();">first</a>
          <xsl:text> </xsl:text>
          <a href="#" onclick="skipRendering = true; while (ExecuteNextInstruction() == true); skipRendering = false; ExecutePreviousInstruction(); ExecuteNextInstruction(); // Enacts rendering of the last instruction where previous rendering was skipped.">last</a>
          <xsl:text> </xsl:text>
          <a href="#" onclick="isAuto = !isAuto; InstructionInterval();">auto</a>
          <xsl:text> </xsl:text>
          <a href="https://hypertext-systems.org/downloads.php#change_tracking_text_editor_1">tool</a>
        </div>
        <p id="rendering"/>
        <div id="text-history" style="display:none;">
          <xsl:comment> The data contained in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <htx:change-tracking-text-editor-1-text-history xmlns:htx="htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z">
            <xsl:apply-templates/>
          </htx:change-tracking-text-editor-1-text-history>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//htx-src:add | //htx-src:delete">
    <xsl:element name="htx:{local-name()}">
      <xsl:copy-of select="@*|text()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
