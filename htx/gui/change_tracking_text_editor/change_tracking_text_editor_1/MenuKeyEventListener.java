/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of change_tracking_text_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_tracking_text_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_tracking_text_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_tracking_text_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/MenuKeyEventListener.java
 * @author Stephan Kreutzer
 * @since 2023-08-30
 */


import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;


class MenuKeyEventListener implements MenuKeyListener
{
    public MenuKeyEventListener(ContextMenu contextMenu)
    {
        if (contextMenu == null)
        {
            throw new NullPointerException();
        }

        this.contextMenu = contextMenu;
    }

    public void menuKeyPressed(MenuKeyEvent event)
    {
        int result = -1;

        if (event.getKeyCode() == KeyEvent.VK_ENTER)
        {
            MenuElement[] path = event.getMenuSelectionManager().getSelectedPath();

            if (path.length != 4)
            {
                return;
            }

            if (!(path[3] instanceof JMenuItem))
            {
                return;
            }

            // Here not passing the return value into result, because
            // let's skip consuming the event for now.
            this.contextMenu.handle(path[3], false, null, 0, 0);
        }

        if (result == ContextMenu.RETURN_HANDLE_EVENTCONSUME)
        {
            event.consume();
        }
    }

    public void menuKeyReleased(MenuKeyEvent event)
    {

    }

    public void menuKeyTyped(MenuKeyEvent event)
    {

    }

    protected ContextMenu contextMenu = null;
}
