/* Copyright (C) 2014-2022 Stephan Kreutzer
 *
 * This file is part of viewer_3, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * viewer_3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * viewer_3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with viewer_3. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/viewer/viewer_3/viewer_3.java
 * @brief A viewer for retrieved resources.
 * @author Stephan Kreutzer
 * @since 2021-11-05
 */



import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.util.Map;
import java.util.HashMap;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.XMLStreamException;
import org.publishing_systems._20140527t120137z.jterosta.JTeroInputStreamInterface;
import org.publishing_systems._20140527t120137z.jterosta.JTeroInputStreamStd;
import org.publishing_systems._20140527t120137z.jterosta.JTeroLoader;
import org.publishing_systems._20140527t120137z.jterosta.JTeroPattern;
import org.publishing_systems._20140527t120137z.jterosta.JTeroFunction;
import org.publishing_systems._20140527t120137z.jterosta.JTeroStAInterpreter;
import org.publishing_systems._20140527t120137z.jterosta.JTeroEvent;
import org.publishing_systems._20140527t120137z.jterosta.JTeroException;
import java.util.Scanner;



public class viewer_3
  extends JFrame
  implements ActionListener
{
    public static void main(String[] args)
    {
        System.out.print("viewer_3 Copyright (C) 2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://hypertext-systems.org.\n\n");

        viewer_3 instance = new viewer_3();

        try
        {
            instance.run(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by viewer_3, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<viewer-3-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</viewer-3-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public viewer_3()
    {
        super("viewer_3");

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event)
            {
                event.getWindow().setVisible(false);
                event.getWindow().dispose();
                System.exit(2);
            }
        });
    }

    public int run(String[] args)
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\tviewer_3 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        viewer_3.resultInfoFile = resultInfoFile;

        this.programPath = viewer_3.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            this.programPath = new File(this.programPath).getCanonicalPath() + File.separator;
            this.programPath = URLDecoder.decode(this.programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }

        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("viewer_3: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));

        String identifier = null;
        this.currentView = "tree";
        String fontFace = null;
        int fontSize = 14;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("temp-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (this.tempDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        this.tempDirectory = new File(attributePath.getValue());

                        if (this.tempDirectory.isAbsolute() != true)
                        {
                            this.tempDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            this.tempDirectory = this.tempDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, this.tempDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, this.tempDirectory.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("storage-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (this.storageDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        this.storageDirectory = new File(attributePath.getValue());

                        if (this.storageDirectory.isAbsolute() != true)
                        {
                            this.storageDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            this.storageDirectory = this.storageDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageStorageDirectoryCantGetCanonicalPath", ex, null, this.storageDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageStorageDirectoryCantGetCanonicalPath", ex, null, this.storageDirectory.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("input") == true)
                    {
                        if (identifier != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        Attribute attributeIdentifier = event.asStartElement().getAttributeByName(new QName("identifier"));

                        if (attributeIdentifier == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "identifier");
                        }

                        identifier = attributeIdentifier.getValue();
                    }
                    else if (tagName.equals("view") == true)
                    {
                        Attribute attributeInitial = event.asStartElement().getAttributeByName(new QName("initial"));

                        if (attributeInitial != null)
                        {
                            String viewInitial = attributeInitial.getValue();

                            if (viewInitial.equals("tree") == true ||
                                viewInitial.equals("text") == true)
                            {
                                this.currentView = viewInitial;
                            }
                            else
                            {
                                throw constructTermination("messageJobFileInitialViewUnknown", null, null, jobFile.getAbsolutePath(), tagName, attributeInitial.getValue());
                            }
                        }
                    }
                    else if (tagName.equals("font") == true)
                    {
                        Attribute attributePoint = event.asStartElement().getAttributeByName(new QName("point"));

                        if (attributePoint != null)
                        {
                            fontSize = Integer.parseInt(attributePoint.getValue());
                        }

                        Attribute attributeFace = event.asStartElement().getAttributeByName(new QName("face"));

                        if (attributePoint != null)
                        {
                            fontFace = attributeFace.getValue();
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (this.storageDirectory == null)
        {
            this.storageDirectory = new File(this.programPath + "storage");
        }

        if (this.tempDirectory == null)
        {
            this.tempDirectory = new File(this.programPath + "temp");
        }

        this.htmlEntityNameResolveList = new HashMap<String, String>();

        this.htmlEntityNameResolveList.put("quot", "\"");
        this.htmlEntityNameResolveList.put("amp", "&");
        this.htmlEntityNameResolveList.put("apos", "'");
        this.htmlEntityNameResolveList.put("lt", "<");
        this.htmlEntityNameResolveList.put("gt", ">");
        this.htmlEntityNameResolveList.put("exclamation", "!");
        this.htmlEntityNameResolveList.put("percent", "%");
        this.htmlEntityNameResolveList.put("add", "+");
        this.htmlEntityNameResolveList.put("equal", "=");
        this.htmlEntityNameResolveList.put("nbsp", " ");
        this.htmlEntityNameResolveList.put("iexcl", "¡");
        this.htmlEntityNameResolveList.put("cent", "¢");
        this.htmlEntityNameResolveList.put("pound", "£");
        this.htmlEntityNameResolveList.put("curren", "¤");
        this.htmlEntityNameResolveList.put("yen", "¥");
        this.htmlEntityNameResolveList.put("brvbar", "¦");
        this.htmlEntityNameResolveList.put("sect", "§");
        this.htmlEntityNameResolveList.put("uml", "¨");
        this.htmlEntityNameResolveList.put("copy", "©");
        this.htmlEntityNameResolveList.put("ordf", "ª");
        this.htmlEntityNameResolveList.put("laquo", "«");
        this.htmlEntityNameResolveList.put("not", "¬");
        this.htmlEntityNameResolveList.put("shy", "­");
        this.htmlEntityNameResolveList.put("reg", "®");
        this.htmlEntityNameResolveList.put("macr", "¯");
        this.htmlEntityNameResolveList.put("deg", "°");
        this.htmlEntityNameResolveList.put("plusmn", "±");
        this.htmlEntityNameResolveList.put("sup2", "²");
        this.htmlEntityNameResolveList.put("sup3", "³");
        this.htmlEntityNameResolveList.put("acute", "´");
        this.htmlEntityNameResolveList.put("micro", "µ");
        this.htmlEntityNameResolveList.put("para", "¶");
        this.htmlEntityNameResolveList.put("middot", "·");
        this.htmlEntityNameResolveList.put("cedil", "¸");
        this.htmlEntityNameResolveList.put("sup1", "¹");
        this.htmlEntityNameResolveList.put("ordm", "º");
        this.htmlEntityNameResolveList.put("raquo", "»");
        this.htmlEntityNameResolveList.put("frac14", "¼");
        this.htmlEntityNameResolveList.put("frac12", "½");
        this.htmlEntityNameResolveList.put("frac34", "¾");
        this.htmlEntityNameResolveList.put("iquest", "¿");
        this.htmlEntityNameResolveList.put("Agrave", "À");
        this.htmlEntityNameResolveList.put("Aacute", "Á");
        this.htmlEntityNameResolveList.put("Acirc", "Â");
        this.htmlEntityNameResolveList.put("Atilde", "Ã");
        this.htmlEntityNameResolveList.put("Auml", "Ä");
        this.htmlEntityNameResolveList.put("Aring", "Å");
        this.htmlEntityNameResolveList.put("AElig", "Æ");
        this.htmlEntityNameResolveList.put("Ccedil", "Ç");
        this.htmlEntityNameResolveList.put("Egrave", "È");
        this.htmlEntityNameResolveList.put("Eacute", "É");
        this.htmlEntityNameResolveList.put("Ecirc", "Ê");
        this.htmlEntityNameResolveList.put("Euml", "Ë");
        this.htmlEntityNameResolveList.put("Igrave", "Ì");
        this.htmlEntityNameResolveList.put("Iacute", "Í");
        this.htmlEntityNameResolveList.put("Icirc", "Î");
        this.htmlEntityNameResolveList.put("Iuml", "Ï");
        this.htmlEntityNameResolveList.put("ETH", "Ð");
        this.htmlEntityNameResolveList.put("Ntilde", "Ñ");
        this.htmlEntityNameResolveList.put("Ograve", "Ò");
        this.htmlEntityNameResolveList.put("Oacute", "Ó");
        this.htmlEntityNameResolveList.put("Ocirc", "Ô");
        this.htmlEntityNameResolveList.put("Otilde", "Õ");
        this.htmlEntityNameResolveList.put("Ouml", "Ö");
        this.htmlEntityNameResolveList.put("times", "×");
        this.htmlEntityNameResolveList.put("Oslash", "Ø");
        this.htmlEntityNameResolveList.put("Ugrave", "Ù");
        this.htmlEntityNameResolveList.put("Uacute", "Ú");
        this.htmlEntityNameResolveList.put("Ucirc", "Û");
        this.htmlEntityNameResolveList.put("Uuml", "Ü");
        this.htmlEntityNameResolveList.put("Yacute", "Ý");
        this.htmlEntityNameResolveList.put("THORN", "Þ");
        this.htmlEntityNameResolveList.put("szlig", "ß");
        this.htmlEntityNameResolveList.put("agrave", "à");
        this.htmlEntityNameResolveList.put("aacute", "á");
        this.htmlEntityNameResolveList.put("acirc", "â");
        this.htmlEntityNameResolveList.put("atilde", "ã");
        this.htmlEntityNameResolveList.put("auml", "ä");
        this.htmlEntityNameResolveList.put("aring", "å");
        this.htmlEntityNameResolveList.put("aelig", "æ");
        this.htmlEntityNameResolveList.put("ccedil", "ç");
        this.htmlEntityNameResolveList.put("egrave", "è");
        this.htmlEntityNameResolveList.put("eacute", "é");
        this.htmlEntityNameResolveList.put("ecirc", "ê");
        this.htmlEntityNameResolveList.put("euml", "ë");
        this.htmlEntityNameResolveList.put("igrave", "ì");
        this.htmlEntityNameResolveList.put("iacute", "í");
        this.htmlEntityNameResolveList.put("icirc", "î");
        this.htmlEntityNameResolveList.put("iuml", "ï");
        this.htmlEntityNameResolveList.put("eth", "ð");
        this.htmlEntityNameResolveList.put("ntilde", "ñ");
        this.htmlEntityNameResolveList.put("ograve", "ò");
        this.htmlEntityNameResolveList.put("oacute", "ó");
        this.htmlEntityNameResolveList.put("ocirc", "ô");
        this.htmlEntityNameResolveList.put("otilde", "õ");
        this.htmlEntityNameResolveList.put("ouml", "ö");
        this.htmlEntityNameResolveList.put("divide", "÷");
        this.htmlEntityNameResolveList.put("oslash", "ø");
        this.htmlEntityNameResolveList.put("ugrave", "ù");
        this.htmlEntityNameResolveList.put("uacute", "ú");
        this.htmlEntityNameResolveList.put("ucirc", "û");
        this.htmlEntityNameResolveList.put("uuml", "ü");
        this.htmlEntityNameResolveList.put("yacute", "ý");
        this.htmlEntityNameResolveList.put("thorn", "þ");
        this.htmlEntityNameResolveList.put("yuml", "ÿ");
        this.htmlEntityNameResolveList.put("OElig", "Œ");
        this.htmlEntityNameResolveList.put("oelig", "œ");
        this.htmlEntityNameResolveList.put("Scaron", "Š");
        this.htmlEntityNameResolveList.put("scaron", "š");
        this.htmlEntityNameResolveList.put("Yuml", "Ÿ");
        this.htmlEntityNameResolveList.put("fnof", "ƒ");
        this.htmlEntityNameResolveList.put("circ", "ˆ");
        this.htmlEntityNameResolveList.put("tilde", "˜");
        this.htmlEntityNameResolveList.put("Alpha", "Α");
        this.htmlEntityNameResolveList.put("Beta", "Β");
        this.htmlEntityNameResolveList.put("Gamma", "Γ");
        this.htmlEntityNameResolveList.put("Delta", "Δ");
        this.htmlEntityNameResolveList.put("Epsilon", "Ε");
        this.htmlEntityNameResolveList.put("Zeta", "Ζ");
        this.htmlEntityNameResolveList.put("Eta", "Η");
        this.htmlEntityNameResolveList.put("Theta", "Θ");
        this.htmlEntityNameResolveList.put("Iota", "Ι");
        this.htmlEntityNameResolveList.put("Kappa", "Κ");
        this.htmlEntityNameResolveList.put("Lambda", "Λ");
        this.htmlEntityNameResolveList.put("Mu", "Μ");
        this.htmlEntityNameResolveList.put("Nu", "Ν");
        this.htmlEntityNameResolveList.put("Xi", "Ξ");
        this.htmlEntityNameResolveList.put("Omicron", "Ο");
        this.htmlEntityNameResolveList.put("Pi", "Π");
        this.htmlEntityNameResolveList.put("Rho", "Ρ");
        this.htmlEntityNameResolveList.put("Sigma", "Σ");
        this.htmlEntityNameResolveList.put("Tau", "Τ");
        this.htmlEntityNameResolveList.put("Upsilon", "Υ");
        this.htmlEntityNameResolveList.put("Phi", "Φ");
        this.htmlEntityNameResolveList.put("Chi", "Χ");
        this.htmlEntityNameResolveList.put("Psi", "Ψ");
        this.htmlEntityNameResolveList.put("Omega", "Ω");
        this.htmlEntityNameResolveList.put("alpha", "α");
        this.htmlEntityNameResolveList.put("beta", "β");
        this.htmlEntityNameResolveList.put("gamma", "γ");
        this.htmlEntityNameResolveList.put("delta", "δ");
        this.htmlEntityNameResolveList.put("epsilon", "ε");
        this.htmlEntityNameResolveList.put("zeta", "ζ");
        this.htmlEntityNameResolveList.put("eta", "η");
        this.htmlEntityNameResolveList.put("theta", "θ");
        this.htmlEntityNameResolveList.put("iota", "ι");
        this.htmlEntityNameResolveList.put("kappa", "κ");
        this.htmlEntityNameResolveList.put("lambda", "λ");
        this.htmlEntityNameResolveList.put("mu", "μ");
        this.htmlEntityNameResolveList.put("nu", "ν");
        this.htmlEntityNameResolveList.put("xi", "ξ");
        this.htmlEntityNameResolveList.put("omicron", "ο");
        this.htmlEntityNameResolveList.put("pi", "π");
        this.htmlEntityNameResolveList.put("rho", "ρ");
        this.htmlEntityNameResolveList.put("sigmaf", "ς");
        this.htmlEntityNameResolveList.put("sigma", "σ");
        this.htmlEntityNameResolveList.put("tau", "τ");
        this.htmlEntityNameResolveList.put("upsilon", "υ");
        this.htmlEntityNameResolveList.put("phi", "φ");
        this.htmlEntityNameResolveList.put("chi", "χ");
        this.htmlEntityNameResolveList.put("psi", "ψ");
        this.htmlEntityNameResolveList.put("omega", "ω");
        this.htmlEntityNameResolveList.put("thetasym", "ϑ");
        this.htmlEntityNameResolveList.put("upsih", "ϒ");
        this.htmlEntityNameResolveList.put("piv", "ϖ");
        this.htmlEntityNameResolveList.put("ensp", " ");
        this.htmlEntityNameResolveList.put("emsp", " ");
        this.htmlEntityNameResolveList.put("thinsp", " ");
        this.htmlEntityNameResolveList.put("zwnj", "‌");
        this.htmlEntityNameResolveList.put("zwj", "‍");
        this.htmlEntityNameResolveList.put("lrm", "‎");
        this.htmlEntityNameResolveList.put("rlm", "‏");
        this.htmlEntityNameResolveList.put("ndash", "–");
        this.htmlEntityNameResolveList.put("mdash", "—");
        this.htmlEntityNameResolveList.put("horbar", "―");
        this.htmlEntityNameResolveList.put("lsquo", "‘");
        this.htmlEntityNameResolveList.put("rsquo", "’");
        this.htmlEntityNameResolveList.put("sbquo", "‚");
        this.htmlEntityNameResolveList.put("ldquo", "“");
        this.htmlEntityNameResolveList.put("rdquo", "”");
        this.htmlEntityNameResolveList.put("bdquo", "„");
        this.htmlEntityNameResolveList.put("dagger", "†");
        this.htmlEntityNameResolveList.put("Dagger", "‡");
        this.htmlEntityNameResolveList.put("bull", "•");
        this.htmlEntityNameResolveList.put("hellip", "…");
        this.htmlEntityNameResolveList.put("permil", "‰");
        this.htmlEntityNameResolveList.put("prime", "′");
        this.htmlEntityNameResolveList.put("Prime", "″");
        this.htmlEntityNameResolveList.put("lsaquo", "‹");
        this.htmlEntityNameResolveList.put("rsaquo", "›");
        this.htmlEntityNameResolveList.put("oline", "‾");
        this.htmlEntityNameResolveList.put("frasl", "⁄");
        this.htmlEntityNameResolveList.put("euro", "€");
        this.htmlEntityNameResolveList.put("image", "ℑ");
        this.htmlEntityNameResolveList.put("weierp", "℘");
        this.htmlEntityNameResolveList.put("real", "ℜ");
        this.htmlEntityNameResolveList.put("trade", "™");
        this.htmlEntityNameResolveList.put("alefsym", "ℵ");
        this.htmlEntityNameResolveList.put("larr", "←");
        this.htmlEntityNameResolveList.put("uarr", "↑");
        this.htmlEntityNameResolveList.put("rarr", "→");
        this.htmlEntityNameResolveList.put("darr", "↓");
        this.htmlEntityNameResolveList.put("harr", "↔");
        this.htmlEntityNameResolveList.put("crarr", "↵");
        this.htmlEntityNameResolveList.put("lArr", "⇐");
        this.htmlEntityNameResolveList.put("uArr", "⇑");
        this.htmlEntityNameResolveList.put("rArr", "⇒");
        this.htmlEntityNameResolveList.put("dArr", "⇓");
        this.htmlEntityNameResolveList.put("hArr", "⇔");
        this.htmlEntityNameResolveList.put("forall", "∀");
        this.htmlEntityNameResolveList.put("part", "∂");
        this.htmlEntityNameResolveList.put("exist", "∃");
        this.htmlEntityNameResolveList.put("empty", "∅");
        this.htmlEntityNameResolveList.put("nabla", "∇");
        this.htmlEntityNameResolveList.put("isin", "∈");
        this.htmlEntityNameResolveList.put("notin", "∉");
        this.htmlEntityNameResolveList.put("ni", "∋");
        this.htmlEntityNameResolveList.put("prod", "∏");
        this.htmlEntityNameResolveList.put("sum", "∑");
        this.htmlEntityNameResolveList.put("minus", "−");
        this.htmlEntityNameResolveList.put("lowast", "∗");
        this.htmlEntityNameResolveList.put("radic", "√");
        this.htmlEntityNameResolveList.put("prop", "∝");
        this.htmlEntityNameResolveList.put("infin", "∞");
        this.htmlEntityNameResolveList.put("ang", "∠");
        this.htmlEntityNameResolveList.put("and", "∧");
        this.htmlEntityNameResolveList.put("or", "∨");
        this.htmlEntityNameResolveList.put("cap", "∩");
        this.htmlEntityNameResolveList.put("cup", "∪");
        this.htmlEntityNameResolveList.put("int", "∫");
        this.htmlEntityNameResolveList.put("there4", "∴");
        this.htmlEntityNameResolveList.put("sim", "∼");
        this.htmlEntityNameResolveList.put("cong", "≅");
        this.htmlEntityNameResolveList.put("asymp", "≈");
        this.htmlEntityNameResolveList.put("ne", "≠");
        this.htmlEntityNameResolveList.put("equiv", "≡");
        this.htmlEntityNameResolveList.put("le", "≤");
        this.htmlEntityNameResolveList.put("ge", "≥");
        this.htmlEntityNameResolveList.put("sub", "⊂");
        this.htmlEntityNameResolveList.put("sup", "⊃");
        this.htmlEntityNameResolveList.put("nsub", "⊄");
        this.htmlEntityNameResolveList.put("sube", "⊆");
        this.htmlEntityNameResolveList.put("supe", "⊇");
        this.htmlEntityNameResolveList.put("oplus", "⊕");
        this.htmlEntityNameResolveList.put("otimes", "⊗");
        this.htmlEntityNameResolveList.put("perp", "⊥");
        this.htmlEntityNameResolveList.put("sdot", "⋅");
        this.htmlEntityNameResolveList.put("lceil", "⌈");
        this.htmlEntityNameResolveList.put("rceil", "⌉");
        this.htmlEntityNameResolveList.put("lfloor", "⌊");
        this.htmlEntityNameResolveList.put("rfloor", "⌋");
        this.htmlEntityNameResolveList.put("lang", "〈");
        this.htmlEntityNameResolveList.put("rang", "〉");
        this.htmlEntityNameResolveList.put("loz", "◊");
        this.htmlEntityNameResolveList.put("spades", "♠");
        this.htmlEntityNameResolveList.put("clubs", "♣");
        this.htmlEntityNameResolveList.put("hearts", "♥");
        this.htmlEntityNameResolveList.put("diams", "♦");


        JPanel panelMain = new JPanel();

        GridBagLayout gridbag = new GridBagLayout();
        panelMain.setLayout(gridbag);

        GridBagConstraints gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.NORTH;
        gridbagConstraints.gridx = 0;
        gridbagConstraints.gridy = 0;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        JPanel panelTop = new JPanel();
        panelTop.setLayout(new GridLayout(1, 2));

        this.textFieldIdentifier = new JTextField();

        KeyEventListener keyListener = new KeyEventListener(this);
        this.textFieldIdentifier.addKeyListener(keyListener);

        panelTop.add(this.textFieldIdentifier);

        String[] viewOptions = new String[2];
        viewOptions[0] = getI10nString("windowViewOptionCaptionTree");
        viewOptions[1] = getI10nString("windowViewOptionCaptionText");

        JComboBox viewSelection = new JComboBox(viewOptions);
        viewSelection.setSelectedItem(translateInternalToViewOptionCaption(this.currentView));
        viewSelection.addActionListener(this);

        panelTop.add(viewSelection);

        panelMain.add(panelTop, gridbagConstraints);


        gridbagConstraints.anchor = GridBagConstraints.NORTH;
        gridbagConstraints.gridy = 1;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.weighty = 1.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.BOTH;

        DefaultMutableTreeNode treeRoot = new DefaultMutableTreeNode(getI10nString("windowTreeNodeRootCaption"));

        this.treeModel = new DefaultTreeModel(treeRoot);
        this.tree = new JTree(this.treeModel);


        this.textArea = new JTextArea();
        this.textArea.setLineWrap(true);
        this.textArea.setWrapStyleWord(true);
        this.textArea.setEditable(false);
        this.textArea.getCaret().setVisible(true);
        this.textArea.getCaret().setSelectionVisible(true);

        {
            Font textAreaFont = null;

            if (fontFace != null)
            {
                String[] fontFamilies = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames(this.getLocale());

                for (int i = 0, max = fontFamilies.length; i < max; i++)
                {
                    if (fontFamilies[i].equals(fontFace) == true)
                    {
                        textAreaFont = new Font(fontFace, Font.PLAIN, fontSize);
                        break;
                    }
                }

                if (textAreaFont == null)
                {
                    this.infoMessages.add(constructInfoMessage("messageJobFileFontFaceNotRecognized", true, null, null, jobFile.getAbsolutePath(), fontFace));
                }
            }

            if (textAreaFont == null)
            {
                textAreaFont = new Font(Font.MONOSPACED, Font.PLAIN, fontSize);
            }

            this.textArea.setFont(textAreaFont);
        }


        this.mainScrollPane = new JScrollPane();

        panelMain.add(this.mainScrollPane, gridbagConstraints);

        gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.SOUTH;
        gridbagConstraints.gridy = 2;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        JTextField positionField = new JTextField();
        positionField.setText(getI10nString("windowStatusInfoStart"));
        positionField.setEditable(false);

        panelMain.add(positionField, gridbagConstraints);

        getContentPane().add(panelMain, BorderLayout.CENTER);


        if (identifier == null)
        {
            identifier = new String();
        }

        changeIdentifier(identifier);


        setLocation(100, 100);
        setSize(500, 400);
        setVisible(true);

        return 0;
    }

    public DefaultMutableTreeNode parseTree(File inputFile)
    {
        JTeroInputStreamInterface fileInputStream = null;

        try
        {
            fileInputStream = new JTeroInputStreamStd(new FileInputStream(inputFile));
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageResourceFileNotFound", ex, null, inputFile.getAbsolutePath());
        }
        catch (JTeroException ex)
        {
            throw constructTermination("messageResourceFileSetupError", ex, null, inputFile.getAbsolutePath());
        }

        DefaultMutableTreeNode treeRoot = new DefaultMutableTreeNode(getI10nString("windowTreeNodeRootCaption"));

        DefaultMutableTreeNode treeNodeH1 = null;
        DefaultMutableTreeNode treeNodeH2 = null;
        DefaultMutableTreeNode treeNodeH3 = null;
        DefaultMutableTreeNode treeNodeH4 = null;
        DefaultMutableTreeNode treeNodeH5 = null;
        DefaultMutableTreeNode treeNodeH6 = null;


        StringBuilder text = new StringBuilder();

        try
        {
            JTeroLoader teroLoader = new JTeroLoader(this.programPath + "Html" + File.separator);
            teroLoader.load();

            Map<String, JTeroPattern> patterns = teroLoader.getPatterns();
            Map<String, JTeroFunction> functions = teroLoader.getFlow();

            JTeroStAInterpreter interpreter = new JTeroStAInterpreter(functions, patterns, "InMain", fileInputStream, false);

            boolean isInParagraph = false;
            boolean isElementEnd = false;
            int tagStackIndex = -1;

            while (interpreter.hasNext() == true)
            {
                JTeroEvent event = interpreter.nextEvent();

                if (event.getCurrentFunctionName().equals("InHtmlTagStart") == true)
                {
                    if (event.getNextFunctionPatternName() != null)
                    {
                        if (event.getNextFunctionPatternName().equals("HtmlElementEndMarker") == true)
                        {
                            isElementEnd = true;
                        }
                    }
                    else
                    {

                    }

                    if (isElementEnd != true)
                    {
                        if (tagStackIndex >= 0)
                        {
                            tagStackIndex += 1;
                        }
                    }
                    else
                    {
                        if (tagStackIndex >= 0)
                        {
                            tagStackIndex -= 1;
                        }
                    }
                }
                else if (event.getCurrentFunctionName().equals("InHtmlElementName") == true)
                {
                    if (event.getData().equalsIgnoreCase("h1") == true ||
                        event.getData().equalsIgnoreCase("h2") == true ||
                        event.getData().equalsIgnoreCase("h3") == true ||
                        event.getData().equalsIgnoreCase("h4") == true ||
                        event.getData().equalsIgnoreCase("h5") == true ||
                        event.getData().equalsIgnoreCase("h6") == true)
                    {
                        isInParagraph = !(isElementEnd);

                        if (isInParagraph != true)
                        {
                            tagStackIndex = -1;

                            if (event.getData().equalsIgnoreCase("h1") == true)
                            {
                                treeNodeH1 = new DefaultMutableTreeNode(text.toString());
                                text = new StringBuilder();

                                treeRoot.add(treeNodeH1);

                                treeNodeH2 = null;
                                treeNodeH3 = null;
                                treeNodeH4 = null;
                                treeNodeH5 = null;
                                treeNodeH6 = null;
                            }
                            else if (event.getData().equalsIgnoreCase("h2") == true)
                            {
                                treeNodeH2 = new DefaultMutableTreeNode(text.toString());
                                text = new StringBuilder();

                                {
                                    DefaultMutableTreeNode treeNodeTarget = treeNodeH1;

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeRoot;
                                    }

                                    treeNodeTarget.add(treeNodeH2);
                                }

                                treeNodeH3 = null;
                                treeNodeH4 = null;
                                treeNodeH5 = null;
                                treeNodeH6 = null;
                            }
                            else if (event.getData().equalsIgnoreCase("h3") == true)
                            {
                                treeNodeH3 = new DefaultMutableTreeNode(text.toString());
                                text = new StringBuilder();

                                {
                                    DefaultMutableTreeNode treeNodeTarget = treeNodeH2;

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH1;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeRoot;
                                    }

                                    treeNodeTarget.add(treeNodeH3);
                                }

                                treeNodeH4 = null;
                                treeNodeH5 = null;
                                treeNodeH6 = null;
                            }
                            else if (event.getData().equalsIgnoreCase("h4") == true)
                            {
                                treeNodeH4 = new DefaultMutableTreeNode(text.toString());
                                text = new StringBuilder();

                                {
                                    DefaultMutableTreeNode treeNodeTarget = treeNodeH3;

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH2;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH1;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeRoot;
                                    }

                                    treeNodeTarget.add(treeNodeH4);
                                }

                                treeNodeH5 = null;
                                treeNodeH6 = null;
                            }
                            else if (event.getData().equalsIgnoreCase("h5") == true)
                            {
                                treeNodeH5 = new DefaultMutableTreeNode(text.toString());
                                text = new StringBuilder();

                                {
                                    DefaultMutableTreeNode treeNodeTarget = treeNodeH4;

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH3;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH2;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH1;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeRoot;
                                    }

                                    treeNodeTarget.add(treeNodeH5);
                                }

                                treeNodeH6 = null;
                            }
                            else if (event.getData().equalsIgnoreCase("h6") == true)
                            {
                                treeNodeH6 = new DefaultMutableTreeNode(text.toString());
                                text = new StringBuilder();

                                {
                                    DefaultMutableTreeNode treeNodeTarget = treeNodeH5;

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH4;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH3;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH2;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeNodeH1;
                                    }

                                    if (treeNodeTarget == null)
                                    {
                                        treeNodeTarget = treeRoot;
                                    }

                                    treeNodeTarget.add(treeNodeH6);
                                }
                            }
                        }
                    }
                }
                else
                {
                    isElementEnd = false;

                    if (isInParagraph == true)
                    {
                        if (event.getCurrentFunctionName().equals("InMain") == true)
                        {
                            text.append(event.getData());
                        }
                        else if (event.getCurrentFunctionName().equals("InHtmlEntityCharacterDecimal") == true)
                        {
                            int codepoint = Integer.parseInt(event.getData(), 10);
                            text.append(Character.toChars(codepoint));
                        }
                        else if (event.getCurrentFunctionName().equals("InHtmlEntityCharacterHexadecimal") == true)
                        {
                            int codepoint = Integer.parseInt(event.getData(), 16);
                            text.append(Character.toChars(codepoint));
                        }
                        else if (event.getCurrentFunctionName().equals("InHtmlEntityName") == true)
                        {
                            String resolvedEntity = this.htmlEntityNameResolveList.get(event.getData());

                            if (resolvedEntity != null)
                            {
                                text.append(resolvedEntity);
                            }
                            else
                            {
                                text.append("&");
                                text.append(event.getData());
                                text.append(";");
                            }
                        }
                    }
                }
            }
        }
        catch (JTeroException ex)
        {
            throw constructTermination("messageResourceFileErrorWhileParsing", ex, null, inputFile.getAbsolutePath());
        }

        return treeRoot;
    }

    public String parseText(File inputFile)
    {
        StringBuilder text = new StringBuilder();

        JTeroInputStreamInterface fileInputStream = null;

        try
        {
            fileInputStream = new JTeroInputStreamStd(new FileInputStream(inputFile));
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageResourceFileNotFound", ex, null, inputFile.getAbsolutePath());
        }
        catch (JTeroException ex)
        {
            throw constructTermination("messageResourceFileSetupError", ex, null, inputFile.getAbsolutePath());
        }


        try
        {
            JTeroLoader teroLoader = new JTeroLoader(this.programPath + "Html" + File.separator);
            teroLoader.load();

            Map<String, JTeroPattern> patterns = teroLoader.getPatterns();
            Map<String, JTeroFunction> functions = teroLoader.getFlow();

            JTeroStAInterpreter interpreter = new JTeroStAInterpreter(functions, patterns, "InMain", fileInputStream, false);

            boolean isInParagraph = false;
            boolean isElementEnd = false;
            int tagStackIndex = -1;

            while (interpreter.hasNext() == true)
            {
                JTeroEvent event = interpreter.nextEvent();

                if (event.getCurrentFunctionName().equals("InHtmlTagStart") == true)
                {
                    if (event.getNextFunctionPatternName() != null)
                    {
                        if (event.getNextFunctionPatternName().equals("HtmlElementEndMarker") == true)
                        {
                            isElementEnd = true;
                        }
                    }
                    else
                    {

                    }

                    if (isElementEnd != true)
                    {
                        if (tagStackIndex >= 0)
                        {
                            tagStackIndex += 1;
                        }
                    }
                    else
                    {
                        if (tagStackIndex >= 0)
                        {
                            tagStackIndex -= 1;
                        }
                    }
                }
                else if (event.getCurrentFunctionName().equals("InHtmlElementName") == true)
                {
                    if (event.getData().equalsIgnoreCase("p") == true)
                    {
                        isInParagraph = !(isElementEnd);

                        if (isInParagraph == true)
                        {
                            tagStackIndex = 0;
                        }
                        else
                        {
                            tagStackIndex = -1;
                            text.append("\n\n");
                        }
                    }
                }
                else
                {
                    isElementEnd = false;

                    if (isInParagraph == true)
                    {
                        if (event.getCurrentFunctionName().equals("InMain") == true)
                        {
                            text.append(event.getData());
                        }
                        else if (event.getCurrentFunctionName().equals("InHtmlEntityCharacterDecimal") == true)
                        {
                            int codepoint = Integer.parseInt(event.getData(), 10);
                            text.append(Character.toChars(codepoint));
                        }
                        else if (event.getCurrentFunctionName().equals("InHtmlEntityCharacterHexadecimal") == true)
                        {
                            int codepoint = Integer.parseInt(event.getData(), 16);
                            text.append(Character.toChars(codepoint));
                        }
                        else if (event.getCurrentFunctionName().equals("InHtmlEntityName") == true)
                        {
                            String resolvedEntity = this.htmlEntityNameResolveList.get(event.getData());

                            if (resolvedEntity != null)
                            {
                                text.append(resolvedEntity);
                            }
                            else
                            {
                                text.append("&");
                                text.append(event.getData());
                                text.append(";");
                            }
                        }
                    }
                }
            }
        }
        catch (JTeroException ex)
        {
            throw constructTermination("messageResourceFileErrorWhileParsing", ex, null, inputFile.getAbsolutePath());
        }

        return text.toString();
    }

    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() instanceof JComboBox)
        {
            JComboBox comboBox = (JComboBox)event.getSource();

            String selectedItem = (String)comboBox.getSelectedItem();
            selectedItem = translateViewOptionCaptionToInternal(selectedItem);

            if (selectedItem.equals("tree") == true ||
                selectedItem.equals("text") == true)
            {
                updateView(selectedItem);
            }
            else
            {
                throw constructTermination("messageViewNotSupported", null, null, selectedItem);
            }
        }
    }

    protected int updateView(String view)
    {
        if (view.equals("tree") == true)
        {
            this.mainScrollPane.getViewport().removeAll();
            this.mainScrollPane.getViewport().add(this.tree);

            this.mainScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            this.mainScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        }
        else if (view.equals("text") == true)
        {
            this.mainScrollPane.getViewport().removeAll();
            this.mainScrollPane.getViewport().add(this.textArea);

            this.mainScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            this.mainScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        }
        else
        {
            throw constructTermination("messageViewNotSupported", null, null, view);
        }

        this.currentView = view;

        return 0;
    }

    public String translateViewOptionCaptionToInternal(String viewOptionCaption)
    {
        if (viewOptionCaption.equals(getI10nString("windowViewOptionCaptionTree")) == true)
        {
            return "tree";
        }
        else if (viewOptionCaption.equals(getI10nString("windowViewOptionCaptionText")) == true)
        {
            return "text";
        }
        else
        {
            throw constructTermination("messageViewOptionCaptionUnknown", null, null, viewOptionCaption);
        }
    }

    public String translateInternalToViewOptionCaption(String internal)
    {
        if (internal.equals("tree") == true)
        {
            return getI10nString("windowViewOptionCaptionTree");
        }
        else if (internal.equals("text") == true)
        {
            return getI10nString("windowViewOptionCaptionText");
        }
        else
        {
            throw constructTermination("messageViewNotSupported", null, null, internal);
        }
    }

    public int identifierChangedEvent()
    {
        return changeIdentifier(this.textFieldIdentifier.getText());
    }

    public int changeIdentifier(String identifier)
    {
        this.textFieldIdentifier.setText(identifier);

        if (identifier.isEmpty() != true)
        {
            this.resourceFile = attemptRetrieval(identifier);

            DefaultMutableTreeNode treeRoot = parseTree(this.resourceFile);
            this.treeModel.setRoot(treeRoot);

            String textModel = parseText(this.resourceFile);
            this.textArea.setText(textModel);
        }
        else
        {
            this.resourceFile = null;

            DefaultMutableTreeNode treeRoot = new DefaultMutableTreeNode(getI10nString("windowTreeNodeRootCaption"));
            this.treeModel.setRoot(treeRoot);

            this.textArea.setText("");
        }

        updateView(this.currentView);

        return 0;
    }

    public File attemptRetrieval(String identifier)
    {
        if (this.tempDirectory.exists() == true)
        {
            if (this.tempDirectory.isDirectory() == true)
            {
                if (this.tempDirectory.canWrite() != true)
                {
                    throw constructTermination("messageTempDirectoryIsntWritable", null, null, this.tempDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageTempPathIsntADirectory", null, null, this.tempDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                this.tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageTempDirectoryCantCreate", ex, null, this.tempDirectory.getAbsolutePath());
            }
        }

        if (this.storageDirectory.exists() == true)
        {
            if (this.storageDirectory.isDirectory() == true)
            {
                if (this.storageDirectory.canWrite() != true)
                {
                    throw constructTermination("messageStorageDirectoryIsntWritable", null, null, this.storageDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageStoragePathIsntADirectory", null, null, this.storageDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                this.storageDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageStorageDirectoryCantCreate", ex, null, this.storageDirectory.getAbsolutePath());
            }
        }


        File jobFile = new File(this.tempDirectory.getAbsolutePath() + File.separator + "storage_resource_retriever_1_jobfile.xml");
        File resultInfoFile = new File(this.tempDirectory.getAbsolutePath() + File.separator + "storage_resource_retriever_1_resultinfo.xml");
        File resourceFile = new File(this.tempDirectory.getAbsolutePath() + File.separator + "resource");

        if (jobFile.exists() == true)
        {
            if (jobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFile.canWrite() != true)
                    {
                        throw constructTermination("messageStorageResourceRetriever1JobFileExistsButIsntWritable", null, null, jobFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageStorageResourceRetriever1JobPathExistsButIsntAFile", null, null, jobFile.getAbsolutePath());
            }
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFile.canWrite() != true)
                    {
                        throw constructTermination("messageStorageResourceRetriever1ResultInfoFileExistsButIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageStorageResourceRetriever1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        if (resourceFile.exists() == true)
        {
            if (resourceFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resourceFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resourceFile.canWrite() != true)
                    {
                        throw constructTermination("messageResourceFileExistsButIsntWritable", null, null, resourceFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageResourcePathExistsButIsntAFile", null, null, resourceFile.getAbsolutePath());
            }
        }

        // Ampersand needs to be the first, otherwise it would double-encode
        // other entities.
        identifier = identifier.replaceAll("&", "&amp;");
        identifier = identifier.replaceAll("<", "&lt;");
        identifier = identifier.replaceAll(">", "&gt;");
        //identifier = identifier.replaceAll("\"", "&quot;");
        identifier = identifier.replaceAll("'", "&apos;");

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by viewer_3, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
            writer.write("<storage-resource-retriever-1-workflow-job>\n");
            writer.write("  <temp-directory path=\"" + this.tempDirectory.getAbsolutePath() + "\"/>\n");
            writer.write("  <storage-directory path=\"" + this.storageDirectory.getAbsolutePath() + "\"/>\n");
            writer.write("  <resources>\n");
            writer.write("    <resource identifier=\"" + identifier + "\" output-file-path=\"" + resourceFile.getAbsolutePath() + "\"/>\n");
            writer.write("  </resources>\n");
            writer.write("</storage-resource-retriever-1-workflow-job>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageStorageResourceRetriever1JobFileWritingError", ex, null, jobFile.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageStorageResourceRetriever1JobFileWritingError", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageStorageResourceRetriever1JobFileWritingError", ex, null, jobFile.getAbsolutePath());
        }

        ProcessBuilder builder = new ProcessBuilder("java", "storage_resource_retriever_1", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath());
        builder.directory(new File(this.programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "workflows" + File.separator + "storage_resource_retriever" + File.separator + "storage_resource_retriever_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageStorageResourceRetriever1ErrorWhileReadingOutput", ex, null);
        }

        if (resultInfoFile.exists() != true)
        {
            throw constructTermination("messageStorageResourceRetriever1ResultInfoFileDoesntExistButShould", null, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.isFile() != true)
        {
            throw constructTermination("messageStorageResourceRetriever1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.canRead() != true)
        {
            throw constructTermination("messageStorageResourceRetriever1ResultInfoFileIsntReadable", null, null, resultInfoFile.getAbsolutePath());
        }

        boolean wasSuccessCall = false;
        boolean wasSuccessRetrieval = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccessCall = true;
                    }
                    else if (tagName.equals("retrieved-resource") == true)
                    {
                        Attribute attributeSuccess = event.asStartElement().getAttributeByName(new QName("success"));

                        if (attributeSuccess == null)
                        {
                            throw constructTermination("messageStorageResourceRetriever1ResultInfoFileEntryIsMissingAnAttribute", null, null, resultInfoFile.getAbsolutePath(), tagName, "success");
                        }

                        if (attributeSuccess.getValue().equals("true") == true)
                        {
                            wasSuccessRetrieval = true;
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageStorageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageStorageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageStorageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (wasSuccessCall != true)
        {
            throw constructTermination("messageStorageResourceRetriever1CallWasntSuccessful", null, null);
        }

        if (wasSuccessRetrieval != true)
        {
            this.infoMessages.add(constructInfoMessage("messageRetrievalAttemptWasntSuccessful", true, null, null, identifier, resultInfoFile.getAbsolutePath()));
        }

        if (resourceFile.exists() != true)
        {
            throw constructTermination("messageResourceFileDoesntExist", null, null, resourceFile.getAbsolutePath());
        }

        if (resourceFile.isFile() != true)
        {
            throw constructTermination("messageResourcePathExistsButIsntAFile", null, null, resourceFile.getAbsolutePath());
        }

        if (resourceFile.canRead() != true)
        {
            throw constructTermination("messageResourceFileIsntReadable", null, null, resourceFile.getAbsolutePath());
        }

        return resourceFile;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "viewer_3: " + getI10nString(id);
            }
            else
            {
                message = "viewer_3: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "viewer_3: " + getI10nString(id);
            }
            else
            {
                message = "viewer_3: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (viewer_3.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(viewer_3.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by viewer_3, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<viewer-3-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</viewer-3-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        viewer_3.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10n == null)
        {
            this.l10n = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10n.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10n.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected File tempDirectory = null;
    protected File storageDirectory = null;

    protected File resourceFile = null;
    protected String currentView = null;

    protected JTextField textFieldIdentifier = null;
    protected JScrollPane mainScrollPane = null;
    protected JTree tree = null;
    protected DefaultTreeModel treeModel = null;
    protected JTextArea textArea = null;

    protected Map<String, String> htmlEntityNameResolveList = null;

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private String programPath = null;
    private static final String L10N_BUNDLE = "l10n.l10nViewer3";
    private ResourceBundle l10n;
}
