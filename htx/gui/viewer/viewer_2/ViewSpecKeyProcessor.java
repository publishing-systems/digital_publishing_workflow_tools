/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of viewer_2, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * viewer_2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * viewer_2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with viewer_2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/viewer/viewer_2/ViewSpecKeyProcessor.java
 * @author Stephan Kreutzer
 * @since 2022-01-14
 */



import javax.swing.*;
import java.awt.event.*;
import java.awt.*;



class ViewSpecKeyProcessor implements KeyListener
{
    public ViewSpecKeyProcessor(viewer_2 parent, ViewSpec viewSpec)
    {
        if (parent == null)
        {
            throw new NullPointerException();
        }

        if (viewSpec == null)
        {
            throw new NullPointerException();
        }

        this.parent = parent;
        this.viewSpec = viewSpec;
    }

    public void keyPressed(KeyEvent event)
    {
        // Otherwise default Ctrl + * could win over the keyReleased() event.
        //event.consume();
    }

    public void keyReleased(KeyEvent event)
    {
        boolean changed = false;

        //if ((event.getModifiers() & KeyEvent.CTRL_MASK) != 0)
        {
            if ((event.getModifiers() & KeyEvent.SHIFT_MASK) == 0)
            {
                int keyCode = event.getKeyCode();

                if (keyCode == KeyEvent.VK_Y)
                {
                    changed = (this.viewSpec.statementLineFeed != true);
                    this.viewSpec.statementLineFeed = true;
                }
                else if (keyCode == KeyEvent.VK_Z)
                {
                    changed = (this.viewSpec.statementLineFeed != false);
                    this.viewSpec.statementLineFeed = false;
                }
                else if (keyCode == KeyEvent.VK_M)
                {
                    changed = (this.viewSpec.statementNumbersIds != true);
                    this.viewSpec.statementNumbersIds = true;
                }
                else if (keyCode == KeyEvent.VK_N)
                {
                    changed = (this.viewSpec.statementNumbersIds != false);
                    this.viewSpec.statementNumbersIds = false;
                }
                else if (keyCode == KeyEvent.VK_T)
                {
                    changed = (this.viewSpec.statementLines != 1);
                    this.viewSpec.statementLines = 1;
                }
                else if (keyCode == KeyEvent.VK_S)
                {
                    changed = (this.viewSpec.statementLines != -1);
                    this.viewSpec.statementLines = -1;
                }

                if (changed == true)
                {
                    this.parent.updateView(this.viewSpec);
                }
                else
                {
                    this.parent.updateView(null);
                }
            }
        }
    }

    public void keyTyped(KeyEvent event)
    {

    }

    protected viewer_2 parent = null;
    protected ViewSpec viewSpec = new ViewSpec();
}
