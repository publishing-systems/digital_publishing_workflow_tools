/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of viewer_2, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * viewer_2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * viewer_2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with viewer_2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/viewer/viewer_2/ViewSpec.java
 * @author Stephan Kreutzer
 * @since 2022-01-14
 */



class ViewSpec
{
    public boolean statementLineFeed = true;
    public boolean statementNumbersIds = false;
    public int statementLines = -1;
}
