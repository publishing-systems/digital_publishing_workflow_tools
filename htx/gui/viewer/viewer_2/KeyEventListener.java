/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of viewer_2, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * viewer_2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * viewer_2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with viewer_2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/viewer/viewer_2/KeyEventListener.java
 * @author Stephan Kreutzer
 * @since 2021-11-18
 */



import javax.swing.*;
import java.awt.event.*;
import java.awt.datatransfer.Clipboard;
import java.awt.*;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.DataFlavor;



class KeyEventListener implements KeyListener
{
    public KeyEventListener(viewer_2 parent)
    {
        if (parent == null)
        {
            throw new NullPointerException();
        }

        this.parent = parent;
    }

    public void keyPressed(KeyEvent event)
    {
        // Otherwise default Ctrl + V could win over the keyReleased() event.
        //event.consume();
    }

    public void keyReleased(KeyEvent event)
    {
        if (event.getKeyCode() == KeyEvent.VK_ENTER)
        {
            this.parent.identifierChangedEvent();
        }
        else if (event.getKeyCode() == KeyEvent.VK_V && ((event.getModifiers() & KeyEvent.CTRL_MASK) != 0))
        {
            /*
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

            Transferable content = clipboard.getContents(null);
            String data = null;

            try
            {
                data = content.getTransferData(DataFlavor.stringFlavor).toString();
            }
            catch (Exception ex)
            {

            }
            */
        }
    }

    public void keyTyped(KeyEvent event)
    {

    }

    protected viewer_2 parent = null;
}
