/* Copyright (C) 2020-2022 Stephan Kreutzer
 *
 * This file is part of hyperdex_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * hyperdex_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * hyperdex_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with hyperdex_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/hyperdex/hyperdex_1/ActionListenerOperationButton.java
 * @author Stephan Kreutzer
 * @since 2020-11-27
 */



import java.awt.event.*;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.UnsupportedEncodingException;



class ActionListenerOperationButton
  implements ActionListener
{
    public ActionListenerOperationButton(hyperdex_1 parent, String nodeId, String command)
    {
        super();

        this.parent = parent;
        this.nodeId = nodeId;
        this.command = command;
    }

    public void actionPerformed(ActionEvent event)
    {
        if (this.command.equals("add") == true)
        {
            this.parent.addNode(this.nodeId);
        }
        else if (this.command.equals("node-up") == true)
        {
            this.parent.moveNode(this.nodeId, "up");
        }
        else if (this.command.equals("node-down") == true)
        {
            this.parent.moveNode(this.nodeId, "down");
        }
        else if (this.command.equals("edge-up") == true)
        {
            this.parent.moveEdge(this.nodeId, "up");
        }
        else if (this.command.equals("edge-down") == true)
        {
            this.parent.moveEdge(this.nodeId, "down");
        }
        else if (this.command.equals("remove") == true)
        {
            this.parent.removeNode(this.nodeId);
        }
        else if (this.command.equals("edit") == true)
        {
            this.parent.updateNode(this.nodeId);
        }
        else if (this.command.equals("connect") == true)
        {
            this.parent.toggleConnection(this.nodeId, true);
        }
        else if (this.command.equals("disconnect") == true)
        {
            this.parent.toggleConnection(this.nodeId, false);
        }
        else if (this.command.equals("node-note") == true)
        {
            this.parent.nodeEditNote(this.nodeId);
        }
        else if (this.command.equals("node-id") == true)
        {
            this.parent.nodeEditId(this.nodeId);
        }
        else
        {
            throw constructTermination("messageUnknownCommand", null, null, this.command);
        }
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "hyperdex_1 (ActionListenerOperationButton): " + getI10nString(id);
            }
            else
            {
                message = "hyperdex_1 (ActionListenerOperationButton): " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10n == null)
        {
            this.l10n = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10n.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10n.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    protected hyperdex_1 parent = null;
    protected String nodeId = null;
    protected String command = null;

    private static final String L10N_BUNDLE = "l10n.l10nActionListenerOperationButton";
    private ResourceBundle l10n;
}
