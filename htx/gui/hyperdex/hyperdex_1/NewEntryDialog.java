/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of hyperdex_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * hyperdex_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * hyperdex_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with hyperdex_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/hyperdex/hyperdex_1/NewEntryDialog.java
 * @brief Dialog supposed to help with making data entry easier
 *     for the case of a centralized main/master node/dimension,
 *     with secondary connections to nodes on the other dimensions
 *     (similar to attributes/properties to the main/master
 *     node/dimension).
 * @details This could/should offer to enter multiple values
 *     for a dimension, and/or also select already pre-existing
 *     node values (from a dropdown and/or auto-complete).
 * @author Stephan Kreutzer
 * @since 2023-06-23
 */



import javax.swing.*;
import java.util.Locale;
import java.util.ResourceBundle;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.UnsupportedEncodingException;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;



/**
 * @class NewEntryDialog
 */
class NewEntryDialog extends JDialog
{
    public NewEntryDialog(JFrame parent, Set<String> dimensions)
    {
        generateGUI(parent, dimensions);
    }

    public final void generateGUI(JFrame parent,  Set<String> dimensions)
    {
        GridBagLayout gridbag = new GridBagLayout();
        setLayout(gridbag);

        JPanel panelList = new JPanel();

        gridbag = new GridBagLayout();
        panelList.setLayout(gridbag);

        GridBagConstraints gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.PAGE_START;
        gridbagConstraints.weightx = 1.0;
        gridbagConstraints.weighty = 0.0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridbagConstraints.insets = new Insets(1, 0, 1, 0);

        for (String dimension : dimensions)
        {
            JPanel group = new JPanel(new GridLayout(2, 1));

            JLabel label = new JLabel(dimension);
            group.add(label);

            JTextArea textArea = new JTextArea();

            textArea.setLineWrap(false);
            textArea.setWrapStyleWord(false);
            textArea.setEditable(true);
            textArea.getCaret().setVisible(true);
            textArea.getCaret().setSelectionVisible(true);

            this.textAreas.put(dimension, textArea);
            group.add(textArea);

            panelList.add(group, gridbagConstraints);
        }

        gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.PAGE_START;
        gridbagConstraints.weightx = 1.0;
        //gridbagConstraints.weighty = 0.0;
        //gridbagConstraints.gridx = 0;
        //gridbagConstraints.gridy = 0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        add(panelList, gridbagConstraints);


        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(gridbag);

        JButton closeButton = new JButton(getI10nString("buttonCancel"));

        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });

        buttonPanel.add(closeButton);

        JButton acceptButton = new JButton(getI10nString("buttonOK"));
        acceptButton.addActionListener(new ActionListenerNewEntryDialogResult(this));

        buttonPanel.add(acceptButton);


        gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.PAGE_START;
        //gridbagConstraints.weightx = 1.0;
        //gridbagConstraints.weighty = 0.0;
        gridbagConstraints.gridx = 0;
        gridbagConstraints.gridy = 1;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        add(buttonPanel, gridbagConstraints);

        setModalityType(ModalityType.APPLICATION_MODAL);

        setTitle(getI10nString("dialogCaption"));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        pack();
        setLocationRelativeTo(parent);
    }

    public int SetResultValues()
    {
        this.newValues = new HashMap<String, String>();

        for (Map.Entry<String, JTextArea> pair : this.textAreas.entrySet())
        {
            if (pair.getValue().getText().isEmpty() != true)
            {
                this.newValues.put(pair.getKey(), pair.getValue().getText());
            }
        }

        return 0;
    }

    public Map<String, String> GetNewValues()
    {
        return this.newValues;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets i10n strings from a .properties file as encoded in UTF-8.
     */
    public String getI10nString(String key)
    {
        if (this.i10nGUI == null)
        {
            this.i10nGUI = ResourceBundle.getBundle("l10n.l10nNewEntryDialog", getLocale());
        }

        try
        {
            return new String(this.i10nGUI.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.i10nGUI.getString(key);
        }
    }

    protected Map<String, JTextArea> textAreas = new HashMap<String, JTextArea>();

    protected Map<String, String> newValues = null;

    private ResourceBundle i10nGUI;
}

