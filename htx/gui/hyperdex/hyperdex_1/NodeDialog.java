/* Copyright (C) 2020-2022  Stephan Kreutzer
 *
 * This file is part of hyperdex_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * hyperdex_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * hyperdex_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with hyperdex_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/hyperdex/hyperdex_1/NodeDialog.java
 * @author Stephan Kreutzer
 * @since 2020-11-26
 */



import javax.swing.*;
import java.util.Locale;
import java.util.ResourceBundle;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.UnsupportedEncodingException;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;



/**
 * @class NodeDialog
 * @brief A dialog to request a value to be entered by the user.
 * @todo Rename, as it's not used only for the node value any more.
 */
class NodeDialog extends JDialog
{
    public NodeDialog(JFrame parent, String value)
    {
        generateGUI(parent, value);
    }

    public final void generateGUI(JFrame parent, String value)
    {
        GridBagLayout gridbag = new GridBagLayout();
        setLayout(gridbag);

        JTextField textFieldNewEntry = new JTextField(value);

        GridBagConstraints gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.PAGE_START;
        gridbagConstraints.weightx = 1.0;
        //gridbagConstraints.weighty = 0.0;
        //gridbagConstraints.gridx = 0;
        //gridbagConstraints.gridy = 0;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        add(textFieldNewEntry, gridbagConstraints);


        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(gridbag);

        JButton closeButton = new JButton(getI10nString("buttonCancel"));

        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });

        buttonPanel.add(closeButton);

        JButton acceptButton = new JButton(getI10nString("buttonOK"));
        acceptButton.addActionListener(new ActionListenerNodeDialogResult(this, textFieldNewEntry));

        buttonPanel.add(acceptButton);


        gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.anchor = GridBagConstraints.PAGE_START;
        //gridbagConstraints.weightx = 1.0;
        //gridbagConstraints.weighty = 0.0;
        gridbagConstraints.gridx = 0;
        gridbagConstraints.gridy = 1;
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.fill = GridBagConstraints.HORIZONTAL;

        add(buttonPanel, gridbagConstraints);

        setModalityType(ModalityType.APPLICATION_MODAL);

        setTitle(getI10nString("dialogCaption"));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        pack();
        setLocationRelativeTo(parent);
    }

    public int SetResultValue(String newValue)
    {
        this.newValue = newValue;
        return 0;
    }

    public String GetNewValue()
    {
        return this.newValue;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets i10n strings from a .properties file as encoded in UTF-8.
     */
    public String getI10nString(String key)
    {
        if (this.i10nGUI == null)
        {
            this.i10nGUI = ResourceBundle.getBundle("l10n.l10nNodeDialog", getLocale());
        }

        try
        {
            return new String(this.i10nGUI.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.i10nGUI.getString(key);
        }
    }

    protected String newValue = null;

    private ResourceBundle i10nGUI;
}

