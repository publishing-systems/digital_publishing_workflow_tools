/* Copyright (C) 2020-2023 Stephan Kreutzer
 *
 * This file is part of hyperdex_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * hyperdex_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * hyperdex_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with hyperdex_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/hyperdex/hyperdex_1/ActionListenerNewEntryDialogResult.java
 * @author Stephan Kreutzer
 * @since 2023-06-23
 */


import java.awt.event.*;


class ActionListenerNewEntryDialogResult
  implements ActionListener
{
    public ActionListenerNewEntryDialogResult(NewEntryDialog parent)
    {
        super();

        this.parent = parent;
    }

    public void actionPerformed(ActionEvent event)
    {
        this.parent.SetResultValues();

        this.parent.setVisible(false);
        this.parent.dispose();
    }

    protected NewEntryDialog parent = null;
}
