/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of hyperdex_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * hyperdex_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * hyperdex_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with hyperdex_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/gui/hyperdex/hyperdex_1/ActionListenerButtonNode.java
 * @author Stephan Kreutzer
 * @since 2020-10-18
 */



import java.awt.event.*;



class ActionListenerButtonNode
  implements ActionListener
{
    public ActionListenerButtonNode(hyperdex_1 parent, String idNode, boolean dimensionalPivot)
    {
        super();

        this.parent = parent;
        this.idNode = idNode;
        this.dimensionalPivot = dimensionalPivot;
    }

    public void actionPerformed(ActionEvent event)
    {
        this.parent.navigateNode(this.idNode, this.dimensionalPivot);
    }

    protected hyperdex_1 parent = null;
    protected String idNode = null;
    protected boolean dimensionalPivot = false;
}
