<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2022 Stephan Kreutzer

This file is part of htx_1 workflow, a submodule of the
digital_publishing_workflow_tools package.

htx_1 workflow is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

htx_1 workflow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with htx_1 workflow. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:ncx="http://www.daisy.org/z3986/2005/ncx/">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>

  <xsl:template match="/">
    <list-picker-1-jobfile>
      <xsl:comment> This file was created by ncx_to_list_picker_1_jobfile.xsl of htx_1 workflow, a submodule of the digital_publishing_workflow_tools package, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). </xsl:comment>
      <title>Choose a resource</title>
      <options>
        <xsl:apply-templates select=".//ncx:navPoint"/>
      </options>
    </list-picker-1-jobfile>
  </xsl:template>

  <xsl:template match="//ncx:navPoint">
    <option id="{./ncx:content/@src}">
      <caption>
        <xsl:apply-templates select="./ncx:navLabel/ncx:text//text()"/>
      </caption>
    </option>
  </xsl:template>

  <xsl:template match="//ncx:navPoint/ncx:navLabel/ncx:text//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
