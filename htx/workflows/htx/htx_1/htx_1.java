/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of htx_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * htx_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * htx_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.`
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with htx_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/workflows/htx/htx_1/htx_1.java
 * @author Stephan Kreutzer
 * @since 2022-11-13
 */



import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.util.Stack;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.util.Scanner;
import java.util.Set;
import java.util.HashSet;



public class htx_1
{
    public static void main(String args[])
    {
        System.out.print("htx_1 Copyright (C) 2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        htx_1 instance = new htx_1();

        try
        {
            instance.call(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by htx_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<htx-1-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</htx-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int call(String args[])
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\thtx_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        htx_1.resultInfoFile = resultInfoFile;
        this.programPath = htx_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("htx_1 workflow: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));


        Stack<String> resourceIdentifierStack = new Stack<String>();
        File tempDirectory = null;
        File storageDirectory = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String elementName = event.asStartElement().getName().getLocalPart();

                    if (elementName.equals("temp-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileElementIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "path");
                        }

                        if (tempDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        tempDirectory = new File(attributePath.getValue());

                        if (tempDirectory.isAbsolute() != true)
                        {
                            tempDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            tempDirectory = tempDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
                        }
                    }
                    else if (elementName.equals("storage-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileElementIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "path");
                        }

                        if (storageDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        storageDirectory = new File(attributePath.getValue());

                        if (storageDirectory.isAbsolute() != true)
                        {
                            storageDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            storageDirectory = storageDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageStorageDirectoryCantGetCanonicalPath", ex, null, storageDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageStorageDirectoryCantGetCanonicalPath", ex, null, storageDirectory.getAbsolutePath());
                        }
                    }
                    else if (elementName.equals("resource") == true)
                    {
                        if (resourceIdentifierStack.empty() != true)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        Attribute attributeIdentifier = event.asStartElement().getAttributeByName(new QName("identifier"));

                        if (attributeIdentifier == null)
                        {
                            throw constructTermination("messageJobFileElementIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "identifier");
                        }

                        resourceIdentifierStack.push(attributeIdentifier.getValue());
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (tempDirectory == null)
        {
            tempDirectory = new File(this.programPath + "temp");
        }

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    throw constructTermination("messageTempDirectoryIsntWritable", null, null, tempDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageTempPathIsntADirectory", null, null, tempDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageTempDirectoryCantCreate", ex, null, tempDirectory.getAbsolutePath());
            }
        }

        if (storageDirectory == null)
        {
            storageDirectory = new File(this.programPath + "storage");
        }

        if (storageDirectory.exists() == true)
        {
            if (storageDirectory.isDirectory() == true)
            {
                if (storageDirectory.canWrite() != true)
                {
                    throw constructTermination("messageStorageDirectoryIsntWritable", null, null, storageDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageStoragePathIsntADirectory", null, null, storageDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                storageDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageStorageDirectoryCantCreate", ex, null, storageDirectory.getAbsolutePath());
            }
        }

        if (resourceIdentifierStack.empty() == true)
        {
            /** @todo Invoke an input edit field GUI component. */
            resourceIdentifierStack.push("http://hyper-augmentation.org");
        }


        boolean cancel = false;

        do
        {
            String contentType = null;

            File resourceFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resource");

            if (resourceFile.exists() == true)
            {
                if (resourceFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = resourceFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (resourceFile.canWrite() != true)
                        {
                            throw constructTermination("messageResourceFileExistsButIsntWritable", null, null, resourceFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageResourcePathExistsButIsntAFile", null, null, resourceFile.getAbsolutePath());
                }
            }

            {
                File storageResourceRetriever1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "storage_resource_retriever_1_resultinfo.xml");

                if (storageResourceRetriever1ResultInfoFile.exists() == true)
                {
                    if (storageResourceRetriever1ResultInfoFile.isFile() == true)
                    {
                        boolean deleteSuccessful = false;

                        try
                        {
                            deleteSuccessful = storageResourceRetriever1ResultInfoFile.delete();
                        }
                        catch (SecurityException ex)
                        {

                        }

                        if (deleteSuccessful != true)
                        {
                            if (storageResourceRetriever1ResultInfoFile.canWrite() != true)
                            {
                                throw constructTermination("messageStorageResourceRetriever1ResultInfoFileExistsButIsntWritable", null, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                            }
                        }
                    }
                    else
                    {
                        throw constructTermination("messageStorageResourceRetriever1ResultInfoPathExistsButIsntAFile", null, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                    }
                }

                File storageResourceRetriever1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "storage_resource_retriever_1_jobfile.xml");

                if (storageResourceRetriever1JobFile.exists() == true)
                {
                    if (storageResourceRetriever1JobFile.isFile() == true)
                    {
                        boolean deleteSuccessful = false;

                        try
                        {
                            deleteSuccessful = storageResourceRetriever1JobFile.delete();
                        }
                        catch (SecurityException ex)
                        {

                        }

                        if (deleteSuccessful != true)
                        {
                            if (storageResourceRetriever1JobFile.canWrite() != true)
                            {
                                throw constructTermination("messageStorageResourceRetriever1JobFileExistsButIsntWritable", null, null, storageResourceRetriever1JobFile.getAbsolutePath());
                            }
                        }
                    }
                    else
                    {
                        throw constructTermination("messageStorageResourceRetriever1JobPathExistsButIsntAFile", null, null, storageResourceRetriever1JobFile.getAbsolutePath());
                    }
                }


                try
                {
                    BufferedWriter writer = new BufferedWriter(
                                            new OutputStreamWriter(
                                            new FileOutputStream(storageResourceRetriever1JobFile),
                                            "UTF-8"));

                    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                    writer.write("<!-- This file was created by htx_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                    writer.write("<storage-resource-retriever-1-workflow-job>\n");
                    writer.write("  <resources>\n");
                    writer.write("    <resource identifier=\"");

                    String identifierEscaped = resourceIdentifierStack.peek();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    identifierEscaped = identifierEscaped.replaceAll("&", "&amp;");
                    identifierEscaped = identifierEscaped.replaceAll("<", "&lt;");
                    identifierEscaped = identifierEscaped.replaceAll(">", "&gt;");
                    //identifierEscaped = identifierEscaped.replaceAll("\"", "&quot;");
                    identifierEscaped = identifierEscaped.replaceAll("'", "&apos;");

                    writer.write(identifierEscaped);
                    writer.write("\" output-file-path=\"" + resourceFile.getAbsolutePath() + "\"/>\n");
                    writer.write("  </resources>\n");
                    writer.write("  <temp-directory path=\"" + tempDirectory.getAbsolutePath() + "\"/>\n");
                    writer.write("  <storage-directory path=\"" + storageDirectory.getAbsolutePath() + "\"/>\n");
                    writer.write("</storage-resource-retriever-1-workflow-job>\n");

                    writer.flush();
                    writer.close();
                }
                catch (FileNotFoundException ex)
                {
                    throw constructTermination("messageStorageResourceRetriever1JobFileWritingError", ex, null, storageResourceRetriever1JobFile.getAbsolutePath());
                }
                catch (UnsupportedEncodingException ex)
                {
                    throw constructTermination("messageStorageResourceRetriever1JobFileWritingError", ex, null, storageResourceRetriever1JobFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageStorageResourceRetriever1JobFileWritingError", ex, null, storageResourceRetriever1JobFile.getAbsolutePath());
                }


                ProcessBuilder builder = new ProcessBuilder("java", "storage_resource_retriever_1", storageResourceRetriever1JobFile.getAbsolutePath(), storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                builder.directory(new File(this.programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "workflows" + File.separator + "storage_resource_retriever" + File.separator + "storage_resource_retriever_1"));
                builder.redirectErrorStream(true);

                try
                {
                    Process process = builder.start();
                    Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                    while (scanner.hasNext() == true)
                    {
                        System.out.println(scanner.next());
                    }

                    scanner.close();
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageStorageResourceRetriever1ErrorWhileReadingOutput", ex, null);
                }


                if (storageResourceRetriever1ResultInfoFile.exists() != true)
                {
                    throw constructTermination("messageStorageResourceRetriever1ResultInfoFileDoesntExistButShould", null, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                }

                if (storageResourceRetriever1ResultInfoFile.isFile() != true)
                {
                    throw constructTermination("messageStorageResourceRetriever1ResultInfoPathExistsButIsntAFile", null, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                }

                if (storageResourceRetriever1ResultInfoFile.canRead() != true)
                {
                    throw constructTermination("messageStorageResourceRetriever1ResultInfoFileIsntReadable", null, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                }

                boolean wasSuccess = false;

                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    InputStream in = new FileInputStream(storageResourceRetriever1ResultInfoFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                    while (eventReader.hasNext() == true)
                    {
                        XMLEvent event = eventReader.nextEvent();

                        if (event.isStartElement() == true)
                        {
                            String elementName = event.asStartElement().getName().getLocalPart();

                            if (elementName.equals("success") == true)
                            {
                                wasSuccess = true;
                            }
                            else if (elementName.equals("retrieved-resource") == true)
                            {
                                if (contentType != null)
                                {
                                    throw constructTermination("messageStorageResourceRetriever1ResultInfoFileElementConfiguredMoreThanOnce", null, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath(), elementName);
                                }

                                Attribute attributeContentType = event.asStartElement().getAttributeByName(new QName("content-type"));

                                if (attributeContentType == null)
                                {
                                    throw constructTermination("messageStorageResourceRetriever1ResultInfoFileElementIsMissingAnAttribute", null, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath(), elementName, "path");
                                }

                                contentType = attributeContentType.getValue();
                            }
                        }
                    }
                }
                catch (XMLStreamException ex)
                {
                    throw constructTermination("messageStorageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageStorageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageStorageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, storageResourceRetriever1ResultInfoFile.getAbsolutePath());
                }

                if (wasSuccess != true)
                {
                    throw constructTermination("messageStorageResourceRetriever1CallWasntSuccessful", null, null);
                }
            }

            if (contentType == null)
            {
                throw constructTermination("messageStorageResourceRetriever1NoContentTypeProvided", null, null);
            }

            if (contentType.equals("xml") == true)
            {
                Set<String> namespaces = new HashSet<String>();

                {
                    File xmlNamespacesFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resource_xml_namespaces.xml");

                    if (xmlNamespacesFile.exists() == true)
                    {
                        if (xmlNamespacesFile.isFile() == true)
                        {
                            boolean deleteSuccessful = false;

                            try
                            {
                                deleteSuccessful = xmlNamespacesFile.delete();
                            }
                            catch (SecurityException ex)
                            {

                            }

                            if (deleteSuccessful != true)
                            {
                                if (xmlNamespacesFile.canWrite() != true)
                                {
                                    throw constructTermination("messageXmlNamespacesFileExistsButIsntWritable", null, null, xmlNamespacesFile.getAbsolutePath());
                                }
                            }
                        }
                        else
                        {
                            throw constructTermination("messageXmlNamespacesPathExistsButIsntAFile", null, null, xmlNamespacesFile.getAbsolutePath());
                        }
                    }

                    File xmlNamespaceExtractor1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "xml_namespace_extractor_1_resultinfo.xml");

                    if (xmlNamespaceExtractor1ResultInfoFile.exists() == true)
                    {
                        if (xmlNamespaceExtractor1ResultInfoFile.isFile() == true)
                        {
                            boolean deleteSuccessful = false;

                            try
                            {
                                deleteSuccessful = xmlNamespaceExtractor1ResultInfoFile.delete();
                            }
                            catch (SecurityException ex)
                            {

                            }

                            if (deleteSuccessful != true)
                            {
                                if (xmlNamespaceExtractor1ResultInfoFile.canWrite() != true)
                                {
                                    throw constructTermination("messageXmlNamespaceExtractor1ResultInfoFileExistsButIsntWritable", null, null, xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                                }
                            }
                        }
                        else
                        {
                            throw constructTermination("messageXmlNamespaceExtractor1ResultInfoPathExistsButIsntAFile", null, null, xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                        }
                    }

                    File xmlNamespaceExtractor1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "xml_namespace_extractor_1_jobfile.xml");

                    if (xmlNamespaceExtractor1JobFile.exists() == true)
                    {
                        if (xmlNamespaceExtractor1JobFile.isFile() == true)
                        {
                            boolean deleteSuccessful = false;

                            try
                            {
                                deleteSuccessful = xmlNamespaceExtractor1JobFile.delete();
                            }
                            catch (SecurityException ex)
                            {

                            }

                            if (deleteSuccessful != true)
                            {
                                if (xmlNamespaceExtractor1JobFile.canWrite() != true)
                                {
                                    throw constructTermination("messageXmlNamespaceExtractor1JobFileExistsButIsntWritable", null, null, xmlNamespaceExtractor1JobFile.getAbsolutePath());
                                }
                            }
                        }
                        else
                        {
                            throw constructTermination("messageXmlNamespaceExtractor1JobPathExistsButIsntAFile", null, null, xmlNamespaceExtractor1JobFile.getAbsolutePath());
                        }
                    }

                    try
                    {
                        BufferedWriter writer = new BufferedWriter(
                                                new OutputStreamWriter(
                                                new FileOutputStream(xmlNamespaceExtractor1JobFile),
                                                "UTF-8"));

                        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                        writer.write("<!-- This file was created by htx_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                        writer.write("<xml-namespace-extractor-1-job>\n");
                        writer.write("  <input-file path=\"" + resourceFile.getAbsolutePath() + "\"/>\n");
                        writer.write("  <output-file path=\"" + xmlNamespacesFile.getAbsolutePath() + "\"/>\n");
                        writer.write("</xml-namespace-extractor-1-job>\n");

                        writer.flush();
                        writer.close();
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1JobFileWritingError", ex, null, xmlNamespaceExtractor1JobFile.getAbsolutePath());
                    }
                    catch (UnsupportedEncodingException ex)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1JobFileWritingError", ex, null, xmlNamespaceExtractor1JobFile.getAbsolutePath());
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1JobFileWritingError", ex, null, xmlNamespaceExtractor1JobFile.getAbsolutePath());
                    }


                    ProcessBuilder builder = new ProcessBuilder("java", "xml_namespace_extractor_1", xmlNamespaceExtractor1JobFile.getAbsolutePath(), xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                    builder.directory(new File(this.programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_namespace_extractor" + File.separator + "xml_namespace_extractor_1"));
                    builder.redirectErrorStream(true);

                    try
                    {
                        Process process = builder.start();
                        Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                        while (scanner.hasNext() == true)
                        {
                            System.out.println(scanner.next());
                        }

                        scanner.close();
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1ErrorWhileReadingOutput", ex, null);
                    }

                    if (xmlNamespaceExtractor1ResultInfoFile.exists() != true)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1ResultInfoFileDoesntExistButShould", null, null, xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                    }

                    if (xmlNamespaceExtractor1ResultInfoFile.isFile() != true)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1ResultInfoPathExistsButIsntAFile", null, null, xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                    }

                    if (xmlNamespaceExtractor1ResultInfoFile.canRead() != true)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1ResultInfoFileIsntReadable", null, null, xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                    }

                    boolean wasSuccess = false;

                    try
                    {
                        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                        InputStream in = new FileInputStream(xmlNamespaceExtractor1ResultInfoFile);
                        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                        while (eventReader.hasNext() == true)
                        {
                            XMLEvent event = eventReader.nextEvent();

                            if (event.isStartElement() == true)
                            {
                                String elementName = event.asStartElement().getName().getLocalPart();

                                if (elementName.equals("success") == true)
                                {
                                    wasSuccess = true;
                                    break;
                                }
                            }
                        }
                    }
                    catch (XMLStreamException ex)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1ResultInfoFileErrorWhileReading", ex, null, xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                    }
                    catch (SecurityException ex)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1ResultInfoFileErrorWhileReading", ex, null, xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1ResultInfoFileErrorWhileReading", ex, null, xmlNamespaceExtractor1ResultInfoFile.getAbsolutePath());
                    }

                    if (wasSuccess != true)
                    {
                        throw constructTermination("messageXmlNamespaceExtractor1CallWasntSuccessful", null, null);
                    }

                    if (xmlNamespacesFile.exists() != true)
                    {
                        throw constructTermination("messageXmlNamespacesFileDoesntExist", null, null, xmlNamespacesFile.getAbsolutePath(), xmlNamespaceExtractor1JobFile.getAbsolutePath());
                    }

                    if (xmlNamespacesFile.isFile() != true)
                    {
                        throw constructTermination("messageXmlNamespacesPathExistsButIsntAFile", null, null, xmlNamespacesFile.getAbsolutePath(), xmlNamespaceExtractor1JobFile.getAbsolutePath());
                    }

                    if (xmlNamespacesFile.canRead() != true)
                    {
                        throw constructTermination("messageXmlNamespacesFileIsntReadable", null, null, xmlNamespacesFile.getAbsolutePath(), xmlNamespaceExtractor1JobFile.getAbsolutePath());
                    }

                    try
                    {
                        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                        InputStream in = new FileInputStream(xmlNamespacesFile);
                        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                        while (eventReader.hasNext() == true)
                        {
                            XMLEvent event = eventReader.nextEvent();

                            if (event.isStartElement() == true)
                            {
                                String elementName = event.asStartElement().getName().getLocalPart();

                                if (elementName.equals("namespace") == true)
                                {
                                    Attribute attributeId = event.asStartElement().getAttributeByName(new QName("id"));

                                    if (attributeId == null)
                                    {
                                        throw constructTermination("messageXmlNamespacesFileElementIsMissingAnAttribute", null, null, xmlNamespacesFile.getAbsolutePath(), elementName, "id");
                                    }

                                    namespaces.add(attributeId.getValue());
                                }
                            }
                        }
                    }
                    catch (XMLStreamException ex)
                    {
                        throw constructTermination("messageXmlNamespacesFileErrorWhileReading", ex, null, xmlNamespacesFile.getAbsolutePath());
                    }
                    catch (SecurityException ex)
                    {
                        throw constructTermination("messageXmlNamespacesFileErrorWhileReading", ex, null, xmlNamespacesFile.getAbsolutePath());
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageXmlNamespacesFileErrorWhileReading", ex, null, xmlNamespacesFile.getAbsolutePath());
                    }
                }

                boolean found = false;

                for (String namespace : namespaces)
                {
                    if (namespace.equals("http://www.daisy.org/z3986/2005/ncx/") == true)
                    {
                        found = true;

                        String resourceIdentifier = null;

                        File listPicker1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "list_picker_1_jobfile.xml");

                        if (listPicker1JobFile.exists() == true)
                        {
                            if (listPicker1JobFile.isFile() == true)
                            {
                                boolean deleteSuccessful = false;

                                try
                                {
                                    deleteSuccessful = listPicker1JobFile.delete();
                                }
                                catch (SecurityException ex)
                                {

                                }

                                if (deleteSuccessful != true)
                                {
                                    if (listPicker1JobFile.canWrite() != true)
                                    {
                                        throw constructTermination("messageListPicker1JobFileExistsButIsntWritable", null, null, listPicker1JobFile.getAbsolutePath());
                                    }
                                }
                            }
                            else
                            {
                                throw constructTermination("messageListPicker1JobPathExistsButIsntAFile", null, null, listPicker1JobFile.getAbsolutePath());
                            }
                        }

                        File stylesheetFile = new File(this.programPath + File.separator + "transformations" + File.separator + "ncx_to_list_picker_1_jobfile.xsl");

                        if (transformXml(resourceFile, tempDirectory, stylesheetFile, listPicker1JobFile) != 0)
                        {
                            cancel = true;
                            break;
                        }

                        if (listPicker1JobFile.exists() != true)
                        {
                            throw constructTermination("messageListPicker1JobFileDoesntExist", null, null, listPicker1JobFile.getAbsolutePath());
                        }

                        if (listPicker1JobFile.isFile() != true)
                        {
                            throw constructTermination("messageListPicker1JobPathIsntAFile", null, null, listPicker1JobFile.getAbsolutePath());
                        }

                        if (listPicker1JobFile.canRead() != true)
                        {
                            throw constructTermination("messageListPicker1JobFileIsntReadable", null, null, listPicker1JobFile.getAbsolutePath());
                        }

                        File listPicker1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "list_picker_1_resultinfo.xml");

                        if (listPicker1ResultInfoFile.exists() == true)
                        {
                            if (listPicker1ResultInfoFile.isFile() == true)
                            {
                                boolean deleteSuccessful = false;

                                try
                                {
                                    deleteSuccessful = listPicker1ResultInfoFile.delete();
                                }
                                catch (SecurityException ex)
                                {

                                }

                                if (deleteSuccessful != true)
                                {
                                    if (listPicker1ResultInfoFile.canWrite() != true)
                                    {
                                        throw constructTermination("messageListPicker1ResultInfoFileExistsButIsntWritable", null, null, listPicker1ResultInfoFile.getAbsolutePath());
                                    }
                                }
                            }
                            else
                            {
                                throw constructTermination("messageListPicker1ResultInfoPathExistsButIsntAFile", null, null, listPicker1ResultInfoFile.getAbsolutePath());
                            }
                        }

                        ProcessBuilder builder = new ProcessBuilder("java", "list_picker_1", listPicker1JobFile.getAbsolutePath(), listPicker1ResultInfoFile.getAbsolutePath());
                        builder.directory(new File(this.programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "list_picker" + File.separator + "list_picker_1"));
                        builder.redirectErrorStream(true);

                        try
                        {
                            Process process = builder.start();
                            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                            while (scanner.hasNext() == true)
                            {
                                System.out.println(scanner.next());
                            }

                            scanner.close();
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageListPicker1ErrorWhileReadingOutput", ex, null);
                        }

                        if (listPicker1ResultInfoFile.exists() != true)
                        {
                            throw constructTermination("messageListPicker1ResultInfoFileDoesntExistButShould", null, null, listPicker1ResultInfoFile.getAbsolutePath());
                        }

                        if (listPicker1ResultInfoFile.isFile() != true)
                        {
                            throw constructTermination("messageListPicker1ResultInfoPathExistsButIsntAFile", null, null, listPicker1ResultInfoFile.getAbsolutePath());
                        }

                        if (listPicker1ResultInfoFile.canRead() != true)
                        {
                            throw constructTermination("messageListPicker1ResultInfoFileIsntReadable", null, null, listPicker1ResultInfoFile.getAbsolutePath());
                        }

                        boolean wasSuccess = false;

                        try
                        {
                            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                            InputStream in = new FileInputStream(listPicker1ResultInfoFile);
                            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                            while (eventReader.hasNext() == true)
                            {
                                XMLEvent event = eventReader.nextEvent();

                                if (event.isStartElement() == true)
                                {
                                    String elementName = event.asStartElement().getName().getLocalPart();

                                    if (elementName.equals("success") == true)
                                    {
                                        wasSuccess = true;
                                    }
                                    else if (elementName.equals("selected-option-id") == true)
                                    {
                                        if (resourceIdentifier != null)
                                        {
                                            throw constructTermination("messageListPicker1ResultInfoFileElementConfiguredMoreThanOnce", null, null, listPicker1ResultInfoFile.getAbsolutePath(), elementName);
                                        }

                                        StringBuilder idBuffer = new StringBuilder();

                                        /** @todo What if no hasNext()? Handle. */
                                        while (eventReader.hasNext() == true)
                                        {
                                            event = eventReader.nextEvent();

                                            if (event.isCharacters() == true)
                                            {
                                                idBuffer.append(event.asCharacters().getData());
                                            }
                                            else if (event.isEndElement() == true)
                                            {
                                                break;
                                            }
                                        }

                                        resourceIdentifier = idBuffer.toString();
                                    }
                                }
                            }
                        }
                        catch (XMLStreamException ex)
                        {
                            throw constructTermination("messageListPicker1ResultInfoFileErrorWhileReading", ex, null, listPicker1ResultInfoFile.getAbsolutePath());
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageListPicker1ResultInfoFileErrorWhileReading", ex, null, listPicker1ResultInfoFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageListPicker1ResultInfoFileErrorWhileReading", ex, null, listPicker1ResultInfoFile.getAbsolutePath());
                        }

                        if (wasSuccess != true)
                        {
                            throw constructTermination("messageListPicker1CallWasntSuccessful", null, null);
                        }

                        if (resourceIdentifier != null)
                        {
                            resourceIdentifierStack.push(resourceIdentifier);
                        }
                        else
                        {
                            cancel = true;
                        }

                        break;
                    }
                    else if (namespace.equals("http://www.w3.org/1999/xhtml") == true)
                    {
                        found = true;

                        String resourceIdentifier = resourceIdentifierStack.pop();

                        File tempViewerDirectory = new File(tempDirectory.getAbsolutePath() + File.separator + "viewer");

                        if (tempViewerDirectory.exists() == true)
                        {
                            if (tempViewerDirectory.isDirectory() == true)
                            {
                                if (tempViewerDirectory.canWrite() != true)
                                {
                                    throw constructTermination("messageTempViewerDirectoryIsntWritable", null, null, tempViewerDirectory.getAbsolutePath());
                                }
                            }
                            else
                            {
                                throw constructTermination("messageTempViewerPathIsntADirectory", null, null, tempViewerDirectory.getAbsolutePath());
                            }
                        }
                        else
                        {
                            try
                            {
                                tempViewerDirectory.mkdirs();
                            }
                            catch (SecurityException ex)
                            {
                                throw constructTermination("messageTempViewerDirectoryCantCreate", ex, null, tempViewerDirectory.getAbsolutePath());
                            }
                        }

                        File viewer3JobFile = new File(tempViewerDirectory.getAbsolutePath() + File.separator + "viewer_3_jobfile.xml");

                        if (viewer3JobFile.exists() == true)
                        {
                            if (viewer3JobFile.isFile() == true)
                            {
                                boolean deleteSuccessful = false;

                                try
                                {
                                    deleteSuccessful = viewer3JobFile.delete();
                                }
                                catch (SecurityException ex)
                                {

                                }

                                if (deleteSuccessful != true)
                                {
                                    if (viewer3JobFile.canWrite() != true)
                                    {
                                        throw constructTermination("messageViewer3JobFileExistsButIsntWritable", null, null, viewer3JobFile.getAbsolutePath());
                                    }
                                }
                            }
                            else
                            {
                                throw constructTermination("messageViewer3JobPathExistsButIsntAFile", null, null, viewer3JobFile.getAbsolutePath());
                            }
                        }

                        File viewer3ResultInfoFile = new File(tempViewerDirectory.getAbsolutePath() + File.separator + "viewer_3_resultinfo.xml");

                        if (viewer3ResultInfoFile.exists() == true)
                        {
                            if (viewer3ResultInfoFile.isFile() == true)
                            {
                                boolean deleteSuccessful = false;

                                try
                                {
                                    deleteSuccessful = viewer3ResultInfoFile.delete();
                                }
                                catch (SecurityException ex)
                                {

                                }

                                if (deleteSuccessful != true)
                                {
                                    if (viewer3ResultInfoFile.canWrite() != true)
                                    {
                                        throw constructTermination("messageViewer3ResultInfoFileExistsButIsntWritable", null, null, viewer3ResultInfoFile.getAbsolutePath());
                                    }
                                }
                            }
                            else
                            {
                                throw constructTermination("messageViewer3ResultInfoPathExistsButIsntAFile", null, null, viewer3ResultInfoFile.getAbsolutePath());
                            }
                        }

                        try
                        {
                            BufferedWriter writer = new BufferedWriter(
                                                    new OutputStreamWriter(
                                                    new FileOutputStream(viewer3JobFile),
                                                    "UTF-8"));

                            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                            writer.write("<!-- This file was created by htx_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                            writer.write("<viewer-3-jobfile>\n");
                            writer.write("  <temp-directory path=\"" + tempViewerDirectory.getAbsolutePath() + "\"/>\n");
                            writer.write("  <storage-directory path=\"" + storageDirectory.getAbsolutePath() + "\"/>\n");
                            writer.write("  <input identifier=\"");

                            String identifierEscaped = resourceIdentifier;

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            identifierEscaped = identifierEscaped.replaceAll("&", "&amp;");
                            identifierEscaped = identifierEscaped.replaceAll("<", "&lt;");
                            identifierEscaped = identifierEscaped.replaceAll(">", "&gt;");
                            //identifierEscaped = identifierEscaped.replaceAll("\"", "&quot;");
                            identifierEscaped = identifierEscaped.replaceAll("'", "&apos;");

                            writer.write(identifierEscaped);
                            writer.write("\"/>\n");
                            writer.write("  <view initial=\"text\"/>\n");
                            writer.write("</viewer-3-jobfile>\n");

                            writer.flush();
                            writer.close();
                        }
                        catch (FileNotFoundException ex)
                        {
                            throw constructTermination("messageViewer3JobFileWritingError", ex, null, viewer3JobFile.getAbsolutePath());
                        }
                        catch (UnsupportedEncodingException ex)
                        {
                            throw constructTermination("messageViewer3JobFileWritingError", ex, null, viewer3JobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageViewer3JobFileWritingError", ex, null, viewer3JobFile.getAbsolutePath());
                        }

                        ProcessBuilder builder = new ProcessBuilder("java", "viewer_3", viewer3JobFile.getAbsolutePath(), viewer3ResultInfoFile.getAbsolutePath());
                        builder.directory(new File(this.programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "viewer" + File.separator + "viewer_3"));
                        builder.redirectErrorStream(true);

                        try
                        {
                            Process process = builder.start();
                            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                            while (scanner.hasNext() == true)
                            {
                                System.out.println(scanner.next());
                            }

                            scanner.close();
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageViewer3ErrorWhileReadingOutput", ex, null);
                        }

                        if (viewer3ResultInfoFile.exists() != true)
                        {
                            throw constructTermination("messageViewer3ResultInfoFileDoesntExistButShould", null, null, viewer3ResultInfoFile.getAbsolutePath());
                        }

                        if (viewer3ResultInfoFile.isFile() != true)
                        {
                            throw constructTermination("messageViewer3ResultInfoPathExistsButIsntAFile", null, null, viewer3ResultInfoFile.getAbsolutePath());
                        }

                        if (viewer3ResultInfoFile.canRead() != true)
                        {
                            throw constructTermination("messageViewer3ResultInfoFileIsntReadable", null, null, viewer3ResultInfoFile.getAbsolutePath());
                        }

                        boolean wasSuccess = false;

                        try
                        {
                            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                            InputStream in = new FileInputStream(viewer3ResultInfoFile);
                            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                            while (eventReader.hasNext() == true)
                            {
                                XMLEvent event = eventReader.nextEvent();

                                if (event.isStartElement() == true)
                                {
                                    String elementName = event.asStartElement().getName().getLocalPart();

                                    if (elementName.equals("success") == true)
                                    {
                                        wasSuccess = true;
                                        break;
                                    }
                                }
                            }
                        }
                        catch (XMLStreamException ex)
                        {
                            throw constructTermination("messageViewer3ResultInfoFileErrorWhileReading", ex, null, viewer3ResultInfoFile.getAbsolutePath());
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageViewer3ResultInfoFileErrorWhileReading", ex, null, viewer3ResultInfoFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageViewer3ResultInfoFileErrorWhileReading", ex, null, viewer3ResultInfoFile.getAbsolutePath());
                        }

                        if (wasSuccess != true)
                        {
                            throw constructTermination("messageViewer3CallWasntSuccessful", null, null);
                        }

                        break;
                    }
                    else if (namespace.equals("http://graphml.graphdrawing.org/xmlns") == true)
                    {
                        found = true;

                        String resourceIdentifier = resourceIdentifierStack.pop();

                        File hyperdex1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "hyperdex_1_jobfile.xml");

                        if (hyperdex1JobFile.exists() == true)
                        {
                            if (hyperdex1JobFile.isFile() == true)
                            {
                                boolean deleteSuccessful = false;

                                try
                                {
                                    deleteSuccessful = hyperdex1JobFile.delete();
                                }
                                catch (SecurityException ex)
                                {

                                }

                                if (deleteSuccessful != true)
                                {
                                    if (hyperdex1JobFile.canWrite() != true)
                                    {
                                        throw constructTermination("messageHyperdex1JobFileExistsButIsntWritable", null, null, hyperdex1JobFile.getAbsolutePath());
                                    }
                                }
                            }
                            else
                            {
                                throw constructTermination("messageHyperdex1JobPathExistsButIsntAFile", null, null, hyperdex1JobFile.getAbsolutePath());
                            }
                        }

                        File hyperdex1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "hyperdex_1_resultinfo.xml");

                        if (hyperdex1ResultInfoFile.exists() == true)
                        {
                            if (hyperdex1ResultInfoFile.isFile() == true)
                            {
                                boolean deleteSuccessful = false;

                                try
                                {
                                    deleteSuccessful = hyperdex1ResultInfoFile.delete();
                                }
                                catch (SecurityException ex)
                                {

                                }

                                if (deleteSuccessful != true)
                                {
                                    if (hyperdex1ResultInfoFile.canWrite() != true)
                                    {
                                        throw constructTermination("messageHyperdex1ResultInfoFileExistsButIsntWritable", null, null, hyperdex1ResultInfoFile.getAbsolutePath());
                                    }
                                }
                            }
                            else
                            {
                                throw constructTermination("messageHyperdex1ResultInfoPathExistsButIsntAFile", null, null, hyperdex1ResultInfoFile.getAbsolutePath());
                            }
                        }

                        try
                        {
                            BufferedWriter writer = new BufferedWriter(
                                                    new OutputStreamWriter(
                                                    new FileOutputStream(hyperdex1JobFile),
                                                    "UTF-8"));

                            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                            writer.write("<!-- This file was created by htx_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n"); 
                            writer.write("<hyperdex-1-jobfile>\n");
                            writer.write("  <input-file path=\"" + resourceFile.getAbsolutePath() + "\"/>\n");
                            writer.write("  <output-file path=\"" + (tempDirectory.getAbsolutePath() + File.separator + "hyperdex_1_output.xml") + "\"/>\n");
                            writer.write("</hyperdex-1-jobfile>\n");
                            writer.flush();
                            writer.close();
                        }
                        catch (FileNotFoundException ex)
                        {
                            throw constructTermination("messageHyperdex1JobFileWritingError", ex, null, hyperdex1JobFile.getAbsolutePath());
                        }
                        catch (UnsupportedEncodingException ex)
                        {
                            throw constructTermination("messageHyperdex1JobFileWritingError", ex, null, hyperdex1JobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageHyperdex1JobFileWritingError", ex, null, hyperdex1JobFile.getAbsolutePath());
                        }

                        ProcessBuilder builder = new ProcessBuilder("java", "hyperdex_1", hyperdex1JobFile.getAbsolutePath(), hyperdex1ResultInfoFile.getAbsolutePath());
                        builder.directory(new File(this.programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "hyperdex" + File.separator + "hyperdex_1"));
                        builder.redirectErrorStream(true);

                        try
                        {
                            Process process = builder.start();
                            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                            while (scanner.hasNext() == true)
                            {
                                System.out.println(scanner.next());
                            }

                            scanner.close();
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageHyperdex1ErrorWhileReadingOutput", ex, null);
                        }

                        if (hyperdex1ResultInfoFile.exists() != true)
                        {
                            throw constructTermination("messageHyperdex1ResultInfoFileDoesntExistButShould", null, null, hyperdex1ResultInfoFile.getAbsolutePath());
                        }

                        if (hyperdex1ResultInfoFile.isFile() != true)
                        {
                            throw constructTermination("messageHyperdex1ResultInfoPathExistsButIsntAFile", null, null, hyperdex1ResultInfoFile.getAbsolutePath());
                        }

                        if (hyperdex1ResultInfoFile.canRead() != true)
                        {
                            throw constructTermination("messageHyperdex1ResultInfoFileIsntReadable", null, null, hyperdex1ResultInfoFile.getAbsolutePath());
                        }

                        boolean wasSuccess = false;

                        try
                        {
                            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                            InputStream in = new FileInputStream(hyperdex1ResultInfoFile);
                            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                            while (eventReader.hasNext() == true)
                            {
                                XMLEvent event = eventReader.nextEvent();

                                if (event.isStartElement() == true)
                                {
                                    String elementName = event.asStartElement().getName().getLocalPart();

                                    if (elementName.equals("success") == true)
                                    {
                                        wasSuccess = true;
                                        break;
                                    }
                                }
                            }
                        }
                        catch (XMLStreamException ex)
                        {
                            throw constructTermination("messageHyperdex1ResultInfoFileErrorWhileReading", ex, null, hyperdex1ResultInfoFile.getAbsolutePath());
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageHyperdex1ResultInfoFileErrorWhileReading", ex, null, hyperdex1ResultInfoFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageHyperdex1ResultInfoFileErrorWhileReading", ex, null, hyperdex1ResultInfoFile.getAbsolutePath());
                        }

                        if (wasSuccess != true)
                        {
                            throw constructTermination("messageHyperdex1CallWasntSuccessful", null, null);
                        }

                        break;
                    }
                    else
                    {
                        this.infoMessages.add(constructInfoMessage("messageXmlNamespaceNotSupported", true, null, null, namespace, resourceIdentifierStack.peek()));
                    }
                }

                if (found != true)
                {
                    throw constructTermination("messageNoSupportedXmlNamespaceInResource", null, null, resourceIdentifierStack.peek());
                }
            }
            else
            {
                throw constructTermination("messageContentTypeNotSupported", null, null, contentType, resourceIdentifierStack.peek());
            }

            if (cancel == true)
            {
                break;
            }

            if (resourceIdentifierStack.empty() == true)
            {
                break;
            }

        } while (true);

        return 0;
    }


    protected int transformXml(File xmlInputFile, File tempDirectory, File stylesheetFile, File xmlOutputFile)
    {
        File xmlXsltTransformator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "xml_xslt_transformator_1_jobfile.xml");

        if (xmlXsltTransformator1JobFile.exists() == true)
        {
            if (xmlXsltTransformator1JobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlXsltTransformator1JobFile.delete();
                }
                catch (SecurityException ex)
                {
                }

                if (deleteSuccessful != true)
                {
                    if (xmlXsltTransformator1JobFile.canWrite() != true)
                    {
                        throw constructTermination("messageXmlXsltTransformator1JobFileExistsButIsntWritable", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlXsltTransformator1JobPathExistsButIsntAFile", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
        }

        File xmlXsltTransformator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "xml_xslt_transformator_1_resultinfo.xml");

        if (xmlXsltTransformator1ResultInfoFile.exists() == true)
        {
            if (xmlXsltTransformator1ResultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlXsltTransformator1ResultInfoFile.delete();
                }
                catch (SecurityException ex)
                {
                }

                if (deleteSuccessful != true)
                {
                    if (xmlXsltTransformator1ResultInfoFile.canWrite() != true)
                    {
                        throw constructTermination("messageXmlXsltTransformator1ResultInfoFileExistsButIsntWritable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlXsltTransformator1ResultInfoPathIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
        }

        if (xmlOutputFile.exists() == true)
        {
            if (xmlOutputFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlOutputFile.delete();
                }
                catch (SecurityException ex)
                {
                }

                if (deleteSuccessful != true)
                {
                    if (xmlOutputFile.canWrite() != true)
                    {
                        throw constructTermination("messageXmlOutputFileIsntWritable", null, null, xmlOutputFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlOutputPathIsntAFile", null, null, xmlOutputFile.getAbsolutePath());
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(xmlXsltTransformator1JobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by htx_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
            writer.write("<xml-xslt-transformator-1-jobfile>\n");
            writer.write("  <job input-file=\"" + xmlInputFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + this.programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_empty.xml\" stylesheet-file=\"" + stylesheetFile.getAbsolutePath() + "\" output-file=\"" + xmlOutputFile.getAbsolutePath() + "\"/>\n");
            writer.write("</xml-xslt-transformator-1-jobfile>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }

        ProcessBuilder builder = new ProcessBuilder("java", "xml_xslt_transformator_1", xmlXsltTransformator1JobFile.getAbsolutePath(), xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        builder.directory(new File(this.programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");
            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }
            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ErrorWhileReadingOutput", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }

        if (xmlXsltTransformator1ResultInfoFile.exists() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileDoesntExistButShould", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (xmlXsltTransformator1ResultInfoFile.isFile() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoPathIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (xmlXsltTransformator1ResultInfoFile.canRead() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileIsntReadable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(xmlXsltTransformator1ResultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (wasSuccess != true)
        {
            throw constructTermination("messageXmlXsltTransformator1CallWasntSuccessful", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "htx_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "htx_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "htx_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "htx_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (htx_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(htx_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by htx_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<htx-1-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</htx-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        htx_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private String programPath = null;
    private static final String L10N_BUNDLE = "l10n.l10nHtx1Console";
    private ResourceBundle l10nConsole;
}
