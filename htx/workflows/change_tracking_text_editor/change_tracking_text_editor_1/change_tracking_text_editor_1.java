/* Copyright (C) 2016-2023 Stephan Kreutzer
 *
 * This file is part of change_tracking_text_editor_1 workflow, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_tracking_text_editor_1 workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_tracking_text_editor_1 workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_tracking_text_editor_1 workflow. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/change_tracking_text_editor_1.java
 * @brief Manages of how to call change_tracking_text_editor_1 and handle the files.
 * @author Stephan Kreutzer
 * @since 2019-04-21
 */



import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.util.Scanner;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;



public class change_tracking_text_editor_1
{
    public static void main(String[] args)
    {
        System.out.print("change_tracking_text_editor_1 workflow Copyright (C) 2016-2023 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://hypertext-systems.org.\n\n");

        change_tracking_text_editor_1 instance = new change_tracking_text_editor_1();

        try
        {
            instance.Run(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            File outputFile = instance.GetOutputFile();
            File outputPlaintextFile = instance.GetOutputPlaintextFile();
            File outputSessionFile = instance.GetOutputSessionFile();
            File outputConcatenatedFile = instance.GetOutputConcatenatedFile();
            File outputOptimizedFile = instance.GetOutputOptimizedFile();
            File outputXhtmlFile = instance.GetOutputXhtmlFile();
            File outputConcatenatedXhtmlFile = instance.GetOutputConcatenatedXhtmlFile();
            File outputOptimizedXhtmlFile = instance.GetOutputOptimizedXhtmlFile();

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_tracking_text_editor1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<change-tracking-text-editor-1-workflow-result-information>\n");
                writer.write("  <success>\n");
                writer.write("    <output-files>\n");

                if (outputFile != null)
                {
                    writer.write("      <output-file path=\"" + outputFile.getAbsolutePath() + "\"/>\n");
                }

                if (outputPlaintextFile != null)
                {
                    writer.write("      <output-plaintext-file path=\"" + outputPlaintextFile.getAbsolutePath() + "\"/>\n");
                }

                if (outputSessionFile != null)
                {
                    writer.write("      <output-session-file path=\"" + outputSessionFile.getAbsolutePath() + "\"/>\n");
                }

                if (outputConcatenatedFile != null)
                {
                    writer.write("      <output-concatenated-file path=\"" + outputConcatenatedFile.getAbsolutePath() + "\"/>\n");
                }

                if (outputOptimizedFile != null)
                {
                    writer.write("      <output-optimized-file path=\"" + outputOptimizedFile.getAbsolutePath() + "\"/>\n");
                }

                if (outputXhtmlFile != null)
                {
                    writer.write("      <output-xhtml-file path=\"" + outputXhtmlFile.getAbsolutePath() + "\"/>\n");
                }

                if (outputConcatenatedXhtmlFile != null)
                {
                    writer.write("      <output-concatenated-xhtml-file path=\"" + outputConcatenatedXhtmlFile.getAbsolutePath() + "\"/>\n");
                }

                if (outputOptimizedXhtmlFile != null)
                {
                    writer.write("      <output-optimized-xhtml-file path=\"" + outputOptimizedXhtmlFile.getAbsolutePath() + "\"/>\n");
                }

                writer.write("    </output-files>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</change-tracking-text-editor-1-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int Run(String[] args)
    {
        this.outputFile = null;
        this.outputPlaintextFile = null;
        this.outputSessionFile = null;
        this.outputConcatenatedFile = null;
        this.outputOptimizedFile = null;
        this.outputXhtmlFile = null;
        this.outputConcatenatedXhtmlFile = null;
        this.outputOptimizedXhtmlFile = null;

        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\tchange_tracking_text_editor_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        change_tracking_text_editor_1.resultInfoFile = resultInfoFile;


        String programPath = change_tracking_text_editor_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }


        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("change_tracking_text_editor_1 workflow: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));


        File outputDirectory = null;
        File inputFile = null;
        boolean isTextHistory = false;
        File inputHistoryFile = null;
        File tempDirectory = null;
        String outputDirectorySelectionStartDirectory = null;
        String inputFileSelectionStartDirectory = null;
        Map<String, String> fileExtensions = new LinkedHashMap<String, String>();
        Integer fontSize = null;
        Integer autosaveCharacters = null;
        // <unique-caption, character>
        Map<String, String> insertCharacterOptions = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String elementName = event.asStartElement().getName().getLocalPart();

                    if (elementName.equals("output-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "path");
                        }

                        if (outputDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        outputDirectory = new File(attributePath.getValue());

                        if (outputDirectory.isAbsolute() != true)
                        {
                            outputDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            outputDirectory = outputDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageJobFileOutputDirectoryCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageJobFileOutputDirectoryCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                        }
                    }
                    else if (elementName.equals("output-directory-selection-start-directory") == true)
                    {
                        Attribute pathAttribute = event.asStartElement().getAttributeByName(new QName("path"));

                        if (pathAttribute == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "path");
                        }

                        if (outputDirectorySelectionStartDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        File selectionStartDirectory = new File(pathAttribute.getValue());

                        if (selectionStartDirectory.isAbsolute() != true)
                        {
                            selectionStartDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + pathAttribute.getValue());
                        }

                        try
                        {
                            selectionStartDirectory = selectionStartDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageJobFileStartDirectoryCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath(), selectionStartDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageJobFileStartDirectoryCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath(), selectionStartDirectory.getAbsolutePath());
                        }

                        outputDirectorySelectionStartDirectory = selectionStartDirectory.getAbsolutePath();
                    }
                    else if (elementName.equals("input-file") == true)
                    {
                        if (inputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "path");
                        }

                        inputFile = new File(attributePath.getValue());

                        if (inputFile.isAbsolute() != true)
                        {
                            inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            inputFile = inputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageJobFileInputFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath(), inputFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageJobFileInputFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath(), inputFile.getAbsolutePath());
                        }

                        if (inputFile.exists() != true)
                        {
                            throw constructTermination("messageJobFileInputFileDoesntExist", null, null, jobFile.getAbsolutePath(), inputFile.getAbsolutePath());
                        }

                        if (inputFile.isFile() != true)
                        {
                            throw constructTermination("messageJobFileInputPathIsntAFile", null, null, jobFile.getAbsolutePath(), inputFile.getAbsolutePath());
                        }

                        if (inputFile.canRead() != true)
                        {
                            throw constructTermination("messageJobFileInputFileIsntReadable", null, null, jobFile.getAbsolutePath(), inputFile.getAbsolutePath());
                        }
                    }
                    else if (elementName.equals("input-file-selection-start-directory") == true)
                    {
                        Attribute pathAttribute = event.asStartElement().getAttributeByName(new QName("path"));

                        if (pathAttribute == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "path");
                        }

                        if (inputFileSelectionStartDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        File selectionStartDirectory = new File(pathAttribute.getValue());

                        if (selectionStartDirectory.isAbsolute() != true)
                        {
                            selectionStartDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + pathAttribute.getValue());
                        }

                        inputFileSelectionStartDirectory = selectionStartDirectory.getAbsolutePath();
                    }
                    else if (elementName.equals("temp-directory") == true)
                    {
                        if (tempDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "path");
                        }

                        tempDirectory = new File(attributePath.getValue());

                        if (tempDirectory.isAbsolute() != true)
                        {
                            tempDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }
                    }
                    else if (elementName.equals("input-file-selection-file-type") == true)
                    {
                        Attribute attributeExtension = event.asStartElement().getAttributeByName(new QName("extension"));

                        if (attributeExtension == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "extension");
                        }

                        String extension = attributeExtension.getValue();
                        String extensionDisplayName = new String();

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isCharacters() == true)
                            {
                                extensionDisplayName += event.asCharacters();
                            }
                            else if (event.isEndElement() == true)
                            {
                                QName elementEndName = event.asEndElement().getName();
                                String elementEndNameString = elementEndName.getLocalPart();

                                if (elementEndNameString.equalsIgnoreCase("input-file-selection-file-type") == true)
                                {
                                    break;
                                }
                            }
                        }

                        if (fileExtensions.containsKey(extension) != true)
                        {
                            fileExtensions.put(extension, extensionDisplayName);
                        }
                        else
                        {
                            throw constructTermination("messageJobFileInputFileSelectionExtensionSpecifiedMoreThanOnce", null, null, jobFile.getAbsolutePath(), extension);
                        }
                    }
                    else if (elementName.equals("font-size") == true)
                    {
                        Attribute attributePoint = event.asStartElement().getAttributeByName(new QName("point"));

                        if (attributePoint == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "point");
                        }

                        if (fontSize != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        try
                        {
                            fontSize = Integer.parseInt(attributePoint.getValue());
                        }
                        catch (NumberFormatException ex)
                        {
                            throw constructTermination("messageJobFileFontSizeIsntANumber", ex, null, jobFile.getAbsolutePath(), attributePoint.getValue());
                        }
                    }
                    else if (elementName.equals("autosave") == true)
                    {
                        Attribute attributeCharacters = event.asStartElement().getAttributeByName(new QName("characters"));

                        if (attributeCharacters == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "characters");
                        }

                        if (autosaveCharacters != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), elementName);
                        }

                        try
                        {
                            autosaveCharacters = Integer.parseInt(attributeCharacters.getValue());
                        }
                        catch (NumberFormatException ex)
                        {
                            throw constructTermination("messageJobFileAutosaveCharactersIsntANumber", ex, null, jobFile.getAbsolutePath(), attributeCharacters.getValue());
                        }
                    }
                    else if (elementName.equals("insert-character-option") == true)
                    {
                        Attribute attributeCaptionL10nId = event.asStartElement().getAttributeByName(new QName("caption-l10n-id"));

                        if (attributeCaptionL10nId == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "caption-l10n-id");
                        }

                        Attribute attributeCharacter = event.asStartElement().getAttributeByName(new QName("character"));

                        if (attributeCharacter == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), elementName, "character");
                        }

                        if (insertCharacterOptions == null)
                        {
                            insertCharacterOptions = new LinkedHashMap<String, String>();
                        }

                        insertCharacterOptions.put(attributeCaptionL10nId.getValue(), attributeCharacter.getValue());
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }


        if (tempDirectory == null)
        {
            tempDirectory = new File(programPath + "temp");
        }

        try
        {
            tempDirectory = tempDirectory.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
        }

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    throw constructTermination("messageTempDirectoryIsntWritable", null, null, tempDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageTempPathIsntADirectory", null, null, tempDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageTempDirectoryCantCreate", ex, null, tempDirectory.getAbsolutePath());
            }
        }

        if (outputDirectory != null)
        {
            if (outputDirectory.exists() == true)
            {
                if (outputDirectory.isDirectory() == true)
                {
                    if (outputDirectory.canWrite() != true)
                    {
                        throw constructTermination("messageJobFileOutputDirectoryIsntWritable", null, null, jobFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                    }
                }
                else
                {
                    throw constructTermination("messageJobFileOutputPathIsntADirectory", null, null, jobFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                }
            }
            else
            {
                try
                {
                    outputDirectory.mkdirs();
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageJobFileOutputDirectoryCantCreate", ex, null, jobFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                }
            }
        }
        else
        {
            File filePicker1OutputDirectoryJobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_file_picker_1_output.xml");

            if (filePicker1OutputDirectoryJobFile.exists() == true)
            {
                if (filePicker1OutputDirectoryJobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = filePicker1OutputDirectoryJobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (filePicker1OutputDirectoryJobFile.canWrite() != true)
                        {
                            throw constructTermination("messageFilePicker1OutputDirectoryJobFileExistsButIsntWritable", null, null, filePicker1OutputDirectoryJobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageFilePicker1OutputDirectoryJobPathExistsButIsntAFile", null, null, filePicker1OutputDirectoryJobFile.getAbsolutePath());
                }
            }

            File filePicker1OutputDirectoryResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_file_picker_1_output.xml");

            if (filePicker1OutputDirectoryResultInfoFile.exists() == true)
            {
                if (filePicker1OutputDirectoryResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = filePicker1OutputDirectoryResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (filePicker1OutputDirectoryResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileExistsButIsntWritable", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageFilePicker1OutputDirectoryResultInfoPathExistsButIsntAFile", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(filePicker1OutputDirectoryJobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<file-picker-1-jobfile>\n");
                writer.write("  <dialog-title>" + getI10nString("windowTitleSelectOutputDirectory") + "</dialog-title>\n");

                if (outputDirectorySelectionStartDirectory != null)
                {
                    writer.write("  <start-directory path=\"" + outputDirectorySelectionStartDirectory + "\"/>\n");
                }

                writer.write("  <directories-only/>\n");
                writer.write("</file-picker-1-jobfile>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryJobFileWritingError", ex, null, filePicker1OutputDirectoryJobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryJobFileWritingError", ex, null, filePicker1OutputDirectoryJobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryJobFileWritingError", ex, null, filePicker1OutputDirectoryJobFile.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", "file_picker_1", filePicker1OutputDirectoryJobFile.getAbsolutePath(), filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "file_picker" + File.separator + "file_picker_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryErrorWhileReadingOutput", ex, null);
            }

            if (filePicker1OutputDirectoryResultInfoFile.exists() != true)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileDoesntExistButShould", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
            }

            if (filePicker1OutputDirectoryResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoPathExistsButIsntAFile", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
            }

            if (filePicker1OutputDirectoryResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileIsntReadable", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
            }

            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(filePicker1OutputDirectoryResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String elementName = event.asStartElement().getName().getLocalPart();

                        if (elementName.equals("success") == true)
                        {
                            wasSuccess = true;
                        }
                        else if (elementName.equals("selected-file") == true)
                        {
                            if (outputDirectory != null)
                            {
                                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileElementConfiguredMoreThanOnce", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath(), elementName);
                            }

                            String outputDirectoryString = new String();

                            while (eventReader.hasNext() == true)
                            {
                                event = eventReader.nextEvent();

                                if (event.isCharacters() == true)
                                {
                                    outputDirectoryString += event.asCharacters();
                                }
                                else if (event.isEndElement() == true)
                                {
                                    if (event.asEndElement().getName().getLocalPart().equals("selected-file") == true)
                                    {
                                        break;
                                    }
                                }
                            }

                            outputDirectory = new File(outputDirectoryString);

                            if (outputDirectory.isAbsolute() != true)
                            {
                                outputDirectory = new File(filePicker1OutputDirectoryResultInfoFile.getAbsoluteFile().getParent() + File.separator + outputDirectoryString);
                            }

                            try
                            {
                                outputDirectory = outputDirectory.getCanonicalFile();
                            }
                            catch (SecurityException ex)
                            {
                                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileOutputDirectoryCantGetCanonicalPath", ex, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                            }
                            catch (IOException ex)
                            {
                                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileOutputDirectoryCantGetCanonicalPath", ex, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                            }

                            if (outputDirectory.exists() == true)
                            {
                                if (outputDirectory.isDirectory() == true)
                                {
                                    if (outputDirectory.canWrite() != true)
                                    {
                                        throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileOutputDirectoryIsntWritable", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                                    }
                                }
                                else
                                {
                                    throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileOutputPathIsntADirectory", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                                }
                            }
                            else
                            {
                                try
                                {
                                    outputDirectory.mkdirs();
                                }
                                catch (SecurityException ex)
                                {
                                    throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileOutputDirectoryCantCreate", ex, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath(), outputDirectory.getAbsolutePath());
                                }
                            }
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileErrorWhileReading", ex, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileErrorWhileReading", ex, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileErrorWhileReading", ex, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryCallWasntSuccessful", null, null);
            }

            if (outputDirectory == null)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileSelectedDirectoryIsntConfigured", null, null, filePicker1OutputDirectoryResultInfoFile.getAbsolutePath());
            }
        }

        this.outputFile = new File(outputDirectory.getAbsolutePath() + File.separator + "output.xml");

        if (this.outputFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryOutputFilePathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputFile.getAbsolutePath());
        }

        this.outputPlaintextFile = new File(outputDirectory.getAbsolutePath() + File.separator + "output.txt");

        if (this.outputPlaintextFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryOutputPlaintextPathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputPlaintextFile.getAbsolutePath());
        }

        if (inputFile == null)
        {
            String localeExtension = new String();

            {
                Locale currentLocale = getLocale();

                if (this.l10nConsole != null)
                {
                    currentLocale = this.l10nConsole.getLocale();
                }

                localeExtension = currentLocale.toString();

                if (localeExtension.length() > 0)
                {
                    localeExtension = "_" + localeExtension;

                    if (new File(programPath + "jobfile_option_picker_1" + localeExtension + ".xml").exists() != true)
                    {
                        localeExtension = new String();
                    }
                }
            }

            File optionPicker1JobFile = new File(programPath + "jobfile_option_picker_1" + localeExtension + ".xml");
            File optionPicker1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_option_picker_1.xml");

            if (optionPicker1ResultInfoFile.exists() == true)
            {
                if (optionPicker1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = optionPicker1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (optionPicker1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageOptionPicker1ResultInfoFileExistsButIsntWritable", null, null, optionPicker1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageOptionPicker1ResultInfoPathExistsButIsntAFile", null, null, optionPicker1ResultInfoFile.getAbsolutePath());
                }
            }

            ProcessBuilder builder = new ProcessBuilder("java", "option_picker_1", optionPicker1JobFile.getAbsolutePath(), optionPicker1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "option_picker" + File.separator + "option_picker_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageOptionPicker1ErrorWhileReadingOutput", ex, null);
            }

            if (optionPicker1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageOptionPicker1ResultInfoFileDoesntExistButShould", null, null, optionPicker1ResultInfoFile.getAbsolutePath());
            }

            if (optionPicker1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageOptionPicker1ResultInfoPathExistsButIsntAFile", null, null, optionPicker1ResultInfoFile.getAbsolutePath());
            }

            if (optionPicker1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageOptionPicker1ResultInfoFileIsntReadable", null, null, optionPicker1ResultInfoFile.getAbsolutePath());
            }

            String selectedOptionId = null;
            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(optionPicker1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String elementName = event.asStartElement().getName().getLocalPart();

                        if (elementName.equals("success") == true)
                        {
                            wasSuccess = true;
                        }
                        else if (elementName.equals("selected-option-id") == true)
                        {
                            selectedOptionId = new String();

                            while (eventReader.hasNext() == true)
                            {
                                event = eventReader.nextEvent();

                                if (event.isCharacters() == true)
                                {
                                    selectedOptionId += event.asCharacters();
                                }
                                else if (event.isEndElement() == true)
                                {
                                    if (event.asEndElement().getName().getLocalPart().equals("selected-option-id") == true)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageOptionPicker1ResultInfoFileErrorWhileReading", ex, null, optionPicker1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageOptionPicker1ResultInfoFileErrorWhileReading", ex, null, optionPicker1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageOptionPicker1ResultInfoFileErrorWhileReading", ex, null, optionPicker1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageOptionPicker1CallWasntSuccessful", null, null);
            }

            if (selectedOptionId == null)
            {
                this.infoMessages.add(constructInfoMessage("messageOptionPicker1CallAborted", true, null, null));
                return 1;
            }

            if (selectedOptionId.equals("new-text") == true)
            {
                inputFile = null;
            }
            else if (selectedOptionId.equals("existing-text") == true)
            {
                File filePicker1InputFileJobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_file_picker_1_input.xml");

                if (filePicker1InputFileJobFile.exists() == true)
                {
                    if (filePicker1InputFileJobFile.isFile() == true)
                    {
                        boolean deleteSuccessful = false;

                        try
                        {
                            deleteSuccessful = filePicker1InputFileJobFile.delete();
                        }
                        catch (SecurityException ex)
                        {

                        }

                        if (deleteSuccessful != true)
                        {
                            if (filePicker1InputFileJobFile.canWrite() != true)
                            {
                                throw constructTermination("messageFilePicker1InputFileJobFileExistsButIsntWritable", null, null, filePicker1InputFileJobFile.getAbsolutePath());
                            }
                        }
                    }
                    else
                    {
                        throw constructTermination("messageFilePicker1InputFileJobPathExistsButIsntAFile", null, null, filePicker1InputFileJobFile.getAbsolutePath());
                    }
                }

                File filePicker1InputFileResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_file_picker_1_input.xml");

                if (filePicker1InputFileResultInfoFile.exists() == true)
                {
                    if (filePicker1InputFileResultInfoFile.isFile() == true)
                    {
                        boolean deleteSuccessful = false;

                        try
                        {
                            deleteSuccessful = filePicker1InputFileResultInfoFile.delete();
                        }
                        catch (SecurityException ex)
                        {

                        }

                        if (deleteSuccessful != true)
                        {
                            if (filePicker1InputFileResultInfoFile.canWrite() != true)
                            {
                                throw constructTermination("messageFilePicker1InputFileResultInfoFileExistsButIsntWritable", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                            }
                        }
                    }
                    else
                    {
                        throw constructTermination("messageFilePicker1InputFileResultInfoPathExistsButIsntAFile", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                    }
                }

                try
                {
                    BufferedWriter writer = new BufferedWriter(
                                            new OutputStreamWriter(
                                            new FileOutputStream(filePicker1InputFileJobFile),
                                            "UTF-8"));

                    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                    writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                    writer.write("<file-picker-1-jobfile>\n");
                    writer.write("  <dialog-title>" + getI10nString("windowTitleSelectInputFile") + "</dialog-title>\n");

                    if (inputFileSelectionStartDirectory != null)
                    {
                        writer.write("  <start-directory path=\"" + inputFileSelectionStartDirectory + "\"/>\n");
                    }

                    for (Map.Entry<String, String> entry : fileExtensions.entrySet())
                    {
                        writer.write("  <file-type extension=\"" + entry.getKey() + "\">" + entry.getValue() + "</file-type>\n");
                    }

                    writer.write("</file-picker-1-jobfile>\n");

                    writer.flush();
                    writer.close();
                }
                catch (FileNotFoundException ex)
                {
                    throw constructTermination("messageFilePicker1InputFileJobFileWritingError", ex, null, filePicker1InputFileJobFile.getAbsolutePath());
                }
                catch (UnsupportedEncodingException ex)
                {
                    throw constructTermination("messageFilePicker1InputFileJobFileWritingError", ex, null, filePicker1InputFileJobFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageFilePicker1InputFileJobFileWritingError", ex, null, filePicker1InputFileJobFile.getAbsolutePath());
                }

                builder = new ProcessBuilder("java", "file_picker_1", filePicker1InputFileJobFile.getAbsolutePath(), filePicker1InputFileResultInfoFile.getAbsolutePath());
                builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "file_picker" + File.separator + "file_picker_1"));
                builder.redirectErrorStream(true);

                try
                {
                    Process process = builder.start();
                    Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                    while (scanner.hasNext() == true)
                    {
                        System.out.println(scanner.next());
                    }

                    scanner.close();
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageFilePicker1InputFileErrorWhileReadingOutput", ex, null);
                }

                if (filePicker1InputFileResultInfoFile.exists() != true)
                {
                    throw constructTermination("messageFilePicker1InputFileResultInfoFileDoesntExistButShould", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                }

                if (filePicker1InputFileResultInfoFile.isFile() != true)
                {
                    throw constructTermination("messageFilePicker1InputFileResultInfoPathExistsButIsntAFile", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                }

                if (filePicker1InputFileResultInfoFile.canRead() != true)
                {
                    throw constructTermination("messageFilePicker1InputFileResultInfoFileIsntReadable", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                }

                wasSuccess = false;

                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    InputStream in = new FileInputStream(filePicker1InputFileResultInfoFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                    while (eventReader.hasNext() == true)
                    {
                        XMLEvent event = eventReader.nextEvent();

                        if (event.isStartElement() == true)
                        {
                            String elementName = event.asStartElement().getName().getLocalPart();

                            if (elementName.equals("success") == true)
                            {
                                wasSuccess = true;
                            }
                            else if (elementName.equals("selected-file") == true)
                            {
                                if (inputFile != null)
                                {
                                    throw constructTermination("messageFilePicker1InputFileResultInfoFileElementConfiguredMoreThanOnce", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath(), elementName);
                                }

                                String inputFileString = new String();

                                while (eventReader.hasNext() == true)
                                {
                                    event = eventReader.nextEvent();

                                    if (event.isCharacters() == true)
                                    {
                                        inputFileString += event.asCharacters();
                                    }
                                    else if (event.isEndElement() == true)
                                    {
                                        if (event.asEndElement().getName().getLocalPart().equals("selected-file") == true)
                                        {
                                            break;
                                        }
                                    }
                                }

                                inputFile = new File(inputFileString);

                                if (inputFile.isAbsolute() != true)
                                {
                                    inputFile = new File(filePicker1InputFileResultInfoFile.getAbsoluteFile().getParent() + File.separator + inputFileString);
                                }

                                try
                                {
                                    inputFile = inputFile.getCanonicalFile();
                                }
                                catch (SecurityException ex)
                                {
                                    throw constructTermination("messageFilePicker1InputFileResultInfoFileCantGetCanonicalPath", ex, null, filePicker1InputFileResultInfoFile.getAbsolutePath(), inputFile.getAbsolutePath());
                                }
                                catch (IOException ex)
                                {
                                    throw constructTermination("messageFilePicker1InputFileResultInfoFileCantGetCanonicalPath", ex, null, filePicker1InputFileResultInfoFile.getAbsolutePath(), inputFile.getAbsolutePath());
                                }

                                if (inputFile.exists() != true)
                                {
                                    throw constructTermination("messageFilePicker1InputFileResultInfoFileSelectedFileDoesntExist", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath(), inputFile.getAbsolutePath());
                                }

                                if (inputFile.isFile() != true)
                                {
                                    throw constructTermination("messageFilePicker1InputFileResultInfoFileSelectedPathIsntAFile", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath(), inputFile.getAbsolutePath());
                                }

                                if (inputFile.canRead() != true)
                                {
                                    throw constructTermination("messageFilePicker1InputFileResultInfoFileSelectedFileIsntReadable", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath(), inputFile.getAbsolutePath());
                                }
                            }
                        }
                    }
                }
                catch (XMLStreamException ex)
                {
                    throw constructTermination("messageFilePicker1InputFileResultInfoFileErrorWhileReading", ex, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageFilePicker1InputFileResultInfoFileErrorWhileReading", ex, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageFilePicker1InputFileResultInfoFileErrorWhileReading", ex, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                }

                if (wasSuccess != true)
                {
                    throw constructTermination("messageFilePicker1InputFileCallWasntSuccessful", null, null);
                }

                if (inputFile == null)
                {
                    throw constructTermination("messageFilePicker1InputFileResultInfoFileSelectedFileIsntConfigured", null, null, filePicker1InputFileResultInfoFile.getAbsolutePath());
                }


                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
                    inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
                    InputStream in = new FileInputStream(inputFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                    while (eventReader.hasNext() == true)
                    {
                        XMLEvent event = eventReader.nextEvent();

                        if (event.isStartElement() == true)
                        {
                            // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                            @SuppressWarnings("unchecked")
                            Iterator<Namespace> iterNamespace = (Iterator<Namespace>)event.asStartElement().getNamespaces();

                            while (iterNamespace.hasNext())
                            {
                                if (iterNamespace.next().getNamespaceURI().equals("htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z") == true)
                                {
                                    isTextHistory = true;
                                    break;
                                }
                            }

                            if (isTextHistory == true)
                            {
                                break;
                            }
                        }
                    }
                }
                catch (XMLStreamException ex)
                {
                    isTextHistory = false;
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageInputFileAnalyzerErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageInputFileAnalyzerErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }

                if (isTextHistory == true)
                {
                    File changeInstructionsExecutor1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_change_instructions_executor_1.xml");

                    if (changeInstructionsExecutor1JobFile.exists() == true)
                    {
                        if (changeInstructionsExecutor1JobFile.isFile() == true)
                        {
                            boolean deleteSuccessful = false;

                            try
                            {
                                deleteSuccessful = changeInstructionsExecutor1JobFile.delete();
                            }
                            catch (SecurityException ex)
                            {

                            }

                            if (deleteSuccessful != true)
                            {
                                if (changeInstructionsExecutor1JobFile.canWrite() != true)
                                {
                                    throw constructTermination("messageChangeInstructionsExecutor1JobFileExistsButIsntWritable", null, null, changeInstructionsExecutor1JobFile.getAbsolutePath());
                                }
                            }
                        }
                        else
                        {
                            throw constructTermination("messageChangeInstructionsExecutor1JobPathExistsButIsntAFile", null, null, changeInstructionsExecutor1JobFile.getAbsolutePath());
                        }
                    }

                    File changeInstructionsExecutor1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_change_instructions_executor_1.xml");

                    if (changeInstructionsExecutor1ResultInfoFile.exists() == true)
                    {
                        if (changeInstructionsExecutor1ResultInfoFile.isFile() == true)
                        {
                            boolean deleteSuccessful = false;

                            try
                            {
                                deleteSuccessful = changeInstructionsExecutor1ResultInfoFile.delete();
                            }
                            catch (SecurityException ex)
                            {

                            }

                            if (deleteSuccessful != true)
                            {
                                if (changeInstructionsExecutor1ResultInfoFile.canWrite() != true)
                                {
                                    throw constructTermination("messageChangeInstructionsExecutor1ResultInfoFileExistsButIsntWritable", null, null, changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                                }
                            }
                        }
                        else
                        {
                            throw constructTermination("messageChangeInstructionsExecutor1ResultInfoPathExistsButIsntAFile", null, null, changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                        }
                    }

                    File changeInstructionsExecutor1OutputDirectory = new File(tempDirectory.getAbsolutePath() + File.separator + "versions-input");

                    if (changeInstructionsExecutor1OutputDirectory.exists() == true)
                    {
                        if (changeInstructionsExecutor1OutputDirectory.isDirectory() == true)
                        {
                            if (changeInstructionsExecutor1OutputDirectory.canWrite() != true)
                            {
                                throw constructTermination("messageChangeInstructionsExecutor1OutputDirectoryIsntWritable", null, null, changeInstructionsExecutor1OutputDirectory.getAbsolutePath());
                            }
                        }
                        else
                        {
                            throw constructTermination("messageChangeInstructionsExecutor1OutputPathIsntADirectory", null, null, changeInstructionsExecutor1OutputDirectory.getAbsolutePath());
                        }
                    }
                    else
                    {
                        try
                        {
                            changeInstructionsExecutor1OutputDirectory.mkdirs();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageChangeInstructionsExecutor1OutputDirectoryCantCreate", ex, null, changeInstructionsExecutor1OutputDirectory.getAbsolutePath());
                        }
                    }

                    try
                    {
                        BufferedWriter writer = new BufferedWriter(
                                                new OutputStreamWriter(
                                                new FileOutputStream(changeInstructionsExecutor1JobFile),
                                                "UTF-8"));

                        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                        writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                        writer.write("<change-instructions-executor-1-job>\n");
                        writer.write("  <input-file path=\"" + inputFile.getAbsolutePath() + "\"/>\n");
                        writer.write("  <output-directory path=\"" + changeInstructionsExecutor1OutputDirectory.getAbsolutePath() + "\" file-last=\"true\"/>\n");
                        writer.write("</change-instructions-executor-1-job>\n");

                        writer.flush();
                        writer.close();
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1JobFileWritingError", ex, null, changeInstructionsExecutor1JobFile.getAbsolutePath());
                    }
                    catch (UnsupportedEncodingException ex)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1JobFileWritingError", ex, null, changeInstructionsExecutor1JobFile.getAbsolutePath());
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1JobFileWritingError", ex, null, changeInstructionsExecutor1JobFile.getAbsolutePath());
                    }

                    builder = new ProcessBuilder("java", "change_instructions_executor_1", changeInstructionsExecutor1JobFile.getAbsolutePath(), changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                    builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "change_instructions_executor" + File.separator + "change_instructions_executor_1"));
                    builder.redirectErrorStream(true);

                    try
                    {
                        Process process = builder.start();
                        Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                        while (scanner.hasNext() == true)
                        {
                            System.out.println(scanner.next());
                        }

                        scanner.close();
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1ErrorWhileReadingOutput", ex, null);
                    }

                    if (changeInstructionsExecutor1ResultInfoFile.exists() != true)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1ResultInfoFileDoesntExistButShould", null, null, changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                    }

                    if (changeInstructionsExecutor1ResultInfoFile.isFile() != true)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1ResultInfoPathExistsButIsntAFile", null, null, changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                    }

                    if (changeInstructionsExecutor1ResultInfoFile.canRead() != true)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1ResultInfoFileIsntReadable", null, null, changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                    }

                    wasSuccess = false;

                    try
                    {
                        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                        InputStream in = new FileInputStream(changeInstructionsExecutor1ResultInfoFile);
                        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                        while (eventReader.hasNext() == true)
                        {
                            XMLEvent event = eventReader.nextEvent();

                            if (event.isStartElement() == true)
                            {
                                String elementName = event.asStartElement().getName().getLocalPart();

                                if (elementName.equals("success") == true)
                                {
                                    wasSuccess = true;
                                    break;
                                }
                            }
                        }
                    }
                    catch (XMLStreamException ex)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                    }
                    catch (SecurityException ex)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsExecutor1ResultInfoFile.getAbsolutePath());
                    }

                    if (wasSuccess != true)
                    {
                        throw constructTermination("messageChangeInstructionsExecutor1CallWasntSuccessful", null, null);
                    }

                    File inputFileSource = new File(changeInstructionsExecutor1OutputDirectory.getAbsolutePath() + File.separator + "last.txt");

                    if (inputFileSource.exists() != true)
                    {
                        throw constructTermination("messageLatestRevisionInputFileDoesntExist", null, null, inputFileSource.getAbsolutePath());
                    }

                    inputHistoryFile = inputFile;
                    inputFile = inputFileSource;
                }
            }
            else
            {
                throw constructTermination("messageOptionPicker1CallReturnedSelectionIdNotSupported", null, null, selectedOptionId);
            }
        }

        this.outputSessionFile = null;

        if (inputFile != null)
        {
            this.outputSessionFile = new File(outputDirectory.getAbsolutePath() + File.separator + "output_session.xml");

            if (this.outputSessionFile.exists() == true)
            {
                throw constructTermination("messageOutputDirectoryOutputSessionFilePathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputSessionFile.getAbsolutePath());
            }

            if (isTextHistory != true)
            {
                File inputFileTarget = new File(outputDirectory.getAbsolutePath() + File.separator + "input.txt");

                if (inputFileTarget.exists() == true)
                {
                    throw constructTermination("messageOutputDirectoryInputFileBackupPathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), inputFileTarget.getAbsolutePath());
                }

                if (CopyFileBinary(inputFile, inputFileTarget) != 0)
                {

                }

                inputFile = inputFileTarget;
            }
            else
            {
                // inputFile already set up, no need for a backup as the text
                // history is available anyway.
            }
        }


        File changeTrackingTextEditor1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_change_tracking_text_editor_1.xml");

        if (changeTrackingTextEditor1JobFile.exists() == true)
        {
            if (changeTrackingTextEditor1JobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = changeTrackingTextEditor1JobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (changeTrackingTextEditor1JobFile.canWrite() != true)
                    {
                        throw constructTermination("messageChangeTrackingTextEditor1JobFileExistsButIsntWritable", null, null, changeTrackingTextEditor1JobFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageChangeTrackingTextEditor1JobPathExistsButIsntAFile", null, null, changeTrackingTextEditor1JobFile.getAbsolutePath());
            }
        }

        File changeTrackingTextEditor1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_change_tracking_text_editor_1.xml");

        if (changeTrackingTextEditor1ResultInfoFile.exists() == true)
        {
            if (changeTrackingTextEditor1ResultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = changeTrackingTextEditor1ResultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (changeTrackingTextEditor1ResultInfoFile.canWrite() != true)
                    {
                        throw constructTermination("messageChangeTrackingTextEditor1ResultInfoFileExistsButIsntWritable", null, null, changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageChangeTrackingTextEditor1ResultInfoPathExistsButIsntAFile", null, null, changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(changeTrackingTextEditor1JobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
            writer.write("<change-tracking-text-editor-1-jobfile>\n");

            if (inputFile != null)
            {
                writer.write("  <input-file path=\"" + inputFile.getAbsolutePath() + "\"/>\n");
            }

            if (this.outputSessionFile != null)
            {
                writer.write("  <output-file path=\"" + this.outputSessionFile.getAbsolutePath() + "\"/>\n");
            }
            else
            {
                writer.write("  <output-file path=\"" + this.outputFile.getAbsolutePath() + "\"/>\n");
            }

            writer.write("  <plaintext-file path=\"" + this.outputPlaintextFile.getAbsolutePath() + "\"/>\n");

            if (fontSize != null)
            {
                writer.write("  <font-size point=\"" + fontSize.toString() + "\"/>\n");
            }

            if (autosaveCharacters != null)
            {
                writer.write("  <autosave characters=\"" + autosaveCharacters.toString() + "\"/>\n");
            }

            if (insertCharacterOptions != null)
            {
                writer.write("  <insert-character-option-list>\n");

                for (Map.Entry<String, String> option : insertCharacterOptions.entrySet())
                {
                    String l10nId = option.getKey();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    l10nId = l10nId.replaceAll("&", "&amp;");
                    l10nId = l10nId.replaceAll("<", "&lt;");
                    l10nId = l10nId.replaceAll(">", "&gt;");
                    l10nId = l10nId.replaceAll("\"", "&quot;");
                    l10nId = l10nId.replaceAll("'", "&apos;");

                    String character = option.getValue();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    character = character.replaceAll("&", "&amp;");
                    character = character.replaceAll("<", "&lt;");
                    character = character.replaceAll(">", "&gt;");
                    character = character.replaceAll("\"", "&quot;");
                    character = character.replaceAll("'", "&apos;");

                    writer.write("    <insert-character-option caption-l10n-id=\"" + l10nId + "\" character=\"" + character + "\"/>\n");
                }

                writer.write("  </insert-character-option-list>\n");
            }

            writer.write("</change-tracking-text-editor-1-jobfile>\n");

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageChangeTrackingTextEditor1JobFileWritingError", ex, null, changeTrackingTextEditor1JobFile.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageChangeTrackingTextEditor1JobFileWritingError", ex, null, changeTrackingTextEditor1JobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageChangeTrackingTextEditor1JobFileWritingError", ex, null, changeTrackingTextEditor1JobFile.getAbsolutePath());
        }

        ProcessBuilder builder = new ProcessBuilder("java", "change_tracking_text_editor_1", changeTrackingTextEditor1JobFile.getAbsolutePath(), changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
        builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "change_tracking_text_editor" + File.separator + "change_tracking_text_editor_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageChangeTrackingTextEditor1ErrorWhileReadingOutput", ex, null);
        }

        if (changeTrackingTextEditor1ResultInfoFile.exists() != true)
        {
            throw constructTermination("messageChangeTrackingTextEditor1ResultInfoFileDoesntExistButShould", null, null, changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
        }

        if (changeTrackingTextEditor1ResultInfoFile.isFile() != true)
        {
            throw constructTermination("messageChangeTrackingTextEditor1ResultInfoPathExistsButIsntAFile", null, null, changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
        }

        if (changeTrackingTextEditor1ResultInfoFile.canRead() != true)
        {
            throw constructTermination("messageChangeTrackingTextEditor1ResultInfoFileIsntReadable", null, null, changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(changeTrackingTextEditor1ResultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String elementName = event.asStartElement().getName().getLocalPart();

                    if (elementName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageChangeTrackingTextEditor1ResultInfoFileErrorWhileReading", ex, null, changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageChangeTrackingTextEditor1ResultInfoFileErrorWhileReading", ex, null, changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageChangeTrackingTextEditor1ResultInfoFileErrorWhileReading", ex, null, changeTrackingTextEditor1ResultInfoFile.getAbsolutePath());
        }

        if (wasSuccess != true)
        {
            throw constructTermination("messageChangeTrackingTextEditor1CallWasntSuccessful", null, null);
        }

        if (inputFile != null)
        {
            if (isTextHistory != true)
            {
                inputHistoryFile = new File(tempDirectory.getAbsolutePath() + File.separator + "input_history.xml");

                if (inputHistoryFile.exists() == true)
                {
                    if (inputHistoryFile.isFile() == true)
                    {
                        boolean deleteSuccessful = false;

                        try
                        {
                            deleteSuccessful = inputHistoryFile.delete();
                        }
                        catch (SecurityException ex)
                        {

                        }

                        if (deleteSuccessful != true)
                        {
                            if (inputHistoryFile.canWrite() != true)
                            {
                                throw constructTermination("messageTempDirectoryHistoryFileExistsButIsntWritable", null, null, inputHistoryFile.getAbsolutePath());
                            }
                        }
                    }
                    else
                    {
                        throw constructTermination("messageTempDirectoryHistoryPathExistsButIsntAFile", null, null, inputHistoryFile.getAbsolutePath());
                    }
                }

                try
                {
                    BufferedWriter writer = new BufferedWriter(
                                            new OutputStreamWriter(
                                            new FileOutputStream(inputHistoryFile),
                                            "UTF-8"));

                    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                    writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                    writer.write("<change-tracking-text-editor-1-text-history xmlns=\"htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z\">\n");
                    writer.write("  <add position=\"0\">");

                    char[] buffer = new char[1024];

                    try
                    {
                        BufferedReader reader = new BufferedReader(
                                                new InputStreamReader(
                                                new FileInputStream(inputFile),
                                                "UTF-8"));

                        int charsRead = reader.read(buffer, 0, buffer.length);

                        while (charsRead > 0)
                        {
                            for (int i = 0; i < charsRead; i++)
                            {
                                if (buffer[i] == '&')
                                {
                                    writer.write("&amp;");
                                }
                                else if (buffer[i] == '<')
                                {
                                    writer.write("&lt;");
                                }
                                else if (buffer[i] == '>')
                                {
                                    writer.write("&gt;");
                                }
                                else
                                {
                                    writer.write(buffer[i]);
                                }
                            }

                            charsRead = reader.read(buffer, 0, buffer.length);
                        }

                        reader.close();
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                    }

                    writer.write("</add>\n");
                    writer.write("</change-tracking-text-editor-1-text-history>\n");

                    writer.flush();
                    writer.close();
                }
                catch (FileNotFoundException ex)
                {
                    throw constructTermination("messageTempDirectoryHistoryFileErrorWhileWriting", ex, null, inputHistoryFile.getAbsolutePath());
                }
                catch (UnsupportedEncodingException ex)
                {
                    throw constructTermination("messageTempDirectoryHistoryFileErrorWhileWriting", ex, null, inputHistoryFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageTempDirectoryHistoryFileErrorWhileWriting", ex, null, inputHistoryFile.getAbsolutePath());
                }
            }


            File changeHistoryConcatenator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_change_history_concatenator_1.xml");

            if (changeHistoryConcatenator1JobFile.exists() == true)
            {
                if (changeHistoryConcatenator1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = changeHistoryConcatenator1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (changeHistoryConcatenator1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageChangeHistoryConcatenator1JobFileExistsButIsntWritable", null, null, changeHistoryConcatenator1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageChangeHistoryConcatenator1JobPathExistsButIsntAFile", null, null, changeHistoryConcatenator1JobFile.getAbsolutePath());
                }
            }

            File changeHistoryConcatenator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_change_history_concatenator_1.xml");

            if (changeHistoryConcatenator1ResultInfoFile.exists() == true)
            {
                if (changeHistoryConcatenator1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = changeHistoryConcatenator1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (changeHistoryConcatenator1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageChangeHistoryConcatenator1ResultInfoFileExistsButIsntWritable", null, null, changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageChangeHistoryConcatenator1ResultInfoPathExistsButIsntAFile", null, null, changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(changeHistoryConcatenator1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<change-history-concatenator-1-job>\n");
                writer.write("  <input-files>\n");
                writer.write("    <input-file path=\"" + inputHistoryFile.getAbsolutePath() + "\"/>\n");
                writer.write("    <input-file path=\"" + this.outputSessionFile.getAbsolutePath() + "\"/>\n");
                writer.write("  </input-files>\n");
                writer.write("  <output-file path=\"" + this.outputFile.getAbsolutePath() + "\" processing-instruction-data=\"encoding=&quot;UTF-8&quot;\"/>\n");
                writer.write("</change-history-concatenator-1-job>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageChangeHistoryConcatenator1JobFileWritingError", ex, null, changeHistoryConcatenator1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageChangeHistoryConcatenator1JobFileWritingError", ex, null, changeHistoryConcatenator1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeHistoryConcatenator1JobFileWritingError", ex, null, changeHistoryConcatenator1JobFile.getAbsolutePath());
            }

            builder = new ProcessBuilder("java", "change_history_concatenator_1", changeHistoryConcatenator1JobFile.getAbsolutePath(), changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "change_history_concatenator" + File.separator + "change_history_concatenator_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeHistoryConcatenator1ErrorWhileReadingOutput", ex, null);
            }

            if (changeHistoryConcatenator1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageChangeHistoryConcatenator1ResultInfoFileDoesntExistButShould", null, null, changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (changeHistoryConcatenator1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageChangeHistoryConcatenator1ResultInfoPathExistsButIsntAFile", null, null, changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (changeHistoryConcatenator1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageChangeHistoryConcatenator1ResultInfoFileIsntReadable", null, null, changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
            }

            wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(changeHistoryConcatenator1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String elementName = event.asStartElement().getName().getLocalPart();

                        if (elementName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageChangeHistoryConcatenator1ResultInfoFileErrorWhileReading", ex, null, changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageChangeHistoryConcatenator1ResultInfoFileErrorWhileReading", ex, null, changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeHistoryConcatenator1ResultInfoFileErrorWhileReading", ex, null, changeHistoryConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageChangeHistoryConcatenator1CallWasntSuccessful", null, null);
            }
        }


        // Generate optimization + rendering output files.


        this.outputConcatenatedFile = new File(outputDirectory.getAbsolutePath() + File.separator + "output_concatenated.xml");

        if (this.outputConcatenatedFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryOutputConcatenatedFilePathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputConcatenatedFile.getAbsolutePath());
        }

        {
            File changeInstructionsConcatenator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_change_instructions_concatenator_1.xml");

            if (changeInstructionsConcatenator1JobFile.exists() == true)
            {
                if (changeInstructionsConcatenator1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = changeInstructionsConcatenator1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (changeInstructionsConcatenator1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageChangeInstructionsConcatenator1JobFileExistsButIsntWritable", null, null, changeInstructionsConcatenator1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageChangeInstructionsConcatenator1JobPathExistsButIsntAFile", null, null, changeInstructionsConcatenator1JobFile.getAbsolutePath());
                }
            }

            File changeInstructionsConcatenator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_change_instructions_concatenator_1.xml");

            if (changeInstructionsConcatenator1ResultInfoFile.exists() == true)
            {
                if (changeInstructionsConcatenator1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = changeInstructionsConcatenator1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (changeInstructionsConcatenator1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageChangeInstructionsConcatenator1ResultInfoFileExistsButIsntWritable", null, null, changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageChangeInstructionsConcatenator1ResultInfoPathExistsButIsntAFile", null, null, changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(changeInstructionsConcatenator1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<change-instructions-concatenator-1-job>\n");
                writer.write("  <input-file path=\"" + this.outputFile.getAbsolutePath() + "\"/>\n");
                writer.write("  <output-file path=\"" + this.outputConcatenatedFile.getAbsolutePath() + "\"/>\n");
                writer.write("</change-instructions-concatenator-1-job>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1JobFileWritingError", ex, null, changeInstructionsConcatenator1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1JobFileWritingError", ex, null, changeInstructionsConcatenator1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1JobFileWritingError", ex, null, changeInstructionsConcatenator1JobFile.getAbsolutePath());
            }

            builder = new ProcessBuilder("java", "change_instructions_concatenator_1", changeInstructionsConcatenator1JobFile.getAbsolutePath(), changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "change_instructions_concatenator" + File.separator + "change_instructions_concatenator_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1ErrorWhileReadingOutput", ex, null);
            }

            if (changeInstructionsConcatenator1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1ResultInfoFileDoesntExistButShould", null, null, changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (changeInstructionsConcatenator1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1ResultInfoPathExistsButIsntAFile", null, null, changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (changeInstructionsConcatenator1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1ResultInfoFileIsntReadable", null, null, changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
            }

            wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(changeInstructionsConcatenator1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String elementName = event.asStartElement().getName().getLocalPart();

                        if (elementName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageChangeInstructionsConcatenator1CallWasntSuccessful", null, null);
            }
        }

        this.outputOptimizedFile = new File(outputDirectory.getAbsolutePath() + File.separator + "output_optimized.xml");

        if (this.outputOptimizedFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryOutputOptimizedFilePathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputOptimizedFile.getAbsolutePath());
        }

        {
            File changeInstructionsOptimizer1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_change_instructions_optimizer_1.xml");

            if (changeInstructionsOptimizer1JobFile.exists() == true)
            {
                if (changeInstructionsOptimizer1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = changeInstructionsOptimizer1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (changeInstructionsOptimizer1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageChangeInstructionsOptimizer1JobFileExistsButIsntWritable", null, null, changeInstructionsOptimizer1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageChangeInstructionsOptimizer1JobPathExistsButIsntAFile", null, null, changeInstructionsOptimizer1JobFile.getAbsolutePath());
                }
            }

            File changeInstructionsOptimizer1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_change_instructions_optimizer_1.xml");

            if (changeInstructionsOptimizer1ResultInfoFile.exists() == true)
            {
                if (changeInstructionsOptimizer1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = changeInstructionsOptimizer1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (changeInstructionsOptimizer1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageChangeInstructionsOptimizer1ResultInfoFileExistsButIsntWritable", null, null, changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageChangeInstructionsOptimizer1ResultInfoPathExistsButIsntAFile", null, null, changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(changeInstructionsOptimizer1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<change-instructions-optimizer-1-job>\n");
                writer.write("  <input-file path=\"" + this.outputFile.getAbsolutePath() + "\"/>\n");
                writer.write("  <output-file path=\"" + this.outputOptimizedFile.getAbsolutePath() + "\"/>\n");
                writer.write("</change-instructions-optimizer-1-job>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1JobFileWritingError", ex, null, changeInstructionsOptimizer1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1JobFileWritingError", ex, null, changeInstructionsOptimizer1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1JobFileWritingError", ex, null, changeInstructionsOptimizer1JobFile.getAbsolutePath());
            }

            builder = new ProcessBuilder("java", "change_instructions_optimizer_1", changeInstructionsOptimizer1JobFile.getAbsolutePath(), changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "change_instructions_optimizer" + File.separator + "change_instructions_optimizer_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1ErrorWhileReadingOutput", ex, null);
            }

            if (changeInstructionsOptimizer1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1ResultInfoFileDoesntExistButShould", null, null, changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
            }

            if (changeInstructionsOptimizer1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1ResultInfoPathExistsButIsntAFile", null, null, changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
            }

            if (changeInstructionsOptimizer1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1ResultInfoFileIsntReadable", null, null, changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
            }

            wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(changeInstructionsOptimizer1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String elementName = event.asStartElement().getName().getLocalPart();

                        if (elementName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1ResultInfoFileErrorWhileReading", ex, null, changeInstructionsOptimizer1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageChangeInstructionsOptimizer1CallWasntSuccessful", null, null);
            }
        }

        this.outputXhtmlFile = new File(outputDirectory.getAbsolutePath() + File.separator + "output.xhtml");

        if (this.outputXhtmlFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryXhtmlFilePathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputXhtmlFile.getAbsolutePath());
        }

        this.outputConcatenatedXhtmlFile = new File(outputDirectory.getAbsolutePath() + File.separator + "output_concatenated.xhtml");

        if (this.outputConcatenatedXhtmlFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryOutputConcatenatedXhtmlFilePathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputConcatenatedXhtmlFile.getAbsolutePath());
        }

        this.outputOptimizedXhtmlFile = new File(outputDirectory.getAbsolutePath() + File.separator + "output_optimized.xhtml");

        if (this.outputOptimizedXhtmlFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryOutputOptimizedXhtmlFilePathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputOptimizedXhtmlFile.getAbsolutePath());
        }

        {
            File xmlXsltTransformator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_xslt_transformator_1.xml");

            if (xmlXsltTransformator1JobFile.exists() == true)
            {
                if (xmlXsltTransformator1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlXsltTransformator1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlXsltTransformator1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlXsltTransformator1JobFileExistsButIsntWritable", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlXsltTransformator1JobPathExistsButIsntAFile", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                }
            }

            File xmlXsltTransformator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_xslt_transformator_1.xml");

            if (xmlXsltTransformator1ResultInfoFile.exists() == true)
            {
                if (xmlXsltTransformator1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlXsltTransformator1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlXsltTransformator1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileExistsButIsntWritable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlXsltTransformator1ResultInfoPathExistsButIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xmlXsltTransformator1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<xml-xslt-transformator-1-jobfile>\n");
                writer.write("  <job input-file=\"" + this.outputFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + (new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_empty.xml")).getAbsolutePath() + "\" stylesheet-file=\"" + (new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "change_tracking_text_editor" + File.separator + "change_tracking_text_editor_1" + File.separator + "change_instructions_navigator_2.xsl")).getAbsolutePath() + "\" output-file=\"" + this.outputXhtmlFile.getAbsolutePath() + "\"/>\n");
                writer.write("  <job input-file=\"" + this.outputConcatenatedFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + (new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_empty.xml")).getAbsolutePath() + "\" stylesheet-file=\"" + (new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "change_tracking_text_editor" + File.separator + "change_tracking_text_editor_1" + File.separator + "change_instructions_navigator_2.xsl")).getAbsolutePath() + "\" output-file=\"" + this.outputConcatenatedXhtmlFile.getAbsolutePath() + "\"/>\n");
                writer.write("  <job input-file=\"" + this.outputOptimizedFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + (new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_empty.xml")).getAbsolutePath() + "\" stylesheet-file=\"" + (new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "change_tracking_text_editor" + File.separator + "change_tracking_text_editor_1" + File.separator + "change_instructions_navigator_2.xsl")).getAbsolutePath() + "\" output-file=\"" + this.outputOptimizedXhtmlFile.getAbsolutePath() + "\"/>\n");
                writer.write("</xml-xslt-transformator-1-jobfile>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1JobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1JobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1JobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }

            builder = new ProcessBuilder("java", "xml_xslt_transformator_1", xmlXsltTransformator1JobFile.getAbsolutePath(), xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1ErrorWhileReadingOutput", ex, null);
            }

            if (xmlXsltTransformator1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageXmlXsltTransformator1ResultInfoFileDoesntExistButShould", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlXsltTransformator1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageXmlXsltTransformator1ResultInfoPathExistsButIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlXsltTransformator1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messagexmlXsltTransformator1ResultInfoFileIsntReadable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(xmlXsltTransformator1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String elementName = event.asStartElement().getName().getLocalPart();

                        if (elementName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messagexmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messagexmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messagexmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messagexmlXsltTransformator1CallWasntSuccessful", null, null);
            }
        }

        return 0;
    }

    public int CopyFileBinary(File from, File to) throws ProgramTerminationException
    {
        if (from.exists() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromDoesntExist", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (from.isFile() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromIsntAFile", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (from.canRead() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromIsntReadable", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (to.exists() == true)
        {
            if (to.isFile() == true)
            {
                if (to.canWrite() != true)
                {
                    throw constructTermination("messageCantCopyBecauseToIsntWritable", null, null, from.getAbsolutePath(), to.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageCantCopyBecauseToIsntAFile", null, null, from.getAbsolutePath(), to.getAbsolutePath());
            }
        }


        ProgramTerminationException exception = null;

        byte[] buffer = new byte[1024];

        FileInputStream reader = null;
        FileOutputStream writer = null;

        try
        {
            to.createNewFile();

            reader = new FileInputStream(from);
            writer = new FileOutputStream(to);

            int bytesRead = reader.read(buffer, 0, buffer.length);

            while (bytesRead > 0)
            {
                writer.write(buffer, 0, bytesRead);
                bytesRead = reader.read(buffer, 0, buffer.length);
            }

            writer.close();
            reader.close();
        }
        catch (FileNotFoundException ex)
        {
            exception = constructTermination("messageCopyFileError", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
        }
        catch (IOException ex)
        {
            exception = constructTermination("messageCopyFileError", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException ex)
                {
                    if (exception == null)
                    {
                        exception = constructTermination("messageCopyFileError", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
                    }
                }
            }

            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException ex)
                {
                    if (exception == null)
                    {
                        exception = constructTermination("messageCopyFileError", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
                    }
                }
            }
        }

        if (exception != null)
        {
            throw exception;
        }

        return 0;
    }

    private InfoMessage constructInfoMessage(String id,
                                             boolean outputToConsole,
                                             Exception exception,
                                             String message,
                                             Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "change_tracking_text_editor_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "change_tracking_text_editor_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "change_tracking_text_editor_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "change_tracking_text_editor_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (change_tracking_text_editor_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(change_tracking_text_editor_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_tracking_text_editor_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<change-tracking-text-editor-1-workflow-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</change-tracking-text-editor-1-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        change_tracking_text_editor_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public File GetOutputFile()
    {
        return this.outputFile;
    }

    public File GetOutputPlaintextFile()
    {
        return this.outputPlaintextFile;
    }

    public File GetOutputSessionFile()
    {
        return this.outputSessionFile;
    }

    public File GetOutputConcatenatedFile()
    {
        return this.outputConcatenatedFile;
    }

    public File GetOutputOptimizedFile()
    {
        return this.outputOptimizedFile;
    }

    public File GetOutputXhtmlFile()
    {
        return this.outputXhtmlFile;
    }

    public File GetOutputConcatenatedXhtmlFile()
    {
        return this.outputConcatenatedXhtmlFile;
    }

    public File GetOutputOptimizedXhtmlFile()
    {
        return this.outputOptimizedXhtmlFile;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected File outputFile = null;
    protected File outputPlaintextFile = null;
    protected File outputSessionFile = null;
    protected File outputConcatenatedFile = null;
    protected File outputOptimizedFile = null;
    protected File outputXhtmlFile = null;
    protected File outputConcatenatedXhtmlFile = null;
    protected File outputOptimizedXhtmlFile = null;

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nChangeTrackingTextEditor1WorkflowConsole";
    private ResourceBundle l10nConsole;
}
