/* Copyright (C) 2018 Stephan Kreutzer
 *
 * This file is part of change_instructions_executor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_instructions_executor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_instructions_executor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_instructions_executor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/change_instructions_executor/change_instructions_executor_1/OutputFile.java
 * @brief Represents an output file that has been generated and metadata about it.
 * @author Stephan Kreutzer
 * @since 2018-10-20
 */



import java.io.File;



class OutputFile
{
    public OutputFile(long instruction,
                      File file)
    {
        this.instruction = instruction;
        this.file = file;
    }

    public long getInstruction()
    {
        return this.instruction;
    }

    public File getFile()
    {
        return this.file;
    }

    protected long instruction;
    protected File file;
}
