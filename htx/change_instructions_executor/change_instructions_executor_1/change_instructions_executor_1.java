/* Copyright (C) 2016-2022 Stephan Kreutzer
 *
 * This file is part of change_instructions_executor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_instructions_executor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_instructions_executor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_instructions_executor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/htx/change_instructions_executor/change_instructions_executor_1/change_instructions_executor_1.java
 * @brief Executes a series of change instructions in order to reconstruct a specific text.
 * @author Stephan Kreutzer
 * @since 2017-10-21
 */



import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;



public class change_instructions_executor_1
{
    public static void main(String args[])
    {
        System.out.print("change_instructions_executor_1 Copyright (C) 2016-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://hypertext-systems.org.\n\n");

        change_instructions_executor_1 executor = new change_instructions_executor_1();

        try
        {
            executor.execute(args);
        }
        catch (ProgramTerminationException ex)
        {
            executor.handleTermination(ex);
        }

        if (executor.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(executor.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_instructions_executor_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<change-instructions-executor-1-result-information>\n");
                writer.write("  <success>\n");

                if (executor.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = executor.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = executor.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (executor.getOutputFiles().size() > 0)
                {
                    writer.write("    <output-files>\n");

                    for (int i = 0, max = executor.getOutputFiles().size(); i < max; i++)
                    {
                        OutputFile outputFile = executor.getOutputFiles().get(i);

                        writer.write("      <output-file instruction=\"" + outputFile.getInstruction() + "\" path=\"" + outputFile.getFile().getAbsolutePath() + "\"/>\n");
                    }

                    writer.write("    </output-files>\n");
                }

                writer.write("  </success>\n");
                writer.write("</change-instructions-executor-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int execute(String args[])
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\tchange_instructions_executor_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        change_instructions_executor_1.resultInfoFile = resultInfoFile;
        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("change_instructions_executor_1: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));


        File inputFile = null;
        File outputDirectory = null;
        boolean fileLast = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input-file") == true)
                    {
                        StartElement inputFileElement = event.asStartElement();
                        Attribute pathAttribute = inputFileElement.getAttributeByName(new QName("path"));

                        if (pathAttribute == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (inputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        inputFile = new File(pathAttribute.getValue());

                        if (inputFile.isAbsolute() != true)
                        {
                            inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + pathAttribute.getValue());
                        }

                        try
                        {
                            inputFile = inputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.exists() != true)
                        {
                            throw constructTermination("messageInputFileDoesntExist", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.isFile() != true)
                        {
                            throw constructTermination("messageInputPathIsntAFile", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.canRead() != true)
                        {
                            throw constructTermination("messageInputFileIsntReadable", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("output-directory") == true)
                    {
                        StartElement outputDirectoryElement = event.asStartElement();
                        Attribute pathAttribute = outputDirectoryElement.getAttributeByName(new QName("path"));

                        if (pathAttribute == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (outputDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        outputDirectory = new File(pathAttribute.getValue());

                        if (outputDirectory.isAbsolute() != true)
                        {
                            outputDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + pathAttribute.getValue());
                        }

                        try
                        {
                            outputDirectory = outputDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageOutputDirectoryCantGetCanonicalPath", ex, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputDirectoryCantGetCanonicalPath", ex, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        Attribute fileLastAttribute = outputDirectoryElement.getAttributeByName(new QName("file-last"));

                        if (fileLastAttribute != null)
                        {
                            if (fileLastAttribute.getValue().equals("true") == true)
                            {
                                fileLast = true;
                            }
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (inputFile == null)
        {
            throw constructTermination("messageJobFileInputFileIsntConfigured", null, null, jobFile.getAbsolutePath(), "input-file");
        }

        if (outputDirectory != null)
        {
            if (outputDirectory.exists() == true)
            {
                if (outputDirectory.isDirectory() == true)
                {
                    if (outputDirectory.canWrite() != true)
                    {
                        throw constructTermination("messageOutputDirectoryIsntWritable", null, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                    }
                }
                else
                {
                    throw constructTermination("messageOutputPathIsntADirectory", null, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                }
            }
            else
            {
                try
                {
                    outputDirectory.mkdirs();
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageOutputDirectoryCantCreate", ex, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                }
            }
        }
        else
        {
            throw constructTermination("messageJobFileOutputDirectoryIsntConfigured", null, null, jobFile.getAbsolutePath(), "output-directory");
        }


        StringBuilder sbOutput = new StringBuilder();
        long instruction = 1L;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
            inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
            InputStream in = new FileInputStream(inputFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("add") == true)
                    {
                        StartElement elementAdd = event.asStartElement();
                        Attribute attributePosition = elementAdd.getAttributeByName(new QName("position"));

                        if (attributePosition == null)
                        {
                            throw constructTermination("messageInputFileEntryIsMissingAnAttribute", null, null, inputFile.getAbsolutePath(), tagName, "position");
                        }

                        int position = -1;

                        try
                        {
                            position = Integer.parseInt(attributePosition.getValue());
                        }
                        catch (NumberFormatException ex)
                        {
                            throw constructTermination("messageInputFileInvalidAttributeNotANumber", ex, null, inputFile.getAbsolutePath(), tagName, attributePosition.getName(), attributePosition.getValue());
                        }

                        int length = sbOutput.length();

                        if (position < 0)
                        {
                            throw constructTermination("messageInputFileInvalidAttributeValue", null, null, inputFile.getAbsolutePath(), tagName, attributePosition.getName(), position);
                        }

                        if (position > (length + 1))
                        {
                            throw constructTermination("messageInputFilePositionExceedsLastPosition", null, null, inputFile.getAbsolutePath(), tagName, position, instruction, length + 1);
                        }

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isEndElement() == true)
                            {
                                if (event.asEndElement().getName().getLocalPart().equals(tagName) == true)
                                {
                                    break;
                                }
                            }
                            else if (event.isCharacters() == true)
                            {
                                length = sbOutput.length();

                                if (position == (length + 1))
                                {
                                    sbOutput.append(event.asCharacters());
                                }
                                else
                                {
                                    sbOutput.insert(position, event.asCharacters());
                                }

                                int lengthAfter = sbOutput.length();

                                if (length < lengthAfter)
                                {
                                    position += lengthAfter - length;
                                }
                                else
                                {

                                }
                            }
                        }

                        File outputFile = new File(outputDirectory.getAbsolutePath() + File.separator + instruction + ".txt");

                        try
                        {
                            BufferedWriter writer = new BufferedWriter(
                                                    new OutputStreamWriter(
                                                    new FileOutputStream(outputFile),
                                                    "UTF-8"));

                            writer.write(sbOutput.toString());

                            writer.flush();
                            writer.close();
                        }
                        catch (FileNotFoundException ex)
                        {
                            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
                        }
                        catch (UnsupportedEncodingException ex)
                        {
                            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
                        }

                        this.outputFiles.add(new OutputFile(instruction, outputFile));

                        ++instruction;
                    }
                    else if (tagName.equals("delete") == true)
                    {
                        StartElement elementDelete = event.asStartElement();
                        Attribute attributePosition = elementDelete.getAttributeByName(new QName("position"));
                        Attribute attributeCount = elementDelete.getAttributeByName(new QName("count"));

                        if (attributePosition == null)
                        {
                            throw constructTermination("messageInputFileEntryIsMissingAnAttribute", null, null, inputFile.getAbsolutePath(), tagName, "position");
                        }

                        if (attributeCount == null)
                        {
                            throw constructTermination("messageInputFileEntryIsMissingAnAttribute", null, null, inputFile.getAbsolutePath(), tagName, "count");
                        }

                        int position = -1;

                        try
                        {
                            position = Integer.parseInt(attributePosition.getValue());
                        }
                        catch (NumberFormatException ex)
                        {
                            throw constructTermination("messageInputFileInvalidAttributeNotANumber", ex, null, inputFile.getAbsolutePath(), tagName, attributePosition.getName(), attributePosition.getValue());
                        }

                        if (position < 0)
                        {
                            throw constructTermination("messageInputFileInvalidAttributeValue", null, null, inputFile.getAbsolutePath(), tagName, attributePosition.getName(), position);
                        }

                        int length = sbOutput.length();

                        if (position > (length + 1))
                        {
                            throw constructTermination("messageInputFilePositionExceedsLastPosition", null, null, inputFile.getAbsolutePath(), tagName, position, instruction, length + 1);
                        }

                        int count = -1;

                        try
                        {
                            count = Integer.parseInt(attributeCount.getValue());
                        }
                        catch (NumberFormatException ex)
                        {
                            throw constructTermination("messageInputFileInvalidAttributeNotANumber", ex, null, inputFile.getAbsolutePath(), tagName, attributeCount.getName(), attributeCount.getValue());
                        }

                        if (count >= 0)
                        {
                            throw constructTermination("messageInputFileInvalidAttributeValue", null, null, inputFile.getAbsolutePath(), tagName, attributeCount.getName(), count);
                        }

                        if ((position - (count * -1)) < 0)
                        {
                            throw constructTermination("messageInputFileCountUndercutsTextBegin", null, null, inputFile.getAbsolutePath(), tagName, position, count, instruction, (position - (count * -1)) * -1);
                        }

                        sbOutput.delete(position - (count * -1), position);

                        File outputFile = new File(outputDirectory.getAbsolutePath() + File.separator + instruction + ".txt");

                        try
                        {
                            BufferedWriter writer = new BufferedWriter(
                                                    new OutputStreamWriter(
                                                    new FileOutputStream(outputFile),
                                                    "UTF-8"));

                            writer.write(sbOutput.toString());

                            writer.flush();
                            writer.close();
                        }
                        catch (FileNotFoundException ex)
                        {
                            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
                        }
                        catch (UnsupportedEncodingException ex)
                        {
                            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
                        }

                        this.outputFiles.add(new OutputFile(instruction, outputFile));

                        ++instruction;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
        }

        if (this.outputFiles.size() > 0 &&
            fileLast == true)
        {
            CopyFileBinary(this.outputFiles.get(this.outputFiles.size() - 1).getFile(), 
                           new File(outputDirectory.getAbsolutePath() + File.separator + "last.txt"));
        }

        return 0;
    }

    public int CopyFileBinary(File from, File to) throws ProgramTerminationException
    {
        if (from.exists() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromDoesntExist", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (from.isFile() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromIsntAFile", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (from.canRead() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromIsntReadable", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (to.exists() == true)
        {
            if (to.isFile() == true)
            {
                if (to.canWrite() != true)
                {
                    throw constructTermination("messageCantCopyBecauseToIsntWritable", null, null, from.getAbsolutePath(), to.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageCantCopyBecauseToIsntAFile", null, null, from.getAbsolutePath(), to.getAbsolutePath());
            }
        }


        ProgramTerminationException exception = null;

        byte[] buffer = new byte[1024];

        FileInputStream reader = null;
        FileOutputStream writer = null;

        try
        {
            to.createNewFile();

            reader = new FileInputStream(from);
            writer = new FileOutputStream(to);

            int bytesRead = reader.read(buffer, 0, buffer.length);

            while (bytesRead > 0)
            {
                writer.write(buffer, 0, bytesRead);
                bytesRead = reader.read(buffer, 0, buffer.length);
            }

            writer.close();
            reader.close();
        }
        catch (FileNotFoundException ex)
        {
            exception = constructTermination("messageCopyFileError", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
        }
        catch (IOException ex)
        {
            exception = constructTermination("messageCopyFileError", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException ex)
                {
                    if (exception == null)
                    {
                        exception = constructTermination("messageCopyFileError", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
                    }
                }
            }

            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException ex)
                {
                    if (exception == null)
                    {
                        exception = constructTermination("messageCopyFileError", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
                    }
                }
            }
        }

        if (exception != null)
        {
            throw exception;
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "change_instructions_executor_1: " + getI10nString(id);
            }
            else
            {
                message = "change_instructions_executor_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "change_instructions_executor_1: " + getI10nString(id);
            }
            else
            {
                message = "change_instructions_executor_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (change_instructions_executor_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(change_instructions_executor_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by change_instructions_executor_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<change-instructions-executor-1-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</change-instructions-executor-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        change_instructions_executor_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public List<OutputFile> getOutputFiles()
    {
        return this.outputFiles;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    public static File resultInfoFile = null;
    protected List<OutputFile> outputFiles = new ArrayList<OutputFile>();
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nChangeInstructionsExecutor1Console";
    private ResourceBundle l10nConsole;
}
