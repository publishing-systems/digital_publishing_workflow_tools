/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of xml_to_dimgra_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_to_dimgra_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_to_dimgra_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_to_dimgra_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/xml_to_dimgra/xml_to_dimgra_1/StructureDefinition.java
 * @author Stephan Kreutzer
 * @since 2021-04-15
 */



class StructureDefinition
{
    public StructureDefinition(String path, String dimension, boolean isMaster)
    {
        this.path = path;
        this.dimension = dimension;
        this.isMaster = isMaster;
    }

    public String getPath()
    {
        return this.path;
    }

    public String getDimension()
    {
        return this.dimension;
    }

    public boolean getIsMaster()
    {
        return this.isMaster;
    }

    protected String path;
    protected String dimension;
    protected boolean isMaster;
}
