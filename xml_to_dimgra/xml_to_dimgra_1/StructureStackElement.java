/* Copyright (C) 2021  Stephan Kreutzer
 *
 * This file is part of xml_to_dimgra_1.
 *
 * xml_to_dimgra_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_to_dimgra_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_to_dimgra_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/xml_to_dimgra/xml_to_dimgra_1/StructureStackElement.java
 * @author Stephan Kreutzer
 * @since 2021-04-18
 */



public class StructureStackElement
{
    public StructureStackElement(String name, String path)
    {
        this.name = name;
        this.path = path;
    }

    public String getName()
    {
        return this.name;
    }

    public String getPath()
    {
        return this.path;
    }

    protected String name;
    protected String path;
}
