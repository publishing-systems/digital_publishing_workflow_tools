/* Copyright (C) 2014-2022 Stephan Kreutzer
 *
 * This file is part of xhtml_prepare_for_latex_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xhtml_prepare_for_latex_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xhtml_prepare_for_latex_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xhtml_prepare_for_latex_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/latex/xhtml_prepare_for_latex/xhtml_prepare_for_latex_1/xhtml_prepare_for_latex_1.java
 * @brief Prepares the text of a XHTML file for consumption by LaTeX (escaping of
 *     special characters).
 * @author Stephan Kreutzer
 * @since 2014-06-14
 */



import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.DTD;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.events.EntityReference;
import javax.xml.stream.events.Comment;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import java.net.URLDecoder;
import java.util.Iterator;



public class xhtml_prepare_for_latex_1
{
    protected xhtml_prepare_for_latex_1()
    {
        // Singleton to protect xhtml_prepare_for_latex_1.resultInfoFile from conflicting use.
    }

    public static xhtml_prepare_for_latex_1 getInstance()
    {
        if (xhtml_prepare_for_latex_1.xhtml_prepare_for_latex_1Instance == null)
        {
            xhtml_prepare_for_latex_1.xhtml_prepare_for_latex_1Instance = new xhtml_prepare_for_latex_1();
        }

        return xhtml_prepare_for_latex_1.xhtml_prepare_for_latex_1Instance;
    }

    public static void main(String args[])
    {
        System.out.print("xhtml_prepare_for_latex_1 Copyright (C) 2014-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        xhtml_prepare_for_latex_1 instance = xhtml_prepare_for_latex_1.getInstance();
        instance.getResults().clear();
        instance.getInfoMessages().clear();

        try
        {
            instance.call(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_prepare_for_latex_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xhtml-prepare-for-latex-1-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getResults() != null)
                {
                    if (instance.getResults().isEmpty() != true)
                    {
                        for (Map.Entry<String, String> entry : instance.getResults().entrySet())
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            String value = entry.getValue();
                            value = value.replaceAll("&", "&amp;");
                            value = value.replaceAll("<", "&lt;");
                            value = value.replaceAll(">", "&gt;");

                            writer.write("    <" + entry.getKey() + ">" + value + "</" + entry.getKey() + ">\n");
                        }
                    }
                }

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</xhtml-prepare-for-latex-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(1);
            }
        }

        instance.getInfoMessages().clear();
        instance.getResults().clear();
        instance.resultInfoFile = null;
    }

    public int call(String args[]) throws ProgramTerminationException
    {
        this.resultInfoFile = null;
        this.getResults().clear();
        this.getInfoMessages().clear();

        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\txhtml_prepare_for_latex_1 " + getI10nString("messageParameterList") + "\n");
        }


        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        xhtml_prepare_for_latex_1.resultInfoFile = resultInfoFile;

        String programPath = xhtml_prepare_for_latex_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }

        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("xhtml_prepare_for_latex_1: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));

        File inputFile = null;
        boolean optionPreserveNonTypographicDoublequotes = false;
        File outputFile = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input-file") == true)
                    {
                        if (inputFile != null)
                        {
                            throw constructTermination("messageJobFileEntrySpecifiedMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StartElement elementInputFile = event.asStartElement();
                        Attribute attributePath = elementInputFile.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        String filePath = attributePath.getValue();

                        if (filePath.isEmpty() == true)
                        {
                            throw constructTermination("messageJobFileAttributeValueIsEmpty", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        inputFile = new File(filePath);

                        if (inputFile.isAbsolute() != true)
                        {
                            inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + filePath);
                        }

                        try
                        {
                            inputFile = inputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath());
                        }

                        if (inputFile.exists() != true)
                        {
                            throw constructTermination("messageInputFileDoesntExist", null, null, jobFile.getAbsolutePath(), inputFile.getAbsolutePath());
                        }

                        if (inputFile.isFile() != true)
                        {
                            throw constructTermination("messageInputPathIsntAFile", null, null, jobFile.getAbsolutePath(), inputFile.getAbsolutePath());
                        }

                        if (inputFile.canRead() != true)
                        {
                            throw constructTermination("messageInputFileIsntReadable", null, null, jobFile.getAbsolutePath(), inputFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("options") == true)
                    {
                        StartElement elementOptions = event.asStartElement();
                        Attribute attributePreserveNonTypographicDoublequotes = elementOptions.getAttributeByName(new QName("preserve-non-typographic-doublequotes"));

                        if (attributePreserveNonTypographicDoublequotes == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "preserve-non-typographic-doublequotes");
                        }

                        optionPreserveNonTypographicDoublequotes = (attributePreserveNonTypographicDoublequotes.getValue().equals("true") == true);
                    }
                    else if (tagName.equals("output-file") == true)
                    {
                        if (outputFile != null)
                        {
                            throw constructTermination("messageJobFileEntrySpecifiedMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StartElement elementOutputFile = event.asStartElement();
                        Attribute pathAttribute = elementOutputFile.getAttributeByName(new QName("path"));

                        if (pathAttribute == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        String filePath = pathAttribute.getValue();

                        if (filePath.isEmpty() == true)
                        {
                            throw constructTermination("messageJobFileAttributeValueIsEmpty", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        outputFile = new File(filePath);

                        if (outputFile.isAbsolute() != true)
                        {
                            outputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + filePath);
                        }

                        try
                        {
                            outputFile = outputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath());
                        }

                        if (outputFile.exists() == true)
                        {
                            throw constructTermination("messageOutputPathDoesAlreadyExist", null, null, outputFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (inputFile == null)
        {
            throw constructTermination("messageJobFileNoInputFileConfigured", null, null, jobFile.getAbsolutePath());
        }

        if (outputFile == null)
        {
            throw constructTermination("messageJobFileNoOutputFileConfigured", null, null, jobFile.getAbsolutePath());
        }


        // The javax.xml.stream.events.DTD XMLEvent can provide the DOCTYPE
        // declaration in a first pass/read as well, but only as a full string
        // too. Would it still work to set the XML resolver this late, as it
        // should be the case with proper XML technology/parsers? But in Java,
        // for the XMLInputFactory?

        String doctype = new String();
        String doctypeDtdName = null;
        String doctypeDtdId = null;

        {
            String doctypeDeclaration = new String("<!DOCTYPE");
            int doctypePosMatching = 0;

            try
            {
                FileInputStream in = new FileInputStream(inputFile);

                int currentByte = 0;

                do
                {
                    currentByte = in.read();

                    if (currentByte < 0 ||
                        currentByte > 255)
                    {
                        break;
                    }

                    char currentByteCharacter = (char) currentByte;

                    if (doctypePosMatching < doctypeDeclaration.length())
                    {
                        if (currentByteCharacter == doctypeDeclaration.charAt(doctypePosMatching))
                        {
                            doctypePosMatching++;
                            doctype += currentByteCharacter;
                        }
                        else
                        {
                            doctypePosMatching = 0;
                            doctype = new String();
                        }
                    }
                    else
                    {
                        doctype += currentByteCharacter;

                        if (currentByteCharacter == '>')
                        {
                            boolean inWhitespace = true;
                            boolean isInQuotes = false;
                            List<StringBuilder> tokens = new ArrayList<StringBuilder>();

                            for (int i = doctypeDeclaration.length(), max = doctype.length() - 1; i < max; i++)
                            {
                                char c = doctype.charAt(i);

                                if (Character.isWhitespace(c) != true)
                                {
                                    if (inWhitespace == true)
                                    {
                                        tokens.add(new StringBuilder());
                                    }

                                    if (c != '"')
                                    {
                                        StringBuilder token = tokens.get(tokens.size()-1);
                                        token.append(c);
                                    }
                                    else
                                    {
                                        isInQuotes = !isInQuotes;
                                    }

                                    inWhitespace = false;
                                }
                                else
                                {
                                    if (isInQuotes != true)
                                    {
                                        inWhitespace = true;
                                    }
                                    else
                                    {
                                        StringBuilder token = tokens.get(tokens.size()-1);
                                        token.append(c);
                                    }
                                }
                            }

                            if (tokens.size() < 2)
                            {
                                throw constructTermination("messageInputFileDoctypeIsEmpty", null, null, inputFile.getAbsolutePath());
                            }

                            if (tokens.get(0).toString().equalsIgnoreCase("html") != true)
                            {
                                /** @todo Maybe reset and continue reading to support multiple DOCTYPE declarations? */

                                // "Root Name" needs to be equal to the root element name.
                                throw constructTermination("messageInputFileDoctypeWithWrongRootName", null, null, inputFile.getAbsolutePath(), "html", tokens.get(0).toString());
                            }

                            for (int i = 1, max = tokens.size(); i < max; i++)
                            {
                                String token = tokens.get(i).toString();

                                if (token.equals("PUBLIC") == true)
                                {
                                    if (i + 2 < max)
                                    {
                                        doctypeDtdName = tokens.get(i + 1).toString();
                                        doctypeDtdId = tokens.get(i + 2).toString();
                                    }
                                    else if (i + 2 >= max)
                                    {
                                        throw constructTermination("messageInputFileDoctypePublicIncomplete", null, null, inputFile.getAbsolutePath());
                                    }

                                    break;
                                }
                                else
                                {

                                }
                            }

                            break;
                        }
                    }

                } while (true);
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
            }
        }

        if (doctype.isEmpty() == true)
        {
            throw constructTermination("messageInputFileNoDoctype", null, null, inputFile.getAbsolutePath());
        }

        if (doctypeDtdName == null ||
            doctypeDtdId == null)
        {
            throw constructTermination("messageInputFileDoctypeWithoutIdentifier", null, null, inputFile.getAbsolutePath());
        }

        XmlResolverLocal entityResolver = null;

        if (doctypeDtdName.equals("-//W3C//DTD XHTML 1.0 Strict//EN") == true)
        {
            entityResolver = new XmlResolverLocal(new File(programPath + "entities" + File.separator + "config_xhtml_1_0_strict.xml"));
        }
        else if (doctypeDtdName.equals("-//W3C//DTD XHTML 1.1//EN") == true)
        {
            entityResolver = new XmlResolverLocal(new File(programPath + "entities" + File.separator + "config_xhtml_1_1.xml"));
        }
        else
        {
            throw constructTermination("messageInputFileDoctypeNotSupported", null, null, inputFile.getAbsolutePath(), doctypeDtdName);
        }

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            inputFactory.setXMLResolver(entityResolver);

            InputStream in = new FileInputStream(inputFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in, "UTF-8");

            XMLEvent event = null;

            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(outputFile),
                                    "UTF-8"));

            while (eventReader.hasNext() == true)
            {
                event = eventReader.nextEvent();

                if (event.isStartDocument() == true)
                {
                    StartDocument startDocument = (StartDocument)event;

                    writer.write("<?xml version=\"" + startDocument.getVersion() + "\"");

                    if (startDocument.encodingSet() == true)
                    {
                        writer.write(" encoding=\"" + startDocument.getCharacterEncodingScheme() + "\"");
                    }

                    if (startDocument.standaloneSet() == true)
                    {
                        writer.write(" standalone=\"");

                        if (startDocument.isStandalone() == true)
                        {
                            writer.write("yes");
                        }
                        else
                        {
                            writer.write("no");
                        }

                        writer.write("\"");
                    }

                    writer.write("?>\n");

                    writer.write(doctype + "\n");
                    writer.write("<!-- This file was generated by xhtml_prepare_for_latex_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                }
                else if (event.isStartElement() == true)
                {
                    QName elementName = event.asStartElement().getName();
                    String fullElementName = elementName.getLocalPart();

                    if (elementName.getPrefix().isEmpty() != true)
                    {
                        fullElementName = elementName.getPrefix() + ":" + fullElementName;
                    }

                    writer.write("<" + fullElementName);

                    // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                    @SuppressWarnings("unchecked")
                    Iterator<Namespace> namespaces = (Iterator<Namespace>)event.asStartElement().getNamespaces();

                    while (namespaces.hasNext() == true)
                    {
                        Namespace namespace = namespaces.next();

                        if (namespace.isDefaultNamespaceDeclaration() == true &&
                            namespace.getPrefix().length() <= 0)
                        {
                            writer.write(" xmlns=\"" + namespace.getNamespaceURI() + "\"");
                        }
                        else
                        {
                            writer.write(" xmlns:" + namespace.getPrefix() + "=\"" + namespace.getNamespaceURI() + "\"");
                        }
                    }

                    // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                    @SuppressWarnings("unchecked")
                    Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

                    while (attributes.hasNext() == true)
                    {
                        Attribute attribute = attributes.next();
                        QName attributeName = attribute.getName();
                        String fullAttributeName = attributeName.getLocalPart();

                        if (attributeName.getPrefix().length() > 0)
                        {
                            fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
                        }

                        if (fullElementName.equals("a") == true &&
                            fullAttributeName.equals("href") == true)
                        {
                            writer.write(" " + fullAttributeName + "=\"");

                            String text = attribute.getValue();
                            int textLength = text.length();

                            for (int i = 0; i < textLength; i++)
                            {
                                char character = text.charAt(i);

                                switch (character)
                                {
                                case '#':
                                    writer.write("\\#");
                                    break;
                                case '$':
                                    writer.write("\\$");
                                    break;
                                case '%':
                                    writer.write("\\%");
                                    break;
                                case '<':
                                    writer.write("&lt;");
                                    break;
                                case '>':
                                    writer.write("&gt;");
                                    break;
                                case '&':
                                    writer.write("\\&amp;");
                                    break;
                                case '\'':
                                    writer.write("&apos;");
                                    break;
                                case '"':
                                    writer.write("&quot;");
                                    break;
                                case '\\':
                                    writer.write("\\textbackslash{}");
                                    break;
                                case '^':
                                    writer.write("\\textasciicircum{}");
                                    break;
                                case '_':
                                    writer.write("\\_");
                                    break;
                                case '{':
                                    writer.write("\\{");
                                    break;
                                case '}':
                                    writer.write("\\}");
                                    break;
                                case '~':
                                    writer.write("\\textasciitilde{}");
                                    break;
                                default:
                                    writer.write(character);
                                    break;
                                }
                            }

                            writer.write("\"");
                        }
                        else
                        {
                            String attributeValue = attribute.getValue();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            attributeValue = attributeValue.replaceAll("&", "&amp;");
                            attributeValue = attributeValue.replaceAll("\"", "&quot;");
                            attributeValue = attributeValue.replaceAll("'", "&apos;");
                            attributeValue = attributeValue.replaceAll("<", "&lt;");
                            attributeValue = attributeValue.replaceAll(">", "&gt;");

                            writer.write(" " + fullAttributeName + "=\"" + attributeValue + "\"");
                        }
                    }

                    writer.write(">");
                }
                else if (event.isEndElement() == true)
                {
                    boolean output = true;

                    QName elementName = event.asEndElement().getName();
                    String fullElementName = elementName.getLocalPart();

                    if (elementName.getPrefix().isEmpty() != true)
                    {
                        fullElementName = elementName.getPrefix() + ":" + fullElementName;
                    }

                    writer.write("</" + fullElementName + ">");
                }
                else if (event.isCharacters() == true)
                {
                    String text = event.asCharacters().getData();

                    int textLength = text.length();

                    for (int i = 0; i < textLength; i++)
                    {
                        char character = text.charAt(i);

                        switch (character)
                        {
                        case '#':
                            writer.write("\\#");
                            break;
                        case '$':
                            writer.write("\\$");
                            break;
                        case '%':
                            writer.write("\\%");
                            break;
                        case '<':
                            writer.write("&lt;");
                            break;
                        case '>':
                            writer.write("&gt;");
                            break;
                        case '&':
                            writer.write("\\&amp;");
                            break;
                        case '\\':
                            writer.write("\\textbackslash{}");
                            break;
                        case '^':
                            writer.write("\\textasciicircum{}");
                            break;
                        case '_':
                            writer.write("\\_");
                            break;
                        case '{':
                            writer.write("\\{");
                            break;
                        case '}':
                            writer.write("\\}");
                            break;
                        case '~':
                            writer.write("\\textasciitilde{}");
                            break;
                        /*
                        case '"':
                            // The ngerman package will try to replace any '"' with leading or followed
                            // by a space character with the corresponding typographic quotation marks.
                            // If this feature would preserve non-typographic quotation marks, users could
                            // be invited to use them in the source files. Therefore, this feature is
                            // disabled in order to let the ngerman package do its magic or to let the
                            // processing fail for those cases which are handled here, where non-typographic
                            // quotation marks are needed explicitly.
                            if (optionPreserveNonTypographicDoublequotes == true)
                            {
                                // Needed for ngerman package (any '"' followed by a character instead
                                // of a space would cause an error).
                                writer.write("\\textquotedbl{}");
                            }
                            else
                            {
                                writer.write(character);
                            }

                            break;
                        */
                        default:
                            writer.write(character);
                            break;
                        }
                    }
                }
                else if (event.getEventType() == XMLStreamConstants.ENTITY_REFERENCE)
                {
                    String entityName = ((EntityReference)event).getName();

                    writer.write("&");
                    writer.write(entityName);
                    writer.write(";");
                }
                else if (event.getEventType() == XMLStreamConstants.COMMENT)
                {
                    writer.write("<!--" + ((Comment)event).getText() + "-->");
                }
                else if (event.getEventType() == XMLStreamConstants.DTD)
                {
                    DTD dtd = (DTD) event;

                    if (dtd != null)
                    {

                    }
                }
                else if (event.isEndDocument() == true)
                {

                }
            }

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageErrorWhileProcessing", ex, null);
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageErrorWhileProcessing", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageErrorWhileProcessing", ex, null);
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xhtml_prepare_for_latex_1: " + getI10nString(id);
            }
            else
            {
                message = "xhtml_prepare_for_latex_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xhtml_prepare_for_latex_1: " + getI10nString(id);
            }
            else
            {
                message = "xhtml_prepare_for_latex_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            innerException.printStackTrace();
        }

        if (xhtml_prepare_for_latex_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xhtml_prepare_for_latex_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_prepare_for_latex_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xhtml-prepare-for-latex-1-result-information>\n");
                writer.write("  <failure>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    ex.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </failure>\n");
                writer.write("</xhtml-prepare-for-latex-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        xhtml_prepare_for_latex_1.resultInfoFile = null;

        System.exit(1);
        return -1;
    }

    public Map<String, String> getResults()
    {
        return this.results;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    private static xhtml_prepare_for_latex_1 xhtml_prepare_for_latex_1Instance;

    public static File resultInfoFile = null;
    protected Map<String, String> results = new HashMap<String, String>();
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nXhtmlPrepareForLatex1Console";
    private ResourceBundle l10nConsole;
}
