/* Copyright (C) 2021-2023 Stephan Kreutzer
 *
 * This file is part of dimgra_to_xml_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * dimgra_to_xml_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * dimgra_to_xml_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with dimgra_to_xml_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/dimgra_to_xml/dimgra_to_xml_1/Node.java
 * @author Stephan Kreutzer
 * @since 2021-05-24
 */



class Node
{
    public Node(String id,
                String value)
    {
        this.id = id;
        this.value = value;
    }

    public String getId()
    {
        return this.id;
    }

    public String getValue()
    {
        return this.value;
    }

    protected String id = null;
    protected String value = null;
}
