/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of dimgra_to_xml_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * dimgra_to_xml_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * dimgra_to_xml_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with dimgra_to_xml_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/dimgra_to_xml/dimgra_to_xml_1/Edge.java
 * @author Stephan Kreutzer
 * @since 2023-06-13
 */



class Edge
{
    public Edge(String sourceId,
                String targetId,
                String dimension,
                boolean isEstablishingDimension)
    {
        this.sourceId = sourceId;
        this.targetId = targetId;
        this.dimension = dimension;
        this.isEstablishingDimension = isEstablishingDimension;
    }

    public String getSourceId()
    {
        return this.sourceId;
    }

    public String getTargetId()
    {
        return this.targetId;
    }

    public String getDimension()
    {
        return this.dimension;
    }

    public boolean getIsEstablishingDimension()
    {
        return this.isEstablishingDimension;
    }

    protected String sourceId = null;
    protected String targetId = null;
    protected String dimension = null;
    protected boolean isEstablishingDimension = false;
}
