<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2021 Stephan Kreutzer

This file is part of xml_xslt_auto_transformator_1 workflow, a submodule of the
digital_publishing_workflow_tools package.

xml_xslt_auto_transformator_1 workflow is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

xml_xslt_auto_transformator_1 workflow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with xml_xslt_auto_transformator_1 workflow. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="//namespaces">
    <xml-xslt-transformator-1-jobfile>
      <xsl:apply-templates select="./namespace[@is-default='true']"/>
      <xsl:apply-templates select="./namespace[@is-default='false']"/>
    </xml-xslt-transformator-1-jobfile>
  </xsl:template>

  <xsl:template match="//namespaces/namespace">
      <xsl:choose>
        <xsl:when test="@id='http://www.w3.org/1999/xhtml'">
          <job input-file="./output.xml" output-file="./output.xml" entities-resolver-config-file="../../../../entities/config_xhtml_1_1.xml" stylesheet-file="../stylesheet_xhtml.xsl" input-file-equals-output-file-overwrite="true"/>
        </xsl:when>
        <xsl:when test="@id='htx-scheme-id://network.murmurations.20190624T170718Z/murmurations.20200612T040000Z'">
          <job input-file="./output.xml" output-file="./output.xml" entities-resolver-config-file="../../../../entities/config_xhtml_1_1.xml" stylesheet-file="../stylesheet_murmurations.xsl" input-file-equals-output-file-overwrite="true"/>
        </xsl:when>
      </xsl:choose>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
