<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2021 Stephan Kreutzer

This file is part of xml_xslt_auto_transformator_1 workflow, a submodule of the
digital_publishing_workflow_tools package.

xml_xslt_auto_transformator_1 workflow is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

xml_xslt_auto_transformator_1 workflow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with xml_xslt_auto_transformator_1 workflow. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:murmurations="htx-scheme-id://network.murmurations.20190624T170718Z/murmurations.20200612T040000Z">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>

  <xsl:template match="//murmurations:murmuration">
    <div style="border: 1px solid #000000;">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="node()|@*|text()">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*|text()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
