/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of xml_namespace_extractor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_namespace_extractor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_namespace_extractor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_namespace_extractor_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/xml_namespace_extractor/xml_namespace_extractor_1/NamespaceInfo.java
 * @author Stephan Kreutzer
 * @since 2020-12-26
 */



class NamespaceInfo
{
    public NamespaceInfo(String id,
                         boolean isDefaultNamespace)
    {
        this.id = id;
        this.isDefaultNamespace = isDefaultNamespace;
    }

    public String getId()
    {
        return this.id;
    }

    public boolean getIsDefaultNamespace()
    {
        return this.isDefaultNamespace;
    }

    protected String id = null;
    protected boolean isDefaultNamespace = true;
}
