# Copyright (C) 2017-2022 Stephan Kreutzer
#
# This file is part of sof_text_uncondenser_1, a submodule of the
# digital_publishing_workflow_tools package.
#
# sof_text_uncondenser_1 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# sof_text_uncondenser_1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with sof_text_uncondenser_1. If not, see <http://www.gnu.org/licenses/>.

messageArgumentsMissingUsage = Usage:
messageParameterList = job-file result-info-file
messageResultInfoFileCantGetCanonicalPath = Can''t get canonical path of result information file "{0}".
messageResultInfoFileIsntWritable = Result information file "{0}" isn''t writable.
messageResultInfoPathIsntAFile = Result information path "{0}" isn''t a file.
messageCantDetermineProgramPath = Can''t determine program path.
messageJobFileCantGetCanonicalPath = Can''t get canonical path of the job file "{0}".
messageJobFileDoesntExist = Job file "{0}" doesn''t exist.
messageJobPathIsntAFile = Job path "{0}" exists, but isn''t a file.
messageJobFileIsntReadable = Job file "{0}" isn''t readable.
messageCallDetails = Called with job file "{0}" and result information file "{1}".
messageJobFileEntryIsMissingAnAttribute = Element "{1}" in job file "{0}" is missing its "{2}" attribute.
messageJobFileElementConfiguredMoreThanOnce = Element "{1}" configured more than once in job file "{0}".
messageInputFileCantGetCanonicalPath = Can''t get canonical path of the input file "{0}" as specified in job file "{1}".
messageInputFileDoesntExist = Input file "{0}" as specified in job file "{1}" doesn''t exist.
messageInputPathIsntAFile = Input path "{0}" as specified in job file "{1}" exists, but isn''t a file.
messageInputFileIsntReadable = Input file "{0}" as specified in job file "{1}" isn''t readable.
messageTempDirectoryCantGetCanonicalPath = Can''t get canonical path of temp directory "{0}".
messageOutputFileCantGetCanonicalPath = Can''t get canonical path of the output file "{0}" as specified in job file "{1}".
messageOutputFileIsntWritable = Output file "{0}" as specified in job file "{1}" does exist, but isn''t writable.
messageOutputPathIsntAFile = Output path "{0}" as specified in job file "{1}" exists, but isn''t a file.
messageJobFileErrorWhileReading = An error occurred while reading job file "{0}".
messageJobFileInputFileNotConfigured = There is no input file configured in job file "{0}".
messageJobFileOutputFileIsntConfigured = There is no output file configured in job file "{0}".
messageJobFileOutputFileExistsAlready = Output file "{1}", configured in job file "{0}", exists already.
messageTempDirectoryIsntWritable = Temporary directory "{0}" isn''t writable.
messageTempPathIsntADirectory = Temporary path "{0}" isn''t a directory.
messageTempDirectoryCantCreate = Can''t create temporary directory "{0}".
messageTextFileExistsButIsntWritable = The temporary text file "{0}" does still exist, but isn''t overwritable.
messageTextPathExistsButIsntAFile = A temporary text path "{0}" does already exist, but isn''t a file.
messageInputFileNestedElementInsideText = Nested element found inside "text" element in input file "{0}".
messageInputFileErrorWhileReading = An error occurred while reading input file "{0}".
messageOutputFileErrorWhileWriting = An error occurred while writing output file "{0}".
messageInputFileEntryIsMissingAnAttribute = Element "{1}" in input file "{0}" is missing its "{2}" attribute.
messageInputFileEntryAttributeValueIsntANumber = Value "{3}" for attribute "{2}" of element "{1}" in input file "{0}" isn''t a number.
messageTextFileLengthVersusInputFileSofPositionsMismatch = Mismatch between text file length of "{0}" versus input file SOF positions of "{1}".
messageErrorWhileCopying = An error occurred while copying from "{0}" to "{1}".
messageInputFileSofPositionDifferenceIsNegative = SOF position difference in sequence is negative in input file "{0}".
messageInputFileNestedElement = Nested element "{2}" found inside "{1}" element in input file "{0}".
messageFailedToDeleteTemporaryTextFile = Failed to delete temporary text file "{1}".
