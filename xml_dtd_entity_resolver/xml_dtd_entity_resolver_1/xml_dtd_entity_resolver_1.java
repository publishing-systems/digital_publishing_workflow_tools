/* Copyright (C) 2014-2022 Stephan Kreutzer
 *
 * This file is part of xml_dtd_entity_resolver_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_dtd_entity_resolver_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_dtd_entity_resolver_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_dtd_entity_resolver_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/xml_dtd_entity_resolver/xml_dtd_entity_resolver_1/xml_dtd_entity_resolver_1.java
 * @brief Resolves DTD entities by replacing them with their textual equivalent.
 * @details This serves to transfer XML data dependent on a DTD to pure, DTD-independent XML
 *     that can use pure XML features to declare its semantics and full content, so no
 *     actual DTDs and a DTD parser/resolver is needed any more for further processing.
 * @author Stephan Kreutzer
 * @since 2018-09-17
 */



import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.DTD;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.EntityReference;
import javax.xml.stream.events.Comment;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;



public class xml_dtd_entity_resolver_1
{
    public static void main(String args[])
    {
        System.out.print("xml_dtd_entity_resolver_1 Copyright (C) 2014-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        xml_dtd_entity_resolver_1 instance = new xml_dtd_entity_resolver_1();

        try
        {
            instance.call(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xml_dtd_entity_resolver_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and http://www.publishing-systems.org). -->\n");
                writer.write("<xml-dtd-entity-resolver-1-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</xml-dtd-entity-resolver-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int call(String args[])
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\txml_dtd_entity_resolver_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        xml_dtd_entity_resolver_1.resultInfoFile = resultInfoFile;
        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("xml_dtd_entity_resolver_1: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));

        File inputFile = null;
        File entitiesResolverConfigFile = null;
        File entitiesResolveDictionary = null;
        boolean ignoreMissingMappings = false;
        File outputFile = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input-file") == true)
                    {
                        if (inputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StartElement inputFileElement = event.asStartElement();
                        Attribute attributePath = inputFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        inputFile = new File(attributePath.getValue());

                        if (inputFile.isAbsolute() != true)
                        {
                            inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            inputFile = inputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.exists() != true)
                        {
                            throw constructTermination("messageInputFileDoesntExist", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.isFile() != true)
                        {
                            throw constructTermination("messageInputPathIsntAFile", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.canRead() != true)
                        {
                            throw constructTermination("messageInputFileIsntReadable", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("entities-resolver-config-file") == true)
                    {
                        if (entitiesResolverConfigFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StartElement entitiesResolverConfigFileElement = event.asStartElement();
                        Attribute attributePath = entitiesResolverConfigFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        entitiesResolverConfigFile = new File(attributePath.getValue());

                        if (entitiesResolverConfigFile.isAbsolute() != true)
                        {
                            entitiesResolverConfigFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            entitiesResolverConfigFile = entitiesResolverConfigFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageEntitiesResolverConfigFileCantGetCanonicalPath", ex, null, entitiesResolverConfigFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageEntitiesResolverConfigFileCantGetCanonicalPath", ex, null, entitiesResolverConfigFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (entitiesResolverConfigFile.exists() != true)
                        {
                            throw constructTermination("messageEntitiesResolverConfigFileDoesntExist", null, null, entitiesResolverConfigFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (entitiesResolverConfigFile.isFile() != true)
                        {
                            throw constructTermination("messageEntitiesResolverConfigPathIsntAFile", null, null, entitiesResolverConfigFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (entitiesResolverConfigFile.canRead() != true)
                        {
                            throw constructTermination("messageEntitiesResolverConfigFileIsntReadable", null, null, entitiesResolverConfigFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("entities-resolve-dictionary") == true)
                    {
                        if (entitiesResolveDictionary != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StartElement entitiesResolveDictionaryFileElement = event.asStartElement();
                        Attribute attributePath = entitiesResolveDictionaryFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        entitiesResolveDictionary = new File(attributePath.getValue());

                        if (entitiesResolveDictionary.isAbsolute() != true)
                        {
                            entitiesResolveDictionary = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            entitiesResolveDictionary = entitiesResolveDictionary.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageEntitiesResolveDictionaryFileCantGetCanonicalPath", ex, null, entitiesResolveDictionary.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageEntitiesResolveDictionaryFileCantGetCanonicalPath", ex, null, entitiesResolveDictionary.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (entitiesResolveDictionary.exists() != true)
                        {
                            throw constructTermination("messageEntitiesResolveDictionaryFileDoesntExist", null, null, entitiesResolveDictionary.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (entitiesResolveDictionary.isFile() != true)
                        {
                            throw constructTermination("messageEntitiesResolveDictionaryPathIsntAFile", null, null, entitiesResolveDictionary.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (entitiesResolveDictionary.canRead() != true)
                        {
                            throw constructTermination("messageEntitiesResolveDictionaryFileIsntReadable", null, null, entitiesResolveDictionary.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        Attribute attributeIgnoreMissingMappings = entitiesResolveDictionaryFileElement.getAttributeByName(new QName("ignore-missing-mappings"));

                        if (attributeIgnoreMissingMappings != null)
                        {
                            if (attributeIgnoreMissingMappings.getValue().equals("true") == true)
                            {
                                ignoreMissingMappings = true;
                            }
                        }
                    }
                    else if (tagName.equals("output-file") == true)
                    {
                        if (outputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        StartElement outputFileElement = event.asStartElement();
                        Attribute attributePath = outputFileElement.getAttributeByName(new QName("path"));

                        outputFile = new File(attributePath.getValue());

                        if (outputFile.isAbsolute() != true)
                        {
                            outputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            outputFile = outputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (outputFile.exists() == true)
                        {
                            if (outputFile.isFile() == true)
                            {
                                if (outputFile.canWrite() != true)
                                {
                                    throw constructTermination("messageOutputFileIsntWritable", null, null, outputFile.getAbsolutePath());
                                }
                            }
                            else
                            {
                                throw constructTermination("messageOutputPathIsntAFile", null, null, outputFile.getAbsolutePath());
                            }
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (inputFile == null)
        {
            throw constructTermination("messageJobFileInputFileIsntConfigured", null, null, jobFile.getAbsolutePath(), "input-file");
        }

        if (entitiesResolverConfigFile == null &&
            entitiesResolveDictionary == null)
        {
            throw constructTermination("messageJobFileResolveFileIsntConfigured", null, null, jobFile.getAbsolutePath(), "entities-resolver-config-file, entities-resolve-dictionary");
        }

        if (outputFile == null)
        {
            throw constructTermination("messageJobFileOutputFileIsntConfigured", null, null, jobFile.getAbsolutePath(), "output-file");
        }


        if (entitiesResolverConfigFile != null)
        {
            XMLResolverLocal xmlResolverLocal = new XMLResolverLocal(entitiesResolverConfigFile);

            try
            {
                /**
                 * @todo This might be shortened by doing a XSLT that copies everything, but
                 *     has an entity resolver configured.
                 */
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(outputFile),
                                        "UTF-8"));

                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    inputFactory.setXMLResolver(xmlResolverLocal); //inputFactory.setProperty(XMLInputFactory.RESOLVER, xmlResolverLocal);
                    inputFactory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, true);

                    InputStream in = new FileInputStream(inputFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
                    XMLEvent event = null;

                    while (eventReader.hasNext() == true)
                    {
                        event = eventReader.nextEvent();

                        if (event.isStartDocument() == true)
                        {
                            StartDocument startDocument = (StartDocument)event;

                            writer.write("<?xml version=\"" + startDocument.getVersion() + "\"");

                            if (startDocument.encodingSet() == true)
                            {
                                writer.write(" encoding=\"" + startDocument.getCharacterEncodingScheme() + "\"");
                            }

                            if (startDocument.standaloneSet() == true)
                            {
                                writer.write(" standalone=\"");

                                if (startDocument.isStandalone() == true)
                                {
                                    writer.write("yes");
                                }
                                else
                                {
                                    writer.write("no");
                                }

                                writer.write("\"");
                            }

                            writer.write("?>\n");
                        }
                        else if (event.isStartElement() == true)
                        {
                            QName elementName = event.asStartElement().getName();
                            String fullElementName = elementName.getLocalPart();

                            if (elementName.getPrefix().isEmpty() != true)
                            {
                                fullElementName = elementName.getPrefix() + ":" + fullElementName;
                            }

                            writer.write("<" + fullElementName);

                            // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                            @SuppressWarnings("unchecked")
                            Iterator<Namespace> namespaces = (Iterator<Namespace>)event.asStartElement().getNamespaces();

                            while (namespaces.hasNext() == true)
                            {
                                Namespace namespace = namespaces.next();

                                if (namespace.isDefaultNamespaceDeclaration() == true &&
                                    namespace.getPrefix().length() <= 0)
                                {
                                    writer.write(" xmlns=\"" + namespace.getNamespaceURI() + "\"");
                                }
                                else
                                {
                                    writer.write(" xmlns:" + namespace.getPrefix() + "=\"" + namespace.getNamespaceURI() + "\"");
                                }
                            }

                            // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                            @SuppressWarnings("unchecked")
                            Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

                            while (attributes.hasNext() == true)
                            {
                                Attribute attribute = attributes.next();
                                QName attributeName = attribute.getName();
                                String fullAttributeName = attributeName.getLocalPart();

                                if (attributeName.getPrefix().length() > 0)
                                {
                                    fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
                                }

                                String attributeValue = attribute.getValue();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                attributeValue = attributeValue.replaceAll("&", "&amp;");
                                attributeValue = attributeValue.replaceAll("\"", "&quot;");
                                attributeValue = attributeValue.replaceAll("'", "&apos;");
                                attributeValue = attributeValue.replaceAll("<", "&lt;");
                                attributeValue = attributeValue.replaceAll(">", "&gt;");

                                writer.write(" " + fullAttributeName + "=\"" + attributeValue + "\"");
                            }

                            writer.write(">");
                        }
                        else if (event.isEndElement() == true)
                        {
                            boolean output = true;

                            QName elementName = event.asEndElement().getName();
                            String fullElementName = elementName.getLocalPart();

                            if (elementName.getPrefix().isEmpty() != true)
                            {
                                fullElementName = elementName.getPrefix() + ":" + fullElementName;
                            }

                            writer.write("</" + fullElementName + ">");
                        }
                        else if (event.isCharacters() == true)
                        {
                            event.writeAsEncodedUnicode(writer); 
                        }
                        else if (event.getEventType() == XMLStreamConstants.COMMENT)
                        {
                            writer.write("<!--" + ((Comment)event).getText() + "-->");
                        }
                        else if (event.getEventType() == XMLStreamConstants.DTD)
                        {
                            DTD dtd = (DTD) event;

                            if (dtd != null)
                            {
                                writer.write(dtd.getDocumentTypeDeclaration());
                            }
                        }
                        else if (event.isEndDocument() == true)
                        {

                        }
                        else
                        {
                            throw constructTermination("messageUnsupportedXMLEventType", null, null, event.getEventType(), "javax.xml.stream.XMLStreamConstants", inputFile.getAbsolutePath());
                        }
                    }
                }
                catch (FileNotFoundException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (XMLStreamException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (UnsupportedEncodingException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
            }

            this.infoMessages.addAll(xmlResolverLocal.getInfoMessages());
        }
        else if (entitiesResolveDictionary != null)
        {
            /**
             * @todo Allow this conversion in reverse from character to entity
             *     (for better human readability, otherwise harmful for pure XML
             *     processing without having a resolve dictionary available and
             *     not good for mixing XML-based formats or if no format indication
             *     can be established).
             */

            Map<String, String> resolveDictionary = new HashMap<String, String>();

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(entitiesResolveDictionary);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                int entityNumber = 0;
                String entityName = null;
                StringBuilder entityReplacement = null;
                boolean entity = false;

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("entity") == true)
                        {
                            entityNumber++;

                            if (entity == true)
                            {
                                throw constructTermination("messageResolveDictionaryFileNestedTags", null, null, entitiesResolveDictionary.getAbsolutePath(), "entity");
                            }

                            entity = true;

                            StartElement elementEntity = event.asStartElement();
                            Attribute attributeName = elementEntity.getAttributeByName(new QName("name"));

                            if (attributeName == null)
                            {
                                throw constructTermination("messageResolveDictionaryFileEntryIsMissingAnAttribute", null, null, entitiesResolveDictionary.getAbsolutePath(), tagName, "name");
                            }

                            entityName = attributeName.getValue();

                            entityReplacement = new StringBuilder();
                        }
                    }
                    else if (event.isCharacters() == true)
                    {
                        if (entity == true)
                        {
                            /**
                             * @todo Check if this results in encoding problems or if encoding is
                             *     properly preserved, considering Java's String using UTF-16
                             *     internally, entitiesResolveDictionary probably expected to be
                             *     UTF-8, the use of UTF-8 for readers/writers and an unknown
                             *     encoding in inputFile.
                             */
                            entityReplacement.append(event.asCharacters().getData());
                        }
                    }
                    else if (event.isEndElement() == true)
                    {
                        String tagName = event.asEndElement().getName().getLocalPart();

                        if (tagName.equals("entity") == true)
                        {
                            entity = false;

                            if (resolveDictionary.containsKey(entityName) != true)
                            {
                                resolveDictionary.put(entityName, entityReplacement.toString());
                            }
                            else
                            {
                                throw constructTermination("messageResolveDictionaryFileEntityConfiguredMoreThanOnce", null, null, entitiesResolveDictionary.getAbsolutePath(), entityName);
                            }
                        }

                        entityReplacement = null;
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageResolveDictionaryFileErrorWhileReading", ex, null, entitiesResolveDictionary.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageResolveDictionaryFileErrorWhileReading", ex, null, entitiesResolveDictionary.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageResolveDictionaryFileErrorWhileReading", ex, null, entitiesResolveDictionary.getAbsolutePath());
            }


            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(outputFile),
                                        "UTF-8"));

                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    inputFactory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
                    inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
                    inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);

                    InputStream in = new FileInputStream(inputFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
                    XMLEvent event = null;

                    while (eventReader.hasNext() == true)
                    {
                        event = eventReader.nextEvent();

                        if (event.isStartDocument() == true)
                        {
                            StartDocument startDocument = (StartDocument)event;

                            writer.write("<?xml version=\"" + startDocument.getVersion() + "\"");

                            if (startDocument.encodingSet() == true)
                            {
                                writer.write(" encoding=\"" + startDocument.getCharacterEncodingScheme() + "\"");
                            }

                            if (startDocument.standaloneSet() == true)
                            {
                                writer.write(" standalone=\"");

                                if (startDocument.isStandalone() == true)
                                {
                                    writer.write("yes");
                                }
                                else
                                {
                                    writer.write("no");
                                }

                                writer.write("\"");
                            }

                            writer.write("?>\n");
                        }
                        else if (event.isStartElement() == true)
                        {
                            QName elementName = event.asStartElement().getName();
                            String fullElementName = elementName.getLocalPart();

                            if (elementName.getPrefix().isEmpty() != true)
                            {
                                fullElementName = elementName.getPrefix() + ":" + fullElementName;
                            }

                            writer.write("<" + fullElementName);

                            // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                            @SuppressWarnings("unchecked")
                            Iterator<Namespace> namespaces = (Iterator<Namespace>)event.asStartElement().getNamespaces();

                            while (namespaces.hasNext() == true)
                            {
                                Namespace namespace = namespaces.next();

                                if (namespace.isDefaultNamespaceDeclaration() == true &&
                                    namespace.getPrefix().length() <= 0)
                                {
                                    writer.write(" xmlns=\"" + namespace.getNamespaceURI() + "\"");
                                }
                                else
                                {
                                    writer.write(" xmlns:" + namespace.getPrefix() + "=\"" + namespace.getNamespaceURI() + "\"");
                                }
                            }

                            // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                            @SuppressWarnings("unchecked")
                            Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

                            while (attributes.hasNext() == true)
                            {
                                Attribute attribute = attributes.next();
                                QName attributeName = attribute.getName();
                                String fullAttributeName = attributeName.getLocalPart();

                                if (attributeName.getPrefix().length() > 0)
                                {
                                    fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
                                }

                                String attributeValue = attribute.getValue();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                attributeValue = attributeValue.replaceAll("&", "&amp;");
                                attributeValue = attributeValue.replaceAll("\"", "&quot;");
                                attributeValue = attributeValue.replaceAll("'", "&apos;");
                                attributeValue = attributeValue.replaceAll("<", "&lt;");
                                attributeValue = attributeValue.replaceAll(">", "&gt;");

                                writer.write(" " + fullAttributeName + "=\"" + attributeValue + "\"");
                            }

                            writer.write(">");
                        }
                        else if (event.isEndElement() == true)
                        {
                            boolean output = true;

                            QName elementName = event.asEndElement().getName();
                            String fullElementName = elementName.getLocalPart();

                            if (elementName.getPrefix().isEmpty() != true)
                            {
                                fullElementName = elementName.getPrefix() + ":" + fullElementName;
                            }

                            writer.write("</" + fullElementName + ">");
                        }
                        else if (event.isCharacters() == true)
                        {
                            // Built-in XML special character escape entities are preserved by
                            // writeAsEncodedUnicode() and not reported as XMLStreamConstants.ENTITY_REFERENCE.
                            // Therefore, xml_dtd_entity_resolver_1 can't be used to de-escape XML special
                            // character escaping, as they're not coming from DTDs anyway.
                            event.writeAsEncodedUnicode(writer);
                        }
                        else if (event.getEventType() == XMLStreamConstants.ENTITY_REFERENCE)
                        {
                            String entityName = ((EntityReference)event).getName();

                            if (resolveDictionary.containsKey(entityName) == true)
                            {
                                writer.write(resolveDictionary.get(entityName));
                            }
                            else
                            {
                                if (ignoreMissingMappings != true)
                                {
                                    throw constructTermination("messageResolveDictionaryCantResolveEntity", null, null, inputFile.getAbsolutePath(), entitiesResolveDictionary.getAbsolutePath(), entityName);
                                }
                                else
                                {
                                    writer.write("&");
                                    writer.write(entityName);
                                    writer.write(";");
                                }
                            }
                        }
                        else if (event.getEventType() == XMLStreamConstants.COMMENT)
                        {
                            writer.write("<!--" + ((Comment)event).getText() + "-->");
                        }
                        else if (event.getEventType() == XMLStreamConstants.DTD)
                        {
                            DTD dtd = (DTD) event;

                            if (dtd != null)
                            {
                                writer.write(dtd.getDocumentTypeDeclaration());
                            }
                        }
                        else if (event.isEndDocument() == true)
                        {

                        }
                        else
                        {
                            throw constructTermination("messageUnsupportedXMLEventType", null, null, event.getEventType(), "javax.xml.stream.XMLStreamConstants", inputFile.getAbsolutePath());
                        }
                    }
                }
                catch (FileNotFoundException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (XMLStreamException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (UnsupportedEncodingException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, outputFile.getAbsolutePath());
            }
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xml_dtd_entity_resolver_1: " + getI10nString(id);
            }
            else
            {
                message = "xml_dtd_entity_resolver_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xml_dtd_entity_resolver_1: " + getI10nString(id);
            }
            else
            {
                message = "xml_dtd_entity_resolver_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (xml_dtd_entity_resolver_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xml_dtd_entity_resolver_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xml_dtd_entity_resolver_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and http://www.publishing-systems.org). -->\n");
                writer.write("<xml-dtd-entity-resolver-1-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</xml-dtd-entity-resolver-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        xml_dtd_entity_resolver_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected ArrayList<String> tokens = null;

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nXmlDtdEntityResolver1Console";
    private ResourceBundle l10nConsole;
}
