/* Copyright (C) 2016-2023 Stephan Kreutzer
 *
 * This file is part of json_to_xml_2, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * json_to_xml_2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * json_to_xml_2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with json_to_xml_2. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/json_to_xml/json_to_xml_2/ParserJson.java
 * @brief Parses the tokens of a JSON file and converts them into XML.
 * @author Stephan Kreutzer
 * @since 2018-04-10
 */



import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.UnsupportedEncodingException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.ArrayList;



public class ParserJson
{
    public ParserJson()
    {

    }

    public int parse(BufferedReader reader,
                     List<InfoMessage> infoMessages,
                     BufferedWriter writer) throws IOException
    {
        this.reader = reader;
        this.infoMessages = infoMessages;
        this.tokens.clear();
        this.writer = writer;

        consumeWhitespace();

        JsonToken token = nextToken();

        if (token.getToken().equals("{") == true)
        {
            this.writer.write("<object>");
            HandleObjectContent();
            this.writer.write("</object>");
        }
        else if (token.getToken().equals("[") == true)
        {
            this.writer.write("<array>");
            HandleArrayContent();
            this.writer.write("</array>");
        }
        else
        {
            throw constructTermination("messageParserUnknownStartToken", null, null, token.getToken());
        }

        return 0;
    }

    protected int HandleObjectContent() throws IOException
    {
        consumeWhitespace();

        JsonToken token = lookAhead();

        if (token == null)
        {
            throw constructTermination("messageParserNoMoreTokens", null, null);
        }

        if (token.getToken().equals("}") == true)
        {
            nextToken();
            return 0;
        }

        do
        {
            consumeWhitespace();

            match("\"");

            String name = HandleString().toString();
            // Ampersand needs to be the first, otherwise it would double-encode
            // other entities.
            name = name.replaceAll("&", "&amp;");
            name = name.replaceAll("\"", "&quot;");
            name = name.replaceAll("'", "&apos;");
            name = name.replaceAll("<", "&lt;");
            name = name.replaceAll(">", "&gt;");

            consumeWhitespace();
            match(":");
            consumeWhitespace();

            token = nextToken();

            if (token.getToken().equals("{") == true)
            {
                this.writer.write("<object name=\"" + name.toString() + "\">");
                HandleObjectContent();
                this.writer.write("</object>");
            }
            else if (token.getToken().equals("[") == true)
            {
                this.writer.write("<array name=\"" + name.toString() + "\">");
                HandleArrayContent();
                this.writer.write("</array>");
            }
            else if (token.getToken().equals("\"") == true)
            {
                this.writer.write("<string name=\"" + name.toString() + "\">");

                String string = HandleString().toString();
                // Ampersand needs to be the first, otherwise it would double-encode
                // other entities.
                string = string.replaceAll("&", "&amp;");
                string = string.replaceAll("<", "&lt;");
                string = string.replaceAll(">", "&gt;");

                this.writer.write(string);
                this.writer.write("</string>");
            }
            else
            {
                this.writer.write("<literal name=\"" + name.toString() + "\">");
                HandleText(token, JSON_TYPE.OBJECT);
                this.writer.write("</literal>");
            }

            consumeWhitespace();

            token = nextToken();

            if (token.getToken().equals(",") == true)
            {
                continue;
            }
            else if (token.getToken().equals("}") == true)
            {
                return 0;
            }
            else
            {
                throw constructTermination("messageParserUnknownTokenInObject", null, null, token.getToken());
            }

        } while (true);
    }

    protected int HandleArrayContent() throws IOException
    {
        consumeWhitespace();

        JsonToken token = lookAhead();

        if (token == null)
        {
            throw constructTermination("messageParserNoMoreTokens", null, null);
        }

        if (token.getToken().equals("]") == true)
        {
            nextToken();
            return 0;
        }

        do
        {
            consumeWhitespace();
            token = nextToken();

            if (token.getToken().equals("{") == true)
            {
                this.writer.write("<object>");
                HandleObjectContent();
                this.writer.write("</object>");
            }
            else if (token.getToken().equals("\"") == true)
            {
                this.writer.write("<string>");

                String string = HandleString().toString();
                // Ampersand needs to be the first, otherwise it would double-encode
                // other entities.
                string = string.replaceAll("&", "&amp;");
                string = string.replaceAll("<", "&lt;");
                string = string.replaceAll(">", "&gt;");

                this.writer.write(string);
                this.writer.write("</string>");
            }
            else if (token.getToken().equals("[") == true)
            {
                this.writer.write("<array>");
                HandleArrayContent();
                this.writer.write("</array>");
            }
            else
            {
                this.writer.write("<literal>");
                HandleText(token, JSON_TYPE.ARRAY);
                this.writer.write("</literal>");
            }

            consumeWhitespace();

            token = nextToken();

            if (token.getToken().equals(",") == true)
            {
                continue;
            }
            else if (token.getToken().equals("]") == true)
            {
                return 0;
            }
            else
            {
                throw constructTermination("messageParserUnknownTokenInArray", null, null, token.getToken());
            }

        } while (true);
    }

    protected StringBuilder HandleString() throws IOException
    {
        StringBuilder sb = new StringBuilder();

        while (true)
        {
            JsonToken token = nextToken();

            if (token.getToken().equals("\"") == true)
            {
                break;
            }
            else if (token.getToken().equals("\\") == true)
            {
                sb.append(HandleEscapeSequence());
            }
            else
            {
                sb.append(token.getToken());
            }
        }

        return sb;
    }

    protected String HandleEscapeSequence() throws IOException
    {
        JsonToken token = nextToken();

        // The tokenizer made sure that the next token will be
        // only one character.
        if (token.getToken().equals("\"") == true)
        {
            return "\"";
        }
        else if (token.getToken().equals("\\") == true)
        {
            return "\\";
        }
        else if (token.getToken().equals("/") == true)
        {
            return "/";
        }
        else if (token.getToken().equals("b") == true)
        {
            throw constructTermination("messageParserUnsupportedEscapeSequence", null, null, "\\b");
        }
        else if (token.getToken().equals("f") == true)
        {
            throw constructTermination("messageParserUnsupportedEscapeSequence", null, null, "\\f");
        }
        else if (token.getToken().equals("n") == true)
        {
            return "\n";
        }
        else if (token.getToken().equals("r") == true)
        {
            return "\r";
        }
        else if (token.getToken().equals("t") == true)
        {
            return "\t";
        }
        else if (token.getToken().equals("u") == true)
        {
            token = lookAhead();

            if (token == null)
            {
                throw constructTermination("messageParserNoMoreTokens", null, null);
            }

            if (token.getToken().length() < 4)
            {
                throw constructTermination("messageParserEscapeSequenceIncomplete", null, null, "\\u" + token.getToken());
            }

            String hex = token.getToken().substring(0, 4);

            int codepoint = Integer.parseInt(hex, 16);
            char character = (char)codepoint;

            String restToken = token.getToken().substring(4);

            if (restToken.length() > 0)
            {
                this.tokens.set(0, new JsonToken(restToken, token.isWhitespace()));
            }
            else
            {
                // Consume the lookAhead().
                nextToken();
            }

            return new String() + (char)character;
        }
        else
        {
            throw constructTermination("messageParserUnsupportedEscapeSequence", null, null, "\\" + token.getToken());
        }
    }

    protected int HandleText(JsonToken token, JSON_TYPE type) throws IOException
    {
        // Already consumed token isn't lookAhead(), but callees must react
        // on end or consecutive elements, so push it back to the front of
        // this.tokens to be interpreted by the callee.
        if ((token.getToken().equals("}") == true ||
             token.getToken().equals(",") == true) &&
            type == JSON_TYPE.OBJECT)
        {
            this.tokens.add(0, token);
            return 0;
        }
        else if ((token.getToken().equals("]") == true ||
                  token.getToken().equals(",") == true) &&
                 type == JSON_TYPE.ARRAY)
        {
            this.tokens.add(0, token);
            return 0;
        }

        boolean whitespaceEntered = false;

        do
        {
            if ((token.getToken().equals("}") == true ||
                 token.getToken().equals(",") == true) &&
                type == JSON_TYPE.OBJECT)
            {
                // Not a problem, token was obtained from lookAhead(), so the
                // callee can react to this tokens.
                return 0;
            }
            else if ((token.getToken().equals("]") == true ||
                      token.getToken().equals(",") == true) &&
                     type == JSON_TYPE.ARRAY)
            {
                // Not a problem, token was obtained from lookAhead(), so the
                // callee can react to this tokens.
                return 0;
            }
            else if (token.getToken().equals("\"") == true)
            {
                throw constructTermination("messageParserDoubleQuoteInTextLiteral", null, null);
            }
            else if (token.getToken().equals("\\") == true)
            {
                if (whitespaceEntered == true)
                {
                    throw constructTermination("messageParserMoreTextAfterWhitespaceInTextLiteral", null, null);
                }

                String string = HandleEscapeSequence();
                // Escaping to make sure that there's no \\u encoded XML special
                // character in there. Ampersand needs to be the first, otherwise
                // it would double-encode other entities.
                string = string.replaceAll("&", "&amp;");
                string = string.replaceAll("<", "&lt;");
                string = string.replaceAll(">", "&gt;");

                this.writer.write(string);
            }
            else if (token.getToken().equals(":") == true)
            {
                if (whitespaceEntered == true)
                {
                    throw constructTermination("messageParserMoreTextAfterWhitespaceInTextLiteral", null, null);
                }

                this.writer.write(token.getToken());
            }
            else if (token.isWhitespace() == true)
            {
                whitespaceEntered = true;

                consumeWhitespace();
            }
            else
            {
                if (whitespaceEntered == true)
                {
                    throw constructTermination("messageParserMoreTextAfterWhitespaceInTextLiteral", null, null);
                }

                String string = token.getToken();
                // Ampersand needs to be the first, otherwise it would double-encode
                // other entities.
                string = string.replaceAll("&", "&amp;");
                string = string.replaceAll("<", "&lt;");
                string = string.replaceAll(">", "&gt;");

                this.writer.write(string);
            }

            token = lookAhead();

            if (token == null)
            {
                throw constructTermination("messageParserNoMoreTokens", null, null);
            }

        } while (true);
    }

    protected boolean match(String required) throws IOException
    {
        JsonToken token = nextToken();

        if (token.getToken().equals(required))
        {
            return true;
        }

        // this.infoMessages.add(constructInfoMessage("messageParsingError", true, null, null, required, token.getToken()));
        throw constructTermination("messageParsingError", null, null, required, token.getToken());
    }

    /**
     * @details As whitespace is collected into a single token,
     *     there shouldn't be two consecutive whitespace tokens
     *     anyway.
     */
    protected int consumeWhitespace() throws IOException
    {
        do
        {
            if (this.tokens.size() <= 0)
            {
                if (readTokens() != 0)
                {
                    return -1;
                }
            }

            if (this.tokens.size() <= 0)
            {
                throw constructTermination("messageParserNoMoreTokens", null, null);
            }

            if (this.tokens.get(0).isWhitespace() == true)
            {
                JsonToken token = this.tokens.remove(0);

                // This can print whitespace/indentation as found
                // between result/output elements, except 1. might be
                // incomplete (for some whitespace byte already consumed/lost
                // and not available any more, as wasn't lookAhead()), and
                // 2. does add it inside <literal/> instead of outside/around,
                // because the <literal/> element is added around the HandleText(),
                // while this consumeWhitespace() prints it inside the text,
                // as found after the literal characters ended. Adding such
                // whitespace inside <literal/> is incorrect, because inside
                // XML, whitespace as part of the "text node" might be
                // interpreted as, defined to be, significant whitespace,
                // where in the JSON source it was not.
                //this.writer.write(token.getToken());
            }
            else
            {
                return 0;
            }

        } while (true);
    }

    protected JsonToken nextToken() throws IOException
    {
        if (this.tokens.size() <= 0)
        {
            if (readTokens() != 0)
            {
                throw constructTermination("messageParserNoMoreTokens", null, null);
            }
        }

        if (this.tokens.size() > 0)
        {
            return this.tokens.remove(0);
        }
        else
        {
            throw constructTermination("messageParserNoMoreTokens", null, null);
        }
    }

    protected JsonToken lookAhead() throws IOException
    {
        if (this.tokens.size() <= 0)
        {
            if (readTokens() != 0)
            {
                return null;
            }
        }

        if (this.tokens.size() > 0)
        {
            return this.tokens.get(0);
        }
        else
        {
            return null;
        }
    }

    protected int readTokens() throws IOException
    {
        int character = this.reader.read();
        String buffer = new String();
        boolean isWhitespace = false;

        while (character >= 0)
        {
            if (character == '{' ||
                character == '}' ||
                character == '"' ||
                character == ':' ||
                character == '[' ||
                character == ']' ||
                character == ',')
            {
                if (buffer.isEmpty() != true)
                {
                    tokens.add(new JsonToken(buffer, isWhitespace));
                    buffer = "";
                    isWhitespace = false;
                }

                tokens.add(new JsonToken(new String() + (char)character, isWhitespace));
                return 0;
            }
            else if (character == '\\')
            {
                if (buffer.isEmpty() != true)
                {
                    tokens.add(new JsonToken(buffer, isWhitespace));
                    buffer = "";
                    isWhitespace = false;
                }

                tokens.add(new JsonToken(new String() + (char)character, isWhitespace));

                character = this.reader.read();

                if (character >= 0)
                {
                    // isWhitespace, ignored for convenience reasons, likely irrelevant
                    // for escape sequences anyway.
                    tokens.add(new JsonToken(new String() + (char)character, false));
                }

                return 0;
            }
            else if (Character.isWhitespace(character) == true)
            {
                if (isWhitespace != true &&
                    buffer.isEmpty() != true)
                {
                    tokens.add(new JsonToken(buffer, isWhitespace));
                    buffer = "";
                }

                buffer += (char)character;
                isWhitespace = true;

                // No break, continue looping (collecting more whitespace).
            }
            else
            {
                if (isWhitespace == true &&
                    buffer.isEmpty() != true)
                {
                    tokens.add(new JsonToken(buffer, isWhitespace));
                    buffer = "";
                    isWhitespace = false;
                }

                buffer += (char)character;

                // No break, continue looping (collecting more non-whitespace).
            }

            character = this.reader.read();
        }

        if (buffer.isEmpty() != true)
        {
            tokens.add(new JsonToken(buffer, isWhitespace));
            return 0;
        }

        if (this.tokens.size() > 0)
        {
            return 0;
        }

        return -1;
    }

    public enum JSON_TYPE
    {
        OBJECT,
        ARRAY
    }

    protected BufferedReader reader = null;
    protected List<JsonToken> tokens = new ArrayList<JsonToken>();
    protected BufferedWriter writer = null;


    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "json_to_xml_2 (ParserJson): " + getI10nString(id);
            }
            else
            {
                message = "json_to_xml_2 (ParserJson): " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "json_to_xml_2 (ParserJson): " + getI10nString(id);
            }
            else
            {
                message = "json_to_xml_2 (ParserJson): " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nParserJson == null)
        {
            this.l10nParserJson = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nParserJson.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nParserJson.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected List<InfoMessage> infoMessages = null;

    private static final String L10N_BUNDLE = "l10n.l10nJsonToXml2ParserJson";
    private ResourceBundle l10nParserJson;
}
