# Copyright (C) 2014-2024 Stephan Kreutzer
#
# This file is part of digital_publishing_workflow_tools.
#
# digital_publishing_workflow_tools is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# digital_publishing_workflow_tools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with digital_publishing_workflow_tools. If not, see <http://www.gnu.org/licenses/>.



directories = ./csv_to_xml ./dimgra_to_xml ./file_discovery ./file_hasher ./gui ./html_embed_images ./http_client ./https_client ./htx ./json_to_xml ./latex ./onix_prepare_for_json ./sof ./text_concatenator ./text_replacer ./workflows ./xml_concatenator ./xml_definition_extractor ./xml_dtd_entity_resolver ./xml_namespace_extractor ./xml_pivoter ./xml_to_dimgra ./xml_xslt_transformator



.PHONY: all $(directories)



all: $(directories)



$(directories):
	$(MAKE) --directory=$@

