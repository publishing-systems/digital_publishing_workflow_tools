/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of xml_pivoter_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_pivoter_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_pivoter_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_pivoter_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/xml_pivoter/xml_pivoter_1/xml_pivoter_1.java
 * @brief Pivots a selected inner element around a fixed outer container element.
 * @author Stephan Kreutzer
 * @since 2021-04-15
 */



import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.HashMap;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.util.Stack;
import java.util.Iterator;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.ProcessingInstruction;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.EntityReference;
import javax.xml.stream.events.Comment;
import javax.xml.stream.events.DTD;
import java.io.BufferedReader;
import java.io.InputStreamReader;



public class xml_pivoter_1
{
    public static void main(String args[])
    {
        System.out.print("xml_pivoter_1 Copyright (C) 2021-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        xml_pivoter_1 instance = new xml_pivoter_1();

        try
        {
            instance.run(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xml_pivoter_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xml-pivoter-1-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</xml-pivoter-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int run(String args[])
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\txml_pivoter_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        xml_pivoter_1.resultInfoFile = resultInfoFile;

        String programPath = xml_pivoter_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }

        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("xml_pivoter_1: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));

        File inputFile = null;
        File tempDirectory = null;
        String containerPath = null;
        Map<String, StructureDefinition> structureDefinitions = new HashMap<String, StructureDefinition>();

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input-file") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (inputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        inputFile = new File(attributePath.getValue());

                        if (inputFile.isAbsolute() != true)
                        {
                            inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            inputFile = inputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.exists() != true)
                        {
                            throw constructTermination("messageInputFileDoesntExist", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.isFile() != true)
                        {
                            throw constructTermination("messageInputPathIsntAFile", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.canRead() != true)
                        {
                            throw constructTermination("messageInputFileIsntReadable", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("temp-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (tempDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        tempDirectory = new File(attributePath.getValue());

                        if (tempDirectory.isAbsolute() != true)
                        {
                            tempDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            tempDirectory = tempDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("outer-container") == true)
                    {
                        Attribute attributeOuterContainerPath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributeOuterContainerPath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (containerPath == null)
                        {
                            containerPath = attributeOuterContainerPath.getValue();
                        }
                        else
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isStartElement() == true)
                            {
                                tagName = event.asStartElement().getName().getLocalPart();

                                if (tagName.equals("inner-selection") == true)
                                {
                                    StartElement elementInnerSelection = event.asStartElement();
                                    Attribute attributeInnerSelectionPath = elementInnerSelection.getAttributeByName(new QName("path"));
                                    Attribute attributeOutputFilePath = elementInnerSelection.getAttributeByName(new QName("output-file-path"));

                                    if (attributeInnerSelectionPath == null)
                                    {
                                        throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                                    }

                                    if (attributeOutputFilePath == null)
                                    {
                                        throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "output-file-path");
                                    }

                                    if (structureDefinitions.containsKey(attributeInnerSelectionPath.getValue()) == true)
                                    {
                                        throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                                    }


                                    File outputFile = new File(attributeOutputFilePath.getValue());

                                    if (outputFile.isAbsolute() != true)
                                    {
                                        outputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributeOutputFilePath.getValue());
                                    }

                                    try
                                    {
                                        outputFile = outputFile.getCanonicalFile();
                                    }
                                    catch (SecurityException ex)
                                    {
                                        throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                                    }
                                    catch (IOException ex)
                                    {
                                        throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                                    }

                                    if (outputFile.exists() == true)
                                    {
                                        if (outputFile.isFile() == true)
                                        {
                                            if (outputFile.canWrite() != true)
                                            {
                                                throw constructTermination("messageOutputFileIsntWritable", null, null, outputFile.getAbsolutePath());
                                            }
                                        }
                                        else
                                        {
                                            throw constructTermination("messageOutputPathIsntAFile", null, null, outputFile.getAbsolutePath());
                                        }
                                    }

                                    /** @todo Check if child! Also: relative or absolute path? Also: not equal parent! */
                                    structureDefinitions.put(attributeInnerSelectionPath.getValue(),
                                                             new StructureDefinition(attributeInnerSelectionPath.getValue(), outputFile));
                                }
                            }
                            else if (event.isEndElement() == true)
                            {
                                if (event.asEndElement().getName().getLocalPart().equals("outer-container") == true)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (inputFile == null)
        {
            throw constructTermination("messageJobFileInputFileIsntConfigured", null, null, jobFile.getAbsolutePath(), "input-file");
        }

        if (containerPath == null)
        {
            throw constructTermination("messageJobFileOuterContainerIsntConfigured", null, null, jobFile.getAbsolutePath(), "outer-container");
        }

        if (structureDefinitions.size() <= 0)
        {
            throw constructTermination("messageJobFileNoInnerSelectionConfigured", null, null, jobFile.getAbsolutePath(), "inner-selection");
        }

        if (tempDirectory == null)
        {
            tempDirectory = new File(programPath + "temp");
        }

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    throw constructTermination("messageTempDirectoryIsntWritable", null, null, tempDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageTempPathIsntADirectory", null, null, tempDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageTempDirectoryCantCreate", ex, null, tempDirectory.getAbsolutePath());
            }
        }


        File outerContainerTempFile = new File(tempDirectory.getAbsolutePath() + File.separator + "outer-container.xml");

        Stack<StructureStackElement> structureStack = new Stack<StructureStackElement>();
        String innerSelectionEndElementFullName = null;

        Iterator<StructureDefinition> iter = structureDefinitions.values().iterator();

        while (iter.hasNext() == true)
        {
            StructureDefinition structureDefinition = iter.next();

            boolean isInOuterContainer = false;
            boolean isInInnerSelection = false;
            boolean hasInnerSelection = false;

            try
            {
                BufferedWriter writerOutput = new BufferedWriter(
                                              new OutputStreamWriter(
                                              new FileOutputStream(structureDefinition.getOutputFile()),
                                              "UTF-8"));
                BufferedWriter writerTemp = null;

                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();

                    // This is a pivoter for generic XML, don't want to deal with legacy DTD
                    // remnants. Consider using $/xml_dtd_entity_resolver/xml_dtd_entity_resolver_1.
                    inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
                    inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);

                    InputStream in = new FileInputStream(inputFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                    while (eventReader.hasNext() == true)
                    {
                        XMLEvent event = eventReader.nextEvent();

                        if (event.isStartDocument() == true)
                        {
                            StartDocument startDocument = (StartDocument)event;

                            writerOutput.write("<?xml version=\"" + startDocument.getVersion() + "\"");

                            if (startDocument.encodingSet() == true)
                            {
                                writerOutput.write(" encoding=\"" + startDocument.getCharacterEncodingScheme() + "\"");
                            }

                            if (startDocument.standaloneSet() == true)
                            {
                                writerOutput.write(" standalone=\"");

                                if (startDocument.isStandalone() == true)
                                {
                                    writerOutput.write("yes");
                                }
                                else
                                {
                                    writerOutput.write("no");
                                }

                                writerOutput.write("\"");
                            }

                            writerOutput.write("?>");
                            writerOutput.write("<!-- This file was created by xml_pivoter_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                        }
                        else if (event.getEventType() == XMLStreamConstants.DTD)
                        {
                            BufferedWriter writer = null;

                            if (isInInnerSelection == true)
                            {
                                writer = writerOutput;
                            }
                            else if (isInOuterContainer == true)
                            {
                                writer = writerTemp;
                            }

                            if (writer != null)
                            {
                                DTD dtd = (DTD) event;

                                if (dtd != null)
                                {
                                    writer.write(dtd.getDocumentTypeDeclaration());
                                }
                            }
                        }
                        else if (event.isStartElement() == true)
                        {
                            String elementName = event.asStartElement().getName().getLocalPart();
                            String elementPath = "/" + elementName;

                            if (structureStack.empty() != true)
                            {
                                elementPath = structureStack.peek().getPath() + elementPath;
                            }

                            boolean isRootElement = structureStack.empty();

                            structureStack.push(new StructureStackElement(elementName, elementPath));

                            if (elementPath.equals(containerPath) == true)
                            {
                                isInOuterContainer = true;
                                hasInnerSelection = false;

                                if (outerContainerTempFile.exists() == true)
                                {
                                    if (outerContainerTempFile.isFile() == true)
                                    {
                                        boolean deleteSuccessful = false;

                                        try
                                        {
                                            deleteSuccessful = outerContainerTempFile.delete();
                                        }
                                        catch (SecurityException ex)
                                        {

                                        }

                                        if (deleteSuccessful != true)
                                        {
                                            if (outerContainerTempFile.canWrite() != true)
                                            {
                                                throw constructTermination("messageTemporaryOuterContainerFileExistsButIsntWritable", null, null, outerContainerTempFile.getAbsolutePath());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        throw constructTermination("messageTemporaryOuterContainerPathExistsButIsntAFile", null, null, outerContainerTempFile.getAbsolutePath());
                                    }
                                }

                                writerTemp = new BufferedWriter(
                                             new OutputStreamWriter(
                                             new FileOutputStream(outerContainerTempFile),
                                             "UTF-8"));
                            }
                            else if (structureDefinition.getPath().equals(elementPath) == true)
                            {
                                if (hasInnerSelection == true)
                                {
                                    throw constructTermination("messageMoreThanOneInnerSelectionWithinOuterContainer", null, null, inputFile.getAbsolutePath(), elementPath);
                                }

                                isInInnerSelection = true;
                                hasInnerSelection = true;

                                // Rebuild every time, for potential difference in prefix? Not namespace-aware anyway.
                                innerSelectionEndElementFullName = elementName;

                                if (event.asStartElement().getName().getPrefix().isEmpty() != true)
                                {
                                    innerSelectionEndElementFullName = event.asStartElement().getName().getPrefix() + ":" + innerSelectionEndElementFullName;
                                }
                            }


                            BufferedWriter writer = null;

                            if (isInInnerSelection == true)
                            {
                                writer = writerOutput;
                            }
                            else if (isInOuterContainer == true)
                            {
                                writer = writerTemp;
                            }
                            else if (isRootElement == true)
                            {
                                writer = writerOutput;
                            }

                            if (writer != null)
                            {
                                String fullElementName = event.asStartElement().getName().getLocalPart();

                                if (event.asStartElement().getName().getPrefix().isEmpty() != true)
                                {
                                    fullElementName = event.asStartElement().getName().getPrefix() + ":" + fullElementName;
                                }

                                writer.write("<" + fullElementName);

                                // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                                @SuppressWarnings("unchecked")
                                Iterator<Namespace> namespaces = (Iterator<Namespace>)event.asStartElement().getNamespaces();

                                while (namespaces.hasNext() == true)
                                {
                                    Namespace namespace = namespaces.next();

                                    if (namespace.isDefaultNamespaceDeclaration() == true &&
                                        namespace.getPrefix().length() <= 0)
                                    {
                                        writer.write(" xmlns=\"" + namespace.getNamespaceURI() + "\"");
                                    }
                                    else
                                    {
                                        writer.write(" xmlns:" + namespace.getPrefix() + "=\"" + namespace.getNamespaceURI() + "\"");
                                    }
                                }

                                // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                                @SuppressWarnings("unchecked")
                                Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

                                while (attributes.hasNext() == true)
                                {
                                    Attribute attribute = attributes.next();
                                    QName attributeName = attribute.getName();
                                    String fullAttributeName = attributeName.getLocalPart();

                                    if (attributeName.getPrefix().length() > 0)
                                    {
                                        fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
                                    }

                                    String attributeValue = attribute.getValue();

                                    // Ampersand needs to be the first, otherwise it would double-encode
                                    // other entities.
                                    attributeValue = attributeValue.replaceAll("&", "&amp;");
                                    attributeValue = attributeValue.replaceAll("\"", "&quot;");
                                    attributeValue = attributeValue.replaceAll("'", "&apos;");
                                    attributeValue = attributeValue.replaceAll("<", "&lt;");
                                    attributeValue = attributeValue.replaceAll(">", "&gt;");

                                    writer.write(" " + fullAttributeName + "=\"" + attributeValue + "\"");
                                }

                                writer.write(">");
                            }
                        }
                        else if (event.isEndElement() == true)
                        {
                            String elementName = event.asEndElement().getName().getLocalPart();

                            if (structureStack.empty() == true)
                            {
                                throw constructTermination("messageStructureStackIsEmptyInEndElement", null, null);
                            }

                            boolean containerEndElementHandled = false;

                            if (structureStack.peek().getPath().equals(containerPath) == true)
                            {
                                isInOuterContainer = false;
                                containerEndElementHandled = true;

                                String fullElementName = elementName;

                                if (event.asEndElement().getName().getPrefix().isEmpty() != true)
                                {
                                    fullElementName = event.asEndElement().getName().getPrefix() + ":" + fullElementName;
                                }

                                writerTemp.write("</" + fullElementName + ">");

                                writerTemp.flush();
                                writerTemp.close();

                                writerTemp = null;

                                if (hasInnerSelection == true)
                                {
                                    if (outerContainerTempFile.exists() != true)
                                    {
                                        throw constructTermination("messageTemporaryOuterContainerFileDoesntExist", null, null, outerContainerTempFile.getAbsolutePath());
                                    }

                                    if (outerContainerTempFile.isFile() != true)
                                    {
                                        throw constructTermination("messageTemporaryOuterContainerPathIsntAFile", null, null, outerContainerTempFile.getAbsolutePath());
                                    }

                                    if (outerContainerTempFile.canRead() != true)
                                    {
                                        throw constructTermination("messageTemporaryOuterContainerFileIsntReadable", null, null, outerContainerTempFile.getAbsolutePath());
                                    }

                                    char[] buffer = new char[1024];

                                    try
                                    {
                                        BufferedReader readerTemp = new BufferedReader(
                                                                    new InputStreamReader(
                                                                    new FileInputStream(outerContainerTempFile),
                                                                    "UTF-8"));

                                        int bytesRead = readerTemp.read(buffer, 0, buffer.length);

                                        while (bytesRead > 0)
                                        {
                                            writerOutput.write(buffer, 0, bytesRead);
                                            bytesRead = readerTemp.read(buffer, 0, buffer.length);
                                        }

                                        readerTemp.close();
                                    }
                                    catch (FileNotFoundException ex)
                                    {
                                        throw constructTermination("messageErrorWhileMergingOuterContainerTemporaryFileIntoOutputFile", ex, null, outerContainerTempFile.getAbsolutePath(), structureDefinition.getOutputFile().getAbsolutePath());
                                    }
                                    catch (IOException ex)
                                    {
                                        throw constructTermination("messageErrorWhileMergingOuterContainerTemporaryFileIntoOutputFile", ex, null, outerContainerTempFile.getAbsolutePath(), structureDefinition.getOutputFile().getAbsolutePath());
                                    }

                                    writerOutput.write("</" + innerSelectionEndElementFullName + ">");
                                }
                            }
                            else if (structureDefinition.getPath().equals(structureStack.peek().getPath()) == true)
                            {
                                isInInnerSelection = false;
                                // Leads to omission of end element.
                            }
                            else
                            {
                                BufferedWriter writer = null;

                                if (isInInnerSelection == true)
                                {
                                    writer = writerOutput;
                                }
                                else if (isInOuterContainer == true)
                                {
                                    writer = writerTemp;
                                }

                                if (writer != null)
                                {
                                    String fullElementName = elementName;

                                    if (event.asEndElement().getName().getPrefix().isEmpty() != true)
                                    {
                                        fullElementName = event.asEndElement().getName().getPrefix() + ":" + fullElementName;
                                    }

                                    writer.write("</" + fullElementName + ">");
                                }
                            }

                            structureStack.pop();

                            if (structureStack.empty() == true &&
                                containerEndElementHandled != true)
                            {
                                String fullElementName = elementName;

                                if (event.asEndElement().getName().getPrefix().isEmpty() != true)
                                {
                                    fullElementName = event.asEndElement().getName().getPrefix() + ":" + fullElementName;
                                }

                                writerOutput.write("</" + fullElementName + ">");
                            }
                        }
                        else if (event.isCharacters() == true)
                        {
                            BufferedWriter writer = null;

                            if (isInInnerSelection == true)
                            {
                                writer = writerOutput;
                            }
                            else if (isInOuterContainer == true)
                            {
                                writer = writerTemp;
                            }

                            if (writer != null)
                            {
                                event.writeAsEncodedUnicode(writer);
                            }
                        }
                        else if (event.isProcessingInstruction() == true)
                        {
                            BufferedWriter writer = null;

                            if (isInInnerSelection == true)
                            {
                                writer = writerOutput;
                            }
                            else if (isInOuterContainer == true)
                            {
                                writer = writerTemp;
                            }

                            if (writer != null)
                            {
                                ProcessingInstruction pi = (ProcessingInstruction)event;

                                writer.write("<?" + pi.getTarget() + " " + pi.getData() + "?>");
                            }
                        }
                        else if (event.isEntityReference() == true)
                        {
                            BufferedWriter writer = null;

                            if (isInInnerSelection == true)
                            {
                                writer = writerOutput;
                            }
                            else if (isInOuterContainer == true)
                            {
                                writer = writerTemp;
                            }

                            if (writer != null)
                            {
                                String entityName = ((EntityReference)event).getName();

                                writer.write("&");
                                writer.write(entityName);
                                writer.write(";");
                            }
                        }
                        else if (event.getEventType() == XMLStreamConstants.COMMENT)
                        {
                            BufferedWriter writer = null;

                            if (isInInnerSelection == true)
                            {
                                writer = writerOutput;
                            }
                            else if (isInOuterContainer == true)
                            {
                                writer = writerTemp;
                            }

                            if (writer != null)
                            {
                                writer.write("<!--" + ((Comment)event).getText() + "-->");
                            }
                        }
                        else if (event.isEndDocument() == true)
                        {

                        }
                    }
                }
                catch (XMLStreamException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
                }

                writerOutput.flush();
                writerOutput.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, structureDefinition.getOutputFile().getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, structureDefinition.getOutputFile().getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageOutputFileErrorWhileWriting", ex, null, structureDefinition.getOutputFile().getAbsolutePath());
            }
        }

        if (outerContainerTempFile.exists() == true)
        {
            if (outerContainerTempFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = outerContainerTempFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (outerContainerTempFile.canWrite() != true)
                    {
                        throw constructTermination("messageTemporaryOuterContainerFileExistsButIsntWritable", null, null, outerContainerTempFile.getAbsolutePath());
                    }
                }
            }
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xml_pivoter_1: " + getI10nString(id);
            }
            else
            {
                message = "xml_pivoter_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xml_pivoter_1: " + getI10nString(id);
            }
            else
            {
                message = "xml_pivoter_1: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (xml_pivoter_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xml_pivoter_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xml_pivoter_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xml-pivoter-1-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</xml-pivoter-1-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        xml_pivoter_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nXmlPivoter1Console";
    private ResourceBundle l10nConsole;
}
