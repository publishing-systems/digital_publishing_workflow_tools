/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of xml_pivoter_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xml_pivoter_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xml_pivoter_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xml_pivoter_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/xml_pivoter/xml_pivoter_1/StructureDefinition.java
 * @author Stephan Kreutzer
 * @since 2021-04-15
 */



import java.io.File;



class StructureDefinition
{
    public StructureDefinition(String path, File outputFile)
    {
        this.path = path;
        this.outputFile = outputFile;
    }

    public String getPath()
    {
        return this.path;
    }

    public File getOutputFile()
    {
        return this.outputFile;
    }

    protected String path;
    protected File outputFile;
}
