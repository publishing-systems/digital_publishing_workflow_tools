# Copyright (C) 2016-2020 Stephan Kreutzer
#
# This file is part of xhtml_to_latex_1 workflow, a submodule of the
# digital_publishing_workflow_tools package.
#
# xhtml_to_latex_1 workflow is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# xhtml_to_latex_1 workflow is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with xhtml_to_latex_1 workflow. If not, see <http://www.gnu.org/licenses/>.

messageArgumentsMissingUsage = Usage:
messageParameterList = job-file result-info-file
messageResultInfoFileCantGetCanonicalPath = Can''t get normalized path of result information file "{0}".
messageResultInfoFileIsntWritable = Result information file "{0}" isn''t writable.
messageResultInfoPathIsntAFile = Result information path "{0}" exists already, but isn''t a file.
messageCantDetermineProgramPath = Can''t determine program path.
messageJobFileCantGetCanonicalPath = Can''t get normalized path of the job file "{0}".
messageJobFileDoesntExist = Job file "{0}" doesn''t exist.
messageJobPathIsntAFile = Job path "{0}" isn''t a file.
messageJobFileIsntReadable = Job file "{0}" isn''t readable.
messageCallDetails = Called with job file "{0}" and result information file "{1}".
messageJobFileEntryIsMissingAnAttribute = Element "{1}" in job file "{0}" is missing its "{2}" attribute.
messageJobFileElementConfiguredMoreThanOnce = Element "{1}" configured more than once in job file "{0}".
messageInputFileCantGetCanonicalPath = Can''t get normalized path of input file "{0}".
messageInputFileDoesntExist = Input file "{0}", configured in job file "{1}", doesn''t exist.
messageInputPathIsntAFile = Input path "{0}", configured in job file "{1}", isn''t a file.
messageInputFileIsntReadable = Input file "{0}", configured in job file "{1}", isn''t readable.
messageStylesheetFileDoesntExist = XSLT stylesheet file "{0}", configured in job file "{1}", doesn''t exist.
messageStylesheetPathIsntAFile = XSLT stylesheet path "{0}", configured in job file "{1}", isn''t a file.
messageStylesheetFileIsntReadable = XSLT stylesheet file "{0}", configured in job file "{1}", isn''t readable.
messageOutputFileCantGetCanonicalPath = Can''t get normalized path of input file "{0}".
messageOutputPathDoesAlreadyExist = The output file "{0}" does already exist.
messageJobFileErrorWhileReading = An error occurred while reading jobfile "{0}".
messageJobFileInputFileIsntConfigured = There is no input file configured via element "{1}" in job file "{0}".
messageTempDirectoryCantGetCanonicalPath = Can''t get canonical path of temp directory "{0}".
messageTempDirectoryIsntWritable = Temporary directory "{0}" isn''t writable.
messageTempPathIsntADirectory = Temporary path "{0}" isn''t a directory.
messageJobFileNoOutputFileConfigured = No output file configured via element "{1}" in job file "{0}".
messageInputFileDoctypeIsEmpty = The DOCTYPE declaration in input file "{0}" is empty.
messageInputFileDoctypeWithWrongRootName = The root name of the DOCTYPE declaration of the input file "{0}" is "{2}", but should be "{1}".
messageInputFileDoctypePublicIncomplete = The DOCTYPE declaration of the input file "{0}" is incomplete.
messageInputFileErrorWhileReading = Error while reading input file "{0}".
messageInputFileNoDoctype = No DOCTYPE declaration found in input file "{0}".
messageInputFileDoctypeWithoutIdentifier = The DOCTYPE declaration in the input file "{0}" is without identifier.
messageInputFileDoctypeNotSupported = The DOCTYPE "{1}" of the input file "{0}" isn''t supported.
messageTempDirectoryCantCreate = Can''t create temporary directory "{0}".
messageTemporaryXhtmlPrepareForLatex1ResultInfoFileExistsButIsntWritable = Temporary xhtml_prepare_for_latex_1 result information file "{0}" exists but isn''t overwritable.
messageTemporaryXhtmlPrepareForLatex1ResultInfoPathExistsButIsntAFile = Temporary xhtml_prepare_for_latex_1 result information path "{0}" exists, but isn''t a file.
messageTemporaryXhtmlPrepareForLatex1JobFileExistsButIsntWritable = Temporary xhtml_prepare_for_latex_1 jobfile "{0}" exists but isn''t overwritable.
messageTemporaryXhtmlPrepareForLatex1JobPathExistsButIsntAFile = Temporary xhtml_prepare_for_latex_1 jobfile path "{0}" exists, but isn''t a file.
messageTemporaryInputPreparedForLatex1FileExistsButIsntWritable = Temporary file prepared for LaTeX "{0}" exists but isn''t overwritable.
messageTemporaryInputPreparedForLatex1PathExistsButIsntAFile = The path of the temporary file prepared for LaTeX "{0}" exists, but isn''t a file.
messageTemporaryXhtmlPrepareForLatex1JobFileErrorWhileWriting = An error occurred while writing the temporary xhtml_prepare_for_latex_1 jobfile "{0}".
messageXhtmlPrepareForLatex1ErrorWhileReadingOutput = An error occurred while reading output of xhtml_prepare_for_latex_1.
messageXhtmlPrepareForLatex1ResultInfoFileDoesntExistButShould = The xhtml_prepare_for_latex_1 result information file "{0}" doesn''t exist, but should by now.
messageXhtmlPrepareForLatex1ResultInfoPathExistsButIsntAFile = The xhtml_prepare_for_latex_1 result information path "{0}" does exist, but isn''t a file.
messageXhtmlPrepareForLatex1ResultInfoFileIsntReadable = The xhtml_prepare_for_latex_1 result information file "{0}" isn''t readable.
messageXhtmlPrepareForLatex1ResultInfoFileErrorWhileReading = An error occurred while reading the xhtml_prepare_for_latex_1 result information file "{0}".
messageXhtmlPrepareForLatex1CallWasntSuccessful = The call of xhtml_prepare_for_latex_1 wasn''t successful.
messageXmlXsltTransformator1ResultInfoFileExistsButIsntWritable = xml_xslt_transformator_1 result information file "{0}" exists but isn''t overwritable.
messageXmlXsltTransformator1ResultInfoPathExistsButIsntAFile = xml_xslt_transformator_1 result information path "{0}" exists, but isn''t a file.
messageXmlXsltTransformator1JobFileExistsButIsntWritable = xml_xslt_transformator_1 jobfile "{0}" exists but isn''t overwritable.
messageXmlXsltTransformator1JobPathExistsButIsntAFile = xml_xslt_transformator_1 jobfile path "{0}" exists, but isn''t a file.
messageXmlXsltTransformator1JobFileErrorWhileWriting = An error occurred while writing the xml_xslt_transformator_1 jobfile "{0}".
messageXmlXsltTransformator1ErrorWhileReadingOutput = An error occurred while reading output of xml_xslt_transformator_1.
messageXmlXsltTransformator1ResultInfoFileDoesntExistButShould = The xml_xslt_transformator_1 result information file "{0}" doesn''t exist, but should by now.
messageXmlXsltTransformator1ResultInfoPathExistsButIsntAFile = The xml_xslt_transformator_1 result information path "{0}" does exist, but isn''t a file.
messageXmlXsltTransformator1ResultInfoFileIsntReadable = The xml_xslt_transformator_1 result information file "{0}" isn''t readable.
messageXmlXsltTransformator1ResultInfoFileErrorWhileReading = An error occurred while reading the xml_xslt_transformator_1 result information file "{0}".
messageXmlXsltTransformator1CallWasntSuccessful = The call of xml_xslt_transformator_1 wasn''t successful.
