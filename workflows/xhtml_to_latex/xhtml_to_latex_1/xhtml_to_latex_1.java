/* Copyright (C) 2016-2022  Stephan Kreutzer
 *
 * This file is part of xhtml_to_latex_1 workflow, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xhtml_to_latex_1 workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xhtml_to_latex_1 workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xhtml_to_latex_1 workflow. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/workflows/xhtml_to_latex/xhtml_to_latex_1/xhtml_to_latex_1.java
 * @brief Workflow to automatically transform a XHTML input file to LaTeX
 *     output according to a XSLT stylesheet.
 * @author Stephan Kreutzer
 * @since 2020-02-01
 */



import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.lang.SecurityException;
import java.util.Scanner;



public class xhtml_to_latex_1
{
    public static void main(String[] args)
    {
        System.out.print("xhtml_to_latex_1 workflow Copyright (C) 2016-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        xhtml_to_latex_1 instance = new xhtml_to_latex_1();

        try
        {
            instance.run(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_latex_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xhtml-to-latex-1-workflow-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</xhtml-to-latex-1-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int run(String[] args)
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\txhtml_to_latex_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        xhtml_to_latex_1.resultInfoFile = resultInfoFile;


        String programPath = xhtml_to_latex_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }


        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("xhtml_to_latex_1 workflow: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));


        File inputFile = null;
        File tempDirectory = null;
        File stylesheetFile = null;
        File outputFile = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input-file") == true)
                    {
                        StartElement inputFileElement = event.asStartElement();
                        Attribute attributePath = inputFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (inputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        inputFile = new File(attributePath.getValue());

                        if (inputFile.isAbsolute() != true)
                        {
                            inputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            inputFile = inputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.exists() != true)
                        {
                            throw constructTermination("messageInputFileDoesntExist", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.isFile() != true)
                        {
                            throw constructTermination("messageInputPathIsntAFile", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputFile.canRead() != true)
                        {
                            throw constructTermination("messageInputFileIsntReadable", null, null, inputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("temp-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (tempDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        tempDirectory = new File(attributePath.getValue());

                        if (tempDirectory.isAbsolute() != true)
                        {
                            tempDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }
                    }
                    else if (tagName.equals("stylesheet-file") == true)
                    {
                        StartElement stylesheetFileElement = event.asStartElement();
                        Attribute attributePath = stylesheetFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (stylesheetFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        stylesheetFile = new File(attributePath.getValue());

                        if (stylesheetFile.isAbsolute() != true)
                        {
                            stylesheetFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            stylesheetFile = stylesheetFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, stylesheetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputFileCantGetCanonicalPath", ex, null, stylesheetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (stylesheetFile.exists() != true)
                        {
                            throw constructTermination("messageStylesheetFileDoesntExist", null, null, stylesheetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (stylesheetFile.isFile() != true)
                        {
                            throw constructTermination("messageStylesheetPathIsntAFile", null, null, stylesheetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (stylesheetFile.canRead() != true)
                        {
                            throw constructTermination("messageStylesheetFileIsntReadable", null, null, stylesheetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("output-file") == true)
                    {
                        StartElement outputFileElement = event.asStartElement();
                        Attribute attributePath = outputFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (outputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        outputFile = new File(attributePath.getValue());

                        if (outputFile.isAbsolute() != true)
                        {
                            outputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            outputFile = outputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (outputFile.exists() == true)
                        {
                            throw constructTermination("messageOutputPathDoesAlreadyExist", null, null, outputFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (inputFile == null)
        {
            throw constructTermination("messageJobFileInputFileIsntConfigured", null, null, jobFile.getAbsolutePath(), "input-file");
        }

        {
            if (tempDirectory == null)
            {
                tempDirectory = new File(programPath + "temp");
            }

            try
            {
                tempDirectory = tempDirectory.getCanonicalFile();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
            }

            if (tempDirectory.exists() == true)
            {
                if (tempDirectory.isDirectory() == true)
                {
                    if (tempDirectory.canWrite() != true)
                    {
                        throw constructTermination("messageTempDirectoryIsntWritable", null, null, tempDirectory.getAbsolutePath());
                    }
                }
                else
                {
                    throw constructTermination("messageTempPathIsntADirectory", null, null, tempDirectory.getAbsolutePath());
                }
            }
            else
            {
                try
                {
                    tempDirectory.mkdirs();
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageTempDirectoryCantCreate", ex, null, tempDirectory.getAbsolutePath());
                }
            }
        }

        if (stylesheetFile == null)
        {
            throw constructTermination("messageJobFileStylesheetFileIsntConfigured", null, null, jobFile.getAbsolutePath(), "stylesheet-file");
        }

        if (outputFile == null)
        {
            throw constructTermination("messageJobFileNoOutputFileConfigured", null, null, jobFile.getAbsolutePath(), "output-file");
        }


        // Could also be implemented using xml_dtd_entity_resolver_1 and
        // using config_empty.xml for the resolver of xml_xslt_transformator_1.
        // xml_dtd_entity_resolver_1 could also try to determine what supported
        // replacement could be used.

        String doctype = new String();
        String doctypeDtdName = null;
        String doctypeDtdId = null;

        {
            String doctypeDeclaration = new String("<!DOCTYPE");
            int doctypePosMatching = 0;

            try
            {
                FileInputStream in = new FileInputStream(inputFile);

                int currentByte = 0;

                do
                {
                    currentByte = in.read();

                    if (currentByte < 0 ||
                        currentByte > 255)
                    {
                        break;
                    }

                    char currentByteCharacter = (char) currentByte;

                    if (doctypePosMatching < doctypeDeclaration.length())
                    {
                        if (currentByteCharacter == doctypeDeclaration.charAt(doctypePosMatching))
                        {
                            doctypePosMatching++;
                            doctype += currentByteCharacter;
                        }
                        else
                        {
                            doctypePosMatching = 0;
                            doctype = new String();
                        }
                    }
                    else
                    {
                        doctype += currentByteCharacter;

                        if (currentByteCharacter == '>')
                        {
                            boolean inWhitespace = true;
                            boolean isInQuotes = false;
                            List<StringBuilder> tokens = new ArrayList<StringBuilder>();

                            for (int i = doctypeDeclaration.length(), max = doctype.length() - 1; i < max; i++)
                            {
                                char c = doctype.charAt(i);

                                if (Character.isWhitespace(c) != true)
                                {
                                    if (inWhitespace == true)
                                    {
                                        tokens.add(new StringBuilder());
                                    }

                                    if (c != '"')
                                    {
                                        StringBuilder token = tokens.get(tokens.size()-1);
                                        token.append(c);
                                    }
                                    else
                                    {
                                        isInQuotes = !isInQuotes;
                                    }

                                    inWhitespace = false;
                                }
                                else
                                {
                                    if (isInQuotes != true)
                                    {
                                        inWhitespace = true;
                                    }
                                    else
                                    {
                                        StringBuilder token = tokens.get(tokens.size()-1);
                                        token.append(c);
                                    }
                                }
                            }

                            if (tokens.size() < 2)
                            {
                                throw constructTermination("messageInputFileDoctypeIsEmpty", null, null, inputFile.getAbsolutePath());
                            }

                            if (tokens.get(0).toString().equalsIgnoreCase("html") != true)
                            {
                                /** @todo Maybe reset and continue reading to support multiple DOCTYPE declarations? */

                                // "Root Name" needs to be equal to the root element name.
                                throw constructTermination("messageInputFileDoctypeWithWrongRootName", null, null, inputFile.getAbsolutePath(), "html", tokens.get(0).toString());
                            }

                            for (int i = 1, max = tokens.size(); i < max; i++)
                            {
                                String token = tokens.get(i).toString();

                                if (token.equals("PUBLIC") == true)
                                {
                                    if (i + 2 < max)
                                    {
                                        doctypeDtdName = tokens.get(i + 1).toString();
                                        doctypeDtdId = tokens.get(i + 2).toString();
                                    }
                                    else if (i + 2 >= max)
                                    {
                                        throw constructTermination("messageInputFileDoctypePublicIncomplete", null, null, inputFile.getAbsolutePath());
                                    }

                                    break;
                                }
                                else
                                {

                                }
                            }

                            break;
                        }
                    }

                } while (true);
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageInputFileErrorWhileReading", ex, null, inputFile.getAbsolutePath());
            }
        }

        if (doctype.isEmpty() == true)
        {
            throw constructTermination("messageInputFileNoDoctype", null, null, inputFile.getAbsolutePath());
        }

        if (doctypeDtdName == null ||
            doctypeDtdId == null)
        {
            throw constructTermination("messageInputFileDoctypeWithoutIdentifier", null, null, inputFile.getAbsolutePath());
        }

        File xmlXsltTransformator1EntityResolverConfigurationFile = null;

        if (doctypeDtdName.equals("-//W3C//DTD XHTML 1.0 Strict//EN") == true)
        {
            xmlXsltTransformator1EntityResolverConfigurationFile = new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_xhtml_1_0_strict.xml");
        }
        else if (doctypeDtdName.equals("-//W3C//DTD XHTML 1.1//EN") == true)
        {
            xmlXsltTransformator1EntityResolverConfigurationFile = new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_xhtml_1_1.xml");
        }
        else
        {
            throw constructTermination("messageInputFileDoctypeNotSupported", null, null, inputFile.getAbsolutePath(), doctypeDtdName);
        }

        File xhtmlPrepareForLatex1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xhtml_prepare_for_latex_1.xml");

        if (xhtmlPrepareForLatex1ResultInfoFile.exists() == true)
        {
            if (xhtmlPrepareForLatex1ResultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xhtmlPrepareForLatex1ResultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (xhtmlPrepareForLatex1ResultInfoFile.canWrite() != true)
                    {
                        throw constructTermination("messageTemporaryXhtmlPrepareForLatex1ResultInfoFileExistsButIsntWritable", null, null, xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageTemporaryXhtmlPrepareForLatex1ResultInfoPathExistsButIsntAFile", null, null, xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
            }
        }

        File jobfileXhtmlPrepareForLatex1 = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xhtml_prepare_for_latex_1.xml");

        if (jobfileXhtmlPrepareForLatex1.exists() == true)
        {
            if (jobfileXhtmlPrepareForLatex1.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobfileXhtmlPrepareForLatex1.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobfileXhtmlPrepareForLatex1.canWrite() != true)
                    {
                        throw constructTermination("messageTemporaryXhtmlPrepareForLatex1JobFileExistsButIsntWritable", null, null, jobfileXhtmlPrepareForLatex1.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageTemporaryXhtmlPrepareForLatex1JobPathExistsButIsntAFile", null, null, jobfileXhtmlPrepareForLatex1.getAbsolutePath());
            }
        }

        File inputPreparedForLatexFile = new File(tempDirectory.getAbsolutePath() + File.separator + "input_prepared_for_latex.xhtml");

        if (inputPreparedForLatexFile.exists() == true)
        {
            if (inputPreparedForLatexFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = inputPreparedForLatexFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (inputPreparedForLatexFile.canWrite() != true)
                    {
                        throw constructTermination("messageTemporaryInputPreparedForLatex1FileExistsButIsntWritable", null, null, inputPreparedForLatexFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageTemporaryInputPreparedForLatex1PathExistsButIsntAFile", null, null, inputPreparedForLatexFile.getAbsolutePath());
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobfileXhtmlPrepareForLatex1),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by xhtml_to_latex_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
            writer.write("<xhtml-prepare-for-latex-1-jobfile>\n");
            writer.write("  <input-file path=\"" + inputFile.getAbsolutePath() + "\"/>\n");
            writer.write("  <!--options preserve-non-typographic-doublequotes=\"false\"/-->\n");
            writer.write("  <output-file path=\"" + inputPreparedForLatexFile.getAbsolutePath() + "\"/>\n");
            writer.write("</xhtml-prepare-for-latex-1-jobfile>\n");

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageTemporaryXhtmlPrepareForLatex1JobFileErrorWhileWriting", ex, null, jobfileXhtmlPrepareForLatex1.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageTemporaryXhtmlPrepareForLatex1JobFileErrorWhileWriting", ex, null, jobfileXhtmlPrepareForLatex1.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageTemporaryXhtmlPrepareForLatex1JobFileErrorWhileWriting", ex, null, jobfileXhtmlPrepareForLatex1.getAbsolutePath());
        }

        ProcessBuilder builder = new ProcessBuilder("java", "xhtml_prepare_for_latex_1", jobfileXhtmlPrepareForLatex1.getAbsolutePath(), xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
        builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "latex" + File.separator + "xhtml_prepare_for_latex" + File.separator + "xhtml_prepare_for_latex_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXhtmlPrepareForLatex1ErrorWhileReadingOutput", ex, null);
        }


        if (xhtmlPrepareForLatex1ResultInfoFile.exists() != true)
        {
            throw constructTermination("messageXhtmlPrepareForLatex1ResultInfoFileDoesntExistButShould", null, null, xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
        }

        if (xhtmlPrepareForLatex1ResultInfoFile.isFile() != true)
        {
            throw constructTermination("messageXhtmlPrepareForLatex1ResultInfoPathExistsButIsntAFile", null, null, xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
        }

        if (xhtmlPrepareForLatex1ResultInfoFile.canRead() != true)
        {
            throw constructTermination("messageXhtmlPrepareForLatex1ResultInfoFileIsntReadable", null, null, xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(xhtmlPrepareForLatex1ResultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageXhtmlPrepareForLatex1ResultInfoFileErrorWhileReading", ex, null, xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageXhtmlPrepareForLatex1ResultInfoFileErrorWhileReading", ex, null, xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXhtmlPrepareForLatex1ResultInfoFileErrorWhileReading", ex, null, xhtmlPrepareForLatex1ResultInfoFile.getAbsolutePath());
        }

        if (wasSuccess != true)
        {
            throw constructTermination("messageXhtmlPrepareForLatex1CallWasntSuccessful", null, null);
        }


        File xmlXsltTransformator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_xslt_transformator_1.xml");

        if (xmlXsltTransformator1ResultInfoFile.exists() == true)
        {
            if (xmlXsltTransformator1ResultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlXsltTransformator1ResultInfoFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (xmlXsltTransformator1ResultInfoFile.canWrite() != true)
                    {
                        throw constructTermination("messageXmlXsltTransformator1ResultInfoFileExistsButIsntWritable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlXsltTransformator1ResultInfoPathExistsButIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
        }

        File xmlXsltTransformator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_xslt_transformator_1.xml");

        if (xmlXsltTransformator1JobFile.exists() == true)
        {
            if (xmlXsltTransformator1JobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlXsltTransformator1JobFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (xmlXsltTransformator1JobFile.canWrite() != true)
                    {
                        throw constructTermination("messageXmlXsltTransformator1JobFileExistsButIsntWritable", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlXsltTransformator1JobPathExistsButIsntAFile", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(xmlXsltTransformator1JobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by xhtml_to_latex_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
            writer.write("<xml-xslt-transformator-1-job>\n");
            writer.write("  <job input-file=\"" + inputPreparedForLatexFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + xmlXsltTransformator1EntityResolverConfigurationFile.getAbsolutePath() + "\" stylesheet-file=\"" + stylesheetFile.getAbsolutePath() + "\" output-file=\"" + outputFile.getAbsolutePath() + "\"/>\n");
            writer.write("</xml-xslt-transformator-1-job>\n");

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }

        builder = new ProcessBuilder("java", "xml_xslt_transformator_1", xmlXsltTransformator1JobFile.getAbsolutePath(), xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ErrorWhileReadingOutput", ex, null);
        }

        if (xmlXsltTransformator1ResultInfoFile.exists() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1FilterInfoFileDoesntExistButShould", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (xmlXsltTransformator1ResultInfoFile.isFile() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1FilterInfoPathExistsButIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (xmlXsltTransformator1ResultInfoFile.canRead() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1FilterInfoFileIsntReadable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(xmlXsltTransformator1ResultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (wasSuccess != true)
        {
            throw constructTermination("messageXmlXsltTransformator1CallWasntSuccessful", null, null);
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xhtml_to_latex_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "xhtml_to_latex_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xhtml_to_latex_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "xhtml_to_latex_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (xhtml_to_latex_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xhtml_to_latex_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_latex_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xhtml-to-latex-1-workflow-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</xhtml-to-latex-1-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        xhtml_to_latex_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nXhtmlToLatex1WorkflowConsole";
    private ResourceBundle l10nConsole;
}
