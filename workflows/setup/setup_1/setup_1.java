/* Copyright (C) 2016-2024 Stephan Kreutzer
 *
 * This file is part of setup_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * setup_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * setup_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with setup_1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/workflows/setup/setup_1/setup_1.java
 * @brief Sets up the various tools that get called by the processing workflows.
 * @author Stephan Kreutzer
 * @since 2016-01-01
 */



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;



public class setup_1
{
    public static void main(String args[])
    {
        System.out.print("setup_1 Copyright (C) 2016-2024 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        setup_1 setup = new setup_1();
        setup.setup();
    }

    public int setup()
    {
        String programPath = setup_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(-1);
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(-1);
        }


        CopyEntitiesONIX_2_1_3_Short(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities");
        CopyEntitiesONIX_2_1_3_Short(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "onix_prepare_for_json" + File.separator + "onix_prepare_for_json_1" + File.separator + "entities");

        CopyEntitiesXHTML_1_0_Strict(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities");
        CopyEntitiesXHTML_1_0_Strict(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "onix_prepare_for_json" + File.separator + "onix_prepare_for_json_1" + File.separator + "entities");
        CopyEntitiesXHTML_1_0_Strict(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_dtd_entity_resolver" + File.separator + "xml_dtd_entity_resolver_1" + File.separator + "entities");
        CopyEntitiesXHTML_1_0_Strict(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "latex" + File.separator + "xhtml_prepare_for_latex" + File.separator + "xhtml_prepare_for_latex_1" + File.separator + "entities");

        CopyEntitiesXHTML_1_1(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities");
        CopyEntitiesXHTML_1_1(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_dtd_entity_resolver" + File.separator + "xml_dtd_entity_resolver_1" + File.separator + "entities");
        CopyEntitiesXHTML_1_1(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "latex" + File.separator + "xhtml_prepare_for_latex" + File.separator + "xhtml_prepare_for_latex_1" + File.separator + "entities");

        CopyEntitiesUnicode(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities");
        CopyEntitiesUnicode(programPath, ".." + File.separator + ".." + File.separator + ".." + File.separator + "onix_prepare_for_json" + File.separator + "onix_prepare_for_json_1" + File.separator + "entities");

        return 0;
    }


    public int CopyEntitiesONIX_2_1_3_Short(String programPath, String to)
    {
        if (programPath.endsWith("/") != true &&
            programPath.endsWith("\\") != true &&
            programPath.endsWith(File.separator) != true)
        {
            programPath += File.separator;
        }

        if (to.startsWith("/") == true)
        {
            to = to.substring(0, new String("/").length());
        }
        else if (to.startsWith("\\") == true)
        {
            to = to.substring(0, new String("\\").length());
        }
        else if (to.startsWith(File.separator) == true)
        {
            to = to.substring(0, new String(File.separator).length());
        }

        if (to.endsWith("/") != true &&
            to.endsWith("\\") != true &&
            to.endsWith(File.separator) != true)
        {
            to += File.separator;
        }

        to += "org.editeur" + File.separator + "onix_2_1_3_short" + File.separator;

        try
        {
            new File(programPath + to).mkdirs();
        }
        catch (SecurityException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.editeur" + File.separator + "onix_2_1_3_short" + File.separator + "onix-international.dtd"), 
                           new File(programPath + to + "onix-international.dtd")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.editeur" + File.separator + "onix_2_1_3_short" + File.separator + "short.elt"), 
                           new File(programPath + to + "short.elt")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.editeur" + File.separator + "onix_2_1_3_short" + File.separator + "onix-xhtml.elt"), 
                           new File(programPath + to + "onix-xhtml.elt")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.editeur" + File.separator + "EDItEUR_IPR_licence_01-06-15.pdf"), 
                           new File(programPath + to + "EDItEUR_IPR_licence_01-06-15.pdf")) != 0)
        {
            System.exit(-1);
        }

        return 0;
    }

    public int CopyEntitiesXHTML_1_0_Strict(String programPath, String to)
    {
        if (programPath.endsWith("/") != true &&
            programPath.endsWith("\\") != true &&
            programPath.endsWith(File.separator) != true)
        {
            programPath += File.separator;
        }

        if (to.startsWith("/") == true)
        {
            to = to.substring(0, new String("/").length());
        }
        else if (to.startsWith("\\") == true)
        {
            to = to.substring(0, new String("\\").length());
        }
        else if (to.startsWith(File.separator) == true)
        {
            to = to.substring(0, new String(File.separator).length());
        }

        if (to.endsWith("/") != true &&
            to.endsWith("\\") != true &&
            to.endsWith(File.separator) != true)
        {
            to += File.separator;
        }

        to += "org.w3c" + File.separator + "xhtml_1_0_strict" + File.separator;

        try
        {
            new File(programPath + to).mkdirs();
        }
        catch (SecurityException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_0_strict" + File.separator + "xhtml1-strict.dtd"), 
                           new File(programPath + to + "xhtml1-strict.dtd")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_0_strict" + File.separator + "xhtml-symbol.ent"), 
                           new File(programPath + to + "xhtml-symbol.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_0_strict" + File.separator + "xhtml-special.ent"), 
                           new File(programPath + to + "xhtml-special.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_0_strict" + File.separator + "xhtml-lat1.ent"), 
                           new File(programPath + to + "xhtml-lat1.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_0_strict" + File.separator + "LICENSE"), 
                           new File(programPath + to + "LICENSE")) != 0)
        {
            System.exit(-1);
        }

        return 0;
    }

    public int CopyEntitiesXHTML_1_1(String programPath, String to)
    {
        if (programPath.endsWith("/") != true &&
            programPath.endsWith("\\") != true &&
            programPath.endsWith(File.separator) != true)
        {
            programPath += File.separator;
        }

        if (to.startsWith("/") == true)
        {
            to = to.substring(0, new String("/").length());
        }
        else if (to.startsWith("\\") == true)
        {
            to = to.substring(0, new String("\\").length());
        }
        else if (to.startsWith(File.separator) == true)
        {
            to = to.substring(0, new String(File.separator).length());
        }

        if (to.endsWith("/") != true &&
            to.endsWith("\\") != true &&
            to.endsWith(File.separator) != true)
        {
            to += File.separator;
        }

        to += "org.w3c" + File.separator + "xhtml_1_1" + File.separator;

        try
        {
            new File(programPath + to).mkdirs();
        }
        catch (SecurityException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(-1);
        }


        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml11.dtd"),
                           new File(programPath + to + "xhtml11.dtd")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-inlstyle-1.mod"),
                           new File(programPath + to + "xhtml-inlstyle-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-datatypes-1.mod"),
                           new File(programPath + to + "xhtml-datatypes-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-framework-1.mod"),
                           new File(programPath + to + "xhtml-framework-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-qname-1.mod"),
                           new File(programPath + to + "xhtml-qname-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-events-1.mod"),
                           new File(programPath + to + "xhtml-events-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-attribs-1.mod"),
                           new File(programPath + to + "xhtml-attribs-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml11-model-1.mod"),
                           new File(programPath + to + "xhtml11-model-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-charent-1.mod"),
                           new File(programPath + to + "xhtml-charent-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-lat1.ent"),
                           new File(programPath + to + "xhtml-lat1.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-symbol.ent"),
                           new File(programPath + to + "xhtml-symbol.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-special.ent"),
                           new File(programPath + to + "xhtml-special.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-text-1.mod"),
                           new File(programPath + to + "xhtml-text-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-inlstruct-1.mod"),
                           new File(programPath + to + "xhtml-inlstruct-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-inlphras-1.mod"),
                           new File(programPath + to + "xhtml-inlphras-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-blkstruct-1.mod"),
                           new File(programPath + to + "xhtml-blkstruct-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-blkphras-1.mod"),
                           new File(programPath + to + "xhtml-blkphras-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-hypertext-1.mod"),
                           new File(programPath + to + "xhtml-hypertext-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-list-1.mod"),
                           new File(programPath + to + "xhtml-list-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-edit-1.mod"),
                           new File(programPath + to + "xhtml-edit-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-bdo-1.mod"),
                           new File(programPath + to + "xhtml-bdo-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-ruby-1.mod"),
                           new File(programPath + to + "xhtml-ruby-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-pres-1.mod"),
                           new File(programPath + to + "xhtml-pres-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-inlpres-1.mod"),
                           new File(programPath + to + "xhtml-inlpres-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-blkpres-1.mod"),
                           new File(programPath + to + "xhtml-blkpres-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-link-1.mod"),
                           new File(programPath + to + "xhtml-link-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-meta-1.mod"),
                           new File(programPath + to + "xhtml-meta-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-base-1.mod"),
                           new File(programPath + to + "xhtml-base-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-script-1.mod"),
                           new File(programPath + to + "xhtml-script-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-style-1.mod"),
                           new File(programPath + to + "xhtml-style-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-image-1.mod"),
                           new File(programPath + to + "xhtml-image-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-csismap-1.mod"),
                           new File(programPath + to + "xhtml-csismap-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-ssismap-1.mod"),
                           new File(programPath + to + "xhtml-ssismap-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-param-1.mod"),
                           new File(programPath + to + "xhtml-param-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-object-1.mod"),
                           new File(programPath + to + "xhtml-object-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-table-1.mod"),
                           new File(programPath + to + "xhtml-table-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-form-1.mod"),
                           new File(programPath + to + "xhtml-form-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "xhtml-struct-1.mod"),
                           new File(programPath + to + "xhtml-struct-1.mod")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.w3c" + File.separator + "xhtml_1_1" + File.separator + "LICENSE"), 
                           new File(programPath + to + "LICENSE")) != 0)
        {
            System.exit(-1);
        }

        return 0;
    }

    public int CopyEntitiesUnicode(String programPath, String to)
    {
        if (programPath.endsWith("/") != true &&
            programPath.endsWith("\\") != true &&
            programPath.endsWith(File.separator) != true)
        {
            programPath += File.separator;
        }

        if (to.startsWith("/") == true)
        {
            to = to.substring(0, new String("/").length());
        }
        else if (to.startsWith("\\") == true)
        {
            to = to.substring(0, new String("\\").length());
        }
        else if (to.startsWith(File.separator) == true)
        {
            to = to.substring(0, new String(File.separator).length());
        }

        if (to.endsWith("/") != true &&
            to.endsWith("\\") != true &&
            to.endsWith(File.separator) != true)
        {
            to += File.separator;
        }

        to += "org.iso" + File.separator + "unicode" + File.separator;

        try
        {
            new File(programPath + to).mkdirs();
        }
        catch (SecurityException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-amsa.ent"), 
                           new File(programPath + to + "iso-amsa.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-amsb.ent"), 
                           new File(programPath + to + "iso-amsb.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-amsc.ent"), 
                           new File(programPath + to + "iso-amsc.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-amsn.ent"), 
                           new File(programPath + to + "iso-amsn.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-amso.ent"), 
                           new File(programPath + to + "iso-amso.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-amsr.ent"), 
                           new File(programPath + to + "iso-amsr.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-box.ent"), 
                           new File(programPath + to + "iso-box.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-cyr1.ent"), 
                           new File(programPath + to + "iso-cyr1.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-cyr2.ent"), 
                           new File(programPath + to + "iso-cyr2.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-dia.ent"), 
                           new File(programPath + to + "iso-dia.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-grk3.ent"), 
                           new File(programPath + to + "iso-grk3.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-lat1.ent"), 
                           new File(programPath + to + "iso-lat1.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-lat2.ent"), 
                           new File(programPath + to + "iso-lat2.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-mfrk.ent"), 
                           new File(programPath + to + "iso-mfrk.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-mopf.ent"), 
                           new File(programPath + to + "iso-mopf.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-mscr.ent"), 
                           new File(programPath + to + "iso-mscr.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-num.ent"), 
                           new File(programPath + to + "iso-num.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-pub.ent"), 
                           new File(programPath + to + "iso-pub.ent")) != 0)
        {
            System.exit(-1);
        }

        if (CopyFileBinary(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "resources" + File.separator + "org.iso" + File.separator + "unicode" + File.separator + "iso-tech.ent"), 
                           new File(programPath + to + "iso-tech.ent")) != 0)
        {
            System.exit(-1);
        }

        return 0;
    }


    public int CopyFileBinary(File from, File to)
    {
        if (from.exists() != true)
        {
            System.out.println("setup_1: " + getI10nStringFormatted("messageCantCopyBecauseFromDoesntExist", from.getAbsolutePath(), to.getAbsolutePath()));
            return -1;
        }

        if (from.isFile() != true)
        {
            System.out.println("setup_1: " + getI10nStringFormatted("messageCantCopyBecauseFromIsntAFile", from.getAbsolutePath(), to.getAbsolutePath()));
            return -2;
        }

        if (from.canRead() != true)
        {
            System.out.println("setup_1: " + getI10nStringFormatted("messageCantCopyBecauseFromIsntReadable", from.getAbsolutePath(), to.getAbsolutePath()));
            return -3;
        }

        if (to.exists() == true)
        {
            if (to.isFile() == true)
            {
                if (to.canWrite() != true)
                {
                    System.out.println("setup_1: " + getI10nStringFormatted("messageCantCopyBecauseToIsntWritable", from.getAbsolutePath(), to.getAbsolutePath()));
                    return -5;
                }
            }
            else
            {
                System.out.println("setup_1: " + getI10nStringFormatted("messageCantCopyBecauseToIsntAFile", from.getAbsolutePath(), to.getAbsolutePath()));
                return -4;
            }
        }


        boolean exception = false;

        byte[] buffer = new byte[1024];

        FileInputStream reader = null;
        FileOutputStream writer = null;

        try
        {
            to.createNewFile();

            reader = new FileInputStream(from);
            writer = new FileOutputStream(to);

            int bytesRead = reader.read(buffer, 0, buffer.length);

            while (bytesRead > 0)
            {
                writer.write(buffer, 0, bytesRead);
                bytesRead = reader.read(buffer, 0, buffer.length);
            }

            writer.close();
            reader.close();
        }
        catch (FileNotFoundException ex)
        {
            exception = true;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            exception = true;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException ex)
                {
                    if (exception == false)
                    {
                        exception = true;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
                    }
                }
            }

            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException ex)
                {
                    if (exception == false)
                    {
                        exception = true;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
                    }
                }
            }
        }

        if (exception != false)
        {
            System.exit(-1);
        }

        return 0;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    private static final String L10N_BUNDLE = "l10n.l10nSetup1Console";
    private ResourceBundle l10nConsole;
}
