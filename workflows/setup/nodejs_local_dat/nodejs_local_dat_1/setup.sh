#!/bin/sh
# Copyright (C) 2017-2022 Stephan Kreutzer
#
# This file is part of nodejs_local_dat_1, a submodule of the
# digital_publishing_workflow_tools package.
#
# nodejs_local_dat_1 is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# nodejs_local_dat_1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with nodejs_local_dat_1. If not, see <http://www.gnu.org/licenses/>.

sudo apt-get install nodejs npm
npm install --save dat-sdk
