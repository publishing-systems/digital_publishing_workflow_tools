/*
Copyright (C) 2017-2020 Stephan Kreutzer

This file is part of nodejs_local_dat_1, a submodule of the
digital_publishing_workflow_tools package.

nodejs_local_dat_1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

nodejs_local_dat_1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with nodejs_local_dat_1. If not, see <http://www.gnu.org/licenses/>.
*/

"use strict";


let urlParser = require('url');

function handleRequest(request, response)
{
    let url = urlParser.parse(request.url, true);

    if (url.pathname == "/hyperdrive.readdir")
    {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        if (url.query.name == undefined ||
            url.query.name == null)
        {
            response.statusCode = 400;
            response.end();
            return;
        }

        const SDK = require('dat-sdk');

        const { Hyperdrive } = SDK();
        const hyperdrive = Hyperdrive("dat://" + url.query.name);

        let callback = function() {
            hyperdrive.readdir('/', function(error, list) {
                if (error != undefined &&
                    error != null)
                {
                    response.write(error);
                    response.statusCode = 500;
                    response.end();
                    return;
                }

                // TODO: JSON-LD. Paths are relative.
                response.write(JSON.stringify(list));

                /*
                // TODO: XML namespace.
                response.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<ul>");

                for (let i = 0, max = list.length; i < max; i++)
                {
                    response.write("<li><a href=\"dat://");
                    response.write(url.query.name);
                    // TODO: Slash at the end of the URL or not?
                    response.write(list[i]);
                    response.write("\">");
                    response.write(list[i]);
                    response.write("</a></li>");
                }

                response.write("</ul>");
                */

                response.statusCode = 200;
                response.end();
            });
        };

        if (hyperdrive.metadata.peers.length > 0)
        {
            hyperdrive.metadata.update({ ifAvailable: true }, callback);
        }
        else
        {
            hyperdrive.metadata.once('peer-add', () => {
                hyperdrive.metadata.update({ ifAvailable: true }, callback);
            });
        }

        return;
    }
    else
    {
        response.statusCode = 404;
        response.end();
        return;
    }
}

console.log("\n" +
            "nodejs_local_dat_1 Copyright (C) 2017-2020 Stephan Kreutzer\n" +
            "This program comes with ABSOLUTELY NO WARRANTY.\n" +
            "This is free software, and you are welcome to redistribute it\n" +
            "under certain conditions. See the GNU Affero General Public License 3\n" +
            "or any later version for details. Also, see the source code repository\n" +
            "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
            "the project website https://publishing-systems.org.\n\n");

let http = require('http');
let server = http.createServer(handleRequest);

server.listen(8080, function() { });

