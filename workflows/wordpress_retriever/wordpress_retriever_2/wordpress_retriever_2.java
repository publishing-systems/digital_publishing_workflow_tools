/* Copyright (C) 2016-2022 Stephan Kreutzer
 *
 * This file is part of wordpress_retriever_2 workflow, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * wordpress_retriever_2 workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * wordpress_retriever_2 workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with wordpress_retriever_2 workflow. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/workflows/wordpress_retriever/wordpress_retriever_2/wordpress_retriever_2.java
 * @brief Retrieves all posts from a WordPress base URL using the RSS feed.
 * @author Stephan Kreutzer
 * @since 2021-03-06
 */



import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.util.Scanner;
import java.util.AbstractMap;



public class wordpress_retriever_2
{
    public static void main(String args[])
    {
        System.out.print("wordpress_retriever_2 workflow Copyright (C) 2016-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        wordpress_retriever_2 instance = new wordpress_retriever_2();

        try
        {
            instance.call(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by wordpress_retriever_2 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<wordpress-retriever-2-workflow-result-information>\n");
                writer.write("  <success>\n");

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</wordpress-retriever-2-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int call(String args[])
    {
        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\twordpress_retriever_2 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        wordpress_retriever_2.resultInfoFile = resultInfoFile;

        String programPath = wordpress_retriever_2.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }

        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("wordpress_retriever_2 workflow: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));


        String inputUrl = null;
        File outputFile = null;
        File tempDirectory = null;
        List<AbstractMap.SimpleEntry<String, String>> replacementDictionary = new ArrayList<AbstractMap.SimpleEntry<String, String>>();

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input") == true)
                    {
                        StartElement inputElement = event.asStartElement();
                        Attribute urlAttribute = inputElement.getAttributeByName(new QName("url"));

                        if (urlAttribute == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "url");
                        }

                        if (inputUrl != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        inputUrl = urlAttribute.getValue();
                    }
                    else if (tagName.equals("temp-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (tempDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        tempDirectory = new File(attributePath.getValue());

                        if (tempDirectory.isAbsolute() != true)
                        {
                            tempDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            tempDirectory = tempDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("replace") == true)
                    {
                        String pattern = new String();
                        String replacement = new String();

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isStartElement() == true)
                            {
                                if (event.asStartElement().getName().getLocalPart().equalsIgnoreCase("pattern") == true)
                                {
                                    while (eventReader.hasNext() == true)
                                    {
                                        event = eventReader.nextEvent();

                                        if (event.isCharacters() == true)
                                        {
                                            pattern += event.asCharacters().getData();
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                else if (event.asStartElement().getName().getLocalPart().equalsIgnoreCase("replacement") == true)
                                {
                                    while (eventReader.hasNext() == true)
                                    {
                                        event = eventReader.nextEvent();

                                        if (event.isCharacters() == true)
                                        {
                                            replacement += event.asCharacters().getData();
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                            else if (event.isEndElement() == true)
                            {
                                if (event.asEndElement().getName().getLocalPart().equalsIgnoreCase("replace") == true)
                                {
                                    break;
                                }
                            }
                        }

                        int patternLength = pattern.length();

                        if (patternLength <= 0)
                        {
                            throw constructTermination("messageJobFilePatternEmpty", null, null, jobFile.getAbsolutePath());
                        }

                        replacementDictionary.add(new AbstractMap.SimpleEntry<String, String>(pattern, replacement));
                    }
                    else if (tagName.equals("output-file") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (outputFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        outputFile = new File(attributePath.getValue());

                        if (outputFile.isAbsolute() != true)
                        {
                            outputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            outputFile = outputFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputFileCantGetCanonicalPath", ex, null, outputFile.getAbsolutePath());
                        }

                        if (outputFile.exists() == true)
                        {
                            if (outputFile.isFile() == true)
                            {
                                if (outputFile.canWrite() != true)
                                {
                                    throw constructTermination("messageOutputFileIsntWritable", null, null, outputFile.getAbsolutePath());
                                }
                            }
                            else
                            {
                                throw constructTermination("messageOutputPathIsntAFile", null, null, outputFile.getAbsolutePath());
                            }
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (inputUrl == null)
        {
            throw constructTermination("messageJobFileInputUrlIsntConfigured", null, null, jobFile.getAbsolutePath(), "input");
        }

        if (outputFile == null)
        {
            throw constructTermination("messageJobFileOutputFileIsntConfigured", null, null, jobFile.getAbsolutePath(), "output-file");
        }

        if (tempDirectory == null)
        {
            tempDirectory = new File(programPath + "temp");
        }

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    throw constructTermination("messageTempDirectoryIsntWritable", null, null, tempDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageTempPathIsntADirectory", null, null, tempDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageTempDirectoryCantCreate", ex, null, tempDirectory.getAbsolutePath());
            }
        }

        if (inputUrl.endsWith("/") != true)
        {
            inputUrl += '/';
        }

        String protocol = null;

        {
            int pos = inputUrl.indexOf("://");

            if (pos < 0)
            {
                throw constructTermination("messageJobFileInputUrlWithoutProtocol", null, null, inputUrl, "url", "input", jobFile.getAbsolutePath());
            }

            protocol = inputUrl.substring(0, pos);
        }

        if (protocol.equals("http") != true &&
            protocol.equals("https") != true)
        {
            throw constructTermination("messageJobFileInputUrlProtocolNotSupported", null, null, protocol, inputUrl, "url", "input", jobFile.getAbsolutePath());
        }


        List<File> pageFiles = new ArrayList<File>();
        int pageIndex = 1;
        boolean wasSuccess = true;

        while (wasSuccess == true)
        {
            File jobFileHttpClient1 = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_" + protocol + "_client_1_job_" + pageIndex + ".xml");

            if (jobFileHttpClient1.exists() == true)
            {
                if (jobFileHttpClient1.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = jobFileHttpClient1.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (jobFileHttpClient1.canWrite() != true)
                        {
                            throw constructTermination("messageH" + protocol.substring(1) + "Client1JobFileIsntWritable", null, null, jobFileHttpClient1.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageH" + protocol.substring(1) + "Client1JobPathExistsButIsntAFile", null, null, jobFileHttpClient1.getAbsolutePath());
                }
            }

            File resultInfoFileHttpClient1 = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_" + protocol + "_client_1_job_" + pageIndex + ".xml");

            if (resultInfoFileHttpClient1.exists() == true)
            {
                if (resultInfoFileHttpClient1.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = resultInfoFileHttpClient1.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (resultInfoFileHttpClient1.canWrite() != true)
                        {
                            throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoFileIsntWritable", null, null, resultInfoFileHttpClient1.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFileHttpClient1.getAbsolutePath());
                }
            }


            File xmlPageFile = new File(tempDirectory.getAbsolutePath() + File.separator + "page_" + pageIndex + ".xml");

            if (xmlPageFile.exists() == true)
            {
                if (xmlPageFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlPageFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlPageFile.canWrite() != true)
                        {
                            throw constructTermination("messagePageFileIsntWritable", null, null, xmlPageFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messagePagePathExistsButIsntAFile", null, null, xmlPageFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(jobFileHttpClient1),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by wordpress_retriever_2 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<" + protocol + "-client-1-jobfile>\n");

                if (pageIndex == 1)
                {
                    /** @todo Escape XML special characters. */
                    /** @todo Stupid crap WordPress does sometimes a 301 Permanent Redirect to "/feed" and sometimes to "/feed/". */
                    writer.write("  <request url=\"" + inputUrl + "feed/\" method=\"GET\"/>\n");
                }
                else
                {
                    /** @todo Escape XML special characters. */
                    writer.write("  <request url=\"" + inputUrl + "feed/?paged=" + pageIndex + "\" method=\"GET\"/>\n");
                }

                /** @todo Escape XML special characters. */
                writer.write("  <response destination=\"" + xmlPageFile.getAbsolutePath() + "\"/>\n");
                writer.write("</" + protocol + "-client-1-jobfile>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1JobFileErrorWhileWriting", ex, null, jobFileHttpClient1.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1JobFileErrorWhileWriting", ex, null, jobFileHttpClient1.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1JobFileErrorWhileWriting", ex, null, jobFileHttpClient1.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", protocol + "_client_1", jobFileHttpClient1.getAbsolutePath(), resultInfoFileHttpClient1.getAbsolutePath());
            builder.directory(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + protocol + "_client" + File.separator + protocol + "_client_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ErrorWhileReadingOutput", ex, null);
            }

            if (resultInfoFileHttpClient1.exists() != true)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoFileDoesntExistButShould", null, null, resultInfoFileHttpClient1.getAbsolutePath());
            }

            if (resultInfoFileHttpClient1.isFile() != true)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFileHttpClient1.getAbsolutePath());
            }

            if (resultInfoFileHttpClient1.canRead() != true)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoFileIsntReadable", null, null, resultInfoFileHttpClient1.getAbsolutePath());
            }

            wasSuccess = false;
            boolean inHttpStatusCode = false;
            String httpStatusCode = null;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(resultInfoFileHttpClient1);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                        }
                        else if (tagName.equals("http-status-code") == true)
                        {
                            inHttpStatusCode = true;
                        }
                    }
                    else if (event.isCharacters() == true)
                    {
                        if (inHttpStatusCode == true)
                        {
                            if (httpStatusCode == null)
                            {
                                httpStatusCode = new String();
                            }

                            httpStatusCode += event.asCharacters().getData();
                        }
                    }
                    else if (event.isEndElement() == true)
                    {
                        String tagName = event.asEndElement().getName().getLocalPart();

                        if (tagName.equals("http-status-code") == true)
                        {
                            inHttpStatusCode = false;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileHttpClient1.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileHttpClient1.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileHttpClient1.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1CallWasntSuccessful", null, null, jobFileHttpClient1.getAbsolutePath());
            }

            if (httpStatusCode == null)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoFileElementMissing", null, null, resultInfoFileHttpClient1.getAbsolutePath(), "http-status-code");
            }

            if (httpStatusCode.equals("404") == true)
            {
                break;
            }

            if (httpStatusCode.equals("200") != true)
            {
                throw constructTermination("messageH" + protocol.substring(1) + "Client1ResultInfoFileUnexpectedHttpStatusCode", null, null, resultInfoFileHttpClient1.getAbsolutePath(), "http-status-code", httpStatusCode, "200");
            }


            File xmlPagePreparedFile = new File(tempDirectory.getAbsolutePath() + File.separator + "page_" + pageIndex + "_prepared.xml");

            if (replacementDictionary.size() > 0)
            {
                if (PreparePageFile(xmlPageFile, pageIndex, programPath, tempDirectory, replacementDictionary, xmlPagePreparedFile) != 0)
                {
                    wasSuccess = false;
                    break;
                }
            }
            else
            {
                xmlPagePreparedFile = xmlPageFile;
            }

            File xmlPageDeescapedFile = new File(tempDirectory.getAbsolutePath() + File.separator + "page_" + pageIndex + "_prepared_deescaped.xml");

            if (TransformXml(xmlPagePreparedFile, pageIndex, programPath, tempDirectory, new File(programPath + "xml_deescape.xsl"), xmlPageDeescapedFile) != 0)
            {
                wasSuccess = false;
                break;
            }

            File pageXhtmlResolvedFile = new File(tempDirectory.getAbsolutePath() + File.separator + "page_" + pageIndex + "_prepared_deescaped_resolved.xml");

            if (ResolveXhtmlEntitiesXml(xmlPageDeescapedFile, pageIndex, programPath, tempDirectory, pageXhtmlResolvedFile) != 0)
            {
                wasSuccess = false;
                break;
            }

            pageFiles.add(pageXhtmlResolvedFile);
            ++pageIndex;

            try
            {
                Thread.sleep(5000);
            }
            catch (InterruptedException ex)
            {
                this.infoMessages.add(constructInfoMessage("messageWaitingPeriodInterrupt", true, ex, null));
            }
        }

        if (pageFiles.size() <= 0)
        {
            this.infoMessages.add(constructInfoMessage("messageNoPagesToProcess", true, null, null));
            return 1;
        }


        {
            File xmlConcatenator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_concatenator_1_job.xml");

            if (xmlConcatenator1JobFile.exists() == true)
            {
                if (xmlConcatenator1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlConcatenator1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {
                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlConcatenator1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlConcatenator1JobFileIsntWritable", null, null, xmlConcatenator1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlConcatenator1JobPathIsntAFile", null, null, xmlConcatenator1JobFile.getAbsolutePath());
                }
            }

            File xmlConcatenator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_concatenator_1_job.xml");

            if (xmlConcatenator1ResultInfoFile.exists() == true)
            {
                if (xmlConcatenator1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlConcatenator1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {
                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlConcatenator1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlConcatenator1ResultInfoFileIsntWritable", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlConcatenator1ResultInfoPathIsntAFile", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xmlConcatenator1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by wordpress_retriever_2 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<xml-concatenator-1-job>\n");
                writer.write("  <input-files>\n");

                for (int i = 0, max = pageFiles.size(); i < max; i++)
                {
                    writer.write("    <input-file path=\"" + pageFiles.get(i).getAbsolutePath() + "\"/>\n");
                }

                writer.write("  </input-files>\n");
                writer.write("  <output-file path=\"" + outputFile.getAbsolutePath() + "\" processing-instruction-data=\"encoding=&quot;UTF-8&quot;\" root-element-name=\"wordpress\"/>\n");
                writer.write("</xml-concatenator-1-job>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageXmlConcatenator1JobFileErrorWhileWriting", ex, null, xmlConcatenator1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageXmlConcatenator1JobFileErrorWhileWriting", ex, null, xmlConcatenator1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlConcatenator1JobFileErrorWhileWriting", ex, null, xmlConcatenator1JobFile.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", "xml_concatenator_1", xmlConcatenator1JobFile.getAbsolutePath(), xmlConcatenator1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_concatenator" + File.separator + "xml_concatenator_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");
                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }
                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlConcatenator1ErrorWhileReadingOutput", ex, null, xmlConcatenator1JobFile.getAbsolutePath());
            }

            if (xmlConcatenator1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileDoesntExistButShould", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlConcatenator1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoPathIsntAFile", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlConcatenator1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileIsntReadable", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }

            wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(xmlConcatenator1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileErrorWhileReading", ex, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileErrorWhileReading", ex, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileErrorWhileReading", ex, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageXmlConcatenator1CallWasntSuccessful", null, null, xmlConcatenator1JobFile.getAbsolutePath());
            }
        }

        return 0;
    }

    protected int PreparePageFile(File unpreparedFile,
                                  int pageIndex,
                                  String programPath,
                                  File tempDirectory,
                                  List<AbstractMap.SimpleEntry<String, String>> replacementDictionary,
                                  File preparedFile)
    {
        if (preparedFile.exists() == true)
        {
            if (preparedFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = preparedFile.delete();
                }
                catch (SecurityException ex)
                {
                }

                if (deleteSuccessful != true)
                {
                    if (preparedFile.canWrite() != true)
                    {
                        throw constructTermination("messagePreparedFileIsntWritable", null, null, preparedFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messagePreparedFilePathIsntAFile", null, null, preparedFile.getAbsolutePath());
            }
        }

        File jobFileTextReplacer1 = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_text_replacer_1_job_" + pageIndex + ".xml");

        if (jobFileTextReplacer1.exists() == true)
        {
            if (jobFileTextReplacer1.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFileTextReplacer1.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFileTextReplacer1.canWrite() != true)
                    {
                        throw constructTermination("messageTextReplacer1JobFileIsntWritable", null, null, jobFileTextReplacer1.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageTextReplacer1JobPathExistsButIsntAFile", null, null, jobFileTextReplacer1.getAbsolutePath());
            }
        }

        File resultInfoFileTextReplacer1 = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_text_replacer_1_job_" + pageIndex + ".xml");

        if (resultInfoFileTextReplacer1.exists() == true)
        {
            if (resultInfoFileTextReplacer1.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFileTextReplacer1.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFileTextReplacer1.canWrite() != true)
                    {
                        throw constructTermination("messageTextReplacer1ResultInfoFileIsntWritable", null, null, resultInfoFileTextReplacer1.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageTextReplacer1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFileTextReplacer1.getAbsolutePath());
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFileTextReplacer1),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by wordpress_retriever_2 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
            writer.write("<text-replacer-1-job>\n");
            writer.write("  <input-file path=\"" + unpreparedFile.getAbsolutePath() + "\"/>\n");
            writer.write("  <temp-directory path=\"./temp\"/>\n");
            writer.write("  <replacement-dictionary>\n");

            for (int i = 0, max = replacementDictionary.size(); i < max; i++)
            {
                String pattern = replacementDictionary.get(i).getKey();
                String replacement = replacementDictionary.get(i).getValue();

                // Ampersand needs to be the first, otherwise it would double-encode
                // other entities.
                pattern = pattern.replaceAll("&", "&amp;");
                pattern = pattern.replaceAll("<", "&lt;");
                pattern = pattern.replaceAll(">", "&gt;");

                // Ampersand needs to be the first, otherwise it would double-encode
                // other entities.
                replacement = replacement.replaceAll("&", "&amp;");
                replacement = replacement.replaceAll("<", "&lt;");
                replacement = replacement.replaceAll(">", "&gt;");

                writer.write("    <replace>\n");
                writer.write("      <pattern>" + pattern + "</pattern>\n");
                writer.write("      <replacement>" + replacement + "</replacement>\n");
                writer.write("    </replace>\n");
            }

            writer.write("  </replacement-dictionary>\n");
            writer.write("  <output-file path=\"" + preparedFile.getAbsolutePath() + "\"/>\n");
            writer.write("</text-replacer-1-job>\n");

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageTextReplacer1JobFileErrorWhileWriting", ex, null, jobFileTextReplacer1.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageTextReplacer1JobFileErrorWhileWriting", ex, null, jobFileTextReplacer1.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageTextReplacer1JobFileErrorWhileWriting", ex, null, jobFileTextReplacer1.getAbsolutePath());
        }

        ProcessBuilder builder = new ProcessBuilder("java", "text_replacer_1", jobFileTextReplacer1.getAbsolutePath(), resultInfoFileTextReplacer1.getAbsolutePath());
        builder.directory(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "text_replacer" + File.separator + "text_replacer_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageTextReplacer1ErrorWhileReadingOutput", ex, null);
        }

        if (resultInfoFileTextReplacer1.exists() != true)
        {
            throw constructTermination("messageTextReplacer1ResultInfoFileDoesntExistButShould", null, null, resultInfoFileTextReplacer1.getAbsolutePath());
        }

        if (resultInfoFileTextReplacer1.isFile() != true)
        {
            throw constructTermination("messageTextReplacer1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFileTextReplacer1.getAbsolutePath());
        }

        if (resultInfoFileTextReplacer1.canRead() != true)
        {
            throw constructTermination("messageTextReplacer1ResultInfoFileIsntReadable", null, null, resultInfoFileTextReplacer1.getAbsolutePath());
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFileTextReplacer1);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageTextReplacer1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileTextReplacer1.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageTextReplacer1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileTextReplacer1.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageTextReplacer1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileTextReplacer1.getAbsolutePath());
        }

        if (wasSuccess != true)
        {
            throw constructTermination("messageTextReplacer1CallWasntSuccessful", null, null, jobFileTextReplacer1.getAbsolutePath());
        }

        return 0;
    }

    protected int TransformXml(File xmlInputFile, int fileIndex, String programPath, File tempDirectory, File stylesheetFile, File xmlOutputFile)
    {
        File xmlXsltTransformator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_xslt_transformator_1_job_" + fileIndex + ".xml");

        if (xmlXsltTransformator1JobFile.exists() == true)
        {
            if (xmlXsltTransformator1JobFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlXsltTransformator1JobFile.delete();
                }
                catch (SecurityException ex)
                {
                }

                if (deleteSuccessful != true)
                {
                    if (xmlXsltTransformator1JobFile.canWrite() != true)
                    {
                        throw constructTermination("messageXmlXsltTransformator1JobFileIsntWritable", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlXsltTransformator1JobPathIsntAFile", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
        }

        File xmlXsltTransformator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_xslt_transformator_1_job_" + fileIndex + ".xml");

        if (xmlXsltTransformator1ResultInfoFile.exists() == true)
        {
            if (xmlXsltTransformator1ResultInfoFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlXsltTransformator1ResultInfoFile.delete();
                }
                catch (SecurityException ex)
                {
                }

                if (deleteSuccessful != true)
                {
                    if (xmlXsltTransformator1ResultInfoFile.canWrite() != true)
                    {
                        throw constructTermination("messageXmlXsltTransformator1ResultInfoFileIsntWritable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlXsltTransformator1ResultInfoPathIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
        }

        if (xmlOutputFile.exists() == true)
        {
            if (xmlOutputFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlOutputFile.delete();
                }
                catch (SecurityException ex)
                {
                }

                if (deleteSuccessful != true)
                {
                    if (xmlOutputFile.canWrite() != true)
                    {
                        throw constructTermination("messageXmlOutputFileIsntWritable", null, null, xmlOutputFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlOutputPathIsntAFile", null, null, xmlOutputFile.getAbsolutePath());
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(xmlXsltTransformator1JobFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by wordpress_retriever_2 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
            writer.write("<xml-xslt-transformator-1-jobfile>\n");
            writer.write("  <job input-file=\"" + xmlInputFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_empty.xml\" stylesheet-file=\"" + stylesheetFile.getAbsolutePath() + "\" output-file=\"" + xmlOutputFile.getAbsolutePath() + "\"/>\n");
            writer.write("</xml-xslt-transformator-1-jobfile>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1JobFileErrorWhileWriting", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }

        ProcessBuilder builder = new ProcessBuilder("java", "xml_xslt_transformator_1", xmlXsltTransformator1JobFile.getAbsolutePath(), xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        builder.directory(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");
            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }
            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ErrorWhileReadingOutput", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }

        if (xmlXsltTransformator1ResultInfoFile.exists() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileDoesntExistButShould", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (xmlXsltTransformator1ResultInfoFile.isFile() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoPathIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (xmlXsltTransformator1ResultInfoFile.canRead() != true)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileIsntReadable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(xmlXsltTransformator1ResultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlXsltTransformator1ResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
        }

        if (wasSuccess != true)
        {
            throw constructTermination("messageXmlXsltTransformator1CallWasntSuccessful", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
        }

        return 0;
    }

    protected int ResolveXhtmlEntitiesXml(File xmlFileUnresolved, int pageIndex, String programPath, File tempDirectory, File xmlFileResolved)
    {
        if (xmlFileResolved.exists() == true)
        {
            if (xmlFileResolved.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = xmlFileResolved.delete();
                }
                catch (SecurityException ex)
                {
                }

                if (deleteSuccessful != true)
                {
                    if (xmlFileResolved.canWrite() != true)
                    {
                        throw constructTermination("messageXmlFileResolvedIsntWritable", null, null, xmlFileResolved.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlFileResolvedPathIsntAFile", null, null, xmlFileResolved.getAbsolutePath());
            }
        }

        File jobFileXmlDtdEntityResolver1 = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_dtd_entity_resolver_1_job_" + pageIndex + ".xml");

        if (jobFileXmlDtdEntityResolver1.exists() == true)
        {
            if (jobFileXmlDtdEntityResolver1.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFileXmlDtdEntityResolver1.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFileXmlDtdEntityResolver1.canWrite() != true)
                    {
                        throw constructTermination("messageXmlDtdEntityResolver1JobFileIsntWritable", null, null, jobFileXmlDtdEntityResolver1.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlDtdEntityResolver1JobPathExistsButIsntAFile", null, null, jobFileXmlDtdEntityResolver1.getAbsolutePath());
            }
        }

        File resultInfoFileXmlDtdEntityResolver1 = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_dtd_entity_resolver_1_job_" + pageIndex + ".xml");

        if (resultInfoFileXmlDtdEntityResolver1.exists() == true)
        {
            if (resultInfoFileXmlDtdEntityResolver1.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFileXmlDtdEntityResolver1.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFileXmlDtdEntityResolver1.canWrite() != true)
                    {
                        throw constructTermination("messageXmlDtdEntityResolver1ResultInfoFileIsntWritable", null, null, resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageXmlDtdEntityResolver1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
            }
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFileXmlDtdEntityResolver1),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by wordpress_retriever_2 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
            writer.write("<xml-dtd-entity-resolver-1-job>\n");
            writer.write("  <input-file path=\"" + xmlFileUnresolved.getAbsolutePath() + "\"/>\n");
            writer.write("  <entities-resolve-dictionary path=\"" + programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_dtd_entity_resolver" + File.separator + "xml_dtd_entity_resolver_1" + File.separator + "dictionaries" + File.separator + "dictionary_xhtml_1_1.xml\" ignore-missing-mappings=\"false\"/>\n");
            writer.write("  <output-file path=\"" + xmlFileResolved.getAbsolutePath() + "\"/>\n");
            writer.write("</xml-dtd-entity-resolver-1-job>\n");

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageXmlDtdEntityResolver1JobFileErrorWhileWriting", ex, null, jobFileXmlDtdEntityResolver1.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageXmlDtdEntityResolver1JobFileErrorWhileWriting", ex, null, jobFileXmlDtdEntityResolver1.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlDtdEntityResolver1JobFileErrorWhileWriting", ex, null, jobFileXmlDtdEntityResolver1.getAbsolutePath());
        }

        ProcessBuilder builder = new ProcessBuilder("java", "xml_dtd_entity_resolver_1", jobFileXmlDtdEntityResolver1.getAbsolutePath(), resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
        builder.directory(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_dtd_entity_resolver" + File.separator + "xml_dtd_entity_resolver_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlDtdEntityResolver1ErrorWhileReadingOutput", ex, null);
        }

        if (resultInfoFileXmlDtdEntityResolver1.exists() != true)
        {
            throw constructTermination("messageXmlDtdEntityResolver1ResultInfoFileDoesntExistButShould", null, null, resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
        }

        if (resultInfoFileXmlDtdEntityResolver1.isFile() != true)
        {
            throw constructTermination("messageXmlDtdEntityResolver1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
        }

        if (resultInfoFileXmlDtdEntityResolver1.canRead() != true)
        {
            throw constructTermination("messageXmlDtdEntityResolver1ResultInfoFileIsntReadable", null, null, resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
        }

        boolean wasSuccess = false;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFileXmlDtdEntityResolver1);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                        break;
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageXmlDtdEntityResolver1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageXmlDtdEntityResolver1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageXmlDtdEntityResolver1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileXmlDtdEntityResolver1.getAbsolutePath());
        }

        if (wasSuccess != true)
        {
            throw constructTermination("messageXmlDtdEntityResolver1CallWasntSuccessful", null, null, jobFileXmlDtdEntityResolver1.getAbsolutePath());
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "wordpress_retriever_2 workflow: " + getI10nString(id);
            }
            else
            {
                message = "wordpress_retriever_2 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "wordpress_retriever_2 workflow: " + getI10nString(id);
            }
            else
            {
                message = "wordpress_retriever_2 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (wordpress_retriever_2.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(wordpress_retriever_2.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by wordpress_retriever_2 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<wordpress-retriever-2-workflow-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</wordpress-retriever-2-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        wordpress_retriever_2.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nWordpressRetriever2WorkflowConsole";
    private ResourceBundle l10nConsole;
}
