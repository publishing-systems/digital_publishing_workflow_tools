/* Copyright (C) 2016-2023 Stephan Kreutzer
 *
 * This file is part of storage_resource_retriever_1 workflow, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * storage_resource_retriever_1 workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * storage_resource_retriever_1 workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with storage_resource_retriever_1 workflow. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/workflows/storage_resource_retriever/storage_resource_retriever_1/storage_resource_retriever_1.java
 * @author Stephan Kreutzer
 * @since 2022-04-02
 */



import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.security.MessageDigest;
import java.security.DigestInputStream;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.math.BigInteger;



public class storage_resource_retriever_1
{
    public static void main(String[] args)
    {
        System.out.print("storage_resource_retriever_1 workflow Copyright (C) 2016-2023 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        storage_resource_retriever_1 instance = new storage_resource_retriever_1();

        try
        {
            instance.invoke(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by storage_resource_retriever_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<storage-resource-retriever-1-workflow-result-information>\n");
                writer.write("  <success>\n");

                if (instance.GetRetrievedResourcesInfo().size() > 0)
                {
                    writer.write("    <retrieved-resources>\n");

                    for (int i = 0, max = instance.GetRetrievedResourcesInfo().size(); i < max; i++)
                    {
                        RetrievedResourceInfo info = instance.GetRetrievedResourcesInfo().get(i);

                        String identifier = info.GetIdentifier();
                        int resourceIndex = info.GetIndex();
                        boolean success = info.GetSuccess();
                        File resourceFile = info.GetResourceFile();
                        String canonicalContentType = info.GetCanonicalContentType();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        identifier = identifier.replaceAll("&", "&amp;");
                        identifier = identifier.replaceAll("<", "&lt;");
                        identifier = identifier.replaceAll(">", "&gt;");
                        //identifier = identifier.replaceAll("\"", "&quot;");
                        identifier = identifier.replaceAll("'", "&apos;");

                        writer.write("      <retrieved-resource identifier=\"" + identifier + "\" index=\"" + resourceIndex + "\"");

                        if (success == true)
                        {
                            writer.write(" success=\"true\"");

                            if (resourceFile != null)
                            {
                                writer.write(" path=\"" + resourceFile.getAbsolutePath() + "\"");
                            }
                        }
                        else
                        {
                            writer.write(" success=\"false\"");
                        }

                        if (canonicalContentType != null)
                        {
                            writer.write(" content-type=\"");

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            canonicalContentType = canonicalContentType.replaceAll("&", "&amp;");
                            canonicalContentType = canonicalContentType.replaceAll("<", "&lt;");
                            canonicalContentType = canonicalContentType.replaceAll(">", "&gt;");
                            //canonicalContentType = canonicalContentType.replaceAll("\"", "&quot;");
                            canonicalContentType = canonicalContentType.replaceAll("'", "&apos;");

                            writer.write(canonicalContentType);
                            writer.write("\"");
                        }

                        writer.write("/>\n");
                    }

                    writer.write("    </retrieved-resources>\n");
                }

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();
                        int resourceIndex = infoMessage.GetResourceIndex();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (resourceIndex >= 0)
                        {
                            writer.write("        <resource-index>" + resourceIndex + "</resource-index>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</storage-resource-retriever-1-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int invoke(String[] args)
    {
        this.retrievedResourcesInfo.clear();

        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\tstorage_resource_retriever_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        storage_resource_retriever_1.resultInfoFile = resultInfoFile;


        String programPath = storage_resource_retriever_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }

        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("storage_resource_retriever_1 workflow: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));


        List<String> requestedResources = new ArrayList<String>();
        Map<Integer, File> outputFiles = new HashMap<Integer, File>();
        File tempDirectory = null;
        File storageDirectory = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("resource") == true)
                    {
                        Attribute attributeIdentifier = event.asStartElement().getAttributeByName(new QName("identifier"));

                        if (attributeIdentifier == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "identifier");
                        }

                        String identifier = attributeIdentifier.getValue();
                        /** @todo This may result in several retrieval attempts, especially if retrieval isn't successful
                          * and hence repeated (for no good reason). But if only adding the identifier once, it doesn't
                          * match up with outputFiles, which can be used to copy the same resource to several output
                          * destination paths. */
                        requestedResources.add(identifier);

                        Attribute attributeOutputFilePath = event.asStartElement().getAttributeByName(new QName("output-file-path"));

                        if (attributeOutputFilePath != null)
                        {
                            File outputFile = new File(attributeOutputFilePath.getValue());

                            if (outputFile.isAbsolute() != true)
                            {
                                outputFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributeOutputFilePath.getValue());
                            }

                            try
                            {
                                outputFile = outputFile.getCanonicalFile();
                            }
                            catch (SecurityException ex)
                            {
                                throw constructTermination("messageJobFileOutputFileCantGetCanonicalPath", ex, null, requestedResources.size() - 1, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                            }
                            catch (IOException ex)
                            {
                                throw constructTermination("messageJobFileOutputFileCantGetCanonicalPath", ex, null, requestedResources.size() - 1, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                            }

                            if (outputFile.exists() == true)
                            {
                                throw constructTermination("messageJobFileOutputFileExistsAlready", null, null, requestedResources.size() - 1, outputFile.getAbsolutePath(), jobFile.getAbsolutePath());
                            }

                            outputFiles.put(requestedResources.size() - 1, outputFile);
                        }
                    }
                    else if (tagName.equals("temp-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (tempDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        tempDirectory = new File(attributePath.getValue());

                        if (tempDirectory.isAbsolute() != true)
                        {
                            tempDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            tempDirectory = tempDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageTempDirectoryCantGetCanonicalPath", ex, null, tempDirectory.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("storage-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (storageDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        storageDirectory = new File(attributePath.getValue());

                        if (storageDirectory.isAbsolute() != true)
                        {
                            storageDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        if (storageDirectory.exists() == true)
                        {
                            if (storageDirectory.isDirectory() == true)
                            {
                                if (storageDirectory.canWrite() != true)
                                {
                                    throw constructTermination("messageStorageDirectoryIsntWritable", null, null, storageDirectory.getAbsolutePath());
                                }
                            }
                            else
                            {
                                throw constructTermination("messageStoragePathIsntADirectory", null, null, storageDirectory.getAbsolutePath());
                            }
                        }
                        else
                        {
                            try
                            {
                                storageDirectory.mkdirs();
                            }
                            catch (SecurityException ex)
                            {
                                throw constructTermination("messageStorageDirectoryCantCreate", ex, null, storageDirectory.getAbsolutePath());
                            }
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }

        if (requestedResources.isEmpty() == true)
        {
            this.infoMessages.add(constructInfoMessage("messageNoRequestedResources", true, null, null, jobFile.getAbsolutePath()));
            return 0;
        }

        if (storageDirectory == null)
        {
            throw constructTermination("messageJobFileNoStorageDirectory", null, null, jobFile.getAbsolutePath());
        }

        if (tempDirectory == null)
        {
            tempDirectory = new File(programPath + "temp");
        }

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    throw constructTermination("messageTempDirectoryIsntWritable", null, null, tempDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageTempPathIsntADirectory", null, null, tempDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageTempDirectoryCantCreate", ex, null, tempDirectory.getAbsolutePath());
            }
        }


        for (int resourceIndex = 0, max = requestedResources.size(); resourceIndex < max; resourceIndex++)
        {
            boolean identifierFound = false;
            File[] retrievalResultFiles = new File[2];
            String canonicalContentType = null;
            String fileHash = null;
            File resourceFoundFile = null;
            List<String> supersededIdentifiers = new ArrayList<String>();

            File storageIndexFile = new File(storageDirectory.getAbsolutePath() + File.separator + "index.xml");

            if (storageIndexFile.exists() == true)
            {
                if (storageIndexFile.isFile() == true)
                {
                    if (storageIndexFile.canRead() != true)
                    {
                        throw constructTermination("messageStorageIndexFileIsntReadable", null, null, storageIndexFile.getAbsolutePath());
                    }
                }
                else
                {
                    throw constructTermination("messageStorageIndexPathIsntAFile", null, null, storageIndexFile.getAbsolutePath());
                }

                try
                {
                    String resourceFileName = null;
                    String resourceContentType = null;

                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    InputStream in = new FileInputStream(storageIndexFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                    while (eventReader.hasNext() == true)
                    {
                        XMLEvent event = eventReader.nextEvent();

                        if (event.isStartElement() == true)
                        {
                            String tagName = event.asStartElement().getName().getLocalPart();

                            if (tagName.equals("resource") == true)
                            {
                                Attribute attributeFileName = event.asStartElement().getAttributeByName(new QName("file-name"));

                                if (attributeFileName == null)
                                {
                                    throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "file-name");
                                }

                                resourceFileName = attributeFileName.getValue();

                                Attribute attributeContentType = event.asStartElement().getAttributeByName(new QName("content-type"));

                                if (attributeContentType == null)
                                {
                                    throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "content-type");
                                }

                                resourceContentType = attributeContentType.getValue();
                            }
                            else if (tagName.equals("identifier") == true)
                            {
                                if (resourceFileName == null)
                                {
                                    throw constructTermination("messageStorageIndexFileElementNoParentData", null, null, storageIndexFile.getAbsolutePath());
                                }

                                if (resourceContentType == null)
                                {
                                    throw constructTermination("messageStorageIndexFileElementNoParentData", null, null, storageIndexFile.getAbsolutePath());
                                }

                                Attribute attributeId = event.asStartElement().getAttributeByName(new QName("id"));

                                if (attributeId == null)
                                {
                                    throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "id");
                                }

                                if (attributeId.getValue().equals(requestedResources.get(resourceIndex)) == true)
                                {
                                    resourceFoundFile = new File(storageDirectory.getAbsolutePath() + File.separator + resourceFileName);

                                    try
                                    {
                                        resourceFoundFile = resourceFoundFile.getCanonicalFile();
                                    }
                                    catch (SecurityException ex)
                                    {
                                        throw constructTermination("messageResourceFoundFileCantGetCanonicalPath", ex, null, resourceFoundFile.getAbsolutePath());
                                    }
                                    catch (IOException ex)
                                    {
                                        throw constructTermination("messageResourceFoundFileCantGetCanonicalPath", ex, null, resourceFoundFile.getAbsolutePath());
                                    }

                                    if (resourceFoundFile.exists() != true)
                                    {
                                        throw constructTermination("messageResourceFoundFileDoesntExist", null, null, resourceFoundFile.getAbsolutePath());
                                    }

                                    if (resourceFoundFile.isFile() != true)
                                    {
                                        throw constructTermination("messageResourceFoundPathIsntAFile", null, null, resourceFoundFile.getAbsolutePath());
                                    }

                                    if (resourceFoundFile.canRead() != true)
                                    {
                                        throw constructTermination("messageResourceFoundFileIsntReadable", null, null, resourceFoundFile.getAbsolutePath());
                                    }

                                    identifierFound = true;
                                    canonicalContentType = resourceContentType;

                                    break;
                                }
                            }
                        }
                    }
                }
                catch (XMLStreamException ex)
                {
                    throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                }
            }


            if (identifierFound != true)
            {
                /** @todo Eventually use results in supersededIdentifiers to add superseded identifiers
                  * to the storage index as superseded (except the last identifier in there, which is
                  * the identifier which was finally successfully retrieved). */
                if (attemptRetrieval(requestedResources.get(resourceIndex),
                                     resourceIndex,
                                     programPath,
                                     tempDirectory,
                                     retrievalResultFiles,
                                     supersededIdentifiers) != 0)
                {
                    continue;
                }

                if (retrievalResultFiles[0] == null)
                {
                    throw new RuntimeException("Return value indicates success, but no resource provided in attemptRetrieval().");
                }

                if (retrievalResultFiles[1] != null)
                {
                    String[] canonicalContentTypeMetadata = new String[1];

                    if (analyzeRetrieverResultInfoFile(retrievalResultFiles[1], resourceIndex, canonicalContentTypeMetadata) != 0)
                    {
                        continue;
                    }

                    canonicalContentType = canonicalContentTypeMetadata[0];

                    /** @todo Obtain canonical content type from analyzing the content. May use hint from analyzing the metadata. */
                    String canonicalContentTypePayload = null;

                    if (canonicalContentType != null)
                    {
                        if (canonicalContentTypePayload != null)
                        {
                            if (canonicalContentTypePayload.equals(canonicalContentType) != true)
                            {
                                // Mismatch between metadata and content/payload content type!
                                /** @todo continue? */
                            }
                        }
                        else
                        {
                            /** @todo Leave it with metadata content type? */
                        }
                    }
                    else
                    {
                        canonicalContentType = canonicalContentTypePayload;
                    }
                }

                if (retrievalResultFiles[0] == null)
                {
                    continue;
                }

                {
                    StringBuffer hash = new StringBuffer();
                    byte[] buffer = new byte[1024];
                    MessageDigest md = null;

                    try
                    {
                        md = MessageDigest.getInstance("SHA-256");
                    }
                    catch (NoSuchAlgorithmException ex)
                    {
                        throw constructTermination("messageHashAlgorithmNotSupported", ex, null, "SHA-256");
                    }

                    try
                    {
                        InputStream in = new FileInputStream(retrievalResultFiles[0]);
                        DigestInputStream reader = new DigestInputStream(in, md);

                        int charactersRead = reader.read(buffer, 0, buffer.length);

                        while (charactersRead > 0)
                        {
                            // Look at all bytes of the file.
                            charactersRead = reader.read(buffer, 0, buffer.length);
                        }

                        reader.close();
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw constructTermination("messageRetrievedResourceFileReadingError", ex, null, retrievalResultFiles[0].getAbsolutePath());
                    }
                    catch (IOException ex)
                    {
                        throw constructTermination("messageRetrievedResourceFileReadingError", ex, null, retrievalResultFiles[0].getAbsolutePath());
                    }

                    byte[] digest = md.digest();

                    for (int i = 0; i < digest.length; i++)
                    {
                        String hex = Integer.toHexString(0xFF & digest[i]);

                        if (hex.length() == 1)
                        {
                            hash.append('0');
                        }

                        hash.append(hex);
                    }

                    fileHash = "sha256-" + hash.toString();
                }
            }

            if (retrievalResultFiles[0] != null &&
                storageIndexFile.exists() == true)
            {
                if (storageIndexFile.isFile() == true)
                {
                    if (storageIndexFile.canRead() != true)
                    {
                        throw constructTermination("messageStorageIndexFileIsntReadable", null, null, storageIndexFile.getAbsolutePath());
                    }
                }
                else
                {
                    throw constructTermination("messageStorageIndexPathIsntAFile", null, null, storageIndexFile.getAbsolutePath());
                }

                try
                {
                    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                    InputStream in = new FileInputStream(storageIndexFile);
                    XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                    while (eventReader.hasNext() == true)
                    {
                        XMLEvent event = eventReader.nextEvent();

                        if (event.isStartElement() == true)
                        {
                            String tagName = event.asStartElement().getName().getLocalPart();

                            if (tagName.equals("resource") == true)
                            {
                                Attribute attributeHash = event.asStartElement().getAttributeByName(new QName("hash"));

                                if (attributeHash == null)
                                {
                                    throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "hash");
                                }

                                if (attributeHash.getValue().equals(fileHash) == true)
                                {
                                    Attribute attributeFileName = event.asStartElement().getAttributeByName(new QName("file-name"));

                                    if (attributeFileName == null)
                                    {
                                        throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "file-name");
                                    }

                                    resourceFoundFile = new File(storageDirectory.getAbsolutePath() + File.separator + attributeFileName.getValue());

                                    try
                                    {
                                        resourceFoundFile = resourceFoundFile.getCanonicalFile();
                                    }
                                    catch (SecurityException ex)
                                    {
                                        throw constructTermination("messageResourceFoundFileCantGetCanonicalPath", ex, null, resourceFoundFile.getAbsolutePath());
                                    }
                                    catch (IOException ex)
                                    {
                                        throw constructTermination("messageResourceFoundFileCantGetCanonicalPath", ex, null, resourceFoundFile.getAbsolutePath());
                                    }

                                    if (resourceFoundFile.exists() != true)
                                    {
                                        throw constructTermination("messageResourceFoundFileDoesntExist", null, null, resourceFoundFile.getAbsolutePath());
                                    }

                                    if (resourceFoundFile.isFile() != true)
                                    {
                                        throw constructTermination("messageResourceFoundPathIsntAFile", null, null, resourceFoundFile.getAbsolutePath());
                                    }

                                    if (resourceFoundFile.canRead() != true)
                                    {
                                        throw constructTermination("messageResourceFoundFileIsntReadable", null, null, resourceFoundFile.getAbsolutePath());
                                    }

                                    Attribute attributeContentType = event.asStartElement().getAttributeByName(new QName("content-type"));

                                    if (attributeContentType == null)
                                    {
                                        throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "content-type");
                                    }

                                    if (canonicalContentType.equals(attributeContentType.getValue()) != true)
                                    {
                                        throw constructTermination("messageContentTypeMismatch", null, null, canonicalContentType, retrievalResultFiles[0].getAbsolutePath(), attributeContentType.getValue(), storageIndexFile.getAbsolutePath());
                                    }

                                    break;
                                }
                            }
                        }
                    }
                }
                catch (XMLStreamException ex)
                {
                    throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                }
            }

            String targetFileName = null;
            File targetFile = null;

            if (resourceFoundFile == null)
            {
                targetFileName = new BigInteger(130, (new SecureRandom())).toString(32);
                targetFile = new File(storageDirectory.getAbsolutePath() + File.separator + targetFileName);

                if (targetFile.exists() == true)
                {
                    throw constructTermination("messageTargetFileExists", null, null, targetFile.getAbsolutePath());
                }

                if (CopyFileBinary(retrievalResultFiles[0], targetFile) != 0)
                {
                    throw constructTermination("messageCopyFileError", null, null, retrievalResultFiles[0].getAbsolutePath(), targetFile.getAbsolutePath());
                }
            }

            /** @todo What if identifier used twice, but for different files/hashes? */
            if (resourceFoundFile == null ||
                identifierFound == false)
            {
                File storageIndexNewFile = new File(storageDirectory.getAbsolutePath() + File.separator + "index_new.xml");

                if (storageIndexNewFile.exists() == true)
                {
                    throw constructTermination("messageNewStorageIndexFileExists", null, null, storageIndexNewFile.getAbsolutePath());
                }

                try
                {
                    BufferedWriter writer = new BufferedWriter(
                                            new OutputStreamWriter(
                                            new FileOutputStream(storageIndexNewFile),
                                            "UTF-8"));

                    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                    writer.write("<!-- This file was created by storage_resource_retriever_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                    writer.write("<storage-index>\n");

                    if (storageIndexFile.exists() == true)
                    {
                        if (storageIndexFile.isFile() == true)
                        {
                            if (storageIndexFile.canRead() != true)
                            {
                                throw constructTermination("messageStorageIndexFileIsntReadable", null, null, storageIndexFile.getAbsolutePath());
                            }
                        }
                        else
                        {
                            throw constructTermination("messageStorageIndexPathIsntAFile", null, null, storageIndexFile.getAbsolutePath());
                        }

                        try
                        {
                            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                            InputStream in = new FileInputStream(storageIndexFile);
                            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                            while (eventReader.hasNext() == true)
                            {
                                XMLEvent event = eventReader.nextEvent();

                                if (event.isStartElement() == true)
                                {
                                    String tagName = event.asStartElement().getName().getLocalPart();

                                    if (tagName.equals("resource") == true)
                                    {
                                        Attribute attributeHash = event.asStartElement().getAttributeByName(new QName("hash"));

                                        if (attributeHash == null)
                                        {
                                            throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "hash");
                                        }

                                        Attribute attributeFileName = event.asStartElement().getAttributeByName(new QName("file-name"));

                                        if (attributeFileName == null)
                                        {
                                            throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "file-name");
                                        }

                                        Attribute attributeContentType = event.asStartElement().getAttributeByName(new QName("content-type"));

                                        if (attributeContentType == null)
                                        {
                                            throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "content-type");
                                        }

                                        writer.write("  <resource hash=\"" + attributeHash.getValue() + "\"");

                                        String fileName = attributeFileName.getValue();
                                        // Ampersand needs to be the first, otherwise it would double-encode
                                        // other entities.
                                        fileName = fileName.replaceAll("&", "&amp;");
                                        fileName = fileName.replaceAll("<", "&lt;");
                                        fileName = fileName.replaceAll(">", "&gt;");
                                        //fileName = fileName.replaceAll("\"", "&quot;");
                                        fileName = fileName.replaceAll("'", "&apos;");

                                        writer.write(" file-name=\"" + fileName + "\"");

                                        String contentType = attributeContentType.getValue();
                                        // Ampersand needs to be the first, otherwise it would double-encode
                                        // other entities.
                                        contentType = contentType.replaceAll("&", "&amp;");
                                        contentType = contentType.replaceAll("<", "&lt;");
                                        contentType = contentType.replaceAll(">", "&gt;");
                                        //contentType = contentType.replaceAll("\"", "&quot;");
                                        contentType = contentType.replaceAll("'", "&apos;");

                                        writer.write(" content-type=\"" + contentType + "\">\n");

                                        if (resourceFoundFile != null)
                                        {
                                            if (attributeHash.getValue().equals(fileHash) == true)
                                            {
                                                /** @todo What if there's no hasNext()? Handle. */
                                                while (eventReader.hasNext() == true)
                                                {
                                                    event = eventReader.nextEvent();

                                                    if (event.isStartElement() == true)
                                                    {
                                                        tagName = event.asStartElement().getName().getLocalPart();

                                                        if (tagName.equals("identifier") == true)
                                                        {
                                                            Attribute attributeId = event.asStartElement().getAttributeByName(new QName("id"));

                                                            if (attributeId == null)
                                                            {
                                                                throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "id");
                                                            }

                                                            String id = attributeId.getValue();
                                                            // Ampersand needs to be the first, otherwise it would double-encode
                                                            // other entities.
                                                            id = id.replaceAll("&", "&amp;");
                                                            id = id.replaceAll("<", "&lt;");
                                                            id = id.replaceAll(">", "&gt;");
                                                            //id = id.replaceAll("\"", "&quot;");
                                                            id = id.replaceAll("'", "&apos;");

                                                            writer.write("    <identifier id=\"" + id + "\"/>\n");
                                                        }
                                                    }
                                                    else if (event.isCharacters() == true)
                                                    {
                                                        continue;
                                                    }
                                                    else if (event.isEndElement() == true)
                                                    {
                                                        if (event.asEndElement().getName().getLocalPart().equals("identifier") != true)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new UnsupportedOperationException();
                                                    }
                                                }

                                                String id = requestedResources.get(resourceIndex);
                                                // Ampersand needs to be the first, otherwise it would double-encode
                                                // other entities.
                                                id = id.replaceAll("&", "&amp;");
                                                id = id.replaceAll("<", "&lt;");
                                                id = id.replaceAll(">", "&gt;");
                                                //id = id.replaceAll("\"", "&quot;");
                                                id = id.replaceAll("'", "&apos;");

                                                writer.write("    <identifier id=\"" + id + "\"/>\n");
                                            }
                                            else
                                            {
                                                /** @todo What if there's no hasNext()? Handle. */
                                                while (eventReader.hasNext() == true)
                                                {
                                                    event = eventReader.nextEvent();

                                                    if (event.isStartElement() == true)
                                                    {
                                                        tagName = event.asStartElement().getName().getLocalPart();

                                                        if (tagName.equals("identifier") == true)
                                                        {
                                                            Attribute attributeId = event.asStartElement().getAttributeByName(new QName("id"));

                                                            if (attributeId == null)
                                                            {
                                                                throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "id");
                                                            }

                                                            String id = attributeId.getValue();
                                                            // Ampersand needs to be the first, otherwise it would double-encode
                                                            // other entities.
                                                            id = id.replaceAll("&", "&amp;");
                                                            id = id.replaceAll("<", "&lt;");
                                                            id = id.replaceAll(">", "&gt;");
                                                            //id = id.replaceAll("\"", "&quot;");
                                                            id = id.replaceAll("'", "&apos;");

                                                            writer.write("    <identifier id=\"" + id + "\"/>\n");
                                                        }
                                                    }
                                                    else if (event.isCharacters() == true)
                                                    {
                                                        continue;
                                                    }
                                                    else if (event.isEndElement() == true)
                                                    {
                                                        if (event.asEndElement().getName().getLocalPart().equals("identifier") != true)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new UnsupportedOperationException();
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            /** @todo What if there's no hasNext()? Handle. */
                                            while (eventReader.hasNext() == true)
                                            {
                                                event = eventReader.nextEvent();

                                                if (event.isStartElement() == true)
                                                {
                                                    tagName = event.asStartElement().getName().getLocalPart();

                                                    if (tagName.equals("identifier") == true)
                                                    {
                                                        Attribute attributeId = event.asStartElement().getAttributeByName(new QName("id"));

                                                        if (attributeId == null)
                                                        {
                                                            throw constructTermination("messageStorageIndexFileElementIsMissingAnAttribute", null, null, storageIndexFile.getAbsolutePath(), tagName, "id");
                                                        }

                                                        String id = attributeId.getValue();
                                                        // Ampersand needs to be the first, otherwise it would double-encode
                                                        // other entities.
                                                        id = id.replaceAll("&", "&amp;");
                                                        id = id.replaceAll("<", "&lt;");
                                                        id = id.replaceAll(">", "&gt;");
                                                        //id = id.replaceAll("\"", "&quot;");
                                                        id = id.replaceAll("'", "&apos;");

                                                        writer.write("    <identifier id=\"" + id + "\"/>\n");
                                                    }
                                                }
                                                else if (event.isCharacters() == true)
                                                {
                                                    continue;
                                                }
                                                else if (event.isEndElement() == true)
                                                {
                                                    if (event.asEndElement().getName().getLocalPart().equals("identifier") != true)
                                                    {
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    throw new UnsupportedOperationException();
                                                }
                                            }
                                        }

                                        writer.write("  </resource>\n");
                                    }
                                }
                            }
                        }
                        catch (XMLStreamException ex)
                        {
                            throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageStorageIndexFileErrorWhileReading", ex, null, storageIndexFile.getAbsolutePath());
                        }
                    }

                    if (resourceFoundFile == null)
                    {
                        writer.write("  <resource hash=\"" + fileHash + "\"");

                        String fileName = targetFileName;
                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        fileName = fileName.replaceAll("&", "&amp;");
                        fileName = fileName.replaceAll("<", "&lt;");
                        fileName = fileName.replaceAll(">", "&gt;");
                        //fileName = fileName.replaceAll("\"", "&quot;");
                        fileName = fileName.replaceAll("'", "&apos;");

                        writer.write(" file-name=\"" + fileName + "\"");

                        String contentType = canonicalContentType;
                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        contentType = contentType.replaceAll("&", "&amp;");
                        contentType = contentType.replaceAll("<", "&lt;");
                        contentType = contentType.replaceAll(">", "&gt;");
                        //contentType = contentType.replaceAll("\"", "&quot;");
                        contentType = contentType.replaceAll("'", "&apos;");

                        writer.write(" content-type=\"" + contentType + "\">\n");

                        String id = requestedResources.get(resourceIndex);
                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        id = id.replaceAll("&", "&amp;");
                        id = id.replaceAll("<", "&lt;");
                        id = id.replaceAll(">", "&gt;");
                        //id = id.replaceAll("\"", "&quot;");
                        id = id.replaceAll("'", "&apos;");

                        writer.write("    <identifier id=\"" + id + "\"/>\n");
                        writer.write("  </resource>\n");
                    }

                    writer.write("</storage-index>\n");
                    writer.flush();
                    writer.close();
                }
                catch (FileNotFoundException ex)
                {
                    throw constructTermination("messageNewStorageIndexFileErrorWhileWriting", ex, null, storageIndexNewFile.getAbsolutePath());
                }
                catch (UnsupportedEncodingException ex)
                {
                    throw constructTermination("messageNewStorageIndexFileErrorWhileWriting", ex, null, storageIndexNewFile.getAbsolutePath());
                }
                catch (IOException ex)
                {
                    throw constructTermination("messageNewStorageIndexFileErrorWhileWriting", ex, null, storageIndexNewFile.getAbsolutePath());
                }

                if (storageIndexFile.exists() == true)
                {
                    try
                    {
                        storageIndexFile.delete();
                    }
                    catch (SecurityException ex)
                    {
                        throw constructTermination("messageStorageIndexFileCantDelete", ex, null, storageIndexFile.getAbsolutePath());
                    }
                }

                if (CopyFileBinary(storageIndexNewFile, storageIndexFile) != 0)
                {
                    throw constructTermination("messageCopyFileError", null, null, storageIndexNewFile.getAbsolutePath(), storageIndexFile.getAbsolutePath());
                }

                try
                {
                    storageIndexNewFile.delete();
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageNewStorageIndexFileCantDelete", ex, null, storageIndexNewFile.getAbsolutePath());
                }
            }


            if (retrievalResultFiles[0] != null ||
                resourceFoundFile != null)
            {
                if (outputFiles.containsKey(resourceIndex) == true)
                {
                    File sourceFile = null;

                    if (resourceFoundFile != null)
                    {
                        sourceFile = resourceFoundFile;
                    }
                    else if (retrievalResultFiles[0] != null)
                    {
                        sourceFile = retrievalResultFiles[0];
                    }

                    if (CopyFileBinary(sourceFile, outputFiles.get(resourceIndex)) != 0)
                    {
                        throw constructTermination("messageCopyFileError", null, null, sourceFile.getAbsolutePath(), outputFiles.get(resourceIndex).getAbsolutePath());
                    }

                    RetrievedResourceInfo info = new RetrievedResourceInfo(requestedResources.get(resourceIndex),
                                                                           resourceIndex,
                                                                           true,
                                                                           outputFiles.get(resourceIndex),
                                                                           canonicalContentType);

                    this.retrievedResourcesInfo.put(resourceIndex, info);
                }
                else
                {
                    RetrievedResourceInfo info = new RetrievedResourceInfo(requestedResources.get(resourceIndex),
                                                                           resourceIndex,
                                                                           true,
                                                                           null,
                                                                           canonicalContentType);

                    this.retrievedResourcesInfo.put(resourceIndex, info);
                }
            }
        }

        for (int resourceIndex = 0, max = requestedResources.size(); resourceIndex < max; resourceIndex++)
        {
            if (this.retrievedResourcesInfo.containsKey(resourceIndex) != true)
            {
                RetrievedResourceInfo info = new RetrievedResourceInfo(requestedResources.get(resourceIndex),
                                                                       resourceIndex,
                                                                       false,
                                                                       null,
                                                                       null);

                this.retrievedResourcesInfo.put(resourceIndex, info);
            }
        }

        return 0;
    }

    /**
     * @param[out] resultFiles On success, resultFiles[0] is the resourceFile and resultFiles[1] the
     *     result information file of the retriever used to obtain it.
     * @param[out] supersededIdentifiers On success, contains superseded identifiers, and if any,
     *     the last entry is the successful final identifier (not actually superseded).
     */
    public int attemptRetrieval(String identifier,
                                int resourceIndex,
                                String programPath,
                                File tempDirectory,
                                File[] resultFiles,
                                List<String> supersededIdentifiers)
    {
        String identifierXmlEscaped = identifier;

        // Ampersand needs to be the first, otherwise it would double-encode
        // other entities.
        identifierXmlEscaped = identifierXmlEscaped.replaceAll("&", "&amp;");
        identifierXmlEscaped = identifierXmlEscaped.replaceAll("<", "&lt;");
        identifierXmlEscaped = identifierXmlEscaped.replaceAll(">", "&gt;");
        identifierXmlEscaped = identifierXmlEscaped.replaceAll("\"", "&quot;");
        identifierXmlEscaped = identifierXmlEscaped.replaceAll("'", "&apos;");

        File jobFileResourceRetriever1 = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_resource_retriever_1_" + resourceIndex + ".xml");
        File resultInfoFileResourceRetriever1 = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_resource_retriever_1_" + resourceIndex + ".xml");

        if (jobFileResourceRetriever1.exists() == true)
        {
            if (jobFileResourceRetriever1.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = jobFileResourceRetriever1.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (jobFileResourceRetriever1.canWrite() != true)
                    {
                        throw constructTermination("messageResourceRetriever1JobFileIsntWritable", null, null, jobFileResourceRetriever1.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageResourceRetriever1JobPathExistsButIsntAFile", null, null, jobFileResourceRetriever1.getAbsolutePath());
            }
        }

        if (resultInfoFileResourceRetriever1.exists() == true)
        {
            if (resultInfoFileResourceRetriever1.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = resultInfoFileResourceRetriever1.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (resultInfoFileResourceRetriever1.canWrite() != true)
                    {
                        throw constructTermination("messageResourceRetriever1ResultInfoFileIsntWritable", null, null, resultInfoFileResourceRetriever1.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageResourceRetriever1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFileResourceRetriever1.getAbsolutePath());
            }
        }

        File outputDirectory = new File(tempDirectory.getAbsolutePath() + File.separator + "output");

        if (outputDirectory.exists() == true)
        {
            if (outputDirectory.isDirectory() == true)
            {
                if (outputDirectory.canWrite() != true)
                {
                    throw constructTermination("messageResourceRetriever1OutputDirectoryIsntWritable", null, null, outputDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResourceRetriever1OutputPathIsntADirectory", null, null, outputDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                outputDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageResourceRetriever1OutputDirectoryCantCreate", ex, null, outputDirectory.getAbsolutePath());
            }
        }


        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(jobFileResourceRetriever1),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by storage_resource_retriever_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
            writer.write("<resource-retriever-1-workflow-job>\n");
            writer.write("  <resources>\n");
            writer.write("    <resource identifier=\"" + identifierXmlEscaped + "\"/>\n");
            writer.write("  </resources>\n");
            //writer.write("  <temp-directory path=\"./temp\"/>\n");
            writer.write("  <output-directory path=\"" + outputDirectory.getAbsolutePath() + "\"/>\n");
            writer.write("</resource-retriever-1-workflow-job>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageResourceRetriever1JobFileErrorWhileWriting", ex, null, jobFileResourceRetriever1.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageResourceRetriever1JobFileErrorWhileWriting", ex, null, jobFileResourceRetriever1.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResourceRetriever1JobFileErrorWhileWriting", ex, null, jobFileResourceRetriever1.getAbsolutePath());
        }

        ProcessBuilder builder = new ProcessBuilder("java", "resource_retriever_1", jobFileResourceRetriever1.getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
        builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + "resource_retriever" + File.separator + "resource_retriever_1"));
        builder.redirectErrorStream(true);

        try
        {
            Process process = builder.start();
            Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

            while (scanner.hasNext() == true)
            {
                System.out.println(scanner.next());
            }

            scanner.close();
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResourceRetriever1ErrorWhileReadingOutput", ex, null);
        }

        if (resultInfoFileResourceRetriever1.exists() != true)
        {
            throw constructTermination("messageResourceRetriever1ResultInfoFileDoesntExistButShould", null, null, resultInfoFileResourceRetriever1.getAbsolutePath());
        }

        if (resultInfoFileResourceRetriever1.isFile() != true)
        {
            throw constructTermination("messageResourceRetriever1ResultInfoPathExistsButIsntAFile", null, null, resultInfoFileResourceRetriever1.getAbsolutePath());
        }

        if (resultInfoFileResourceRetriever1.canRead() != true)
        {
            throw constructTermination("messageResourceRetriever1ResultInfoFileIsntReadable", null, null, resultInfoFileResourceRetriever1.getAbsolutePath());
        }


        boolean wasSuccess = false;
        File resourceFile = null;
        File retrieverResultInfoFile = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(resultInfoFileResourceRetriever1);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("success") == true)
                    {
                        wasSuccess = true;
                    }
                    else if (tagName.equals("retrieved-resource") == true)
                    {
                        if (resourceFile != null)
                        {
                            throw constructTermination("messageResourceRetriever1ResultInfoFileElementConfiguredMoreThanOnce", null, null, resultInfoFileResourceRetriever1.getAbsolutePath(), tagName);
                        }

                        if (retrieverResultInfoFile != null)
                        {
                            throw constructTermination("messageResourceRetriever1ResultInfoFileElementConfiguredMoreThanOnce", null, null, resultInfoFileResourceRetriever1.getAbsolutePath(), tagName);
                        }

                        StartElement retrievedResourceElement = event.asStartElement();
                        Attribute attributeIdentifier = retrievedResourceElement.getAttributeByName(new QName("identifier"));
                        Attribute attributeResourceFilePath = retrievedResourceElement.getAttributeByName(new QName("path"));
                        Attribute attributeRetrieverResultInfoFilePath = retrievedResourceElement.getAttributeByName(new QName("retriever-result-info-file-path"));

                        if (attributeResourceFilePath != null)
                        {
                            String resourceFilePath = attributeResourceFilePath.getValue();

                            resourceFile = new File(resourceFilePath);

                            if (resourceFile.isAbsolute() != true)
                            {
                                resourceFile = new File(resultInfoFileResourceRetriever1.getAbsoluteFile().getParent() + File.separator + resourceFilePath);
                            }

                            try
                            {
                                resourceFile = resourceFile.getCanonicalFile();
                            }
                            catch (SecurityException ex)
                            {
                                throw constructTermination("messageResourceFileCantGetCanonicalPath", ex, null, new File(resourceFilePath).getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }
                            catch (IOException ex)
                            {
                                throw constructTermination("messageResourceFileCantGetCanonicalPath", ex, null, new File(resourceFilePath).getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }

                            if (resourceFile.exists() != true)
                            {
                                throw constructTermination("messageResourceFileDoesntExist", null, null, resourceFile.getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }

                            if (resourceFile.isFile() != true)
                            {
                                throw constructTermination("messageResourcePathIsntAFile", null, null, resourceFile.getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }

                            if (resourceFile.canRead() != true)
                            {
                                throw constructTermination("messageResourceFileIsntReadable", null, null, resourceFile.getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }
                        }

                        if (attributeRetrieverResultInfoFilePath != null)
                        {
                            String retrieverResultInfoFilePath = attributeRetrieverResultInfoFilePath.getValue();

                            retrieverResultInfoFile = new File(retrieverResultInfoFilePath);

                            if (retrieverResultInfoFile.isAbsolute() != true)
                            {
                                retrieverResultInfoFile = new File(resultInfoFileResourceRetriever1.getAbsoluteFile().getParent() + File.separator + retrieverResultInfoFilePath);
                            }

                            try
                            {
                                retrieverResultInfoFile = retrieverResultInfoFile.getCanonicalFile();
                            }
                            catch (SecurityException ex)
                            {
                                throw constructTermination("messageRetrieverResultInfoFileCantGetCanonicalPath", ex, null, new File(retrieverResultInfoFilePath).getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }
                            catch (IOException ex)
                            {
                                throw constructTermination("messageRetrieverResultInfoFileCantGetCanonicalPath", ex, null, new File(retrieverResultInfoFilePath).getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }

                            if (retrieverResultInfoFile.exists() != true)
                            {
                                throw constructTermination("messageRetrieverResultInfoFileDoesntExist", null, null, retrieverResultInfoFile.getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }

                            if (retrieverResultInfoFile.isFile() != true)
                            {
                                throw constructTermination("messageRetrieverResultInfoPathIsntAFile", null, null, retrieverResultInfoFile.getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }

                            if (retrieverResultInfoFile.canRead() != true)
                            {
                                throw constructTermination("messageRetrieverResultInfoFileIsntReadable", null, null, retrieverResultInfoFile.getAbsolutePath(), resultInfoFileResourceRetriever1.getAbsolutePath());
                            }
                        }

                        while (eventReader.hasNext() == true)
                        {
                            event = eventReader.nextEvent();

                            if (event.isStartElement() == true)
                            {
                                tagName = event.asStartElement().getName().getLocalPart();

                                if (tagName.equals("superseded-identifier") == true)
                                {
                                    Attribute attributeSupersededIdentifier = event.asStartElement().getAttributeByName(new QName("identifier"));

                                    if (attributeSupersededIdentifier == null)
                                    {
                                        throw constructTermination("messageRetrieverResultInfoFileIsMissingAnAttribute", null, null, resultInfoFileResourceRetriever1.getAbsolutePath(), tagName, "identifier");
                                    }

                                    if (supersededIdentifiers == null)
                                    {
                                        supersededIdentifiers = new ArrayList<String>();
                                    }

                                    supersededIdentifiers.add(attributeSupersededIdentifier.getValue());
                                }
                            }
                        }

                        if (supersededIdentifiers.isEmpty() != true)
                        {
                            if (attributeIdentifier == null)
                            {
                                throw constructTermination("messageRetrieverResultInfoFileIsMissingTheIdentifierElement", null, null, resultInfoFileResourceRetriever1.getAbsolutePath(), "retrieved-resource", "identifier");
                            }

                            supersededIdentifiers.add(attributeIdentifier.getValue());
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileResourceRetriever1.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileResourceRetriever1.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResourceRetriever1ResultInfoFileErrorWhileReading", ex, null, resultInfoFileResourceRetriever1.getAbsolutePath());
        }

        if (wasSuccess != true)
        {
            throw constructTermination("messageResourceRetriever1CallWasntSuccessful", null, null, resourceIndex);
        }

        resultFiles[0] = resourceFile;
        resultFiles[1] = retrieverResultInfoFile;

        return 0;
    }

    /**
     * @param[out] contentTypeResult On success, contentTypeResult[0] contains the canonical
     *     content type as obtained from the retriever's result info file.
     */
    public int analyzeRetrieverResultInfoFile(File retrieverResultInfoFile, int resourceIndex, String[] contentTypeResult)
    {
        String canonicalContentType = null;

        if (retrieverResultInfoFile.exists() != true)
        {
            throw constructTermination("messageRetrieverResultInfoFileDoesntExist", null, null, retrieverResultInfoFile.getAbsolutePath());
        }

        if (retrieverResultInfoFile.isFile() != true)
        {
            throw constructTermination("messageRetrieverResultInfoPathIsntAFile", null, null, retrieverResultInfoFile.getAbsolutePath());
        }

        if (retrieverResultInfoFile.canRead() != true)
        {
            throw constructTermination("messageRetrieverResultInfoFileIsntReadable", null, null, retrieverResultInfoFile.getAbsolutePath());
        }

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(retrieverResultInfoFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            String tagNameRoot = null;

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    tagNameRoot = event.asStartElement().getName().getLocalPart();

                    if (tagNameRoot.equals("https-client-1-result-information") == true ||
                        tagNameRoot.equals("http-client-1-result-information") == true)
                    {
                        break;
                    }
                    else
                    {
                        throw constructTermination("messageRetrieverResultInfoFileNotSupported", null, null, retrieverResultInfoFile.getAbsolutePath(), tagNameRoot);
                    }
                }
            }

            if (tagNameRoot == null)
            {
                throw constructTermination("messageRetrieverResultInfoMissing", null, null, retrieverResultInfoFile.getAbsolutePath());
            }

            if (tagNameRoot.equals("https-client-1-result-information") == true ||
                tagNameRoot.equals("http-client-1-result-information") == true)
            {
                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("field") == true)
                        {
                            Attribute attributeName = event.asStartElement().getAttributeByName(new QName("name"));

                            if (attributeName == null)
                            {
                                continue;
                            }

                            if (attributeName.getValue().equals("Content-Type") == true)
                            {
                                if (eventReader.hasNext() != true)
                                {
                                    throw constructTermination("messageRetrieverResultInfoCantContinueReading", null, null, retrieverResultInfoFile.getAbsolutePath());
                                }

                                event = eventReader.nextEvent();

                                if (event.isCharacters() != true)
                                {
                                    throw constructTermination("messageRetrieverResultInfoUnexpectedXmlNodeType", null, null, retrieverResultInfoFile.getAbsolutePath());
                                }

                                String contentType = event.asCharacters().getData();
                                String[] fragments = contentType.split(";");

                                if (fragments.length < 1)
                                {
                                    throw constructTermination("messageRetrieverResultInfoHttpContentTypeWithoutFragments", null, null, retrieverResultInfoFile.getAbsolutePath());
                                }

                                if (fragments[0].equals("application/xml") == true ||
                                    fragments[0].equals("text/xml") == true ||
                                    fragments[0].equals("application/x-dtbncx+xml") == true ||
                                    fragments[0].equals("application/xhtml+xml") == true ||
                                    fragments[0].equals("application/rss+xml") == true)
                                {
                                    canonicalContentType = "xml";
                                    break;
                                }
                                else
                                {
                                    this.infoMessages.add(constructInfoMessageWithResourceIndex("messageRetrieverResultInfoHttpContentTypeUnknown", resourceIndex, true, null, null, retrieverResultInfoFile.getAbsolutePath(), fragments[0]));
                                    return -1;
                                }
                            }
                        }
                    }
                }

                if (canonicalContentType == null)
                {
                    throw constructTermination("messageRetrieverResultInfoHttpContentTypeNotFound", null, null, retrieverResultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageRetrieverResultInfoFileNotSupported", null, null, retrieverResultInfoFile.getAbsolutePath(), tagNameRoot);
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageRetrieverResultInfoFileErrorWhileReading", ex, null, retrieverResultInfoFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageRetrieverResultInfoFileErrorWhileReading", ex, null, retrieverResultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageRetrieverResultInfoFileErrorWhileReading", ex, null, retrieverResultInfoFile.getAbsolutePath());
        }

        contentTypeResult[0] = canonicalContentType;

        return 0;
    }

    public int CopyFileBinary(File from, File to) throws ProgramTerminationException
    {
        if (from.exists() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromDoesntExist", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (from.isFile() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromIsntAFile", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (from.canRead() != true)
        {
            throw constructTermination("messageCantCopyBecauseFromIsntReadable", null, null, from.getAbsolutePath(), to.getAbsolutePath());
        }

        if (to.exists() == true)
        {
            if (to.isFile() == true)
            {
                if (to.canWrite() != true)
                {
                    throw constructTermination("messageCantCopyBecauseToIsntWritable", null, null, from.getAbsolutePath(), to.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageCantCopyBecauseToIsntAFile", null, null, from.getAbsolutePath(), to.getAbsolutePath());
            }
        }


        ProgramTerminationException exception = null;

        byte[] buffer = new byte[1024];

        FileInputStream reader = null;
        FileOutputStream writer = null;

        try
        {
            to.createNewFile();

            reader = new FileInputStream(from);
            writer = new FileOutputStream(to);

            int bytesRead = reader.read(buffer, 0, buffer.length);

            while (bytesRead > 0)
            {
                writer.write(buffer, 0, bytesRead);
                bytesRead = reader.read(buffer, 0, buffer.length);
            }

            writer.close();
            reader.close();
        }
        catch (FileNotFoundException ex)
        {
            exception = constructTermination("messageErrorWhileCopying", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
        }
        catch (IOException ex)
        {
            exception = constructTermination("messageErrorWhileCopying", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException ex)
                {
                    if (exception == null)
                    {
                        exception = constructTermination("messageErrorWhileCopying", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
                    }
                }
            }

            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException ex)
                {
                    if (exception == null)
                    {
                        exception = constructTermination("messageErrorWhileCopying", ex, null, from.getAbsolutePath(), to.getAbsolutePath());
                    }
                }
            }
        }

        if (exception != null)
        {
            throw exception;
        }

        return 0;
    }

    public InfoMessage constructInfoMessageWithResourceIndex(String id,
                                                             int resourceIndex,
                                                             boolean outputToConsole,
                                                             Exception exception,
                                                             String message,
                                                             Object ... arguments)
    {
        InfoMessage info = constructInfoMessage(id, outputToConsole, exception, message, arguments);
        info.SetResourceIndex(resourceIndex);

        return info;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "storage_resource_retriever_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "storage_resource_retriever_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "storage_resource_retriever_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "storage_resource_retriever_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (storage_resource_retriever_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(storage_resource_retriever_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by storage_resource_retriever_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n");
                writer.write("<storage-resource-retriever-1-workflow-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</storage-resource-retriever-1-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        storage_resource_retriever_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public Map<Integer, RetrievedResourceInfo> GetRetrievedResourcesInfo()
    {
        return this.retrievedResourcesInfo;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();
    protected Map<Integer, RetrievedResourceInfo> retrievedResourcesInfo = new HashMap<Integer, RetrievedResourceInfo>();

    private static final String L10N_BUNDLE = "l10n.l10nStorageResourceRetriever1WorkflowConsole";
    private ResourceBundle l10nConsole;
}
