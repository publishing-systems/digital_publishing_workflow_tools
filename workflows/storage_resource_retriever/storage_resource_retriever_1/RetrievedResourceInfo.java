/* Copyright (C) 2017-2022 Stephan Kreutzer
 *
 * This file is part of storage_resource_retriever_1 workflow, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * storage_resource_retriever_1 workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * storage_resource_retriever_1 workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with storage_resource_retriever_1 workflow. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/workflows/storage_resource_retriever/storage_resource_retriever_1/RetrievedResourceInfo.java
 * @brief Metadata information about a retrieved and stored resource.
 * @author Stephan Kreutzer
 * @since 2022-04-09
 */



import java.io.File;



class RetrievedResourceInfo
{
    public RetrievedResourceInfo(String identifier,
                                 int index,
                                 boolean success,
                                 File resourceFile,
                                 String canonicalContentType)
    {
        this.identifier = identifier;
        this.index = index;
        this.success = success;
        this.resourceFile = resourceFile;
        this.canonicalContentType = canonicalContentType;
    }

    public String GetIdentifier()
    {
        return this.identifier;
    }

    public int GetIndex()
    {
        return this.index;
    }

    public boolean GetSuccess()
    {
        return this.success;
    }

    public File GetResourceFile()
    {
        return this.resourceFile;
    }

    public String GetCanonicalContentType()
    {
        return this.canonicalContentType;
    }

    protected String identifier;
    protected int index;
    protected boolean success;
    protected File resourceFile;
    protected String canonicalContentType;
}
