<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2023 Stephan Kreutzer

This file is part of xhtml_to_glossary_1 workflow, a submodule of the
digital_publishing_workflow_tools package.

xhtml_to_glossary_1 workflow is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

xhtml_to_glossary_1 workflow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with xhtml_to_glossary_1 workflow. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml-src="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xhtml-src">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment> This file was created by hyperglossary_1.xsl of xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). </xsl:comment>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2023 Stephan Kreutzer

This file is part of Glossary.

Glossary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

Glossary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with Glossary. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="text-content"/&gt; and &lt;div id="glossary"/&gt; is
not part of this program, it's user data that is only processed. A different
license may apply.
</xsl:comment>
<xsl:text>&#x0A;</xsl:text>
        <title>
          <xsl:value-of select="./hyperglossary/xhtml-src:html/xhtml-src:head/xhtml-src:title//text()"/>
        </title>
        <script type="text/javascript">
          "use strict";

          function Load()
          {
              ApplyGlossary();
              console.log("Loading completed.");
          }

          function ApplyGlossary()
          {
              let glossary = new Array();
              let definitions = document.getElementsByTagName("dl");

              let entry = new Object();
              entry.terms = null;
              entry.descriptions = null;

              for (let i = 0; i &lt; definitions.length; i++)
              {
                  let definition = definitions[i];

                  for (let j = 0; j &lt; definition.children.length; j++)
                  {
                      let child = definition.children[j];

                      if (child.tagName.toLowerCase() == "dt")
                      {
                          if (entry.descriptions != null)
                          {
                              if (entry.descriptions != null)
                              {
                                  glossary.push(entry);

                                  entry = new Object();
                                  entry.terms = null;
                                  entry.descriptions = null;
                              }
                              else
                              {
                                  throw "Definition terms without descriptions.";
                              }
                          }

                          if (entry.terms == null)
                          {
                              entry.terms = new Array();
                          }

                          let termTokens = tokenize(child.innerText.toLowerCase());

                          entry.terms.push(termTokens);
                      }
                      else if (child.tagName.toLowerCase() == "dd")
                      {
                          if (entry.terms == null)
                          {
                              throw "Definition description without terms.";
                          }

                          if (entry.descriptions == null)
                          {
                              entry.descriptions = new Array();
                          }

                          entry.descriptions.push(child.innerText);
                      }
                  }
              }

              if (entry.descriptions != null)
              {
                  glossary.push(entry);
              }

              let posts = document.getElementsByClassName("text-content");

              for (let i = 0; i &lt; posts.length; i++)
              {
                  var nodes = ReplaceText(posts[i], glossary);

                  if (nodes.length &gt; 0)
                  {
                      for (let j = 0; j &lt; nodes.length; j++)
                      {
                          node.insertBefore(nodes[j], node.childNodes[i]);
                          i += 1;
                      }

                      node.removeChild(node.childNodes[i]);
                  }
              }
          }

          function ReplaceText(node, glossary)
          {
              let result = new Array();

              if (node.nodeType == Node.TEXT_NODE)
              {
                  let tokens = tokenize(node.data);
                  let innerText = "";

                  for (let i = 0; i &lt; tokens.length; i++)
                  {
                      let found = false;

                      for (let j = 0; j &lt; glossary.length &amp;&amp; found == false; j++)
                      {
                          for (let k = 0; k &lt; glossary[j].terms.length &amp;&amp; found == false; k++)
                          {
                              let termTokens = glossary[j].terms[k];
                              let matched = true;

                              if (tokens.length - i &lt; termTokens.length)
                              {
                                  continue;
                              }

                              for (let l = 0; l &lt; termTokens.length; l++)
                              {
                                  if (i + l &gt;= tokens.length)
                                  {
                                      matched = false;
                                      break;
                                  }

                                  if (tokens[i + l].toLowerCase() !== termTokens[l].toLowerCase())
                                  {
                                      matched = false;
                                      break;
                                  }
                              }

                              if (matched == true)
                              {
                                  if (innerText.length &gt; 0)
                                  {
                                      result.push(document.createTextNode(innerText));
                                      innerText = "";
                                  }

                                  let span = document.createElement("span");
                                  span.setAttribute("class", "glossary-usage");
                                  span.setAttribute("onclick", "showTerm('term-" + j + "');");

                                  let textContent = "";

                                  for (let l = 0; l &lt; termTokens.length; l++)
                                  {
                                      textContent += tokens[i];
                                      ++i;
                                  }

                                  // Because the next iteration will increment it again.
                                  --i;

                                  let spanText = document.createTextNode(textContent);
                                  span.appendChild(spanText);

                                  result.push(span);
                                  found = true;
                                  break;
                              }
                          }

                          if (found == true)
                          {
                              break;
                          }
                      }

                      if (found == false)
                      {
                          // Ampersand needs to be the first, otherwise it would
                          // double-escape other entities.
                          innerText += XmlEscapeCharacters(tokens[i]);
                      }
                  }

                  if (result.length &gt; 0 &amp;&amp; innerText.length &gt; 0)
                  {
                      result.push(document.createTextNode(innerText));
                  }
              }
              else if (node.nodeType == Node.ELEMENT_NODE)
              {
                  if (node.tagName.toLowerCase() == "a")
                  {
                      return result;
                  }

                  for (let i = 0; i &lt; node.childNodes.length; i++)
                  {
                      var nodes = ReplaceText(node.childNodes[i], glossary);

                      if (nodes.length &gt; 0)
                      {
                          for (let j = 0; j &lt; nodes.length; j++)
                          {
                              node.insertBefore(nodes[j], node.childNodes[i]);
                              i += 1;
                          }

                          node.removeChild(node.childNodes[i]);
                      }
                  }
              }

              return result;
          }

          function tokenize(text)
          {
              let tokens = new Array();
              let startPos = -1;

              for (let i = 0; i &lt; text.length; i++)
              {
                  if (/[a-zA-Z]/i.test(text[i]) == true)
                  {
                      if (startPos &lt; 0)
                      {
                          startPos = i;
                      }
                  }
                  else
                  {
                      if (startPos &gt;= 0)
                      {
                          tokens.push(text.substring(startPos, i));
                          startPos = -1;
                      }

                      tokens.push(text[i]);
                  }
              }

              if (startPos &gt;= 0)
              {
                  tokens.push(text.substring(startPos));
                  startPos = -1;
              }

              return tokens;
          }

          function XmlEscapeCharacters(input)
          {
              // Ampersand needs to be the first, otherwise it would
              // double-escape other entities.
              return input.replace(new RegExp('&amp;', 'g'), "&amp;amp;")
                          .replace(new RegExp('&lt;', 'g'), "&amp;lt;")
                          .replace(new RegExp('&gt;', 'g'), "&amp;gt;");
          }

          function showTerm(id)
          {
              hideTerm();

              let definition = document.getElementsByClassName(id);

              if (definition == null)
              {
                  return -1;
              }

              if (definition.length &lt;= 0)
              {
                  return -1;
              }

              let parent = document.getElementById('glossary-pane');

              if (parent == null)
              {
                  return -1;
              }

              let destination = document.createElement("dl");
              parent.appendChild(destination);

              for (let i = 0; i &lt; definition.length; i++)
              {
                  let element = definition[i].cloneNode(true);
                  // Otherwise DOM would update definition.length as the nodes would
                  // be cloned with their class attribute, leading to an infinite loop.
                  element.removeAttribute("class");
                  destination.appendChild(element);
              }

              return 0;
          }

          function hideTerm()
          {
              let parent = document.getElementById('glossary-pane');

              if (parent == null)
              {
                  return -1;
              }

              let glossary = parent.getElementsByTagName("dl");

              // Stupid JavaScript has a cloneNode(deep), but no removeNode(deep).
              let removeNode = function(element, parent)
              {
                  while (element.hasChildNodes() == true)
                  {
                      removeNode(element.lastChild, element);
                  }

                  parent.removeChild(element);
              }

              for (let i = 0; i &lt; glossary.length; i++)
              {
                  removeNode(glossary[i], parent);
              }

              return 0;
          }
        </script>
        <style type="text/css">
          body
          {
              font-family: sans-serif;
          }

          #content-pane
          {
              float: left;
              width: 50%;
              position: relative;
          }

          #glossary-pane
          {
              right: 0;
              width: 50%;
              border-left: 2px solid black;
              box-sizing: border-box;
              position: fixed;
          }

          .text-content
          {
              padding: 2em;
              text-align: justify;
          }

          .glossary-usage
          {
              color: #87CEEB;
              font-style: italic;
          }

          dd
          {
              text-align: justify;
          }

          #glossary
          {
              display: none;
          }
        </style>
      </head>
      <body onload="Load();">
        <div id="content">
          <div id="content-pane">
            <div class="text-content">
              <xsl:comment> The data contained in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
              <xsl:apply-templates select="./hyperglossary/xhtml-src:html/xhtml-src:body"/>
            </div>
          </div>
          <div id="glossary-pane">
            <h2>Glossary</h2>
          </div>
        </div>
        <div id="glossary" class="glossary">
          <h2>Definitions</h2>
          <xsl:comment> The data contained in this element and sub-elements is not part of this program, it's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <xsl:apply-templates select="./hyperglossary/glossary"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:h1">
    <h1>
      <xsl:apply-templates/>
    </h1>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:h2">
    <h2>
      <xsl:apply-templates/>
    </h2>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:h3">
    <h3>
      <xsl:apply-templates/>
    </h3>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:h4">
    <h4>
      <xsl:apply-templates/>
    </h4>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:h5">
    <h5>
      <xsl:apply-templates/>
    </h5>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:h6">
    <h6>
      <xsl:apply-templates/>
    </h6>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:p">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:ol">
    <ol>
      <xsl:apply-templates/>
    </ol>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:ul">
    <ul>
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:li">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:a">
    <a href="{@href}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:dl">
    <dl>
      <xsl:apply-templates/>
    </dl>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:dt">
    <dt>
      <xsl:apply-templates/>
    </dt>
  </xsl:template>

  <xsl:template match="/hyperglossary/xhtml-src:html/xhtml-src:body//xhtml-src:dd">
    <dd>
      <xsl:apply-templates/>
    </dd>
  </xsl:template>

  <xsl:template match="/hyperglossary/glossary">
    <!-- xsl:text></xsl:text -->
    <xsl:apply-templates select=".//dl"/>
  </xsl:template>

  <xsl:template match="/hyperglossary/glossary//dl">
    <dl>
      <xsl:apply-templates select="./definition"/>
    </dl>
  </xsl:template>

  <xsl:template match="/hyperglossary/glossary//dl/definition">
    <xsl:apply-templates select="./dt | ./dd">
      <xsl:with-param name="term-count" select="position() - 1"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="/hyperglossary/glossary//dl/definition/dt">
    <xsl:param name="term-count"/>
    <dt class="term-{$term-count}">
      <xsl:value-of select=".//text()"/>
    </dt>
  </xsl:template>

  <xsl:template match="/hyperglossary/glossary//dl/definition/dd">
    <xsl:param name="term-count"/>
    <dd class="term-{$term-count}">
      <xsl:apply-templates/>
    </dd>
  </xsl:template>

  <xsl:template match="/hyperglossary/glossary//dl/definition/dd//a">
    <a href="{@href}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template match="/hyperglossary/glossary//dl/definition/dd//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
