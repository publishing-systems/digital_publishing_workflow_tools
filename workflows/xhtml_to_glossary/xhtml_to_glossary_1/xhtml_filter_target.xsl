<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2019 Stephan Kreutzer

This file is part of xhtml_to_glossary_1 workflow, a submodule of the
digital_publishing_workflow_tools package.

xhtml_to_glossary_1 workflow is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

xhtml_to_glossary_1 workflow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with xhtml_to_glossary_1 workflow. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml-src="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xhtml-src">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:comment> This file was created by xhtml_filter_target.xsl of xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see http://www.hypertext-systems.org). </xsl:comment>
<xsl:text>&#x0A;</xsl:text>
        <title>
          <xsl:value-of select="./xhtml-src:html/xhtml-src:head/xhtml-src:title//text()"/>
        </title>
      </head>
      <body>
        <xsl:apply-templates select="./xhtml-src:html/xhtml-src:body"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:h1">
    <h1>
      <xsl:apply-templates/>
    </h1>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:h2">
    <h2>
      <xsl:apply-templates/>
    </h2>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:h3">
    <h3>
      <xsl:apply-templates/>
    </h3>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:h4">
    <h4>
      <xsl:apply-templates/>
    </h4>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:h5">
    <h5>
      <xsl:apply-templates/>
    </h5>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:h6">
    <h6>
      <xsl:apply-templates/>
    </h6>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:p">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:ol">
    <ol>
      <xsl:apply-templates/>
    </ol>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:ul">
    <ul>
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:li">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:a">
    <a href="{@href}">
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:dl">
    <dl>
      <xsl:apply-templates/>
    </dl>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:dt">
    <dt>
      <xsl:apply-templates/>
    </dt>
  </xsl:template>

  <xsl:template match="/xhtml-src:html/xhtml-src:body//xhtml-src:dd">
    <dd>
      <xsl:apply-templates/>
    </dd>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
