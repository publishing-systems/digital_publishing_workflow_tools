/* Copyright (C) 2016-2022  Stephan Kreutzer
 *
 * This file is part of xhtml_to_glossary_1 workflow, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * xhtml_to_glossary_1 workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xhtml_to_glossary_1 workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xhtml_to_glossary_1 workflow. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/workflows/xhtml_to_glossary/xhtml_to_glossary_1/xhtml_to_glossary_1.java
 * @brief Workflow generates a glossary rendering from a glossary source and a separate
 *     source on which the glossary is applied onto.
 * @author Stephan Kreutzer
 * @since 2019-05-08
 */



import java.util.Locale;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.io.File;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.lang.SecurityException;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.ArrayList;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Attribute;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import java.net.URLDecoder;
import java.util.Scanner;



public class xhtml_to_glossary_1
{
    public static void main(String[] args)
    {
        System.out.print("xhtml_to_glossary_1 workflow Copyright (C) 2016-2022 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://hypertext-systems.org.\n\n");

        xhtml_to_glossary_1 instance = new xhtml_to_glossary_1();

        try
        {
            instance.run(args);
        }
        catch (ProgramTerminationException ex)
        {
            instance.handleTermination(ex);
        }

        if (instance.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(instance.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<xhtml-to-glossary-1-workflow-result-information>\n");
                writer.write("  <success>\n");

                File outputGlossaryFile = instance.GetOutputGlossaryFile();
                File outputHyperglossaryFile = instance.GetOutputHyperglossaryFile();

                if (outputGlossaryFile != null)
                {
                    writer.write("    <output-glossary-xml-file path=\"" + outputGlossaryFile.getAbsolutePath() + "\"/>\n");
                }

                if (outputHyperglossaryFile != null)
                {
                    writer.write("    <output-hyperglossary-xhtml-file path=\"" + outputHyperglossaryFile.getAbsolutePath() + "\"/>\n");
                }

                if (instance.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = instance.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = instance.getInfoMessages().get(i);

                        writer.write("      <info-message number=\"" + i + "\">\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = infoMessage.getId();
                        String infoMessageBundle = infoMessage.getBundle();
                        Object[] infoMessageArguments = infoMessage.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        Exception exception = infoMessage.getException();

                        if (exception != null)
                        {
                            writer.write("        <exception>\n");

                            String className = exception.getClass().getName();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            className = className.replaceAll("&", "&amp;");
                            className = className.replaceAll("<", "&lt;");
                            className = className.replaceAll(">", "&gt;");

                            writer.write("          <class>" + className + "</class>\n");

                            StringWriter stringWriter = new StringWriter();
                            PrintWriter printWriter = new PrintWriter(stringWriter);
                            exception.printStackTrace(printWriter);
                            String stackTrace = stringWriter.toString();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            stackTrace = stackTrace.replaceAll("&", "&amp;");
                            stackTrace = stackTrace.replaceAll("<", "&lt;");
                            stackTrace = stackTrace.replaceAll(">", "&gt;");

                            writer.write("          <stack-trace>" + stackTrace + "</stack-trace>\n");
                            writer.write("        </exception>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                writer.write("  </success>\n");
                writer.write("</xhtml-to-glossary-1-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public int run(String[] args)
    {
        this.outputGlossaryFile = null;
        this.outputHyperglossaryFile = null;

        if (args.length < 2)
        {
            throw constructTermination("messageArgumentsMissing", null, getI10nString("messageArgumentsMissingUsage") + "\n\txhtml_to_glossary_1 " + getI10nString("messageParameterList") + "\n");
        }

        File resultInfoFile = new File(args[1]);

        try
        {
            resultInfoFile = resultInfoFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageResultInfoFileCantGetCanonicalPath", ex, null, resultInfoFile.getAbsolutePath());
        }

        if (resultInfoFile.exists() == true)
        {
            if (resultInfoFile.isFile() == true)
            {
                if (resultInfoFile.canWrite() != true)
                {
                    throw constructTermination("messageResultInfoFileIsntWritable", null, null, resultInfoFile.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageResultInfoPathIsntAFile", null, null, resultInfoFile.getAbsolutePath());
            }
        }

        xhtml_to_glossary_1.resultInfoFile = resultInfoFile;


        String programPath = xhtml_to_glossary_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }
        catch (IOException ex)
        {
            throw constructTermination("messageCantDetermineProgramPath", ex, null);
        }

        File tempDirectory = new File(programPath + "temp");

        if (tempDirectory.exists() == true)
        {
            if (tempDirectory.isDirectory() == true)
            {
                if (tempDirectory.canWrite() != true)
                {
                    throw constructTermination("messageTempDirectoryIsntWritable", null, null, tempDirectory.getAbsolutePath());
                }
            }
            else
            {
                throw constructTermination("messageTempPathIsntADirectory", null, null, tempDirectory.getAbsolutePath());
            }
        }
        else
        {
            try
            {
                tempDirectory.mkdirs();
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageTempDirectoryCantCreate", ex, null, tempDirectory.getAbsolutePath());
            }
        }

        File jobFile = new File(args[0]);

        try
        {
            jobFile = jobFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileCantGetCanonicalPath", ex, null, jobFile.getAbsolutePath());
        }

        if (jobFile.exists() != true)
        {
            throw constructTermination("messageJobFileDoesntExist", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.isFile() != true)
        {
            throw constructTermination("messageJobPathIsntAFile", null, null, jobFile.getAbsolutePath());
        }

        if (jobFile.canRead() != true)
        {
            throw constructTermination("messageJobFileIsntReadable", null, null, jobFile.getAbsolutePath());
        }

        System.out.println("xhtml_to_glossary_1 workflow: " + getI10nStringFormatted("messageCallDetails", jobFile.getAbsolutePath(), resultInfoFile.getAbsolutePath()));

        File inputGlossaryFile = null;
        File inputTargetFile = null;
        File outputDirectory = null;

        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(jobFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext() == true)
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    String tagName = event.asStartElement().getName().getLocalPart();

                    if (tagName.equals("input-glossary-file") == true)
                    {
                        StartElement inputFileElement = event.asStartElement();
                        Attribute attributePath = inputFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (inputGlossaryFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        inputGlossaryFile = new File(attributePath.getValue());

                        if (inputGlossaryFile.isAbsolute() != true)
                        {
                            inputGlossaryFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            inputGlossaryFile = inputGlossaryFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputGlossaryFileCantGetCanonicalPath", ex, null, inputGlossaryFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputGlossaryFileCantGetCanonicalPath", ex, null, inputGlossaryFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputGlossaryFile.exists() != true)
                        {
                            throw constructTermination("messageInputGlossaryFileDoesntExist", null, null, inputGlossaryFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputGlossaryFile.isFile() != true)
                        {
                            throw constructTermination("messageInputGlossaryPathIsntAFile", null, null, inputGlossaryFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputGlossaryFile.canRead() != true)
                        {
                            throw constructTermination("messageInputGlossaryFileIsntReadable", null, null, inputGlossaryFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("input-target-file") == true)
                    {
                        StartElement inputFileElement = event.asStartElement();
                        Attribute attributePath = inputFileElement.getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (inputTargetFile != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        inputTargetFile = new File(attributePath.getValue());

                        if (inputTargetFile.isAbsolute() != true)
                        {
                            inputTargetFile = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            inputTargetFile = inputTargetFile.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageInputTargetFileCantGetCanonicalPath", ex, null, inputTargetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageInputTargetFileCantGetCanonicalPath", ex, null, inputTargetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputTargetFile.exists() != true)
                        {
                            throw constructTermination("messageInputTargetFileDoesntExist", null, null, inputTargetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputTargetFile.isFile() != true)
                        {
                            throw constructTermination("messageInputTargetPathIsntAFile", null, null, inputTargetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }

                        if (inputTargetFile.canRead() != true)
                        {
                            throw constructTermination("messageInputTargetFileIsntReadable", null, null, inputTargetFile.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                    else if (tagName.equals("output-directory") == true)
                    {
                        Attribute attributePath = event.asStartElement().getAttributeByName(new QName("path"));

                        if (attributePath == null)
                        {
                            throw constructTermination("messageJobFileEntryIsMissingAnAttribute", null, null, jobFile.getAbsolutePath(), tagName, "path");
                        }

                        if (outputDirectory != null)
                        {
                            throw constructTermination("messageJobFileElementConfiguredMoreThanOnce", null, null, jobFile.getAbsolutePath(), tagName);
                        }

                        outputDirectory = new File(attributePath.getValue());

                        if (outputDirectory.isAbsolute() != true)
                        {
                            outputDirectory = new File(jobFile.getAbsoluteFile().getParent() + File.separator + attributePath.getValue());
                        }

                        try
                        {
                            outputDirectory = outputDirectory.getCanonicalFile();
                        }
                        catch (SecurityException ex)
                        {
                            throw constructTermination("messageOutputDirectoryCantGetCanonicalPath", ex, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                        catch (IOException ex)
                        {
                            throw constructTermination("messageOutputDirectoryCantGetCanonicalPath", ex, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (XMLStreamException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (SecurityException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageJobFileErrorWhileReading", ex, null, jobFile.getAbsolutePath());
        }


        if (inputGlossaryFile == null)
        {
            File filePicker1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_glossary_file_picker_1.xml");

            if (filePicker1JobFile.exists() == true)
            {
                if (filePicker1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = filePicker1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (filePicker1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageFilePicker1GlossaryJobFileExistsButIsntWritable", null, null, filePicker1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageFilePicker1GlossaryJobPathExistsButIsntAFile", null, null, filePicker1JobFile.getAbsolutePath());
                }
            }

            File filePicker1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_glossary_file_picker_1.xml");

            if (filePicker1ResultInfoFile.exists() == true)
            {
                if (filePicker1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = filePicker1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (filePicker1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageFilePicker1GlossaryResultInfoFileExistsButIsntWritable", null, null, filePicker1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageFilePicker1GlossaryResultInfoPathExistsButIsntAFile", null, null, filePicker1ResultInfoFile.getAbsolutePath());
                }
            }


            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(filePicker1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<file-picker-1-jobfile>\n");
                writer.write("  <file-type extension=\"xhtml\">XHTML definition list (*.xhtml)</file-type>\n");
                writer.write("  <file-type extension=\"xml\">XHTML definition list in generic XML (*.xml)</file-type>\n");
                writer.write("  <dialog-title>" + getI10nString("windowTitleSelectGlossaryInputFile") + "</dialog-title>\n");
                writer.write("</file-picker-1-jobfile>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageFilePicker1GlossaryJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageFilePicker1GlossaryJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1GlossaryJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", "file_picker_1", filePicker1JobFile.getAbsolutePath(), filePicker1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "file_picker" + File.separator + "file_picker_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1GlossaryErrorWhileReadingOutput", ex, null);
            }

            if (filePicker1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageFilePicker1GlossaryResultInfoFileDoesntExistButShould", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (filePicker1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageFilePicker1GlossaryResultInfoPathExistsButIsntAFile", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (filePicker1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageFilePicker1GlossaryResultInfoFileIsntReadable", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(filePicker1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                        }
                        else if (tagName.equals("selected-file") == true)
                        {
                            if (inputGlossaryFile != null)
                            {
                                throw constructTermination("messageFilePicker1GlossaryResultInfoFileElementConfiguredMoreThanOnce", null, null, filePicker1ResultInfoFile.getAbsolutePath(), tagName);
                            }

                            String inputFileString = new String();

                            while (eventReader.hasNext() == true)
                            {
                                event = eventReader.nextEvent();

                                if (event.isCharacters() == true)
                                {
                                    inputFileString += event.asCharacters();
                                }
                                else if (event.isEndElement() == true)
                                {
                                    QName elementEndName = event.asEndElement().getName();
                                    String elementEndNameString = elementEndName.getLocalPart();

                                    if (elementEndNameString.equalsIgnoreCase("selected-file") == true)
                                    {
                                        break;
                                    }
                                }
                            }

                            inputGlossaryFile = new File(inputFileString);

                            if (inputGlossaryFile.isAbsolute() != true)
                            {
                                inputGlossaryFile = new File(filePicker1ResultInfoFile.getAbsoluteFile().getParent() + File.separator + inputFileString);
                            }

                            if (inputGlossaryFile.exists() != true)
                            {
                                throw constructTermination("messageFilePicker1GlossaryResultInfoFileSelectedFileDoesntExist", null, null, filePicker1ResultInfoFile.getAbsolutePath(), inputGlossaryFile.getAbsolutePath());
                            }

                            if (inputGlossaryFile.isFile() != true)
                            {
                                throw constructTermination("messageFilePicker1GlossaryResultInfoFileSelectedPathIsntAFile", null, null, filePicker1ResultInfoFile.getAbsolutePath(), inputGlossaryFile.getAbsolutePath());
                            }

                            if (inputGlossaryFile.canRead() != true)
                            {
                                throw constructTermination("messageFilePicker1GlossaryResultInfoFileSelectedFileIsntReadable", null, null, filePicker1ResultInfoFile.getAbsolutePath(), inputGlossaryFile.getAbsolutePath());
                            }
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageFilePicker1GlossaryResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageFilePicker1GlossaryResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1GlossaryResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageFilePicker1GlossaryCallWasntSuccessful", null, null);
            }

            if (inputGlossaryFile == null)
            {
                throw constructTermination("messageFilePicker1GlossaryResultInfoFileSelectedFileIsntConfigured", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
        }

        if (inputTargetFile == null)
        {
            File filePicker1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_target_file_picker_1.xml");

            if (filePicker1JobFile.exists() == true)
            {
                if (filePicker1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = filePicker1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (filePicker1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageFilePicker1TargetJobFileExistsButIsntWritable", null, null, filePicker1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageFilePicker1TargetJobPathExistsButIsntAFile", null, null, filePicker1JobFile.getAbsolutePath());
                }
            }

            File filePicker1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_target_file_picker_1.xml");

            if (filePicker1ResultInfoFile.exists() == true)
            {
                if (filePicker1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = filePicker1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (filePicker1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageFilePicker1TargetResultInfoFileExistsButIsntWritable", null, null, filePicker1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageFilePicker1TargetResultInfoPathExistsButIsntAFile", null, null, filePicker1ResultInfoFile.getAbsolutePath());
                }
            }


            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(filePicker1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<file-picker-1-jobfile>\n");
                writer.write("  <file-type extension=\"xhtml\">XHTML (*.xhtml)</file-type>\n");
                writer.write("  <dialog-title>" + getI10nString("windowTitleSelectTargetInputFile") + "</dialog-title>\n");
                writer.write("</file-picker-1-jobfile>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageFilePicker1TargetJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageFilePicker1TargetJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1TargetJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", "file_picker_1", filePicker1JobFile.getAbsolutePath(), filePicker1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "file_picker" + File.separator + "file_picker_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1TargetErrorWhileReadingOutput", ex, null);
            }

            if (filePicker1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageFilePicker1TargetResultInfoFileDoesntExistButShould", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (filePicker1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageFilePicker1TargetResultInfoPathExistsButIsntAFile", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (filePicker1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageFilePicker1TargetResultInfoFileIsntReadable", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(filePicker1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                        }
                        else if (tagName.equals("selected-file") == true)
                        {
                            if (inputTargetFile != null)
                            {
                                throw constructTermination("messageFilePicker1TargetResultInfoFileElementConfiguredMoreThanOnce", null, null, filePicker1ResultInfoFile.getAbsolutePath(), tagName);
                            }

                            String inputFileString = new String();

                            while (eventReader.hasNext() == true)
                            {
                                event = eventReader.nextEvent();

                                if (event.isCharacters() == true)
                                {
                                    inputFileString += event.asCharacters();
                                }
                                else if (event.isEndElement() == true)
                                {
                                    QName elementEndName = event.asEndElement().getName();
                                    String elementEndNameString = elementEndName.getLocalPart();

                                    if (elementEndNameString.equalsIgnoreCase("selected-file") == true)
                                    {
                                        break;
                                    }
                                }
                            }

                            inputTargetFile = new File(inputFileString);

                            if (inputTargetFile.isAbsolute() != true)
                            {
                                inputTargetFile = new File(filePicker1ResultInfoFile.getAbsoluteFile().getParent() + File.separator + inputFileString);
                            }

                            if (inputTargetFile.exists() != true)
                            {
                                throw constructTermination("messageFilePicker1TargetResultInfoFileSelectedFileDoesntExist", null, null, filePicker1ResultInfoFile.getAbsolutePath(), inputTargetFile.getAbsolutePath());
                            }

                            if (inputTargetFile.isFile() != true)
                            {
                                throw constructTermination("messageFilePicker1TargetResultInfoFileSelectedPathIsntAFile", null, null, filePicker1ResultInfoFile.getAbsolutePath(), inputTargetFile.getAbsolutePath());
                            }

                            if (inputTargetFile.canRead() != true)
                            {
                                throw constructTermination("messageFilePicker1TargetResultInfoFileSelectedFileIsntReadable", null, null, filePicker1ResultInfoFile.getAbsolutePath(), inputTargetFile.getAbsolutePath());
                            }
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageFilePicker1TargetResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageFilePicker1TargetResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1TargetResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageFilePicker1TargetCallWasntSuccessful", null, null);
            }

            if (inputTargetFile == null)
            {
                throw constructTermination("messageFilePicker1TargetResultInfoFileSelectedFileIsntConfigured", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
        }

        if (outputDirectory != null)
        {
            if (outputDirectory.exists() == true)
            {
                if (outputDirectory.isDirectory() == true)
                {
                    if (outputDirectory.canWrite() != true)
                    {
                        throw constructTermination("messageOutputDirectoryIsntWritable", null, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                    }
                }
                else
                {
                    throw constructTermination("messageOutputPathIsntADirectory", null, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                }
            }
            else
            {
                try
                {
                    outputDirectory.mkdirs();
                }
                catch (SecurityException ex)
                {
                    throw constructTermination("messageOutputDirectoryCantCreate", ex, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                }
            }
        }
        else
        {
            File filePicker1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_output_file_picker_1.xml");

            if (filePicker1JobFile.exists() == true)
            {
                if (filePicker1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = filePicker1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (filePicker1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageFilePicker1OutputDirectoryJobFileExistsButIsntWritable", null, null, filePicker1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageFilePicker1OutputDirectoryJobPathExistsButIsntAFile", null, null, filePicker1JobFile.getAbsolutePath());
                }
            }

            File filePicker1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_output_file_picker_1.xml");

            if (filePicker1ResultInfoFile.exists() == true)
            {
                if (filePicker1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = filePicker1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (filePicker1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileExistsButIsntWritable", null, null, filePicker1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageFilePicker1OutputDirectoryResultInfoPathExistsButIsntAFile", null, null, filePicker1ResultInfoFile.getAbsolutePath());
                }
            }


            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(filePicker1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<file-picker-1-jobfile>\n");
                writer.write("  <directories-only/>\n");
                writer.write("  <dialog-title>" + getI10nString("windowTitleSelectOutputDirectory") + "</dialog-title>\n");
                writer.write("</file-picker-1-jobfile>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryJobFileWritingError", ex, null, filePicker1JobFile.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", "file_picker_1", filePicker1JobFile.getAbsolutePath(), filePicker1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "gui" + File.separator + "file_picker" + File.separator + "file_picker_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryErrorWhileReadingOutput", ex, null);
            }

            if (filePicker1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileDoesntExistButShould", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (filePicker1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoPathExistsButIsntAFile", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (filePicker1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileIsntReadable", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(filePicker1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                        }
                        else if (tagName.equals("selected-file") == true)
                        {
                            if (outputDirectory != null)
                            {
                                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileElementConfiguredMoreThanOnce", null, null, filePicker1ResultInfoFile.getAbsolutePath(), tagName);
                            }

                            String outputFileString = new String();

                            while (eventReader.hasNext() == true)
                            {
                                event = eventReader.nextEvent();

                                if (event.isCharacters() == true)
                                {
                                    outputFileString += event.asCharacters();
                                }
                                else if (event.isEndElement() == true)
                                {
                                    QName elementEndName = event.asEndElement().getName();
                                    String elementEndNameString = elementEndName.getLocalPart();

                                    if (elementEndNameString.equalsIgnoreCase("selected-file") == true)
                                    {
                                        break;
                                    }
                                }
                            }

                            outputDirectory = new File(outputFileString);

                            if (outputDirectory.isAbsolute() != true)
                            {
                                outputDirectory = new File(filePicker1ResultInfoFile.getAbsoluteFile().getParent() + File.separator + outputFileString);
                            }

                            if (outputDirectory.exists() == true)
                            {
                                if (outputDirectory.isDirectory() == true)
                                {
                                    if (outputDirectory.canWrite() != true)
                                    {
                                        throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileOutputDirectoryIsntWritable", null, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                                    }
                                }
                                else
                                {
                                    throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileOutputPathIsntADirectory", null, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                                }
                            }
                            else
                            {
                                try
                                {
                                    outputDirectory.mkdirs();
                                }
                                catch (SecurityException ex)
                                {
                                    throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileOutputDirectoryCantCreate", ex, null, outputDirectory.getAbsolutePath(), jobFile.getAbsolutePath());
                                }
                            }
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileErrorWhileReading", ex, null, filePicker1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryCallWasntSuccessful", null, null);
            }

            if (outputDirectory == null)
            {
                throw constructTermination("messageFilePicker1OutputDirectoryResultInfoFileSelectedFileIsntConfigured", null, null, filePicker1ResultInfoFile.getAbsolutePath());
            }
        }

        this.outputGlossaryFile = new File(outputDirectory.getAbsolutePath() + File.separator + "glossary.xml");

        if (this.outputGlossaryFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryOutputGlossaryPathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputGlossaryFile.getAbsolutePath());
        }

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(this.outputGlossaryFile),
                                    "UTF-8"));

            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
            writer.write("<glossary>\n");

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                /** @todo Entity resolving for arbitrary XML input? Call xml_dtd_entity_resolver? */
                inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
                inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
                InputStream in = new FileInputStream(inputGlossaryFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equalsIgnoreCase("dl") == true)
                        {
                            writer.write("<dl>");

                            long termNumber = 0L;
                            String lastElement = null;

                            while (eventReader.hasNext() == true)
                            {
                                event = eventReader.nextEvent();

                                if (event.isStartElement() == true)
                                {
                                    tagName = event.asStartElement().getName().getLocalPart();

                                    if (tagName.equalsIgnoreCase("dt") == true)
                                    {
                                        if (lastElement != null)
                                        {
                                            if (lastElement.equalsIgnoreCase(tagName) != true)
                                            {
                                                ++termNumber;
                                                lastElement = tagName;

                                                writer.write("</definition><definition>");
                                            }
                                        }
                                        else
                                        {
                                            lastElement = tagName;

                                            writer.write("<definition>");
                                        }

                                        writer.write("<dt>");

                                        while (eventReader.hasNext() == true)
                                        {
                                            event = eventReader.nextEvent();

                                            if (event.isCharacters() == true)
                                            {
                                                event.writeAsEncodedUnicode(writer);
                                            }
                                            else if (event.isEndElement() == true)
                                            {
                                                if (event.asEndElement().getName().getLocalPart().equalsIgnoreCase(tagName) == true)
                                                {
                                                    break;
                                                }
                                            }
                                        }

                                        writer.write("</dt>");
                                    }
                                    else if (tagName.equalsIgnoreCase("dd") == true)
                                    {
                                        if (lastElement != null)
                                        {
                                            if (lastElement.equalsIgnoreCase(tagName) != true)
                                            {
                                                lastElement = tagName;
                                            }
                                        }
                                        else
                                        {
                                            throw constructTermination("messageInputGlossaryFileDefinitionDescriptionWithoutTermsAtStart", null, null, inputGlossaryFile.getAbsolutePath());
                                        }

                                        writer.write("<dd>");

                                        while (eventReader.hasNext() == true)
                                        {
                                            event = eventReader.nextEvent();

                                            if (event.isCharacters() == true)
                                            {
                                                event.writeAsEncodedUnicode(writer);
                                            }
                                            else if (event.isEndElement() == true)
                                            {
                                                if (event.asEndElement().getName().getLocalPart().equalsIgnoreCase(tagName) == true)
                                                {
                                                    break;
                                                }
                                            }
                                        }

                                        writer.write("</dd>");
                                    }
                                }
                                else if (event.isEndElement() == true)
                                {
                                    if (event.asEndElement().getName().getLocalPart().equalsIgnoreCase("dl") == true)
                                    {
                                        break;
                                    }
                                }
                            }

                            if (lastElement != null)
                            {
                                if (lastElement.equalsIgnoreCase("dt") == true)
                                {
                                    throw constructTermination("messageInputGlossaryFileDefinitionTermWithoutDescriptionAtEnd", null, null, inputGlossaryFile.getAbsolutePath());
                                }

                                writer.write("</definition>");
                            }

                            writer.write("</dl>");
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageInputGlossaryFileErrorWhileReading", ex, null, inputGlossaryFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageInputGlossaryFileErrorWhileReading", ex, null, inputGlossaryFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageInputGlossaryFileErrorWhileReading", ex, null, inputGlossaryFile.getAbsolutePath());
            }

            writer.write("\n</glossary>\n");
            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            throw constructTermination("messageOutputGlossaryFileErrorWhileWriting", ex, null, this.outputGlossaryFile.getAbsolutePath());
        }
        catch (UnsupportedEncodingException ex)
        {
            throw constructTermination("messageOutputGlossaryFileErrorWhileWriting", ex, null, this.outputGlossaryFile.getAbsolutePath());
        }
        catch (IOException ex)
        {
            throw constructTermination("messageOutputGlossaryFileErrorWhileWriting", ex, null, this.outputGlossaryFile.getAbsolutePath());
        }

        File filteredTargetFile = new File(tempDirectory.getAbsolutePath() + File.separator + "filtered_target.xhtml");

        if (filteredTargetFile.exists() == true)
        {
            if (filteredTargetFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = filteredTargetFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (filteredTargetFile.canWrite() != true)
                    {
                        throw constructTermination("messageFilteredTargetFileExistsButIsntWritable", null, null, filteredTargetFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageFilteredTargetPathExistsButIsntAFile", null, null, filteredTargetFile.getAbsolutePath());
            }
        }

        {
            File xmlXsltTransformator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_filter_target_xml_xslt_transformator_1.xml");

            if (xmlXsltTransformator1JobFile.exists() == true)
            {
                if (xmlXsltTransformator1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlXsltTransformator1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlXsltTransformator1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlXsltTransformator1FilterTargetJobFileExistsButIsntWritable", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlXsltTransformator1FilterTargetJobPathExistsButIsntAFile", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                }
            }

            File xmlXsltTransformator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_filter_target_xml_xslt_transformator_1.xml");

            if (xmlXsltTransformator1ResultInfoFile.exists() == true)
            {
                if (xmlXsltTransformator1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlXsltTransformator1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlXsltTransformator1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlXsltTransformator1FilterTargetResultInfoFileExistsButIsntWritable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlXsltTransformator1FilterTargetResultInfoPathExistsButIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xmlXsltTransformator1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<xml-xslt-transformator-1-job>\n");
                writer.write("  <job input-file=\"" + inputTargetFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_xhtml_1_1.xml\" stylesheet-file=\"" + programPath + "xhtml_filter_target.xsl\" output-file=\"" + filteredTargetFile.getAbsolutePath() + "\"/>\n");
                writer.write("</xml-xslt-transformator-1-job>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetJobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetJobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetJobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", "xml_xslt_transformator_1", xmlXsltTransformator1JobFile.getAbsolutePath(), xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetErrorWhileReadingOutput", ex, null);
            }

            if (xmlXsltTransformator1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetResultInfoFileDoesntExistButShould", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlXsltTransformator1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetResultInfoPathExistsButIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlXsltTransformator1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetResultInfoFileIsntReadable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(xmlXsltTransformator1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageXmlXsltTransformator1FilterTargetCallWasntSuccessful", null, null);
            }
        }

        File concatenatedFile = new File(tempDirectory.getAbsolutePath() + File.separator + "glossary_concatenated.xml");

        if (concatenatedFile.exists() == true)
        {
            if (concatenatedFile.isFile() == true)
            {
                boolean deleteSuccessful = false;

                try
                {
                    deleteSuccessful = concatenatedFile.delete();
                }
                catch (SecurityException ex)
                {

                }

                if (deleteSuccessful != true)
                {
                    if (concatenatedFile.canWrite() != true)
                    {
                        throw constructTermination("messageConcatenatedFileExistsButIsntWritable", null, null, concatenatedFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                throw constructTermination("messageConcatenatedPathExistsButIsntAFile", null, null, concatenatedFile.getAbsolutePath());
            }
        }

        {
            File xmlConcatenator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_xml_concatenator_1.xml");

            if (xmlConcatenator1JobFile.exists() == true)
            {
                if (xmlConcatenator1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlConcatenator1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {
                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlConcatenator1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlConcatenator1JobFileIsntWritable", null, null, xmlConcatenator1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlConcatenator1JobPathIsntAFile", null, null, xmlConcatenator1JobFile.getAbsolutePath());
                }
            }

            File xmlConcatenator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_xml_concatenator_1.xml");

            if (xmlConcatenator1ResultInfoFile.exists() == true)
            {
                if (xmlConcatenator1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlConcatenator1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {
                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlConcatenator1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlConcatenator1ResultInfoFileIsntWritable", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlConcatenator1ResultInfoPathIsntAFile", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xmlConcatenator1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<xml-concatenator-1-job>\n");
                writer.write("  <input-files>\n");
                writer.write("    <input-file path=\"" + filteredTargetFile.getAbsolutePath() + "\"/>\n");
                writer.write("    <input-file path=\"" + this.outputGlossaryFile.getAbsolutePath() + "\"/>\n");
                writer.write("  </input-files>\n");
                writer.write("  <output-file path=\"" + concatenatedFile.getAbsolutePath() + "\" processing-instruction-data=\"encoding=&quot;UTF-8&quot;\" root-element-name=\"hyperglossary\"/>\n");
                writer.write("</xml-concatenator-1-job>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageXmlConcatenator1JobFileErrorWhileWriting", ex, null, xmlConcatenator1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageXmlConcatenator1JobFileErrorWhileWriting", ex, null, xmlConcatenator1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlConcatenator1JobFileErrorWhileWriting", ex, null, xmlConcatenator1JobFile.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", "xml_concatenator_1", xmlConcatenator1JobFile.getAbsolutePath(), xmlConcatenator1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_concatenator" + File.separator + "xml_concatenator_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");
                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }
                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlConcatenator1ErrorWhileReadingOutput", ex, null, xmlConcatenator1JobFile.getAbsolutePath());
            }

            if (xmlConcatenator1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileDoesntExistButShould", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlConcatenator1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoPathIsntAFile", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlConcatenator1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileIsntReadable", null, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }

            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(xmlConcatenator1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileErrorWhileReading", ex, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileErrorWhileReading", ex, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlConcatenator1ResultInfoFileErrorWhileReading", ex, null, xmlConcatenator1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageXmlConcatenator1CallWasntSuccessful", null, null, xmlConcatenator1JobFile.getAbsolutePath());
            }
        }

        this.outputHyperglossaryFile = new File(outputDirectory.getAbsolutePath() + File.separator + "glossary.xhtml");

        if (this.outputHyperglossaryFile.exists() == true)
        {
            throw constructTermination("messageOutputDirectoryOutputHyperglossaryPathAlreadyExists", null, null, outputDirectory.getAbsolutePath(), this.outputHyperglossaryFile.getAbsolutePath());
        }

        {
            File xmlXsltTransformator1JobFile = new File(tempDirectory.getAbsolutePath() + File.separator + "jobfile_hyperglossary_xml_xslt_transformator_1.xml");

            if (xmlXsltTransformator1JobFile.exists() == true)
            {
                if (xmlXsltTransformator1JobFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlXsltTransformator1JobFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlXsltTransformator1JobFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlXsltTransformator1HyperglossaryJobFileExistsButIsntWritable", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlXsltTransformator1HyperglossaryJobPathExistsButIsntAFile", null, null, xmlXsltTransformator1JobFile.getAbsolutePath());
                }
            }

            File xmlXsltTransformator1ResultInfoFile = new File(tempDirectory.getAbsolutePath() + File.separator + "resultinfo_hyperglossary_xml_xslt_transformator_1.xml");

            if (xmlXsltTransformator1ResultInfoFile.exists() == true)
            {
                if (xmlXsltTransformator1ResultInfoFile.isFile() == true)
                {
                    boolean deleteSuccessful = false;

                    try
                    {
                        deleteSuccessful = xmlXsltTransformator1ResultInfoFile.delete();
                    }
                    catch (SecurityException ex)
                    {

                    }

                    if (deleteSuccessful != true)
                    {
                        if (xmlXsltTransformator1ResultInfoFile.canWrite() != true)
                        {
                            throw constructTermination("messageXmlXsltTransformator1HyperglossaryResultInfoFileExistsButIsntWritable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    throw constructTermination("messageXmlXsltTransformator1HyperglossaryResultInfoPathExistsButIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
                }
            }

            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xmlXsltTransformator1JobFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<xml-xslt-transformator-1-job>\n");
                writer.write("  <job input-file=\"" + concatenatedFile.getAbsolutePath() + "\" entities-resolver-config-file=\"" + programPath + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1" + File.separator + "entities" + File.separator + "config_empty.xml\" stylesheet-file=\"" + programPath + "hyperglossary_1.xsl\" output-file=\"" + this.outputHyperglossaryFile.getAbsolutePath() + "\"/>\n");
                writer.write("</xml-xslt-transformator-1-job>\n");

                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryJobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
            catch (UnsupportedEncodingException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryJobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryJobFileWritingError", ex, null, xmlXsltTransformator1JobFile.getAbsolutePath());
            }

            ProcessBuilder builder = new ProcessBuilder("java", "xml_xslt_transformator_1", xmlXsltTransformator1JobFile.getAbsolutePath(), xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            builder.directory(new File(programPath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "xml_xslt_transformator" + File.separator + "xml_xslt_transformator_1"));
            builder.redirectErrorStream(true);

            try
            {
                Process process = builder.start();
                Scanner scanner = new Scanner(process.getInputStream()).useDelimiter("\n");

                while (scanner.hasNext() == true)
                {
                    System.out.println(scanner.next());
                }

                scanner.close();
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryErrorWhileReadingOutput", ex, null);
            }

            if (xmlXsltTransformator1ResultInfoFile.exists() != true)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryResultInfoFileDoesntExistButShould", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlXsltTransformator1ResultInfoFile.isFile() != true)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryResultInfoPathExistsButIsntAFile", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (xmlXsltTransformator1ResultInfoFile.canRead() != true)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryResultInfoFileIsntReadable", null, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            boolean wasSuccess = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                InputStream in = new FileInputStream(xmlXsltTransformator1ResultInfoFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true)
                    {
                        String tagName = event.asStartElement().getName().getLocalPart();

                        if (tagName.equals("success") == true)
                        {
                            wasSuccess = true;
                            break;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
            catch (SecurityException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }
            catch (IOException ex)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryResultInfoFileErrorWhileReading", ex, null, xmlXsltTransformator1ResultInfoFile.getAbsolutePath());
            }

            if (wasSuccess != true)
            {
                throw constructTermination("messageXmlXsltTransformator1HyperglossaryCallWasntSuccessful", null, null);
            }
        }

        return 0;
    }

    public InfoMessage constructInfoMessage(String id,
                                            boolean outputToConsole,
                                            Exception exception,
                                            String message,
                                            Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xhtml_to_glossary_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "xhtml_to_glossary_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        if (outputToConsole == true)
        {
            System.out.println(message);

            if (exception != null)
            {
                System.out.println(exception.getMessage());
                exception.printStackTrace();
            }
        }

        return new InfoMessage(id, exception, message, L10N_BUNDLE, arguments);
    }

    public ProgramTerminationException constructTermination(String id, Exception cause, String message, Object ... arguments)
    {
        if (message == null)
        {
            if (arguments == null)
            {
                message = "xhtml_to_glossary_1 workflow: " + getI10nString(id);
            }
            else
            {
                message = "xhtml_to_glossary_1 workflow: " + getI10nStringFormatted(id, arguments);
            }
        }

        return new ProgramTerminationException(id, cause, message, L10N_BUNDLE, arguments);
    }

    public int handleTermination(ProgramTerminationException ex)
    {
        String message = ex.getMessage();
        String id = ex.getId();
        String bundle = ex.getBundle();
        Object[] arguments = ex.getArguments();
        boolean normalTermination = ex.isNormalTermination();

        if (message != null)
        {
            System.err.println(message);
        }

        Throwable innerException = ex.getCause();

        if (innerException != null)
        {
            System.out.println(innerException.getMessage());
            innerException.printStackTrace();
        }

        if (xhtml_to_glossary_1.resultInfoFile != null)
        {
            try
            {
                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(
                                        new FileOutputStream(xhtml_to_glossary_1.resultInfoFile),
                                        "UTF-8"));

                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                writer.write("<!-- This file was created by xhtml_to_glossary_1 workflow, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://hypertext-systems.org). -->\n");
                writer.write("<xhtml-to-glossary-1-workflow-result-information>\n");

                if (normalTermination == false)
                {
                    writer.write("  <failure>\n");
                }
                else
                {
                    writer.write("  <success>\n");
                }

                writer.write("    <timestamp>" + ex.getTimestamp() + "</timestamp>\n");

                if (bundle != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    bundle = bundle.replaceAll("&", "&amp;");
                    bundle = bundle.replaceAll("<", "&lt;");
                    bundle = bundle.replaceAll(">", "&gt;");

                    writer.write("    <id-bundle>" + bundle + "</id-bundle>\n");
                }

                if (id != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    id = id.replaceAll("&", "&amp;");
                    id = id.replaceAll("<", "&lt;");
                    id = id.replaceAll(">", "&gt;");

                    writer.write("    <id>" + id + "</id>\n");
                }

                if (message != null)
                {
                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    message = message.replaceAll("&", "&amp;");
                    message = message.replaceAll("<", "&lt;");
                    message = message.replaceAll(">", "&gt;");

                    writer.write("    <message>" + message + "</message>\n");
                }

                if (arguments != null)
                {
                    writer.write("    <arguments>\n");

                    int argumentCount = arguments.length;

                    for (int i = 0; i < argumentCount; i++)
                    {
                        if (arguments[i] == null)
                        {
                            writer.write("      <argument number=\"" + i + "\">\n");
                            writer.write("        <class></class>\n");
                            writer.write("        <value>null</value>\n");
                            writer.write("      </argument>\n");

                            continue;
                        }

                        String className = arguments[i].getClass().getName();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        className = className.replaceAll("&", "&amp;");
                        className = className.replaceAll("<", "&lt;");
                        className = className.replaceAll(">", "&gt;");

                        String value = arguments[i].toString();

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        value = value.replaceAll("&", "&amp;");
                        value = value.replaceAll("<", "&lt;");
                        value = value.replaceAll(">", "&gt;");

                        writer.write("      <argument number=\"" + i + "\">\n");
                        writer.write("        <class>" + className + "</class>\n");
                        writer.write("        <value>" + value + "</value>\n");
                        writer.write("      </argument>\n");
                    }

                    writer.write("    </arguments>\n");
                }

                if (innerException != null)
                {
                    writer.write("    <exception>\n");

                    String className = innerException.getClass().getName();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    className = className.replaceAll("&", "&amp;");
                    className = className.replaceAll("<", "&lt;");
                    className = className.replaceAll(">", "&gt;");

                    writer.write("      <class>" + className + "</class>\n");

                    StringWriter stringWriter = new StringWriter();
                    PrintWriter printWriter = new PrintWriter(stringWriter);
                    innerException.printStackTrace(printWriter);
                    String stackTrace = stringWriter.toString();

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    stackTrace = stackTrace.replaceAll("&", "&amp;");
                    stackTrace = stackTrace.replaceAll("<", "&lt;");
                    stackTrace = stackTrace.replaceAll(">", "&gt;");

                    writer.write("      <stack-trace>" + stackTrace + "</stack-trace>\n");
                    writer.write("    </exception>\n");
                }

                if (this.getInfoMessages().size() > 0)
                {
                    writer.write("    <info-messages>\n");

                    for (int i = 0, max = this.getInfoMessages().size(); i < max; i++)
                    {
                        InfoMessage infoMessage = this.getInfoMessages().get(i);

                        writer.write("      <info-message>\n");
                        writer.write("        <timestamp>" + infoMessage.getTimestamp() + "</timestamp>\n");

                        String infoMessageText = infoMessage.getMessage();
                        String infoMessageId = ex.getId();
                        String infoMessageBundle = ex.getBundle();
                        Object[] infoMessageArguments = ex.getArguments();

                        if (infoMessageBundle != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageBundle = infoMessageBundle.replaceAll("&", "&amp;");
                            infoMessageBundle = infoMessageBundle.replaceAll("<", "&lt;");
                            infoMessageBundle = infoMessageBundle.replaceAll(">", "&gt;");

                            writer.write("        <id-bundle>" + infoMessageBundle + "</id-bundle>\n");
                        }

                        if (infoMessageId != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageId = infoMessageId.replaceAll("&", "&amp;");
                            infoMessageId = infoMessageId.replaceAll("<", "&lt;");
                            infoMessageId = infoMessageId.replaceAll(">", "&gt;");

                            writer.write("        <id>" + infoMessageId + "</id>\n");
                        }

                        if (infoMessageText != null)
                        {
                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            infoMessageText = infoMessageText.replaceAll("&", "&amp;");
                            infoMessageText = infoMessageText.replaceAll("<", "&lt;");
                            infoMessageText = infoMessageText.replaceAll(">", "&gt;");

                            writer.write("        <message>" + infoMessageText + "</message>\n");
                        }

                        if (infoMessageArguments != null)
                        {
                            writer.write("        <arguments>\n");

                            int argumentCount = infoMessageArguments.length;

                            for (int j = 0; j < argumentCount; j++)
                            {
                                if (infoMessageArguments[j] == null)
                                {
                                    writer.write("          <argument number=\"" + j + "\">\n");
                                    writer.write("            <class></class>\n");
                                    writer.write("            <value>null</value>\n");
                                    writer.write("          </argument>\n");

                                    continue;
                                }

                                String className = infoMessageArguments[j].getClass().getName();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                className = className.replaceAll("&", "&amp;");
                                className = className.replaceAll("<", "&lt;");
                                className = className.replaceAll(">", "&gt;");

                                String value = infoMessageArguments[j].toString();

                                // Ampersand needs to be the first, otherwise it would double-encode
                                // other entities.
                                value = value.replaceAll("&", "&amp;");
                                value = value.replaceAll("<", "&lt;");
                                value = value.replaceAll(">", "&gt;");

                                writer.write("          <argument number=\"" + j + "\">\n");
                                writer.write("            <class>" + className + "</class>\n");
                                writer.write("            <value>" + value + "</value>\n");
                                writer.write("          </argument>\n");
                            }

                            writer.write("        </arguments>\n");
                        }

                        writer.write("      </info-message>\n");
                    }

                    writer.write("    </info-messages>\n");
                }

                if (normalTermination == false)
                {
                    writer.write("  </failure>\n");
                }
                else
                {
                    writer.write("  </success>\n");
                }

                writer.write("</xhtml-to-glossary-1-workflow-result-information>\n");
                writer.flush();
                writer.close();
            }
            catch (FileNotFoundException ex2)
            {
                ex2.printStackTrace();
            }
            catch (UnsupportedEncodingException ex2)
            {
                ex2.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
        }

        xhtml_to_glossary_1.resultInfoFile = null;

        System.exit(-1);
        return -1;
    }

    public File GetOutputGlossaryFile()
    {
        return this.outputGlossaryFile;
    }

    public File GetOutputHyperglossaryFile()
    {
        return this.outputHyperglossaryFile;
    }

    public List<InfoMessage> getInfoMessages()
    {
        return this.infoMessages;
    }

    public Locale getLocale()
    {
        return Locale.getDefault();
    }

    /**
     * @brief This method interprets l10n strings from a .properties file as encoded in UTF-8.
     */
    private String getI10nString(String key)
    {
        if (this.l10nConsole == null)
        {
            this.l10nConsole = ResourceBundle.getBundle(L10N_BUNDLE, this.getLocale());
        }

        try
        {
            return new String(this.l10nConsole.getString(key).getBytes("UTF-8"), "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return this.l10nConsole.getString(key);
        }
    }

    private String getI10nStringFormatted(String i10nStringName, Object ... arguments)
    {
        MessageFormat formatter = new MessageFormat("");
        formatter.setLocale(this.getLocale());

        formatter.applyPattern(getI10nString(i10nStringName));
        return formatter.format(arguments);
    }

    protected File outputGlossaryFile = null;
    protected File outputHyperglossaryFile = null;

    public static File resultInfoFile = null;
    protected List<InfoMessage> infoMessages = new ArrayList<InfoMessage>();

    private static final String L10N_BUNDLE = "l10n.l10nXhtmlToGlossary1WorkflowConsole";
    private ResourceBundle l10nConsole;
}
